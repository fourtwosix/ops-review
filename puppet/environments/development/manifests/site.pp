include git

include nodejs

include curl

class ops_postgres {
  class { 'postgresql::globals':
    version => '9.4',
    manage_package_repo => true
  }->
  class { 'postgresql::server':
    ip_mask_deny_postgres_user => '0.0.0.0/32',
    ip_mask_allow_all_users => '0.0.0.0/0',
    listen_addresses => '*',
    postgres_password => 'postgres',
  }

  class { 'postgresql::lib::devel': }

  class { 'postgresql::server::plperl': }

  class { 'postgresql::server::plpython': }

  class { 'postgresql::server::contrib': }

  postgresql::server::pg_hba_rule { 'allow host machine connections':
    description => 'allow host machine connections',
    type => 'host',
    database => 'all',
    user => 'all',
    address => '0.0.0.0/0',
    auth_method => 'md5',
  }

  postgresql::server::role { 'insighter':
    superuser => true, # This is purely a development configuration we would have another user with dba role in production
    password_hash => postgresql_password('insighter', 'insighter'),
  } ->
  postgresql::server::db { 'insight_development':
    user => 'insighter',
    password => postgresql_password('insighter', 'insighter'),
  } ->
  postgresql::server::db { 'insight_test':
    user => 'insighter',
    password => postgresql_password('insighter', 'insighter'),
  } ->
  exec { "/usr/bin/psql -d insight_development -c 'CREATE EXTENSION plpythonu;'":
    user   => "postgres",
    cwd    => "/",
    unless => "/usr/bin/psql -d insight_development -c '\\dx' | grep plpythonu",
  } ->
  exec { "/usr/bin/psql -d insight_development -c 'CREATE EXTENSION tablefunc;'":
    user   => "postgres",
    cwd    => "/",
    unless => "/usr/bin/psql -d insight_development -c '\\dx' | grep tablefunc",
  } ->
  exec { "/usr/bin/psql -d insight_test -c 'CREATE EXTENSION plpythonu;'":
    user   => "postgres",
    cwd    => "/",
    unless => "/usr/bin/psql -d insight_test -c '\\dx' | grep plpythonu",
  } ->
  exec { "/usr/bin/psql -d insight_test -c 'CREATE EXTENSION tablefunc;'":
    user   => "postgres",
    cwd    => "/",
    unless => "/usr/bin/psql -d insight_test -c '\\dx' | grep tablefunc",
  }
}

include ops_postgres

package {'rubygems':
  ensure => present
}

package { 'bower':
  ensure => 'present',
  provider => 'npm',
  require => Class['nodejs']
}

package { 'grunt-cli':
  ensure => 'present',
  provider => 'npm',
  require => Class['nodejs']
}

package { 'sass':
  ensure => 'present',
  provider => 'gem',
  require => Package['rubygems']
}

service { 'iptables':
  ensure => 'stopped',
  enable => 'false'
}

class { 'redis::install':
  redis_version     => '3.0.5'
}

redis::server {
  'instance1':
    redis_memory    => '1g',
    redis_ip        => '0.0.0.0',
    redis_port      => 6379,
    redis_mempolicy => 'allkeys-lru',
    redis_timeout   => 0,
    redis_nr_dbs    => 16,
    redis_loglevel  => 'notice',
    running         => true,
    enabled         => true
}
