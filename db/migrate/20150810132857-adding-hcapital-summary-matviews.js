'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("CREATE MATERIALIZED VIEW human_capital_summary_layer_" + layer + "_manager AS SELECT layer_" + (layer - 1) + "_manager, layer_" + layer + "_manager, " +
          "count(distinct(CASE WHEN employment_status LIKE '%active' AND report_date = (SELECT max(report_date) FROM permast_data) THEN personnel_number END)) AS \"Head Count\", " +
          "count(distinct(CASE WHEN hire_date BETWEEN '2015-04-04' AND '2016-04-06' THEN personnel_number END)) AS \"Hires\", " +
          "count(distinct(CASE WHEN termination_type LIKE '% voluntary' AND termination_date BETWEEN '2015-04-04' AND '2016-04-06' THEN personnel_number END)) AS \"Voluntary Terminations\", " +
          "count(distinct(CASE WHEN termination_type LIKE '%involuntary' AND termination_date BETWEEN '2015-04-04' AND '2016-04-06' THEN personnel_number END)) AS \"Involuntary Terminations\" " +
          "FROM permast_data WHERE layer_" + layer + "_manager IS NOT NULL AND date_trunc('day', report_date)::DATE BETWEEN '2015-04-04' AND '2016-04-06' GROUP BY layer_" + (layer - 1) + "_manager, layer_" + layer + "_manager;")
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });

    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS human_capital_summary_layer_" + layer + "_manager RESTRICT;")
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
