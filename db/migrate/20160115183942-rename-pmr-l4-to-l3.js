'use strict';

// renaming the columns is a consequence of the split out of CSGov from CSC

var Bluebird = require('bluebird');

module.exports = {
    up: function (migration, Sequelize) {
        return new Bluebird(function (resolve, reject) {

            migration.renameColumn('program_metadata_manual_entry', 'l4_portfolio', 'l3_portfolio');
            migration.renameColumn('program_metadata_manual_entry', 'l4_manager',   'l3_manager');

            // NOTE: "Please protect my intent from the evil Javascript" prays the Ruby programmer.
            migration.renameColumn('contract_data', 'l4_portfolio', 'l3_portfolio')
            .then(function(){
              migration.sequelize.query('drop view if exists contract_data_secure;')
              .then(function(){
                migration.sequelize.query('CREATE VIEW contract_data_secure WITH (security_barrier) AS SELECT * FROM contract_data WHERE auth_name IS NULL OR auth_name = ANY(SELECT auth_name FROM users_auths WHERE user_id = current_setting(\'config.user_id\')::int);')
                .then(resolve)
                .catch(reject)
              })
            })
        });
    },

    down: function (migration, Sequelize) {
        return new Bluebird(function (resolve, reject) {

            migration.renameColumn('program_metadata_manual_entry', 'l3_portfolio', 'l4_portfolio');
            migration.renameColumn('program_metadata_manual_entry', 'l3_manager',   'l4_manager');
            
            migration.renameColumn('contract_data', 'l3_portfolio', 'l4_portfolio')
            .then(function(){
              migration.sequelize.query('drop view if exists contract_data_secure;')
              .then(function(){
                migration.sequelize.query('CREATE VIEW contract_data_secure WITH (security_barrier) AS SELECT * FROM contract_data WHERE auth_name IS NULL OR auth_name = ANY(SELECT auth_name FROM users_auths WHERE user_id = current_setting(\'config.user_id\')::int);')
                .then(resolve)
                .catch(reject)
              })
            })
        });
    }
};

