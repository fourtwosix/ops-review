'use strict';
var now = new Date().toUTCString();
var queries = [
  "UPDATE pmr_groups_divisions SET l3_manager = 'Pamela McGregor' WHERE lower(division_name) = lower('lpo/wpo')",
  "DELETE FROM static_display_data WHERE lower(display_value) = 'steve hyde'"
];

var metadataColumns = [
  "program_manager",
  "pra",
  "l3_manager",
  "l2_manager",
  "delivery_manager"
];


module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return Sequelize.Promise.map(queries, function (query) {
        return queryInterface.sequelize.query(query, {transaction: t});
      }).then(function () {
        return Sequelize.Promise.map(metadataColumns, function (col) {
          return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + col + " = 'Pamela McGregor' WHERE lower(" + col + ") = lower('steve hyde') OR lower(" + col + ") = lower('stephen hyde')", {transaction: t});
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return queryInterface.sequelize.query("UPDATE pmr_groups_divisions SET l3_manager = 'Steve Hyde' WHERE lower(division_name) = lower('lpo/wpo')", {transaction: t}).then(function () {
        return queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES ('Steve Hyde', 'pmrName', '" + now + "', '" + now + "')", {transaction: t}).then(function () {
          return Sequelize.Promise.map(metadataColumns, function (col) {
            return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + col + " = 'Steve Hyde' WHERE lower(" + col + ") = lower('pamela mcgregor')", {transaction: t});
          });
        });
      });
    });
  }
};
