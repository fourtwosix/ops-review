'use strict';

var Promise = require('bluebird');

module.exports = {
    up: function(queryInterface, Sequelize) {
        return new Promise(function(resolve, reject) {
            return queryInterface.addColumn('labor_utilization_data', 'wtd_investments', {type: Sequelize.FLOAT, defaultValue: 0.0} )
                .then(function() {
                    return queryInterface.addColumn('labor_utilization_data', 'mtd_investments', {type: Sequelize.FLOAT, defaultValue: 0.0} )
                        .then(function() {
                            return queryInterface.addColumn('labor_utilization_data', 'ytd_investments', {type: Sequelize.FLOAT, defaultValue: 0.0} )
                                .then(resolve)
                                .catch(reject)
                        }).catch(reject)
                }).catch(reject)
        })
    },

    down: function(queryInterface) {
        return new Promise(function(resolve, reject) {
            queryInterface.removeColumn('labor_utilization_data', 'wtd_investments')
                .then(function() {
                    return queryInterface.removeColumn('labor_utilization_data', 'mtd_investments')
                        .then(function() {
                            return queryInterface.removeColumn('labor_utilization_data', 'ytd_investments')
                                .then(resolve)
                                .catch(reject)
                        }).catch(reject)
                }).catch(reject)
        });
    }
};
