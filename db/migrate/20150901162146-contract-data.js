'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.each([
        ['task_order', Sequelize.STRING],
        ['top_level_contract_number', Sequelize.STRING],
        ['prime_contract_number', Sequelize.STRING],
        ['top_level_project_number', Sequelize.STRING],
        ['bpa_number', Sequelize.STRING],
        ['require_lcd_in_lcdb', Sequelize.STRING],
        ['status_code', Sequelize.STRING],
        ['contracting_officer', Sequelize.STRING],
        ['contracting_officer_phone', Sequelize.STRING],
        ['contracting_officer_email', Sequelize.STRING],
        ['client_1_address', Sequelize.STRING],
        ['client_1_abbreviation', Sequelize.STRING],
        ['client_2', Sequelize.STRING],
        ['client_2_address', Sequelize.STRING],
        ['client_2_abbreviation', Sequelize.STRING],
        ['client_3', Sequelize.STRING],
        ['client_3_address', Sequelize.STRING],
        ['client_3_abbreviation', Sequelize.STRING],
        ['exception_to_cas_coverage', Sequelize.STRING],
        ['duns_cage_and_ccr', Sequelize.STRING],
        ['arra_funding', Sequelize.BOOLEAN],
        ['is_this_contract_cas_covered', Sequelize.BOOLEAN],
        ['contract_value_cost', Sequelize.DECIMAL(14, 2)],
        ['prime_contractor_assignment_completed', Sequelize.BOOLEAN],
        ['contract_value_other', Sequelize.DECIMAL(14, 2)],
        ['total_contract_value_awarded', Sequelize.DECIMAL(14, 2)],
        ['total_contract_value_potential', Sequelize.DECIMAL(14, 2)],
        ['total_funding_cost_plus', Sequelize.DECIMAL(14, 2)],
        ['total_funding_fixed_price', Sequelize.DECIMAL(14, 2)],
        ['previous_project_number', Sequelize.STRING(400)],
        ['date_prime_contract_assignment_completed', Sequelize.DATE]
      ], function (field, done) {
        queryInterface.addColumn('contract_data', field[0], field[1])
          .then(function () {
            done();
          }).catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });
    });
  },


  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([
        'task_order',
        'top_level_contract_number',
        'prime_contract_number',
        'top_level_project_number',
        'bpa_number',
        'require_lcd_in_lcdb',
        'status_code',
        'contracting_officer',
        'contracting_officer_phone',
        'contracting_officer_email',
        'client_1_address',
        'client_1_abbreviation',
        'client_2',
        'client_2_address',
        'client_2_abbreviation',
        'client_3',
        'client_3_address',
        'client_3_abbreviation',
        'exception_to_cas_coverage',
        'duns_cage_and_ccr',
        'arra_funding',
        'is_this_contract_cas_covered',
        'contract_value_cost',
        'prime_contractor_assignment_completed',
        'contract_value_other',
        'total_contract_value_awarded',
        'total_contract_value_potential',
        'total_funding_cost_plus',
        'total_funding_fixed_price',
        'previous_project_number',
        'date_prime_contract_assignment_completed'
      ], function (layer, done) {
        queryInterface.removeColumn('contract_data', layer)
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err) : resolve();
      });
    });
  }
}
;









