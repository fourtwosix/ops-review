'use strict';
var Promise = require('bluebird');
var _ = require('lodash');
var newOfferings = [
  "None",
  "42six Engineering",
  "Business Process Services",
  "Cyber & Networks",
  "Digital Services",
  "Enterprise IT Services"
];
var oldOfferings = [
  "mission services",
  "nbs",
  "nms",
  "nes",
  "nis",
  "big data",
  "cloud",
  "cyber security",
  "n/a",
  "tbd"
];
var now = new Date().toUTCString();

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.sequelize.query("TRUNCATE offerings CASCADE")
        .then(function() {
          var values = _.trimRight(_.reduce(newOfferings, function(result, offering) {
            return result + "('" + offering + "','" + offering + "','" + now + "','" + now +"'), ";
          }, ""), ", ");
          queryInterface.sequelize.query("INSERT INTO offerings (offering_name, offering_code, created_at, updated_at) VALUES " + values)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.sequelize.query("TRUNCATE offerings CASCADE")
        .then(function() {
          var values = _.trimRight(_.reduce(oldOfferings, function(result, offering) {
            return result + "('" + offering + "','" + offering + "','" + offering + "','" + offering + "','" + now + "','" + now +"'), ";
          }, ""), ", ");
          queryInterface.sequelize.query("INSERT INTO offerings (offering_name, los_name, offering_code, offering_description, created_at, updated_at) VALUES " + values)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  }
};
