'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {

    return queryInterface.sequelize.transaction(function (t) {
      return queryInterface.addColumn('users', 'receives_emails', Sequelize.BOOLEAN, {transaction: t}).then(function () {
        return queryInterface.sequelize.query("UPDATE users SET receives_emails = true WHERE receives_emails IS NULL", {transaction: t})
      })
    }).catch(function (err) {
      console.log(err);
    });

  },

  down: function (queryInterface, Sequelize) {

    return queryInterface.removeColumn('users', 'receives_emails');

  }
};
