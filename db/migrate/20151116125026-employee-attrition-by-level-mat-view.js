'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([2, 3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS layer_" + layer + "_manager_terms_by_level RESTRICT;")
          .then(function () {
            queryInterface.sequelize.query("CREATE MATERIALIZED VIEW layer_" + layer + "_manager_terms_by_level AS " +
              "SELECT layer_" + layer + "_manager, date_trunc('month', report_date)::DATE AS MONTH, " +
              "CASE WHEN job_description ~ '.*:' THEN trim(split_part(job_description, ':', 1)) ELSE job_description END AS job_level, " +
              "count(distinct(CASE WHEN employment_status LIKE '%active' THEN personnel_number END)) AS head_count, " +
              "count(distinct(CASE WHEN termination_type LIKE '%voluntary' AND termination_date BETWEEN '2015-04-04' AND '2016-04-06' THEN personnel_number END)) AS all_terminations, " +
              "count(distinct(CASE WHEN termination_type LIKE '% voluntary' AND termination_date BETWEEN '2015-04-04' AND '2016-04-06' THEN personnel_number END)) AS vol_terminations, " +
              "count(distinct(CASE WHEN termination_type LIKE '% involuntary' AND termination_date BETWEEN '2015-04-04' AND '2016-04-06' THEN personnel_number END)) AS invol_terminations, " +
              "report_date, employee_group " +
              "FROM permast_data WHERE layer_" + layer + "_manager is NOT NULL GROUP BY layer_" + layer + "_manager, MONTH, job_level, report_date, employee_group ORDER BY MONTH ")
              .then(function () {
                done();
              })
              .catch(done);
          }).catch(done);

      }, function (err) {
        err ? reject(err): resolve();
      });

    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([2, 3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS layer_" + layer + "_manager_terms_by_level RESTRICT;")
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
