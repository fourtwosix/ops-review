'use strict';
var Promise = require('bluebird');

var mappings = [
  {
    oldVal: "USAF and Joint Mission Systems",
    newVal: "Joint Forces"
  },
  {
    oldVal: "US Airforce & Joint Mission Systems",
    newVal: "Joint Forces"
  },
  {
    oldVal: "naval and joint",
    newVal: "Naval Forces"
  },
  {
    oldVal: "Naval Warfare & Systems Center",
    newVal: "Naval Forces"
  },
  {
    oldVal: "Navy-IPPS",
    newVal: "Naval Forces"
  },
  {
    oldVal: "Naval Warfare and Systems Center",
    newVal: "Naval Forces"
  },
  {
    oldVal: "air force & missile defense",
    newVal: "Army SETA"
  },
  {
    oldVal: "Army Aviation and Missile Defense",
    newVal: "Army SETA"
  },
  {
    oldVal: "State and Local",
    newVal: "State and Local Health"
  },
  {
    oldVal: "Science and Engineering",
    newVal: "Science & Engineering"
  },
  {
    oldVal: "Army Advanced Electronics and Ground",
    newVal: "Army"
  }
];

var addConstraints = [
  "ALTER TABLE program_metadata_manual_entry ADD CONSTRAINT program_metadata_manual_entry_group_fkey FOREIGN KEY (\"group\") REFERENCES pmr_groups (group_name) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL",
  "ALTER TABLE program_metadata_manual_entry ADD CONSTRAINT program_metadata_manual_entry_division_fkey FOREIGN KEY (division) REFERENCES pmr_divisions (division_name) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL"
];

var removeConstraints = [
  "ALTER TABLE program_metadata_manual_entry DROP CONSTRAINT program_metadata_manual_entry_group_fkey",
  "ALTER TABLE program_metadata_manual_entry DROP CONSTRAINT program_metadata_manual_entry_division_fkey"
];

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return Promise.map(mappings, function (mapping) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET division = '" + mapping.newVal + "' WHERE division = '" + mapping.oldVal + "';", {transaction: t});
      }).then(function () {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET division = NULL WHERE division NOT IN (SELECT division_name FROM pmr_divisions);", {transaction: t}).then(function () {
          return Promise.map(addConstraints, function (constraint) {
            return queryInterface.sequelize.query(constraint, {transaction: t});
          });
        });
      })
    }).catch(function (err) {
      console.error(err);
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return Promise.map(removeConstraints, function (constraint) {
        return queryInterface.sequelize.query(constraint, {transaction: t}).then(function () {
          return Promise.map(mappings, function (mapping) {
            return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET division = '" + mapping.oldVal + "' WHERE division = '" + mapping.newVal + "';", {transaction: t});
          });
        });
      })
    }).catch(function (err) {
      console.error(err);
    });
  }
};
