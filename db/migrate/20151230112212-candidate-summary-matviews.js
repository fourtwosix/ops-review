'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
        queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS candidate_summary_bar_data_wf RESTRICT;")
          .then(function () {
            queryInterface.sequelize.query("CREATE MATERIALIZED VIEW candidate_summary_bar_data_wf" +
            "    AS SELECT Count(DISTINCT( recruiterFunded.requisition_number" +
            "        || recruiterFunded.candidate_id ))           AS" +
            "    fundedRecruitingAction," +
            "    Count(DISTINCT( recruiterConditional.requisition_number" +
            "        || recruiterConditional.candidate_id ))      AS" +
            "    conditionalRecruitingAction," +
            "    Count(DISTINCT( hiringmanagerFunded.requisition_number" +
            "        || hiringmanagerFunded.candidate_id ))       AS" +
            "    fundedHiringManagerAction," +
            "    Count(DISTINCT( hiringmanagerConditional.requisition_number" +
            "        || hiringmanagerConditional.candidate_id ))  AS" +
            "    conditionalHiringManagerAction," +
            "    Count(DISTINCT( sharedservicesFunded.requisition_number" +
            "        || sharedservicesFunded.candidate_id ))      AS" +
            "    fundedSharedServicesAction," +
            "    Count(DISTINCT( sharedservicesConditional.requisition_number" +
            "        || sharedservicesConditional.candidate_id )) AS" +
            "    conditionalSharedServicesAction," +
            "    base.layer_2_manager," +
            "    base.layer_3_manager," +
            "    base.layer_4_manager," +
            "    base.client_program_name," +
            "    base.req_hiring_manager_name," +
            "    base.req_recruiter_name," +
            "    base.requisition_number," +
            "    base.application_cws_status" +
            "    FROM   candidate_data AS base" +
            "    LEFT JOIN candidate_data AS recruiterFunded" +
            "     ON ( ( recruiterFunded.requisition_number" +
            "      || recruiterFunded.candidate_id ) = (" +
            "       base.requisition_number" +
            "       || base.candidate_id )" +
            "       AND recruiterFunded.application_cws_step IN (" +
            "        'new', 'phone screen', 'offer' )" +
            "       AND recruiterFunded.req_type = 'funded' )" +
            "    LEFT JOIN candidate_data AS recruiterConditional" +
            "     ON ( ( recruiterConditional.requisition_number" +
            "      || recruiterConditional.candidate_id ) = (" +
            "       base.requisition_number" +
            "       || base.candidate_id )" +
            "       AND recruiterConditional.application_cws_step IN (" +
            "        'new', 'phone screen', 'offer' )" +
            "       AND recruiterConditional.req_type = 'conditional' )" +
            "    LEFT JOIN candidate_data AS hiringManagerFunded" +
            "     ON ( ( hiringManagerFunded.requisition_number" +
            "      || hiringManagerFunded.candidate_id ) = (" +
            "       base.requisition_number" +
            "       || base.candidate_id )" +
            "       AND hiringManagerFunded.application_cws_step IN (" +
            "         'hm review', '1st interview'," +
            "         'additional interview(s)'," +
            "         'Optional Interviews'" +
            "         ," +
            "         'Pre-Offer' )" +
            "       AND hiringManagerFunded.req_type = 'funded' )" +
            "    LEFT JOIN candidate_data AS hiringManagerConditional" +
            "     ON ( ( hiringManagerConditional.requisition_number" +
            "      || hiringManagerConditional.candidate_id ) = (" +
            "       base.requisition_number" +
            "       || base.candidate_id )" +
            "       AND hiringManagerConditional.application_cws_step IN" +
            "        (" +
            "         'hm review', '1st interview'," +
            "         'additional interview(s)'," +
            "         'Optional Interviews'" +
            "         ," +
            "         'Pre-Offer'" +
            "        )" +
            "       AND hiringManagerConditional.req_type = 'conditional' )" +
            "    LEFT JOIN candidate_data AS sharedServicesFunded" +
            "     ON ( ( sharedServicesFunded.requisition_number" +
            "      || sharedServicesFunded.candidate_id ) = (" +
            "       base.requisition_number" +
            "       || base.candidate_id )" +
            "       AND sharedServicesFunded.application_cws_step IN" +
            "        ( 'pre-offer actions', 'hire' )" +
            "       AND sharedServicesFunded.req_type = 'funded' )" +
            "    LEFT JOIN candidate_data AS sharedServicesConditional" +
            "     ON ( ( sharedServicesConditional.requisition_number" +
            "      || sharedServicesConditional.candidate_id ) = (" +
            "       base.requisition_number" +
            "       || base.candidate_id )" +
            "       AND sharedServicesConditional.application_cws_step IN" +
            "        ( 'pre-offer actions', 'hire' )" +
            "       AND sharedServicesConditional.req_type = 'conditional' )" +
            "    GROUP  BY base.layer_2_manager," +
            "    base.layer_3_manager," +
            "    base.layer_3_manager," +
            "    base.layer_4_manager," +
            "    base.client_program_name," +
            "    base.req_hiring_manager_name," +
            "    base.req_recruiter_name," +
            "    base.requisition_number," +
            "    base.application_cws_status;")
          .then(resolve)
          .catch(reject);
      }, function (err) {
        err ? reject(err): resolve();
      });

    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
        queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS candidate_summary_bar_data_wf RESTRICT;")
          .then(resolve)
          .catch(reject);
    });
  }
};
