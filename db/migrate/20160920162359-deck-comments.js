'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function(t) {
      return queryInterface.createTable(
        'program_deck_comments',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          program_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'programs',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
          },
          fiscal_month_end: Sequelize.DATEONLY,
          deck: Sequelize.STRING,
          comment: Sequelize.TEXT,
          created_by: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
          },
          updated_by: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
          },
          deleted_by: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
          },
          is_deleted: Sequelize.BOOLEAN,
          created_at: Sequelize.DATE,
          updated_at: Sequelize.DATE,
          delete_at: Sequelize.DATE
        },
        { transaction: t });
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function(t) {
      return queryInterface.dropTable('program_deck_comments', {transaction: t});
    });
  }
};
