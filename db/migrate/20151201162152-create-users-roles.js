'use strict';
var Promise = require('bluebird');
var async = require('async');
var tableName = 'users_roles';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function(done) {
          queryInterface.createTable(
            tableName,
            {
              user_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                references: 'users',
                referencesKey: 'id',
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
              },
              role_name: {
                type: Sequelize.STRING,
                primaryKey: true,
                references: 'roles',
                referencesKey: 'name',
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
              },
              created_at: Sequelize.DATE,
              updated_at: Sequelize.DATE
            })
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        },
        function(done) {
          queryInterface.sequelize.query("ALTER TABLE " + tableName + " OWNER TO insighter;")
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        }
      ], function (err) {
        err ? reject(err): resolve();
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable(tableName)
        .then(resolve)
        .catch(reject);
    });
  }
};
