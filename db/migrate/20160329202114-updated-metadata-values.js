'use strict';
var Promise = require('bluebird');
var _ = require('lodash');
var async = require('async');
var Converter = require("csvtojson").Converter;
var converter = new Converter({});
var csvFile = '20160329202114-program_metadata-org-updates.csv';
var tableName = 'program_metadata_manual_entry';
var groupMappings = [
  {old: 'civil/hls/le/fa', new: 'Health & Civil'},
  {old: 'nps health', new: 'Health & Civil'},
  {old: 'Defense IT', new: 'Defense'},
  {old: 'Defense Mission', new: 'Defense'},
  {old: 'Civil', new: 'Health & Civil'},
  {old: 'Health', new: 'Health & Civil'}
];
var now = new Date().toUTCString();

function getCSVRecord(csvRow) {
  return {
    group: csvRow["group"],
    l2_manager: csvRow["l2_manager"],
    division: csvRow["division"],
    l3_manager: csvRow["l3_manager"],
    pra: csvRow["pra"],
    updated_at: now
  };
}

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      converter.fromFile(__dirname + "/" + csvFile, function (err, csvRows) {
        if (err) {
          return reject(err);
        }
        async.eachSeries(csvRows, function (csvRow, done) {
          var updateRecord = getCSVRecord(csvRow);
          var projectId = ("'" + csvRow["program_number"] + "'").toLowerCase()
          var whereClause = "program_id = (SELECT programs.id FROM programs INNER JOIN program_metadata_manual_entry ON programs.id = program_metadata_manual_entry.program_id WHERE programs.project_id = " + projectId + " LIMIT 1)";

          var updateQuery = 'UPDATE ' + tableName + ' SET ';

          _.forOwn(updateRecord, function (value, key) {
            updateQuery += "\"" + key + "\" = ";

            if (value === '') {
              updateQuery += null;
            } else {
              updateQuery += "$$" + value + "$$";
            }

            updateQuery += ","
          });

          updateQuery = updateQuery.slice(0, -1) + ' WHERE ' + whereClause;

          queryInterface.sequelize.query(updateQuery).then(function () {
            done();
          }).catch(function (err) {
            done(err);
          });

        }, function (err) {
          err ? reject(err) : resolve();
        });
      });
    }).then(function () {
      return Promise.each(groupMappings, function (mapping) {
        return queryInterface.sequelize.query("UPDATE " + tableName + " SET \"group\" = '" + mapping.new + "', updated_at = '" + now + "' WHERE \"group\" = '" + mapping.old + "'");
      }).then(function () {
        return queryInterface.sequelize.query("UPDATE " + tableName + " SET \"group\" = NULL, division = NULL, l2_manager = NULL, l3_manager = NULL, pra = NULL, updated_at = '" + now + "' WHERE updated_at < '" + now + "'");
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return Promise.each(groupMappings, function(mapping) {
      return queryInterface.sequelize.query("UPDATE " + tableName + " SET \"group\" = '" + mapping.old + "' WHERE \"group\" = '" + mapping.new + "'");
    })
  }
};
