'use strict';
var Promise = require('bluebird');
var tableName = 'program_heatmap_manual_entry';
var column1 = 'staffing';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.addColumn(tableName, column1, Sequelize.STRING)
        .then(function () {
          queryInterface.sequelize.query("UPDATE program_heatmap_manual_entry SET staffing = (CASE WHEN (program_metadata_manual_entry.contract_type LIKE '%ffp%' and program_heatmap_manual_entry.staffing_percentage < 105) THEN 'G' WHEN (program_metadata_manual_entry.contract_type LIKE '%ffp%' and program_heatmap_manual_entry.staffing_percentage >= 105 and program_heatmap_manual_entry.staffing_percentage < 110) THEN 'Y' WHEN (program_metadata_manual_entry.contract_type LIKE '%ffp%' and program_heatmap_manual_entry.staffing_percentage > 110) THEN 'R' WHEN (program_metadata_manual_entry.contract_type  NOT LIKE '%ffp%' and program_heatmap_manual_entry.staffing_percentage > 95) THEN 'G' WHEN (program_metadata_manual_entry.contract_type NOT LIKE '%ffp%' and program_heatmap_manual_entry.staffing_percentage >= 85 and program_heatmap_manual_entry.staffing_percentage <= 95) THEN 'Y' WHEN (program_metadata_manual_entry.contract_type NOT LIKE '%ffp%' and program_heatmap_manual_entry.staffing_percentage < 85) THEN 'R' ELSE 'N/A' END ) FROM programs, program_metadata_manual_entry WHERE program_heatmap_manual_entry.program_id = programs.id AND programs.id = program_metadata_manual_entry.program_id;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.removeColumn(tableName, column1)
        .then(resolve)
        .catch(reject);
    });
  }
};
