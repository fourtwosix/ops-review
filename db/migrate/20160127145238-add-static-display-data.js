'use strict';
var Promise = require('bluebird');
var async = require('async');
var tableName = 'static_display_data';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function(done) {
          queryInterface.createTable(
            tableName,
            {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
              display_value: Sequelize.STRING,
              type: Sequelize.STRING,
              description: Sequelize.STRING,
              created_at: Sequelize.DATE,
              updated_at: Sequelize.DATE,
              created_by: Sequelize.INTEGER,
              updated_by: Sequelize.INTEGER
            })
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        },
        function(done) {
          queryInterface.sequelize.query("ALTER TABLE " + tableName + " OWNER TO insighter;")
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        },
        function(done) {
          var pmrDivisionValues = [
            "42Six",
            "AF/MD",
            "ADI-R",
            "Applied Technology",
            "Army & Defense Intel",
            "BITT",
            "CS",
            "CS - S&L",
            "DHS EO",
            "DHS IIP",
            "Eagle Alliance",
            "Foreign Affairs",
            "FDIC",
            "FedHlth",
            "GWAC",
            "ICS",
            "ITIP",
            "Justice/LE",
            "Missions Systems",
            "National Security",
            "Naval & Joint Ops",
            "NPS Internal",
            "SHD",
            "Technology/Industry SMEs",
            "Training",
            "VA/Military Health",
            "VIS",
            "Welkin"
          ];
          async.each(pmrDivisionValues, function(division, done) {
            var now = new Date().toUTCString();
            queryInterface.sequelize.query(
                "INSERT INTO " + tableName +
                " (display_value, type, created_at, updated_at) values ('" +
                division + "','pmrDivision','" + now + "', '" + now + "')"
              )
              .then(function() {
                done();
              }).catch(function(err) {
                done(err);
              })
          }, function(err) {
            done(err);
          });
        }
      ], function (err) {
        err ? reject(err): resolve();
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable(tableName)
        .then(resolve)
        .catch(reject);
    });
  }
};
