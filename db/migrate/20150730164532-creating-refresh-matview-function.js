'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("CREATE OR REPLACE FUNCTION RefreshAllMaterializedViews(schema_arg TEXT DEFAULT 'public', table_arg TEXT DEFAULT '%') " +
        "RETURNS INT AS $$ DECLARE r RECORD; BEGIN RAISE NOTICE 'Refreshing materialized view in schema %', schema_arg; " +
        "FOR r IN SELECT matviewname FROM pg_matviews WHERE schemaname = schema_arg AND definition LIKE table_arg " +
        "LOOP " +
        "RAISE NOTICE 'Refreshing %.%', schema_arg, r.matviewname; " +
        "EXECUTE 'REFRESH MATERIALIZED VIEW ' || schema_arg || '.' || r.matviewname; END LOOP; " +
        "RETURN 1; END $$ LANGUAGE plpgsql;")
        .then(resolve)
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP FUNCTION RefreshAllMaterializedViews RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
