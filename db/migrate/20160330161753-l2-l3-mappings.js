'use strict';
var Promise = require('bluebird');
var _ = require('lodash');
var async = require('async');
var pmrGroupsTable = 'pmr_groups';
var pmrDivisionsTable = 'pmr_divisions';
var pmrGroupsDivisionsTable = 'pmr_groups_divisions';
var pmrGroupsDivisionsL2ManagerView = 'pmr_groups_divisions_l2_manager';
var staticDisplayDataTable = 'static_display_data';
var oldDivisionType = 'pmrDivision';
var oldGroupType = 'pmrGroup';
var oldDivisions = [
  'LPO/WPO',
  'MPO',
  'NGA/DIA',
  'ODNI',
  'Groundbreaker',
  'Financial Agencies',
  'Government Operations',
  'Foreign Affairs',
  'Science & Engineering',
  'Department of Transportation',
  'FDIC',
  'Veterans Administration',
  'Health and Human Services',
  'State and Local Health',
  'Military Health',
  'Naval Warfare & Systems Center',
  'Maritime Systems',
  'US Airforce & Joint Mission Systems',
  'Army Advanced Electronics & Ground',
  'Army Aviation and Missile Defense',
  'Warfighter Focus',
  'Flight School XXI',
  'Air Force',
  'Joint Commands & Defense Agencies',
  'Navy',
  'Army',
  'Logistics Modernization Program',
  'Integrated Personnel and Pay System - Navy',
  'DHS Division 1',
  'DHS Division 2',
  'Law Enforcement Division',
  'TSA/ITIP',
  'DC1/Stennis'
];
var oldGroups = [
  'Civil',
  'Defense IT',
  'Defense Mission',
  'Homeland Security',
  'Health',
  'Intelligence'
];

var newGroups = [
  'Health & Civil',
  'Defense',
  'Homeland Security',
  'Intelligence'
];

var newDivisions = [
  'Air Force',
  'Army',
  'Army Advanced Electronics & Ground',
  'Army Aviation and Missile Defense',
  'Flight School XXI',
  'Joint Commands & Defense Agencies',
  'Maritime Systems',
  'Naval Warfare & Systems Center',
  'Navy',
  'Navy - Integrated Personnel and Pay System',
  'US Airforce & Joint Mission Systems',
  'Warfighter Focus',
  'Financial Agencies',
  'Foreign Affairs',
  'Government Operations',
  'Health and Human Services',
  'Military Health',
  'Science & Engineering',
  'State and Local Health',
  'Veterans Administration',
  'DC1/Stennis',
  'DHS Division 1',
  'DHS Division 2',
  'Law Enforcement',
  'PCC',
  'TSA/ITIP',
  'LPO/WPO',
  'MPO',
  'NGA/DIA',
  'ODNI'
];

var l2GroupManagerMappings = [
  {org: 'Defense', manager: 'Kenneth Deutsch'},
  {org: 'Health & Civil', manager: 'Paul Nedzbala'},
  {org: 'Homeland Security', manager: 'Sally Sullivan'},
  {org: 'Intelligence', manager: 'Leigh Palmer'}
];
var l3DivisionManagerMappings = {
  'Defense': [
    {'Air Force': 'David Rue'},
    {'Army': 'Mark Nagle'},
    {'Army Advanced Electronics & Ground': 'Alan Smith'},
    {'Army Aviation and Missile Defense': 'Doug Allen'},
    {'Flight School XXI': 'Tim Butler'},
    {'Joint Commands & Defense Agencies': 'John Anderson'},
    {'Maritime Systems': 'Peter Zahn'},
    {'Naval Warfare & Systems Center': 'Rick Beck'},
    {'Navy': 'Nick Trzcinski'},
    {'Navy - Integrated Personnel and Pay System': 'Shelly Gorordo'},
    {'US Airforce & Joint Mission Systems': 'Lee Phillips'},
    {'Warfighter Focus': 'Bill Cheverie'},
  ],
  'Health & Civil': [
    {'Financial Agencies': 'Ben Gieseman'},
    {'Foreign Affairs': 'Joanne Sullivan'},
    {'Government Operations': 'John Ludeke'},
    {'Health and Human Services': 'Kamal Narang'},
    {'Military Health': 'David Meyer'},
    {'Science & Engineering': 'Kevin Connell'},
    {'State and Local Health': 'Andrew Saxe'},
    {'Veterans Administration': 'Mike Cardarelli'},
  ],
  'Homeland Security': [
    {'DC1/Stennis': 'John Xereas'},
    {'DHS Division 1': 'Todd Morris'},
    {'DHS Division 2': 'Matt Slaight'},
    {'Law Enforcement': 'Jim McClave'},
    {'PCC': 'Peter Balazy'},
    {'TSA/ITIP': 'Aaron Fuller'},
  ],
  'Intelligence': [
    {'LPO/WPO': 'Steve Hyde'},
    {'MPO': 'Rob Warner'},
    {'NGA/DIA': 'Kelly Artz'},
    {'ODNI': 'Bob Sheppard'},
  ]
};

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function (done) {
          queryInterface.sequelize.query("DELETE FROM " + staticDisplayDataTable + " WHERE type in ('" + oldGroupType + "','" + oldDivisionType + "')")
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function (done) {
          queryInterface.createTable(
            pmrGroupsTable,
            {
              group_name: {
                type: Sequelize.STRING,
                primaryKey: true
              },
              description: Sequelize.TEXT,
              l2_manager: Sequelize.STRING,
              created_at: Sequelize.DATE,
              updated_at: Sequelize.DATE
            })
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function (done) {
          queryInterface.sequelize.query("ALTER TABLE " + pmrGroupsTable + " OWNER TO insighter;")
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function (done) {
          queryInterface.createTable(
            pmrDivisionsTable,
            {
              division_name: {
                type: Sequelize.STRING,
                primaryKey: true
              },
              description: Sequelize.TEXT,
              created_at: Sequelize.DATE,
              updated_at: Sequelize.DATE
            })
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function (done) {
          queryInterface.sequelize.query("ALTER TABLE " + pmrDivisionsTable + " OWNER TO insighter;")
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function (done) {
          queryInterface.createTable(
            pmrGroupsDivisionsTable,
            {
              group_name: {
                type: Sequelize.STRING,
                primaryKey: true,
                references: pmrGroupsTable,
                referencesKey: 'group_name',
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
              },
              division_name: {
                type: Sequelize.STRING,
                primaryKey: true,
                references: pmrDivisionsTable,
                referencesKey: 'division_name',
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
              },
              l3_manager: Sequelize.STRING,
              created_at: Sequelize.DATE,
              updated_at: Sequelize.DATE
            }).then(function () {
            done();
          }).catch(done);
        },
        function (done) {
          queryInterface.sequelize.query("ALTER TABLE " + pmrGroupsDivisionsTable + " OWNER TO insighter;").then(function () {
            done();
          }).catch(done);
        },
        function (done) {
          async.each(newGroups, function (newGroup, done) {
            var l2Manager = _.find(l2GroupManagerMappings, {'org': newGroup}).manager;
            var now = new Date().toUTCString();
            queryInterface.sequelize.query(
                "INSERT INTO " + pmrGroupsTable +
                " (group_name, l2_manager, created_at, updated_at) values ('" +
                newGroup + "','" + l2Manager + "','" + now + "', '" + now + "')"
              )
              .then(function () {
                done();
              })
              .catch(function (err) {
                done(err);
              })
          }, function (err) {
            done(err);
          });
        },
        function (done) {
          async.each(newDivisions, function (newDivision, done) {
            var now = new Date().toUTCString();
            queryInterface.sequelize.query(
              "INSERT INTO " + pmrDivisionsTable +
              " (division_name, created_at, updated_at) values ('" +
              newDivision + "','" + now + "', '" + now + "')"
            ).then(function () {
              done();
            }).catch(done)
          }, done);
        },
        function (done) {
          async.forEachOf(l3DivisionManagerMappings, function (l3MappingArray, l2Org, done) {
            async.each(l3MappingArray, function (l3Mapping, done) {
              var now = new Date().toUTCString();
              var l3Org = Object.keys(l3Mapping)[0];
              var l3Manager = l3Mapping[l3Org];

              queryInterface.sequelize.query(
                "INSERT INTO " + pmrGroupsDivisionsTable +
                " (group_name, division_name, l3_manager, created_at, updated_at) values ('" +
                l2Org + "','" + l3Org + "',$$" + l3Manager + "$$,'" + now + "', '" + now + "')"
              ).then(function () {
                done();
              }).catch(done)
            }, done);
          }, done);
        },
        function (done) {
          queryInterface.sequelize.query("CREATE MATERIALIZED VIEW " + pmrGroupsDivisionsL2ManagerView + " AS " +
              "SELECT pmr_groups.l2_manager, pmr_groups_divisions.* " +
              " FROM pmr_groups, pmr_groups_divisions " +
              " WHERE pmr_groups.group_name = pmr_groups_divisions.group_name")
            .then(function () {
              done();
            }).catch(done);
        },
        function (done) {
          queryInterface.sequelize.query("ALTER TABLE " + pmrGroupsDivisionsL2ManagerView + " OWNER TO insighter;")
            .then(function () {
              done();
            }).catch(done);
        }
      ], function (err) {
        err ? reject(err) : resolve();
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function (done) {
          queryInterface.sequelize.query("DROP MATERIALIZED VIEW " + pmrGroupsDivisionsL2ManagerView)
            .then(function () {
              done();
            }).catch(function (err) {
            done(err);
          });
        },
        function (done) {
          queryInterface.dropTable(pmrGroupsDivisionsTable)
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function (done) {
          queryInterface.dropTable(pmrGroupsTable)
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function (done) {
          queryInterface.dropTable(pmrDivisionsTable)
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function (done) {
          var now = new Date().toUTCString();
          var values = _.trimRight(_.reduce(oldGroups, function (result, group) {
            return result + "('" + group + "','" + oldGroupType + "','" + now + "','" + now + "'), ";
          }, ""), ", ");
          queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES " + values).then(function() {
              queryInterface.sequelize.query("DELETE FROM static_display_data WHERE id NOT IN (SELECT DISTINCT ON (type, display_value) id FROM static_display_data ORDER BY type, display_value, updated_at DESC)");
            }).then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function (done) {
          var now = new Date().toUTCString();
          var values = _.trimRight(_.reduce(oldDivisions, function (result, division) {
            return result + "('" + division + "','" + oldDivisionType + "','" + now + "','" + now + "'), ";
          }, ""), ", ");
          queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES " + values).then(function() {
              queryInterface.sequelize.query("DELETE FROM static_display_data WHERE id NOT IN (SELECT DISTINCT ON (type, display_value) id FROM static_display_data ORDER BY type, display_value, updated_at DESC)");
          }).then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        }
      ], function (err) {
        err ? reject(err) : resolve();
      });
    });
  }
};
