'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'glassdoor_company_data',
        {
          company_name: {
            type: Sequelize.STRING,
            primaryKey: true
          },
          glassdoor_id: {
            type: Sequelize.INTEGER
          },
          glassdoor_industry: {
            type: Sequelize.STRING
          },
          number_of_ratings: {
            type: Sequelize.INTEGER
          },
          overall_rating: {
            type: Sequelize.FLOAT
          },
          culture_and_values_rating: {
            type: Sequelize.FLOAT
          },
          senior_leadership_rating: {
            type: Sequelize.FLOAT
          },
          compensation_and_benefits_rating: {
            type: Sequelize.FLOAT
          },
          career_opportunities_rating: {
            type: Sequelize.FLOAT
          },
          work_life_balance_rating: {
            type: Sequelize.FLOAT
          },
          recommend_to_friend_rating: {
            type: Sequelize.FLOAT
          },
          created_at: {
            type: Sequelize.DATE,
            primaryKey: true
          },
          updated_at: {
            type: Sequelize.DATE
          }
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE glassdoor_company_data OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable("glassdoor_company_data")
        .then(resolve)
        .catch(reject);
    });
  }
};
