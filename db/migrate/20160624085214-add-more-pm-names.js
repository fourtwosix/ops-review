'use strict';
var Promise = require('bluebird');
var now = new Date().toUTCString();
var _ = require('lodash');

var managers = [
  "Genevieve Gadwale",
  "Peter Lugo"
];

module.exports = {
  up: function (queryInterface, Sequelize) {
    return Promise.each(managers, function (manager) {
      return queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES ('" + manager + "', 'pmrName', '" + now + "', '" + now + "')");
    });
  },

  down: function (queryInterface, Sequelize) {
    return Promise.each(managers, function (manager) {
      return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE lower(display_value) = lower('" + manager + "')");
    });
  }
};