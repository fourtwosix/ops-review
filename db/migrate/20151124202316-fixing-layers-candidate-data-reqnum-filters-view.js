'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS candidate_data_reqnum_filters RESTRICT;")
      .then(function () {
        queryInterface.sequelize.query("CREATE MATERIALIZED VIEW candidate_data_reqnum_filters AS  " +
        "SELECT DISTINCT candidate_data.requisition_number, candidate_data.client_program_name, candidate_data.req_hiring_manager_name, candidate_data.req_recruiter_name, candidate_data.layer_2_manager, candidate_data.layer_3_manager, candidate_data.layer_4_manager, candidate_data.layer_5_manager FROM candidate_data WHERE candidate_data.requisition_number IS NOT NULL;")
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE candidate_data_reqnum_filters OWNER TO insighter;")
          .then(resolve)
          .catch(reject);
        })
        .catch(reject);
      })
      .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS candidate_data_reqnum_filters RESTRICT;")
      .then(resolve)
      .catch(reject);
    });
  }
};