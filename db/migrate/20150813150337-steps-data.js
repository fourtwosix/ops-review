'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'steps_data',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          report_date: {
            type: Sequelize.DATE
          },
          unique_id: {
            type: Sequelize.STRING,
            unique: true
          },
          org_id: {
            type: Sequelize.STRING
          },
          vendor: {
            type: Sequelize.STRING
          },
          vendor_name: {
            type: Sequelize.STRING
          },
          last_name: {
            type: Sequelize.STRING
          },
          first_name: {
            type: Sequelize.STRING
          },
          mi: {
            type: Sequelize.STRING
          },
          tes_id: {
            type: Sequelize.STRING
          },
          project: {
            type: Sequelize.STRING
          },
          project_task: {
            type: Sequelize.STRING
          },
          weekend_date: {
            type: Sequelize.DATE
          },
          total_hours: {
            type: Sequelize.FLOAT
          },
          run_date: {
            type: Sequelize.DATE
          },
          created_at: {
            type: Sequelize.DATE
          },
          updated_at: {
            type: Sequelize.DATE
          }
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE steps_data OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable("steps_data")
        .then(resolve)
        .catch(reject);
    });
  }
};
