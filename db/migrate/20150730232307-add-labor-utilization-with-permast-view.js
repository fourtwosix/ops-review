'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("CREATE MATERIALIZED VIEW labor_utilization_with_permast AS " +
        "SELECT labor_utilization_data.*, first_name, last_name, layer_2_manager, layer_3_manager, " +
        "layer_4_manager, layer_5_manager, layer_6_manager, layer_7_manager FROM labor_utilization_data " +
        "JOIN permast_data ON " +
        "permast_data.short_name = labor_utilization_data.short_name " +
        "AND date_trunc('month', permast_data.report_date)::DATE = date_trunc('month', week_ending_date)::DATE " +
        "AND date_trunc('day', permast_data.report_date)::DATE BETWEEN (date_trunc('week', week_ending_date) - '1 day'::INTERVAL)::DATE " +
        "AND (date_trunc('week', week_ending_date) + '5 day'::INTERVAL)::DATE WITH NO DATA;")
      .then(function () {
        queryInterface.sequelize.query("ALTER TABLE labor_utilization_with_permast OWNER TO insighter;")
          .then(resolve)
          .catch(reject);

      })
      .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS labor_utilization_with_permast RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
