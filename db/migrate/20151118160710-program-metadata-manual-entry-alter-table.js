/**
 * Created by cedam on 11/5/15.
 */

'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface,Sequelize) {
    return new Promise(function (resolve, reject) {
      async.waterfall([
        function (done) {
          console.log("Add Column client")
          queryInterface.addColumn('program_metadata_manual_entry', 'client', Sequelize.STRING).then(function() {
            done(null);
          }).catch(done);
        },
        function (done) {
          console.log("Add Column industry gm")
          queryInterface.addColumn('program_metadata_manual_entry', 'industry_gm', Sequelize.STRING).then(function() {
            done(null);
          }).catch(done);
        }
      ], function (err) {
        err ? reject(err): resolve();
      });
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.waterfall([
        function (done) {
          console.log("Drop Column client")
          queryInterface.removeColumn('program_metadata_manual_entry', 'client').then(function() {
            done(null);
          }).catch(done);
        },
        function (done) {
          console.log("Drop Column industry gm")
          queryInterface.removeColumn('program_metadata_manual_entry', 'industry_gm').then(function() {
            done(null);
          }).catch(done);
        }
      ], function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
