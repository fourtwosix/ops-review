'use strict';
var Promise = require('bluebird');
var now = new Date().toUTCString();
var _ = require('lodash');

var columns = [
  "program_manager",
  "pra",
  "l3_manager",
  "l2_manager"
];

var updates = [
  {
    type: 'pmrName',
    new: "John Ludecke",
    old: "John Ludeke"
  }
];

module.exports = {
  up: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.new + "', updated_at = '" + now + "' WHERE lower(" + column + ") = lower('" + update.old + "')");
      })
    }).then(function () {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE static_display_data SET display_value = '" + update.new + "', updated_at = '" + now + "' WHERE display_value = '" + update.old + "'");
      })
    }).then(function () {
      return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE id NOT IN (SELECT DISTINCT ON (type, display_value) id FROM static_display_data ORDER BY type, display_value, updated_at DESC)");
    });
  },

  down: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.old + "', updated_at = '" + now + "' WHERE lower(" + column + ") = lower('" + update.new + "')");
      })
    }).then(function () {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE static_display_data SET display_value = '" + update.new + "', updated_at = '" + now + "' WHERE display_value = '" + update.old + "'");
      })
    }).then(function () {
      return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE id NOT IN (SELECT DISTINCT ON (type, display_value) id FROM static_display_data ORDER BY type, display_value, updated_at DESC)");
    });
  }
};