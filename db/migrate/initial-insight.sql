CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;






DROP TYPE IF EXISTS boxplot_values CASCADE;




CREATE TYPE boxplot_values AS (
	min numeric,
	q1 numeric,
	median numeric,
	q3 numeric,
	max numeric,
	mean numeric,
	upper_limit numeric,
	lower_limit numeric
);





DROP TYPE IF EXISTS histogram_values CASCADE;




CREATE TYPE histogram_values AS (
	range text,
	freq bigint
);






CREATE FUNCTION _final_boxplot(arr numeric[]) RETURNS boxplot_values
    LANGUAGE plpythonu IMMUTABLE
    AS $$
    arr.sort()
    i = len(arr)

    q1 = arr[i/4]
    q3 = arr[i*3/4]

    lower_limit = float(q1) - 1.5 * (float(q3) - float(q1))
    upper_limit = float(q3) + 1.5 * (float(q3) - float(q1))

    filtered_arr = [v for v in arr if float(v) >= lower_limit and float(v) <= upper_limit]
    mean = float(sum(filtered_arr)) / float(len(filtered_arr))

    return ( arr[0], arr[i/4], arr[i/2], arr[i*3/4], arr[-1], mean, upper_limit, lower_limit)
$$;








CREATE FUNCTION adjust_rows_capacity(week_ending date) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  update labor_utilization_data as l
    set
      wtd_adjusted_capacity =
        case when needs_adjustment then
          ((wtd_adjusted_capacity + wtd_lwop) / 40.0 * hours_per_week) - wtd_lwop
        else
          wtd_adjusted_capacity
        end,
      mtd_adjusted_capacity =
        case when needs_adjustment then
          ((mtd_adjusted_capacity + mtd_lwop) / 40.0 * hours_per_week) - mtd_lwop
        else
          mtd_adjusted_capacity
        end,
      ytd_adjusted_capacity =
        case when needs_adjustment then
          ((ytd_adjusted_capacity + ytd_lwop) / 40.0 * hours_per_week) - ytd_lwop
        else
          ytd_adjusted_capacity
        end
    from (
      select hours_per_week, short_name, (hours_per_week is not null and hours_per_week <> 40) as needs_adjustment
      from permast_data
         WHERE hours_per_week <> 40
         AND date_trunc('month', report_date)::DATE = date_trunc('month', week_ending)::DATE
         AND date_trunc('day', report_date)::DATE BETWEEN (date_trunc('week', week_ending) - '1 day'::interval)::DATE
         AND (date_trunc('week', week_ending) + '5 days'::interval)::DATE
      ) as subquery
    where l.short_name = subquery.short_name
    and l.week_ending_date::DATE = week_ending;
END
$$;






CREATE FUNCTION get_hours_per_week(shortname text, week_ending date) RETURNS TABLE(hours_per_week integer)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY
SELECT permast_data.hours_per_week
FROM permast_data WHERE short_name = shortname
    AND date_trunc('day', report_date)::DATE BETWEEN
    (date_trunc('week', week_ending) - '1 day'::interval)::DATE
    AND (date_trunc('week', week_ending) + '5 days'::interval)::DATE
    ORDER BY report_date DESC LIMIT 1;
END
$$;






CREATE FUNCTION histogram(data double precision[], number_of_bins integer) RETURNS SETOF histogram_values
    LANGUAGE plpythonu IMMUTABLE
    AS $$
import numpy

if not data:
  return [{'range': 0, 'freq': 0}] * number_of_bins

histogram = numpy.histogram(data, number_of_bins)

return_val = []
for i in range(number_of_bins):
 r = str(int(histogram[1][i])) + '-' + str(int(histogram[1][i+1] - 1))
 f = int(histogram[0][i])
 return_val.append({'range': r, 'freq': f})

return_val[0]['range'] = '< ' + str(int(return_val[0]['range'].split('-')[1]) + 1)
return_val[len(return_val)-1]['range'] = '> ' + str(int(return_val[len(return_val)-1]['range'].split('-')[0]) - 1)
return return_val
$$;






CREATE FUNCTION histogram(data double precision[], number_of_bins integer, upper_limit integer) RETURNS SETOF histogram_values
    LANGUAGE plpythonu IMMUTABLE
    AS $$
    import numpy

    # Partition data if there is an upperlimit
    last_bin = []
    new_data = data
    if upper_limit:
       last_bin = [v for v in data if v >= float(upper_limit)]
       new_data = [v for v in data if v < float(upper_limit)]

    # If we created a last bin based on the upper_limit then we
    # have already accounted for a bin (subtract 1 from original bin number)
    real_number_of_bins = number_of_bins - 1 if last_bin else number_of_bins

    histogram, bin_edges = numpy.histogram(new_data, real_number_of_bins)

    if last_bin:
        histogram = numpy.append(histogram, [len(last_bin)])
        bin_edges = numpy.append(bin_edges, [bin_edges[-1] + 1]) # get the min of last_bin

    return_val = []

    for i in range(number_of_bins):
        r = str(int(bin_edges[i]) + 1) + '-' + str(int(bin_edges[i+1]))
        f = int(histogram[i])
        return_val.append({'range': r, 'freq': f})

    return_val[0]['range'] = '&lt; ' + str(int(return_val[0]['range'].split('-')[1]) + 1)
    return_val[len(return_val)-1]['range'] = '&gt; ' + str(int(return_val[len(return_val)-1]['range'].split('-')[0]) - 1)
    return return_val
$$;






CREATE FUNCTION histogrambinsize(data double precision[], binsize integer) RETURNS SETOF histogram_values
    LANGUAGE plpythonu IMMUTABLE
    AS $$
import numpy

def baseRound(x, base):
    return int(base * round(float(x)/base))

minData = min(data)
minEdge = baseRound(minData-(binsize/2), binsize)
if(minData < minEdge):
 minEdge = minEdge - binsize

maxData = max(data)
maxEdge = baseRound(maxData+(binsize/2), binsize)
if(maxData >= maxEdge):
 maxEdge = maxEdge + binsize

print(maxEdge)
print(minEdge)

number_of_bins = ((maxEdge - minEdge) / binsize)

print(number_of_bins)

histogram = numpy.histogram(data, number_of_bins, (minEdge, maxEdge))

print(histogram)

return_val = []
for i in range(number_of_bins):
 r = str(int(histogram[1][i])) + '-' + str(int(histogram[1][i+1]) - 1)
 f = int(histogram[0][i])
 return_val.append({'range': r, 'freq': f})

return return_val
$$;






CREATE FUNCTION refresh_matview(matview text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    REFRESH MATERIALIZED VIEW CONCURRENTLY matview;
END
$$;






CREATE AGGREGATE boxplot(numeric) (
    SFUNC = array_append,
    STYPE = numeric[],
    INITCOND = '{}',
    FINALFUNC = _final_boxplot
);








CREATE TABLE labor_utilization_data (
    id integer NOT NULL,
    report_date timestamp with time zone,
    week_ending_date timestamp with time zone,
    unique_id character varying(255),
    line_of_service character varying(255),
    industry character varying(255),
    offering character varying(255),
    pool character varying(255),
    "group" character varying(255),
    cp_org character varying(255),
    personnel_number character varying(255),
    full_employee_name character varying(255),
    short_name character varying(255),
    employee_type_code character varying(255),
    employee_salary_code character varying(255),
    hire_date timestamp with time zone,
    termination_date timestamp with time zone,
    full_time_weeks_worked integer,
    wtd_adjusted_capacity double precision,
    mtd_adjusted_capacity double precision,
    ytd_adjusted_capacity double precision,
    ytd_capacity double precision,
    wtd_billable double precision,
    wtd_unbillable double precision,
    wtd_bid_proposal double precision,
    wtd_odo double precision,
    wtd_internal_cost_recovery double precision,
    wtd_general_overhead double precision,
    wtd_service_center double precision,
    wtd_available_bench double precision,
    wtd_training double precision,
    wtd_unallowable_indirect double precision,
    wtd_holiday double precision,
    wtd_vacation double precision,
    wtd_sick_leave double precision,
    wtd_emergency double precision,
    wtd_lwop double precision,
    wtd_other_leave double precision,
    wtd_suspense double precision,
    wtd_total_hours double precision,
    mtd_billable double precision,
    mtd_unbillable double precision,
    mtd_bid_proposal double precision,
    mtd_odo double precision,
    mtd_internal_cost_recovery double precision,
    mtd_general_overhead double precision,
    mtd_service_center double precision,
    mtd_available_bench double precision,
    mtd_training double precision,
    mtd_unallowable_indirect double precision,
    mtd_holiday double precision,
    mtd_vacation double precision,
    mtd_sick_leave double precision,
    mtd_emergency double precision,
    mtd_lwop double precision,
    mtd_other_leave double precision,
    mtd_suspense double precision,
    mtd_total_hours double precision,
    ytd_billable double precision,
    ytd_unbillable double precision,
    ytd_bid_proposal double precision,
    ytd_odo double precision,
    ytd_internal_cost_recovery double precision,
    ytd_general_overhead double precision,
    ytd_service_center double precision,
    ytd_available_bench double precision,
    ytd_training double precision,
    ytd_unallowable_indirect double precision,
    ytd_holiday double precision,
    ytd_vacation double precision,
    ytd_sick_leave double precision,
    ytd_emergency double precision,
    ytd_lwop double precision,
    ytd_other_leave double precision,
    ytd_suspense double precision,
    ytd_total_hours double precision,
    direct_billable_utilization_wtd double precision,
    cost_recovery_utilization_wtd double precision,
    total_productive_utilization_wtd double precision,
    direct_billable_utilization_mtd double precision,
    cost_recovery_utilization_mtd double precision,
    total_productive_utilization_mtd double precision,
    direct_billable_utilization_ytd double precision,
    cost_recovery_utilization_ytd double precision,
    total_productive_utilization_ytd double precision,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);






CREATE TABLE permast_data (
    id integer NOT NULL,
    report_date timestamp with time zone,
    unique_id character varying(255),
    personnel_number character varying(255),
    last_name character varying(255),
    first_name character varying(255),
    short_name character varying(255),
    company_code character varying(255),
    company_name character varying(255),
    business_unit_area character varying(255),
    employment_status character varying(255),
    cost_center_code character varying(255),
    cost_center_name character varying(255),
    employee_group character varying(255),
    employee_subgroup character varying(255),
    work_schedule character varying(255),
    hours_per_week integer,
    exempt_status character varying(255),
    job_code character varying(255),
    job_description character varying(255),
    short_job_description character varying(255),
    position_number character varying(255),
    position_name character varying(255),
    ps_grp character varying(255),
    hire_date timestamp with time zone,
    termination_date timestamp with time zone,
    termination_type character varying(255),
    leave_accrual_date timestamp with time zone,
    service_date timestamp with time zone,
    supervisor_personnel_number character varying(255),
    superior_short_name character varying(255),
    superior character varying(255),
    layer_2_org_name character varying(255),
    layer_2_manager character varying(255),
    layer_3_org_name character varying(255),
    layer_3_manager character varying(255),
    layer_4_org_name character varying(255),
    layer_4_manager character varying(255),
    layer_5_org_name character varying(255),
    layer_5_manager character varying(255),
    layer_6_org_name character varying(255),
    layer_6_manager character varying(255),
    layer_7_org_name character varying(255),
    layer_7_manager character varying(255),
    location character varying(255),
    latitude double precision,
    longitude double precision,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);






CREATE MATERIALIZED VIEW billable_productive_data AS
  SELECT labor_utilization_data.wtd_adjusted_capacity,
    labor_utilization_data.wtd_billable,
    (((((labor_utilization_data.wtd_billable + labor_utilization_data.wtd_unbillable) + labor_utilization_data.wtd_bid_proposal) + labor_utilization_data.wtd_odo) + labor_utilization_data.wtd_internal_cost_recovery) + labor_utilization_data.wtd_service_center) AS wtd_productive,
    labor_utilization_data.ytd_adjusted_capacity,
    labor_utilization_data.ytd_billable,
    (((((labor_utilization_data.ytd_billable + labor_utilization_data.ytd_unbillable) + labor_utilization_data.ytd_bid_proposal) + labor_utilization_data.ytd_odo) + labor_utilization_data.ytd_internal_cost_recovery) + labor_utilization_data.ytd_service_center) AS ytd_productive,
    labor_utilization_data.full_employee_name,
    labor_utilization_data.week_ending_date,
    permast_data.layer_4_manager,
    permast_data.layer_3_manager,
    permast_data.layer_5_manager,
    permast_data.layer_6_manager,
    permast_data.layer_7_manager,
    CASE
    WHEN ((permast_data.job_description)::text ~ '(vp|mgr|dir|supv)'::text) THEN 'true'::text
    ELSE 'false'::text
    END AS is_manager
  FROM (labor_utilization_data
    JOIN permast_data ON (((((permast_data.short_name)::text = (labor_utilization_data.short_name)::text) AND ((date_trunc('month'::text, permast_data.report_date))::DATE = (date_trunc('month'::text, labor_utilization_data.week_ending_date))::date)) AND (((date_trunc('day'::text, permast_data.report_date))::DATE >= ((date_trunc('week'::text, labor_utilization_data.week_ending_date) - '1 day'::interval))::date) AND ((date_trunc('day'::text, permast_data.report_date))::DATE <= ((date_trunc('week'::text, labor_utilization_data.week_ending_date) + '5 days'::interval))::date)))))
  WITH NO DATA;






CREATE TABLE candidate_data (
    id integer NOT NULL,
    report_date timestamp with time zone,
    unique_id character varying(255),
    requisition_number character varying(255),
    requisition_title character varying(255),
    req_creation_date timestamp with time zone,
    req_first_fully_approved_date timestamp with time zone,
    req_first_sourcing_date timestamp with time zone,
    job_title character varying(255),
    candidate_name character varying(255),
    candidate_id character varying(255),
    application_cws_step character varying(255),
    application_cws_status character varying(255),
    application_cws_start_date timestamp with time zone,
    client_program_name character varying(255),
    candidate_is_internal boolean,
    job_code character varying(255),
    req_current_parent_status character varying(255),
    req_current_detail_status character varying(255),
    req_type character varying(255),
    work_country character varying(255),
    work_state character varying(255),
    work_city character varying(255),
    req_hiring_manager_name character varying(255),
    req_recruiter_name character varying(255),
    org_layer_2 character varying(255),
    org_layer_3 character varying(255),
    org_layer_4 character varying(255),
    org_layer_5 character varying(255),
    org_layer_6 character varying(255),
    org_layer_7 character varying(255),
    layer_2_manager character varying(255),
    layer_3_manager character varying(255),
    layer_4_manager character varying(255),
    layer_5_manager character varying(255),
    layer_6_manager character varying(255),
    layer_7_manager character varying(255),
    latitude double precision,
    longitude double precision,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);






CREATE SEQUENCE candidate_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE candidate_data_id_seq OWNED BY candidate_data.id;






CREATE TABLE cards (
    id integer NOT NULL,
    config json,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    deck_id integer
);






CREATE SEQUENCE cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE cards_id_seq OWNED BY cards.id;






CREATE TABLE deck_editors (
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    deck_id integer NOT NULL
);






CREATE TABLE deck_viewers (
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    deck_id integer NOT NULL
);






CREATE TABLE decks (
    id integer NOT NULL,
    config json,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    owner_id integer
);






CREATE SEQUENCE decks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE decks_id_seq OWNED BY decks.id;






CREATE SEQUENCE labor_utilization_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE labor_utilization_data_id_seq OWNED BY labor_utilization_data.id;






CREATE SEQUENCE permast_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE permast_data_id_seq OWNED BY permast_data.id;






CREATE TABLE project_data (
    id integer NOT NULL,
    report_date timestamp with time zone,
    unique_id character varying(255),
    personnel_number character varying(255),
    short_name character varying(255),
    full_employee_name character varying(255),
    employee_type_code character varying(255),
    timesheet_type character varying(255),
    home_org_id character varying(255),
    business_organization character varying(255),
    home_org_name character varying(255),
    level_3_org_segment_id character varying(255),
    line_of_service character varying(255),
    line_of_service_code character varying(255),
    level_3_org_name character varying(255),
    project_classification character varying(255),
    project_id character varying(255),
    project_name character varying(255),
    labor_type_id character varying(255),
    labor_type character varying(255),
    hours integer,
    timesheet_date timestamp with time zone,
    employee_layer character varying(255),
    layer_4_manager character varying(255),
    layer_5_manager character varying(255),
    layer_6_manager character varying(255),
    layer_7_manager character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);






CREATE SEQUENCE project_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE project_data_id_seq OWNED BY project_data.id;








CREATE TABLE resource_management_data (
    id integer NOT NULL,
    report_date timestamp with time zone,
    unique_id character varying(255),
    cost_center_code character varying(255),
    req_user_group character varying(255),
    requisition_number character varying(255),
    workday_position_id character varying(255),
    requisition_title character varying(255),
    csc_title character varying(255),
    csc_job_code character varying(255),
    req_current_parent_status character varying(255),
    req_current_detail_status character varying(255),
    req_recruiter_name character varying(255),
    req_hiring_manager_name character varying(255),
    req_creation_date timestamp with time zone,
    req_first_fully_approved_date timestamp with time zone,
    req_first_sourcing_date timestamp with time zone,
    req_latest_on_hold_date timestamp with time zone,
    req_latest_filled_date timestamp with time zone,
    req_days_open integer,
    aging character varying(255),
    req_number_openings integer,
    number_applications integer,
    number_candidates integer,
    number_new_applications integer,
    number_first_interview integer,
    number_second_interview integer,
    number_third_interview integer,
    number_reviewed integer,
    req_citizenship_status character varying(255),
    req_recruiter_email character varying(255),
    req_hiring_manager_email character varying(255),
    number_applications_offered integer,
    number_job_offers_accepted integer,
    number_rejected_declined integer,
    number_hired integer,
    number_position_remaining_open integer,
    overtime_status character varying(255),
    labor_category character varying(255),
    req_job_grade character varying(255),
    job_code character varying(255),
    business_unit_area character varying(255),
    company_code character varying(255),
    union_job boolean,
    replacement_name character varying(255),
    relocation_assist character varying(255),
    add_replacement character varying(255),
    citizenship character varying(255),
    division character varying(255),
    req_type character varying(255),
    req_job_type character varying(255),
    req_staffing_type character varying(255),
    security_clearance character varying(255),
    employment_status character varying(255),
    client character varying(255),
    work_region character varying(255),
    work_country character varying(255),
    work_state character varying(255),
    work_city character varying(255),
    req_last_updated timestamp with time zone,
    org_layer_2 character varying(255),
    org_layer_3 character varying(255),
    org_layer_4 character varying(255),
    org_layer_5 character varying(255),
    org_layer_6 character varying(255),
    org_layer_7 character varying(255),
    org_layer_8 character varying(255),
    layer_2_manager character varying(255),
    layer_3_manager character varying(255),
    layer_4_manager character varying(255),
    layer_5_manager character varying(255),
    layer_6_manager character varying(255),
    layer_7_manager character varying(255),
    layer_8_manager character varying(255),
    latitude double precision,
    longitude double precision,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);






CREATE SEQUENCE resource_management_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE resource_management_data_id_seq OWNED BY resource_management_data.id;






CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255),
    email character varying(255),
    role character varying(255) DEFAULT 'user'::character varying,
    hashed_password character varying(255),
    provider character varying(255),
    salt character varying(255),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);






CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;






ALTER SEQUENCE users_id_seq OWNED BY users.id;






ALTER TABLE ONLY candidate_data ALTER COLUMN id SET DEFAULT nextval('candidate_data_id_seq'::regclass);






ALTER TABLE ONLY cards ALTER COLUMN id SET DEFAULT nextval('cards_id_seq'::regclass);






ALTER TABLE ONLY decks ALTER COLUMN id SET DEFAULT nextval('decks_id_seq'::regclass);






ALTER TABLE ONLY labor_utilization_data ALTER COLUMN id SET DEFAULT nextval('labor_utilization_data_id_seq'::regclass);






ALTER TABLE ONLY permast_data ALTER COLUMN id SET DEFAULT nextval('permast_data_id_seq'::regclass);






ALTER TABLE ONLY project_data ALTER COLUMN id SET DEFAULT nextval('project_data_id_seq'::regclass);






ALTER TABLE ONLY resource_management_data ALTER COLUMN id SET DEFAULT nextval('resource_management_data_id_seq'::regclass);






ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);






ALTER TABLE ONLY candidate_data
    ADD CONSTRAINT candidate_data_pkey PRIMARY KEY (id);






ALTER TABLE ONLY candidate_data
    ADD CONSTRAINT candidate_data_unique_id_key UNIQUE (unique_id);






ALTER TABLE ONLY cards
    ADD CONSTRAINT cards_pkey PRIMARY KEY (id);






ALTER TABLE ONLY deck_editors
    ADD CONSTRAINT deck_editors_pkey PRIMARY KEY (user_id, deck_id);






ALTER TABLE ONLY deck_viewers
    ADD CONSTRAINT deck_viewers_pkey PRIMARY KEY (user_id, deck_id);






ALTER TABLE ONLY decks
    ADD CONSTRAINT decks_pkey PRIMARY KEY (id);






ALTER TABLE ONLY labor_utilization_data
    ADD CONSTRAINT labor_utilization_data_pkey PRIMARY KEY (id);






ALTER TABLE ONLY labor_utilization_data
    ADD CONSTRAINT labor_utilization_data_unique_id_key UNIQUE (unique_id);






ALTER TABLE ONLY permast_data
    ADD CONSTRAINT permast_data_pkey PRIMARY KEY (id);






ALTER TABLE ONLY permast_data
    ADD CONSTRAINT permast_data_unique_id_key UNIQUE (unique_id);






ALTER TABLE ONLY project_data
    ADD CONSTRAINT project_data_pkey PRIMARY KEY (id);






ALTER TABLE ONLY project_data
    ADD CONSTRAINT project_data_unique_id_key UNIQUE (unique_id);






ALTER TABLE ONLY resource_management_data
    ADD CONSTRAINT resource_management_data_pkey PRIMARY KEY (id);






ALTER TABLE ONLY resource_management_data
    ADD CONSTRAINT resource_management_data_unique_id_key UNIQUE (unique_id);






ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);






ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);






CREATE INDEX candidate_data_application_cws_status ON candidate_data USING btree (application_cws_status);






CREATE INDEX candidate_data_application_cws_step ON candidate_data USING btree (application_cws_step);






CREATE INDEX candidate_data_client_program_name ON candidate_data USING btree (client_program_name);






CREATE INDEX candidate_data_layer_3_manager ON candidate_data USING btree (layer_3_manager);






CREATE INDEX candidate_data_layer_4_manager ON candidate_data USING btree (layer_4_manager);






CREATE INDEX candidate_data_layer_5_manager ON candidate_data USING btree (layer_5_manager);






CREATE INDEX candidate_data_report_date ON candidate_data USING btree (report_date);






CREATE INDEX candidate_data_req_creation_date ON candidate_data USING btree (req_creation_date);






CREATE INDEX candidate_data_req_first_fully_approved_date ON candidate_data USING btree (req_first_fully_approved_date);






CREATE INDEX candidate_data_req_first_sourcing_date ON candidate_data USING btree (req_first_sourcing_date);






CREATE INDEX candidate_data_requisition_number ON candidate_data USING btree (requisition_number);






CREATE INDEX labor_utilization_data_personnel_number ON labor_utilization_data USING btree (personnel_number);






CREATE INDEX labor_utilization_data_report_date ON labor_utilization_data USING btree (report_date);






CREATE INDEX labor_utilization_data_short_name ON labor_utilization_data USING btree (short_name);






CREATE INDEX labor_utilization_data_week_ending_date ON labor_utilization_data USING btree (week_ending_date);






CREATE INDEX permast_data_employment_status ON permast_data USING btree (employment_status);






CREATE INDEX permast_data_hire_date ON permast_data USING btree (hire_date);






CREATE INDEX permast_data_layer_3_manager ON permast_data USING btree (layer_3_manager);






CREATE INDEX permast_data_layer_4_manager ON permast_data USING btree (layer_4_manager);






CREATE INDEX permast_data_layer_5_manager ON permast_data USING btree (layer_5_manager);






CREATE INDEX permast_data_personnel_number ON permast_data USING btree (personnel_number);






CREATE INDEX permast_data_report_date ON permast_data USING btree (report_date);






CREATE INDEX permast_data_short_name ON permast_data USING btree (short_name);






CREATE INDEX permast_data_termination_date ON permast_data USING btree (termination_date);






CREATE INDEX permast_data_termination_type ON permast_data USING btree (termination_type);






CREATE INDEX req_by_candidate ON candidate_data USING btree (requisition_number, candidate_id);






CREATE INDEX resource_management_data_layer_3_manager ON resource_management_data USING btree (layer_3_manager);






CREATE INDEX resource_management_data_layer_4_manager ON resource_management_data USING btree (layer_4_manager);






CREATE INDEX resource_management_data_layer_5_manager ON resource_management_data USING btree (layer_5_manager);






CREATE INDEX resource_management_data_report_date ON resource_management_data USING btree (report_date);






CREATE INDEX resource_management_data_req_creation_date ON resource_management_data USING btree (req_creation_date);






CREATE INDEX resource_management_data_req_current_detail_status ON resource_management_data USING btree (req_current_detail_status);






CREATE INDEX resource_management_data_req_current_parent_status ON resource_management_data USING btree (req_current_parent_status);






CREATE INDEX resource_management_data_req_first_fully_approved_date ON resource_management_data USING btree (req_first_fully_approved_date);






CREATE INDEX resource_management_data_req_first_sourcing_date ON resource_management_data USING btree (req_first_sourcing_date);






CREATE INDEX resource_management_data_req_last_updated ON resource_management_data USING btree (req_last_updated);






CREATE INDEX resource_management_data_req_latest_filled_date ON resource_management_data USING btree (req_latest_filled_date);






CREATE INDEX resource_management_data_req_latest_on_hold_date ON resource_management_data USING btree (req_latest_on_hold_date);






CREATE INDEX resource_management_data_requisition_number ON resource_management_data USING btree (requisition_number);






ALTER TABLE ONLY cards
    ADD CONSTRAINT cards_deck_id_fkey FOREIGN KEY (deck_id) REFERENCES decks(id) ON UPDATE CASCADE ON DELETE CASCADE;






ALTER TABLE ONLY deck_editors
    ADD CONSTRAINT deck_editors_deck_id_fkey FOREIGN KEY (deck_id) REFERENCES decks(id) ON UPDATE CASCADE ON DELETE CASCADE;






ALTER TABLE ONLY deck_editors
    ADD CONSTRAINT deck_editors_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;






ALTER TABLE ONLY deck_viewers
    ADD CONSTRAINT deck_viewers_deck_id_fkey FOREIGN KEY (deck_id) REFERENCES decks(id) ON UPDATE CASCADE ON DELETE CASCADE;






ALTER TABLE ONLY deck_viewers
    ADD CONSTRAINT deck_viewers_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;



GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO insighter;
