'use strict';
var now = new Date().toUTCString();
var queries = [
  "UPDATE pmr_groups_divisions SET l3_manager = 'Ed Farler' WHERE lower(division_name) = '42six'",
  "DELETE FROM static_display_data WHERE lower(display_value) = 'hank tseu'"
];

var metadataColumns = [
  "program_manager",
  "pra",
  "l3_manager",
  "l2_manager",
  "delivery_manager"
];


module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return Sequelize.Promise.map(queries, function (query) {
        return queryInterface.sequelize.query(query, {transaction: t});
      }).then(function () {
        return Sequelize.Promise.map(metadataColumns, function (col) {
          return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + col + " = 'Ed Farler' WHERE lower(" + col + ") = 'hank tseu'", {transaction: t});
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return queryInterface.sequelize.query("UPDATE pmr_groups_divisions SET l3_manager = 'Hank Tseu' WHERE lower(division_name) = '42six'", {transaction: t}).then(function () {
        return queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES ('Hank Tseu', 'pmrName', '" + now + "', '" + now + "')", {transaction: t}).then(function () {
          return Sequelize.Promise.map(metadataColumns, function (col) {
            return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + col + " = 'Hank Tseu' WHERE lower(" + col + ") = 'ed farler'", {transaction: t});
          });
        });
      });
    });
  }
};
