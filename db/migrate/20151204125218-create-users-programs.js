'use strict';
var Promise = require('bluebird');
var async = require('async');
var tableName = 'users_programs';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function(done) {
          var queries = [
            //**** DROP CONSTRAINTS ****
            "ALTER TABLE program_heatmap_manual_entry DROP CONSTRAINT program_heatmap_manual_entry_program_id_fkey",
            "ALTER TABLE program_metadata_manual_entry DROP CONSTRAINT program_metadata_manual_entry_program_id_fkey",
            "ALTER TABLE program_offering_association DROP CONSTRAINT program_offering_association_program_id_fkey",
            "DROP TABLE IF EXISTS program_heatmap",
            "ALTER TABLE programs DROP CONSTRAINT programs_pkey",
            //**** ADD CONSTRAINTS ****
            "ALTER TABLE programs ADD CONSTRAINT programs_pkey PRIMARY KEY (project_id)",
            "ALTER TABLE programs ADD CONSTRAINT programs_id_unique UNIQUE (id)",
            "ALTER TABLE program_heatmap_manual_entry ADD CONSTRAINT program_heatmap_manual_entry_program_id_fkey FOREIGN KEY (program_id) REFERENCES programs (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT",
            "ALTER TABLE program_metadata_manual_entry ADD CONSTRAINT program_metadata_manual_entry_program_id_fkey FOREIGN KEY (program_id) REFERENCES programs (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT",
            "ALTER TABLE program_offering_association ADD CONSTRAINT program_offering_association_program_id_fkey FOREIGN KEY (program_id) REFERENCES programs (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT"
          ];
          async.eachSeries(queries, function(query, done) {
            queryInterface.sequelize.query(query)
              .then(function() {
                done();
              })
              .catch(function(err) {
                done(err);
              })
          }, function(err) {
            err ? done(err) : done();
          });
        },
        function(done) {
          queryInterface.createTable(
            tableName,
            {
              user_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                references: 'users',
                referencesKey: 'id',
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
              },
              project_id: {
                type: Sequelize.STRING,
                primaryKey: true,
                references: 'programs',
                referencesKey: 'project_id',
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
              },
              created_at: Sequelize.DATE,
              updated_at: Sequelize.DATE
            })
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        },
        function(done) {
          queryInterface.sequelize.query("ALTER TABLE " + tableName + " OWNER TO insighter;")
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        }
      ], function (err) {
        err ? reject(err): resolve();
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      async.series([
        function(done) {
          queryInterface.dropTable(tableName)
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        },
        function(done) {
          var queries = [
            //**** DROP CONSTRAINTS ****
            "ALTER TABLE program_heatmap_manual_entry DROP CONSTRAINT program_heatmap_manual_entry_program_id_fkey",
            "ALTER TABLE program_metadata_manual_entry DROP CONSTRAINT program_metadata_manual_entry_program_id_fkey",
            "ALTER TABLE program_offering_association DROP CONSTRAINT program_offering_association_program_id_fkey",
            "ALTER TABLE programs DROP CONSTRAINT programs_pkey",
            "ALTER TABLE programs DROP CONSTRAINT programs_id_unique",
            //**** ADD CONSTRAINTS ****
            "ALTER TABLE programs ADD CONSTRAINT programs_pkey PRIMARY KEY (id)",
            "ALTER TABLE program_heatmap_manual_entry ADD CONSTRAINT program_heatmap_manual_entry_program_id_fkey FOREIGN KEY (program_id) REFERENCES programs (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT",
            "ALTER TABLE program_metadata_manual_entry ADD CONSTRAINT program_metadata_manual_entry_program_id_fkey FOREIGN KEY (program_id) REFERENCES programs (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT",
            "ALTER TABLE program_offering_association ADD CONSTRAINT program_offering_association_program_id_fkey FOREIGN KEY (program_id) REFERENCES programs (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT"
          ];
          async.eachSeries(queries, function(query, done) {
            queryInterface.sequelize.query(query)
              .then(function() {
                done();
              })
              .catch(function(err) {
                done(err);
              })
          }, function(err) {
            err ? done(err) : done();
          });
        }
      ], function (err) {
        err ? reject(err): resolve();
      })
    });
  }
};
