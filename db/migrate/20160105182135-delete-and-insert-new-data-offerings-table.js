/**
 * Created by cedam on 12/22/15.
 */

'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each(["mission services", "nbs", "nms", "nes", "nis", "big data", "cloud", "cyber security", "n/a", "tbd"], function (offer_code, done) {
        queryInterface.sequelize.query("UPDATE OFFERINGS SET OFFERING_NAME=" + "'" + offer_code + "'," +
          "LOS_NAME=" + "'" + offer_code + "'," +
          "OFFERING_CODE=" + "'" + offer_code + "'," +
          "OFFERING_DESCRIPTION=" + "'" + offer_code + "' WHERE OFFERING_CODE = UPPER('" + offer_code + "');")
          .then(resolve).catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
