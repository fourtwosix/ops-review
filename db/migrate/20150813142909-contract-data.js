'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'contract_data',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          report_date: {
            type: Sequelize.DATE
          },
          unique_id: {
            type: Sequelize.STRING,
            unique: true
          },
          project_num: {
            type: Sequelize.STRING
          },
          project_number_description: {
            type: Sequelize.STRING(400)
          },
          status: {
            type: Sequelize.STRING
          },
          industry: {
            type: Sequelize.STRING
          },
          division: {
            type: Sequelize.STRING
          },
          offering: {
            type: Sequelize.STRING
          },
          l4_portfolio: {
            type: Sequelize.STRING
          },
          market_sector: {
            type: Sequelize.STRING
          },
          salesforce_num: {
            type: Sequelize.STRING
          },
          contractor_status: {
            type: Sequelize.STRING
          },
          accounting_org: {
            type: Sequelize.STRING
          },
          contract_type: {
            type: Sequelize.STRING
          },
          start_date: {
            type: Sequelize.DATE
          },
          end_date: {
            type: Sequelize.DATE
          },
          final_contract_end_date: {
            type: Sequelize.DATE
          },
          contract_administrator: {
            type: Sequelize.STRING
          },
          program_control: {
            type: Sequelize.STRING
          },
          program_manager: {
            type: Sequelize.STRING
          },
          industry_executive: {
            type: Sequelize.STRING
          },
          offering_executive: {
            type: Sequelize.STRING
          },
          client_1: {
            type: Sequelize.STRING
          },
          created_at: {
            type: Sequelize.DATE
          },
          updated_at: {
            type: Sequelize.DATE
          }
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE contract_data OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable("contract_data")
        .then(resolve)
        .catch(reject);
    });
  }
};
