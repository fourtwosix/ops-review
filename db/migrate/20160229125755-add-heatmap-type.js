'use strict';
var Promise = require('bluebird');
var async   = require('async');
var heatmapTableName = 'program_heatmap_manual_entry';
var enumTypeName = 'enum_program_heatmap_manual_entry_type';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
        async.series([
          function (done) {
            queryInterface.addColumn(heatmapTableName, 'type', {
              type: Sequelize.ENUM,
              values: ['actual', 'forecast', 'pra'],
              defaultValue: 'actual'
            }).then(function() {
              done();
            }).catch(done);
          }
        ], function (err) {
          err ? reject(err): resolve();
        });
    });
  },

  down: function (queryInterface) {
    return new Promise(function(resolve, reject) {
      async.series([
        function(done) {
          queryInterface.removeColumn(heatmapTableName, 'type')
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        },
        function(done) {
          queryInterface.sequelize.query('DROP TYPE "' + enumTypeName + '"')
            .then(function () {
              done();
            })
            .catch(function (err) {
              done(err);
            });
        }
      ], function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
