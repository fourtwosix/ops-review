'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'program_metadata_manual_entry',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          program_id: {
            type: Sequelize.INTEGER,
            references: 'programs',
            referencesKey: 'id',
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT'
          },
          unique_id: {
            type: Sequelize.STRING,
            unique: true
          },
          group: Sequelize.STRING,
          account_gm: Sequelize.STRING,
          primary_offering: Sequelize.STRING,
          primary_los: Sequelize.STRING,
          los_manager: Sequelize.STRING,
          pra: Sequelize.STRING,
          pmr_frequency: Sequelize.STRING,
          program_status: Sequelize.STRING,

          key_subs: Sequelize.STRING,
          period_of_performance: Sequelize.STRING,
          payment_terms: Sequelize.STRING,
          project_scope: Sequelize.STRING,
          created_by: Sequelize.STRING,
          updated_by: Sequelize.STRING,
          created_at: Sequelize.DATE,
          updated_at: Sequelize.DATE
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE program_metadata_manual_entry OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable("program_metadata_manual_entry")
        .then(resolve)
        .catch(reject);
    });
  }
};
