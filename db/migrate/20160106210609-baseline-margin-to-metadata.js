'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.removeColumn('program_heatmap_manual_entry', 'bid_margin_percentage').then(function() {
        queryInterface.addColumn('program_metadata_manual_entry', 'baseline_margin_percentage', Sequelize.DECIMAL(14,1)).then(function() {
          queryInterface.sequelize.query("CREATE UNIQUE INDEX program_id_created_at_unique_idx ON program_metadata_manual_entry USING btree (program_id, created_at DESC)")
            .then(resolve)
              .catch(reject);
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.removeColumn('program_metadata_manual_entry', 'baseline_margin_percentage').then(function() {
        queryInterface.addColumn('program_heatmap_manual_entry', 'bid_margin_percentage', Sequelize.DECIMAL(14,1)).then(function() {
          queryInterface.sequelize.query("DROP INDEX program_id_created_at_unique_idx")
            .then(resolve)
            .catch(reject);
        });
      });
    });
  }
};
