
'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
        async.waterfall([
          function (done) {
            console.log("Creating Materialized View")
            queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_fullnames RESTRICT;")
              .then(function() {
                done(null);
              }).catch(done);
          },
          function (done) {
            console.log("Creating Materialized View")
            queryInterface.sequelize.query("CREATE MATERIALIZED VIEW permast_fullnames AS " +
            "SELECT DISTINCT CONCAT(first_name,' ',last_name) as NAME from permast_data;")
              .then(function() {
              done(null);
            }).catch(done);
          },
          function (done) {
            console.log("Altering Table")
            queryInterface.sequelize.query("ALTER TABLE permast_fullnames OWNER TO insighter;").then(function() {
              done(null);
            }).catch(done);
          },
          function (done) {
            console.log("Creating Index");
            queryInterface.sequelize.query("CREATE INDEX fullname ON permast_fullnames(name);").then(function() {
              done(null);
            }).catch(done);
          }
        ], function (err) {
          err ? reject(err): resolve();
        });
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_fullnames RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
