'use strict';
var Promise = require('bluebird');
var now = new Date().toUTCString();
var _ = require('lodash');

var columns = [
  "program_manager",
  "pra",
  "l3_manager",
  "l2_manager"
];

var updates = [
  {
    type: 'pmrName',
    new: "Nick Trzcinski",
    old: "Trzcinski Nick"
  },
  {
    type: 'pmrName',
    new: "Jason Enwright",
    old: "Enwright Jason"
  }
];

module.exports = {
  up: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.new + "', updated_at = '" + now + "' WHERE " + column + " = '" + update.old + "'");
      })
    }).then(function () {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE static_display_data SET display_value = '" + update.new + "', updated_at = '" + now + "' WHERE display_value = '" + update.old + "'");
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.old + "', updated_at = '" + now + "' WHERE " + column + " = '" + update.new + "'");
      })
    }).then(function () {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE static_display_data SET display_value = '" + update.new + "', updated_at = '" + now + "' WHERE display_value = '" + update.old + "'");
      })
    });
  }
};