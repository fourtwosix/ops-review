'use strict';

var Promise = require('bluebird');
var fs = require('fs');
var async = require('async');

var columnUpdates = [
  "UPDATE program_metadata_manual_entry SET program_manager = initcap(split_part(replace(program_manager, ',', ''), ' ', 2) || ' ' || split_part(replace(program_manager, ',', ''), ' ',1)) where array_length(regexp_split_to_array(replace(program_manager, ',', ''), ' '), 1) = 2",
  "UPDATE program_metadata_manual_entry SET pra = initcap(split_part(replace(pra, ',', ''), ' ', 2) || ' ' || split_part(replace(pra, ',', ''), ' ',1)) where array_length(regexp_split_to_array(replace(pra, ',', ''), ' '), 1) = 2",
  "UPDATE program_metadata_manual_entry SET l3_manager = initcap(split_part(replace(l3_manager, ',', ''), ' ', 2) || ' ' || split_part(replace(l3_manager, ',', ''), ' ',1)) where array_length(regexp_split_to_array(replace(l3_manager, ',', ''), ' '), 1) = 2",
  "UPDATE program_metadata_manual_entry SET account_gm = initcap(split_part(replace(account_gm, ',', ''), ' ', 2) || ' ' || split_part(replace(account_gm, ',', ''), ' ',1)) where array_length(regexp_split_to_array(replace(account_gm, ',', ''), ' '), 1) = 2",
  "UPDATE program_metadata_manual_entry SET los_manager = initcap(split_part(replace(los_manager, ',', ''), ' ', 2) || ' ' || split_part(replace(los_manager, ',', ''), ' ',1)) where array_length(regexp_split_to_array(replace(los_manager, ',', ''), ' '), 1) = 2",
  "UPDATE program_metadata_manual_entry SET industry_gm = initcap(split_part(replace(industry_gm, ',', ''), ' ', 2) || ' ' || split_part(replace(industry_gm, ',', ''), ' ',1)) where array_length(regexp_split_to_array(replace(industry_gm, ',', ''), ' '), 1) = 2",
  "UPDATE program_metadata_manual_entry SET l2_manager = initcap(split_part(replace(l2_manager, ',', ''), ' ', 2) || ' ' || split_part(replace(l2_manager, ',', ''), ' ',1)) where array_length(regexp_split_to_array(replace(l2_manager, ',', ''), ' '), 1) = 2"
];

module.exports = {

  up: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
        async.waterfall([
          function (done) {
            async.eachSeries(columnUpdates, function (query, done) {
              queryInterface.sequelize.query(query).then(function () {
                done();
              }).catch(done);
            }, done);
          },
          function (done) {
            fs.readFile("./db/migrate/pmr-592-update.json", "utf8", function (err, json) {
              var updates = JSON.parse(json);
              async.eachSeries(updates, function (update, done) {
                var query = "UPDATE program_metadata_manual_entry SET " + update.column + " = initcap('" + update.new + "') WHERE " + update.column + " = '" + update.old + "'";
                queryInterface.sequelize.query(query).then(function () {
                  done();
                }).catch(done);
              }, done);
            });
          }
        ], function (err) {
          err ? reject(err) : resolve();
        });
    });
  },

  down: function (queryInterface, Sequelize) {
  }
};
