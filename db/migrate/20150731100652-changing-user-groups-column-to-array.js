'use strict';

var async = require('async');
var User = require('../../server/models').User;
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.waterfall([
        function (done) {
          queryInterface.sequelize.query("SELECT id, GROUPS FROM users", { type: queryInterface.sequelize.QueryTypes.SELECT})
            .then(function (users) {
              done(null, users);
            })
            .catch(reject)
        },
        function (users, done) {
          queryInterface.removeColumn("users", "groups")
            .then(function () {
              done(null, users);
            })
            .catch(reject)
        },
        function (users, done) {
          queryInterface.addColumn("users", "groups", Sequelize.ARRAY(Sequelize.STRING))
            .then(function () {
              done(null, users);
            })
            .catch(reject)
        },
        function (users, done) {
          async.each(users, function (user, done) {
            var id = user.id;
            var groups = user.groups;
            User.find(id).then(function(user) {
              user.groups = groups;
              user.save().then(function () {
                done();
              }).catch(done);
            }).catch(done);
          }, done);
        }
      ], function (err) {
         err ? reject(err) : resolve();
      });

    });
  },

  down: function (queryInterface, Sequelize) {
  }
};
