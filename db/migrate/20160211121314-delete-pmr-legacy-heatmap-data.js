'use strict';
var Promise = require('bluebird');
var async   = require('async');

module.exports = {

  up: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {

      // SMELL: not DRY
      var columnsAffected =
      [
      // [0] name                 [1] type
        ['legacy_created_by',     Sequelize.STRING],
        ['legacy_created_date',   Sequelize.DATE],
        ['legacy_reporting_date', Sequelize.DATE]
      ];

      queryInterface.sequelize.query(
        "DELETE FROM program_heatmap_manual_entry " +
        "WHERE legacy_reporting_date IS NOT NULL")
        .then(function () {

          async.each(columnsAffected, function (affectedColumn, done) {
            queryInterface.removeColumn('program_heatmap_manual_entry', affectedColumn[0])
              .then(function () {
                done();
              }).catch(done);
          }, function (err) {
            err ? reject(err) : resolve();
          });

        })
        .then(resolve)
        .catch(reject);
    });
  },


  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {

      // SMELL: not DRY
      var columnsAffected =
      [
      // [0] name                 [1] type
        ['legacy_created_by',     Sequelize.STRING],
        ['legacy_created_date',   Sequelize.DATE],
        ['legacy_reporting_date', Sequelize.DATE]
      ];

      async.each(columnsAffected, function (affectedColumn, done) {
        queryInterface.addColumn('program_heatmap_manual_entry', affectedColumn[0], affectedColumn[1])
          .then(function () {
            done();
          }).catch(done);
      }, function (err) {
        err ? reject(err) : resolve();
      });
    });
  }
};
