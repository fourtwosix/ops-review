'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("CREATE MATERIALIZED VIEW permast_data_layer_manager_filters AS " +
        "SELECT DISTINCT permast_data.layer_3_manager, NULL::text AS layer_4_manager, NULL::text AS layer_5_manager, permast_data.layer_3_org_name FROM permast_data WHERE permast_data.layer_3_manager IS NOT NULL " +
        "UNION " +
        "SELECT DISTINCT NULL::text AS layer_3_manager, permast_data.layer_4_manager, NULL::text AS layer_5_manager, permast_data.layer_3_org_name FROM permast_data WHERE permast_data.layer_4_manager IS NOT NULL " +
        "UNION " +
        "SELECT DISTINCT NULL::text AS layer_3_manager, NULL::text AS layer_4_manager, permast_data.layer_5_manager, permast_data.layer_3_org_name FROM permast_data WHERE permast_data.layer_5_manager IS NOT NULL;")
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE permast_data_layer_manager_filters OWNER TO insighter;")
            .then(resolve)
            .catch(reject);

        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_data_layer_manager_filters RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
