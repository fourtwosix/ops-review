'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.addColumn('users', 'groups', Sequelize.JSON)
        .then(resolve)
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.removeColumn('users', 'groups')
        .then(resolve)
        .catch(reject);
    });
  }
};
