'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("UPDATE pmr_groups_divisions SET l3_manager = 'John Ludecke' WHERE lower(l3_manager) = lower('John Ludeke')").then(function() {
      return queryInterface.sequelize.query("REFRESH MATERIALIZED VIEW pmr_groups_divisions_l2_manager");
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("UPDATE pmr_groups_divisions SET l3_manager = 'John Ludeke' WHERE lower(l3_manager) = lower('John Ludecke')").then(function() {
      return queryInterface.sequelize.query("REFRESH MATERIALIZED VIEW pmr_groups_divisions_l2_manager");
    });
  }
};