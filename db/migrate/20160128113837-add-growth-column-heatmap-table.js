'use strict';
var Promise = require('bluebird');
var tableName = 'program_heatmap_manual_entry';
var column1 = 'growth';
var column2 = 'growth_notes';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.addColumn(tableName, column1, Sequelize.STRING)
        .then(function () {
          queryInterface.addColumn(tableName, column2, Sequelize.TEXT)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.removeColumn(tableName, column1)
        .then(function () {
          queryInterface.removeColumn(tableName, column2)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  }
};
