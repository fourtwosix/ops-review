'use strict';
var Promise = require('bluebird');
var _ = require('lodash');
var contractTypes = [
  'FFP',
  'CPFF',
  'T&M',
  'CP',
  'Labor Hours',
  'CPAF',
  'CPIF',
  'FUP',
  'FFP LOE',
  'T&M LOE',
  'FUP LOE',
  'Cost Only',
  'CPFF LOE'
];

module.exports = {
  up: function (queryInterface, Sequelize) {
    var now = new Date().toUTCString();
    var values = _.trimRight(_.reduce(contractTypes, function(result, type) {
      return result + "('" + type + "','pmrContractType','" + now + "','" + now +"'), ";
    }, ""), ", ");

    return new Promise(function(resolve, reject) {
      queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES " + values)
        .then(resolve)
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      var deletes = [];

      _.each(contractTypes, function(type) {
        deletes.push(queryInterface.sequelize.query("DELETE FROM static_display_data WHERE type = 'pmrContractType' AND display_value = '" + type + "'"));
      });

      Promise.all(deletes)
        .then(resolve)
        .catch(reject);
    })
  }
};
