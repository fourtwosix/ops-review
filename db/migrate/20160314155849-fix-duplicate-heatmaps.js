'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("DELETE FROM program_heatmap_manual_entry WHERE id NOT IN (SELECT DISTINCT ON (program_id, fiscal_month_end) id FROM program_heatmap_manual_entry ORDER BY program_id, fiscal_month_end, updated_at DESC);").then(function() {
      return queryInterface.sequelize.query("CREATE UNIQUE INDEX program_heatmap_manual_entry_unique_program_id_fiscal_month_end ON program_heatmap_manual_entry USING btree (program_id, fiscal_month_end DESC)").then(function() {
          return queryInterface.removeIndex('program_heatmap_manual_entry', 'program_heatmap_manual_entry_program_id_fiscal_month_end_index');
        });
      });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("CREATE INDEX program_heatmap_manual_entry_program_id_fiscal_month_end_index ON program_heatmap_manual_entry USING btree (program_id, fiscal_month_end DESC)").then(function() {
      return queryInterface.removeIndex('program_heatmap_manual_entry', 'program_heatmap_manual_entry_unique_program_id_fiscal_month_end');
    });
  }
};
