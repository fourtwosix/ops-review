'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'program_heatmap_manual_entry',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          program_id: {
            type: Sequelize.INTEGER,
            references: 'programs',
            referencesKey: 'id',
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT'
          },
          unique_id: {
            type: Sequelize.STRING,
            unique: true
          },
          fiscal_month_end: Sequelize.DATE,
          contractual: Sequelize.STRING,
          technical: Sequelize.STRING,
          cpi: Sequelize.DECIMAL(14,2),
          cost: Sequelize.STRING,
          spi: Sequelize.DECIMAL(14,2),
          schedule: Sequelize.STRING,
          bid_margin_percentage: Sequelize.DECIMAL(14,1),
          eac_margin_percentage: Sequelize.DECIMAL(14,1),
          fyoi_percentage: Sequelize.DECIMAL(14,1),
          customer_satisfaction: Sequelize.STRING,
          staffing_percentage: Sequelize.DECIMAL(14,1),
          supplier_status: Sequelize.STRING,
          offerings: Sequelize.STRING,
          acquisition_stability: Sequelize.STRING,
          overall_project: Sequelize.STRING,
          notes: Sequelize.STRING,
          legacy_created_date: Sequelize.DATE,
          created_by: Sequelize.STRING,
          updated_by: Sequelize.STRING,
          created_at: Sequelize.DATE,
          updated_at: Sequelize.DATE
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE program_heatmap_manual_entry OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable("program_heatmap_manual_entry")
        .then(resolve)
        .catch(reject);
    });
  }
};
