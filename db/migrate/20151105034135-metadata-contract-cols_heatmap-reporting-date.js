'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface, Sequelize) {
    var addCols = [
      ['program_manager', Sequelize.STRING],
      ['l4_manager', Sequelize.STRING],
      ['l4_portfolio', Sequelize.STRING],
      ['division', Sequelize.STRING],
      ['contract_type', Sequelize.STRING],
      ['total_contract_value_potential', Sequelize.DECIMAL(14, 2)],
      ['total_contract_value_awarded', Sequelize.DECIMAL(14, 2)],
      ['funded_value', Sequelize.DECIMAL(14, 2)],
      ['start_date', Sequelize.DATE],
      ['end_date', Sequelize.DATE],
      ['lcdb_required', Sequelize.BOOLEAN]
    ];
    return new Promise(function (resolve, reject) {
      queryInterface.addColumn('program_heatmap_manual_entry', 'legacy_reporting_date', Sequelize.DATE).then(function () {
        async.each(addCols, function (col, done) {
          queryInterface.addColumn('program_metadata_manual_entry', col[0], col[1])
            .then(function () {
              done();
            }).catch(done);
        }, function (err) {
          err ? reject(err) : resolve();
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    var addCols = [
      ['program_manager', Sequelize.STRING],
      ['l4_manager', Sequelize.STRING],
      ['l4_portfolio', Sequelize.STRING],
      ['division', Sequelize.STRING],
      ['contract_type', Sequelize.STRING],
      ['total_contract_value_potential', Sequelize.DECIMAL(14, 2)],
      ['total_contract_value_awarded', Sequelize.DECIMAL(14, 2)],
      ['funded_value', Sequelize.DECIMAL(14, 2)],
      ['start_date', Sequelize.DATE],
      ['end_date', Sequelize.DATE],
      ['lcdb_required', Sequelize.BOOLEAN]
    ];

    return new Promise(function (resolve, reject) {
      queryInterface.removeColumn('program_heatmap_manual_entry', 'legacy_reporting_date').then(function () {
        async.each(addCols, function (col, done) {
          queryInterface.removeColumn('program_metadata_manual_entry', col[0])
            .then(function () {
              done();
            }).catch(done);
        }, function (err) {
          err ? reject(err) : resolve();
        });
      });
    });
  }
};
