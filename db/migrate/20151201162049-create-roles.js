'use strict';
var Promise = require('bluebird');
var async = require('async');
var tableName = 'roles';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function(done) {
          queryInterface.createTable(
            tableName,
            {
              name: {
                type: Sequelize.STRING,
                primaryKey: true
              },
              notes: Sequelize.STRING,
              created_at: Sequelize.DATE,
              updated_at: Sequelize.DATE
            })
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        },
        function(done) {
          queryInterface.sequelize.query("ALTER TABLE " + tableName + " OWNER TO insighter;")
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        },
        function(done) {
          var roles = ['Program Manager', 'Administrator', 'Executive'];
          async.each(roles, function(role, done) {
            var now = new Date().toUTCString();
            queryInterface.sequelize.query("INSERT INTO " + tableName + " (name, created_at, updated_at) values ('" + role + "','" + now + "', '" + now + "')")
              .then(function() {
              done();
            }).catch(function(err) {
              done(err);
            })
          }, function(err) {
            done(err);
          });
        }
      ], function (err) {
        err ? reject(err): resolve();
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable(tableName)
        .then(resolve)
        .catch(reject);
    });
  }
};
