'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'program_offering_association',
        {
          program_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: 'programs',
            referencesKey: 'id',
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT'
          },
          offering_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: 'offerings',
            referencesKey: 'id',
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT'
          },
          type: Sequelize.STRING,
          created_at: Sequelize.DATE,
          updated_at: Sequelize.DATE
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE program_offering_association OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable("program_offering_association")
        .then(resolve)
        .catch(reject);
    });
  }
};
