'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.sequelize.query(
        "UPDATE program_metadata_manual_entry " +
        "   SET \"group\" = '', division = '', primary_offering = ''")
      .then(resolve)
      .catch(reject);
    });
  },
  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.sequelize.query(
        "UPDATE program_metadata_manual_entry " +
        "   SET \"group\" = 'unknown', division = 'unknown', primary_offering = 'unknown'")
      .then(resolve)
      .catch(reject);
    });
  }
};
