'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("CREATE MATERIALIZED VIEW resource_management_with_accepted AS " +
        "SELECT resource_management_data.*, (SELECT EXISTS(SELECT requisition_number FROM candidate_data " +
        "WHERE application_cws_status = 'onboarding process initiated - waiting on new hire to start' " +
        "AND requisition_number = resource_management_data.requisition_number)) AS has_accepted " +
        "FROM resource_management_data;")
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE resource_management_with_accepted OWNER TO insighter;")
            .then(resolve)
            .catch(reject);

        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS resource_management_with_accepted RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
