'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS billable_productive_data").then(function(){
        queryInterface.sequelize.query("CREATE MATERIALIZED VIEW billable_productive_data AS " +
          "SELECT labor_utilization_data.wtd_adjusted_capacity, " +
          "labor_utilization_data.wtd_billable, " +
          "( COALESCE( labor_utilization_data.wtd_billable, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.wtd_unbillable, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.wtd_bid_proposal, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.wtd_odo, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.wtd_internal_cost_recovery, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.wtd_service_center, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.wtd_investments, 0.0 ) " +
          ") AS wtd_productive, " +
          "labor_utilization_data.mtd_adjusted_capacity, " +
          "labor_utilization_data.mtd_billable, " +
          "( COALESCE( labor_utilization_data.mtd_billable, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.mtd_unbillable, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.mtd_bid_proposal, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.mtd_odo, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.mtd_internal_cost_recovery, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.mtd_service_center, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.mtd_investments, 0.0 ) " +
          ") AS mtd_productive, " +
          "labor_utilization_data.ytd_adjusted_capacity, " +
          "labor_utilization_data.ytd_billable, " +
          "( COALESCE( labor_utilization_data.ytd_billable, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_unbillable, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_bid_proposal, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_odo, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_internal_cost_recovery, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_service_center, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_investments, 0.0 ) " +
          ") AS ytd_productive, " +
          "(  COALESCE( labor_utilization_data.ytd_billable, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_unbillable, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_bid_proposal, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_odo, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_internal_cost_recovery, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_general_overhead, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_service_center, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_available_bench, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_training, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_unallowable_indirect, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_holiday, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_vacation, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_sick_leave, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_emergency, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_lwop, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_other_leave, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_suspense, 0.0 ) " +
          "+ COALESCE( labor_utilization_data.ytd_investments, 0.0 ) " +
          ") AS ytd_total, " +
          "labor_utilization_data.full_employee_name, " +
          "labor_utilization_data.week_ending_date, " +
          "permast_data.layer_2_manager, " +
          "permast_data.layer_3_manager, " +
          "permast_data.layer_4_manager, " +
          "permast_data.layer_5_manager, " +
          "permast_data.layer_6_manager, " +
          "permast_data.layer_7_manager, " +
          "CASE WHEN ((permast_data.job_description)::TEXT ~ '(vp|mgr|dir|supv)'::TEXT) THEN 'true'::TEXT " +
          "ELSE 'false'::TEXT " +
          "END AS is_manager " +
          "FROM (labor_utilization_data " +
          "JOIN permast_data ON (((((permast_data.short_name)::TEXT = (labor_utilization_data.short_name)::TEXT) " +
          "AND ((date_trunc('month'::TEXT, permast_data.report_date))::date = (date_trunc('month'::TEXT, labor_utilization_data.week_ending_date))::date)) " +
          "AND (((date_trunc('day'::TEXT, permast_data.report_date))::date >= ((date_trunc('week'::TEXT, labor_utilization_data.week_ending_date) - '1 day'::INTERVAL))::date) " +
          "AND ((date_trunc('day'::TEXT, permast_data.report_date))::date <= ((date_trunc('week'::TEXT, labor_utilization_data.week_ending_date) + '5 days'::INTERVAL))::date)))));"
        )
          .then(resolve)
          .catch(reject);
      }).catch(reject);
    });
  }
};


