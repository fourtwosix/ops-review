'use strict';
var Promise = require('bluebird');
var async = require('async');



module.exports = {
  up: function (queryInterface, Sequelize) {
    var addCols = [
      ['contractual_notes', Sequelize.TEXT],
      ['technical_notes', Sequelize.TEXT],
      ['cost_notes', Sequelize.TEXT],
      ['schedule_notes', Sequelize.TEXT],
      ['customer_satisfaction_notes', Sequelize.TEXT],
      ['staffing_notes', Sequelize.TEXT],
      ['supplier_status_notes', Sequelize.TEXT],
      ['offerings_notes', Sequelize.TEXT],
      ['acquisition_stability_notes', Sequelize.TEXT],
      ['overall_project_notes', Sequelize.TEXT],
      ['validated', Sequelize.BOOLEAN]
    ];
    return new Promise(function (resolve, reject) {
      queryInterface.removeColumn('program_heatmap_manual_entry', 'notes').then(function () {
        queryInterface.removeColumn('program_heatmap_manual_entry', 'created_by').then(function () {
          queryInterface.removeColumn('program_heatmap_manual_entry', 'updated_by').then(function () {
            queryInterface.addColumn('program_heatmap_manual_entry', 'created_by', {
              type: Sequelize.INTEGER,
              references: 'users',
              referencesKey: 'id',
              onUpdate: 'CASCADE',
              onDelete: 'RESTRICT'
            }).then(function () {
              queryInterface.addColumn('program_heatmap_manual_entry', 'updated_by', {
                type: Sequelize.INTEGER,
                references: 'users',
                referencesKey: 'id',
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT'
              }).then(function () {
                queryInterface.addColumn('program_heatmap_manual_entry', 'legacy_created_by', Sequelize.STRING)
                  .then(function () {
                    async.each(addCols, function (col, done) {
                      queryInterface.addColumn('program_heatmap_manual_entry', col[0], col[1])
                        .then(function () {
                          done();
                        }).catch(done);
                    }, function (err) {
                      err ? reject(err) : resolve();
                    });
                  });
              });
            });
          });
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    var addCols = [
      ['contractual_notes', Sequelize.TEXT],
      ['technical_notes', Sequelize.TEXT],
      ['cost_notes', Sequelize.TEXT],
      ['schedule_notes', Sequelize.TEXT],
      ['customer_satisfaction_notes', Sequelize.TEXT],
      ['staffing_notes', Sequelize.TEXT],
      ['supplier_status_notes', Sequelize.TEXT],
      ['offerings_notes', Sequelize.TEXT],
      ['acquisition_stability_notes', Sequelize.TEXT],
      ['overall_project_notes', Sequelize.TEXT],
      ['validated', Sequelize.BOOLEAN]
    ];
    return new Promise(function (resolve, reject) {
      queryInterface.addColumn('program_heatmap_manual_entry', 'notes', Sequelize.TEXT).then(function () {
        queryInterface.removeColumn('program_heatmap_manual_entry', 'created_by').then(function () {
          queryInterface.removeColumn('program_heatmap_manual_entry', 'updated_by').then(function () {
            queryInterface.addColumn('program_heatmap_manual_entry', 'created_by', Sequelize.STRING).then(function () {
              queryInterface.addColumn('program_heatmap_manual_entry', 'updated_by', Sequelize.STRING).then(function () {
                queryInterface.removeColumn('program_heatmap_manual_entry', 'legacy_created_by')
                  .then(function () {
                    async.each(addCols, function (col, done) {
                      queryInterface.removeColumn('program_heatmap_manual_entry', col[0])
                        .then(function () {
                          done();
                        }).catch(done);
                    }, function (err) {
                      err ? reject(err) : resolve();
                    });
                  });
              });
            });
          });
        });
      });
    });
  }
};
