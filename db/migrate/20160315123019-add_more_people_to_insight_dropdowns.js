'use strict';
var Promise = require('bluebird');
var _ = require('lodash');
var now = 'to_timestamp(' + new Date().getTime() / 1000 + ')';

var newNames = [
  "John Cordone",
  "Patrick Nino"
];

module.exports = {
  up: function (queryInterface, Sequelize) {

    var values = _.trimRight(_.reduce(newNames, function(result, name) {
      return result + "('" + name + "','pmrName'," + now + "," + now +"), ";
    }, ""), ", ")

    return queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES "+ values)
  },


  down: function (queryInterface, Sequelize) {
  return new Promise(function(resolve, reject) {
    var deletes = [];

    _.each(newNames, function(newName) {
      deletes.push(queryInterface.sequelize.query("DELETE from static_display_data where display_value = '" + newName + "'"));
    });

    Promise.all(deletes)
      .then(resolve)
      .catch(reject);
  });
}

};
