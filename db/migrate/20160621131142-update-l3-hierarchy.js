'use strict';
var Promise = require('bluebird');
var now = new Date().toUTCString();

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET division = 'Health and Human Services', l3_manager = 'Kamal Narang' WHERE (division = 'CDC' OR division = 'CMS') AND 'group' = 'Health & Civil'").then(function() {
      return queryInterface.sequelize.query("DELETE FROM pmr_divisions WHERE division_name = 'CDC' OR division_name = 'CMS'");
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("INSERT INTO pmr_divisions (division_name, created_at, updated_at) VALUES ('CDC', '" + now + "', '" + now + "')").then(function() {
      return queryInterface.sequelize.query("INSERT INTO pmr_divisions (division_name, created_at, updated_at) VALUES ('CMS', '" + now + "', '" + now + "')").then(function() {
        return queryInterface.sequelize.query("INSERT INTO pmr_groups_divisions (group_name, division_name, l3_manager, created_at, updated_at) VALUES ('Health & Civil', 'CDC', 'David Cox', '" + now + "', '" + now + "')").then(function() {
          return queryInterface.sequelize.query("INSERT INTO pmr_groups_divisions (group_name, division_name, l3_manager, created_at, updated_at) VALUES ('Health & Civil', 'CMS', 'Doug Kelly', '" + now + "', '" + now + "')");
        });
      });
    });
  }
};