'use strict';

// inverse of 20150915162009-program-info-data.js
// table is OBE; ticket #pmr-434

var Promise = require('bluebird');

module.exports = {
  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'program_info_data',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          unique_id: {
            type: Sequelize.STRING,
            unique: true
          },
          program_name: Sequelize.STRING,
          project_id: Sequelize.STRING,
          program_manager: Sequelize.STRING,
          l3_manager: Sequelize.STRING,
          primary_offering: Sequelize.STRING,
          l4_manager: Sequelize.STRING,
          l4_portfolio: Sequelize.STRING,
          l5_manager: Sequelize.STRING,
          l5_los: Sequelize.STRING,
          pra: Sequelize.STRING,
          industry_gm: Sequelize.STRING,
          account_gm: Sequelize.STRING,
          group: Sequelize.STRING,
          division: Sequelize.STRING,
          other_offerings: Sequelize.STRING,
          pmr_frequency: Sequelize.STRING,
          contract_type: Sequelize.STRING,
          tcv_potential: Sequelize.INTEGER,
          tcv_awarded: Sequelize.INTEGER,
          funded_value: Sequelize.INTEGER,
          start_date: Sequelize.DATE,
          end_date: Sequelize.DATE,
          pra_: Sequelize.STRING,
          program_status: Sequelize.STRING,
          pmr_status: Sequelize.STRING,
          lcdb_required: Sequelize.BOOLEAN,
          item_type: Sequelize.STRING,
          path: Sequelize.STRING,
          report_date: Sequelize.DATE,
          created_at: Sequelize.DATE,
          updated_at: Sequelize.DATE
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE program_info_data OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  up: function (queryInterface) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable("program_info_data")
        .then(resolve)
        .catch(reject);
    });
  }
};
