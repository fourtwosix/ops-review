'use strict';
var Promise = require('bluebird');
var now = new Date().toUTCString();
var _ = require('lodash');

var columns = [
  "program_manager",
  "pra",
  "l3_manager",
  "account_gm",
  "los_manager",
  "industry_gm",
  "l2_manager"
];

var updates = [
  {
    type: 'pmrName',
    new: "John Xereas",
    old: "Xereas John"
  },
  {
    type: 'pmrName',
    new: "Edmund Olsen",
    old: "Olsen Edmund"
  }
];

module.exports = {
  up: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.new + "', updated_at = '" + now + "' WHERE " + column + " = '" + update.old + "'");
      })
    }).then(function () {
      var values = _.trimRight(_.reduce(updates, function (result, update) {
        return result + "('" + update.new + "','" + update.type + "','" + now + "','" + now + "'), ";
      }, ""), ", ");

      return queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES " + values).then(function() {
        return Promise.each(updates, function(update) {
          return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE id NOT IN (SELECT DISTINCT ON (type, display_value) id FROM static_display_data ORDER BY type, display_value, updated_at DESC) OR display_value = '" + update.old + "'");
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.old + "', updated_at = '" + now + "' WHERE " + column + " = '" + update.new + "'");
      })
    }).then(function () {
      var values = _.trimRight(_.reduce(updates, function (result, update) {
        return result + "('" + update.old + "','" + update.type + "','" + now + "','" + now + "'), ";
      }, ""), ", ");

      return queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES " + values).then(function() {
        return Promise.each(updates, function(update) {
          return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE id NOT IN (SELECT DISTINCT ON (type, display_value) id FROM static_display_data ORDER BY type, display_value, updated_at DESC) OR display_value = '" + update.new + "'");
        });
      });
    });
  }
};
