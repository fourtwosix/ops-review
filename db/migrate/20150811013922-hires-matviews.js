'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("CREATE MATERIALIZED VIEW layer_" + layer + "_manager_hires AS SELECT layer_" + layer + "_manager, date_trunc('month', hire_date)::DATE AS MONTH, count(DISTINCT personnel_number) AS hires FROM permast_data GROUP BY layer_" + layer + "_manager, MONTH")
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });

    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS layer_" + layer + "_manager_hires RESTRICT;")
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
