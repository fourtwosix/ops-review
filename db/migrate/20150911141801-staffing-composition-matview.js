'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("CREATE MATERIALIZED VIEW staffing_composition AS (SELECT costpoint_data.project_id, date_trunc('month', weekend_date) AS month, COALESCE(SUM(costpoint_data.total_hours), 0) / (40 * CASE (COUNT(DISTINCT (date_trunc('week', weekend_date) + '4 days'::interval))) WHEN 0 THEN 1 ELSE (COUNT(DISTINCT (date_trunc('week', weekend_date) + '4 days'::interval))) END) AS hours, 'CSC' AS \"resource\",'csc' AS \"type\", (SELECT l4_portfolio AS los FROM contract_data WHERE costpoint_data.project_id = contract_data.project_num LIMIT 1) FROM costpoint_data GROUP BY costpoint_data.project_id, month UNION ALL SELECT steps_data.project AS project_id, date_trunc('month', weekend_date) AS month, COALESCE(SUM(steps_data.total_hours), 0) / (40 * CASE (COUNT(DISTINCT (date_trunc('week', weekend_date) + '4 days'::interval))) WHEN 0 THEN 1 ELSE (COUNT(DISTINCT (date_trunc('week', weekend_date) + '4 days'::interval))) END) AS hours, vendor_name AS resource, 'sub' AS \"type\", (SELECT l4_portfolio AS los FROM contract_data WHERE steps_data.project = contract_data.project_num LIMIT 1) FROM steps_data GROUP BY project_id, month, resource);")
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE staffing_composition OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS staffing_composition RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};

