'use strict';
var Promise = require('bluebird');
var tableName = 'program_metadata_manual_entry';
var columnName = 'l2_manager';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.addColumn(tableName, columnName, Sequelize.STRING)
        .then(resolve)
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.removeColumn(tableName, columnName)
        .then(resolve)
        .catch(reject);
    });
  }
};
