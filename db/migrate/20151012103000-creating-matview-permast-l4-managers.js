/**
 * Created by cedam on 10/12/15.
 */

'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("CREATE MATERIALIZED VIEW permast_l4_managers AS " +
      "select distinct layer_4_manager, layer_4_org_name from permast_data;")
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE permast_l4_managers OWNER TO insighter;")
            .then(resolve)
            .catch(reject);

        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_l4_managers RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
