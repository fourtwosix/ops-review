'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function(t) {
      return queryInterface.removeColumn('program_metadata_manual_entry', 'target_revenue', {transaction: t}).then(function() {
        return queryInterface.removeColumn('program_metadata_manual_entry', 'target_profit', {transaction: t});
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function(t) {
      return queryInterface.addColumn('program_metadata_manual_entry', 'target_revenue', Sequelize.BIGINT, {transaction: t}).then(function() {
        return queryInterface.addColumn('program_metadata_manual_entry', 'target_profit', Sequelize.BIGINT, {transaction: t});
      });
    });
  }
};
