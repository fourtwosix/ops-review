'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'offerings',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          parent_offering_id: {
            type: Sequelize.INTEGER,
            references: 'offerings',
            referencesKey: 'id',
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT'
          },
          unique_id: {
            type: Sequelize.STRING,
            unique: true
          },
          offering_name: Sequelize.STRING,
          los_name: Sequelize.STRING,
          offering_code: Sequelize.STRING,
          offering_description: Sequelize.STRING,
          created_at: Sequelize.DATE,
          updated_at: Sequelize.DATE
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE offerings OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable("offerings")
        .then(resolve)
        .catch(reject);
    });
  }
};
