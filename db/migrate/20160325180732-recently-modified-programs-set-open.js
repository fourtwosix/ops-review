'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET program_status = 'open' WHERE id IN (SELECT m.id FROM program_heatmap_manual_entry h INNER JOIN program_heatmap_manual_entry m ON m.program_id = h.program_id WHERE h.fiscal_month_end >= to_date('2016-01-01', 'YYYY-MM-DD'));");
  },

  down: function (queryInterface, Sequelize) {
    return Promise.resolve();
  }
};
