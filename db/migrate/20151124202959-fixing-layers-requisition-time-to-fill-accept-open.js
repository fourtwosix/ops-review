'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS requisition_time_to_fill_accept_open RESTRICT;")
      .then(function () {
        queryInterface.sequelize.query("CREATE MATERIALIZED VIEW requisition_time_to_fill_accept_open AS " +
        "SELECT * FROM (WITH rm_data AS (SELECT requisition_number, req_first_fully_approved_date, " +
        "req_current_parent_status, req_latest_filled_date, job_offer_accepted_date, req_days_open, " +
        "report_date, req_type, date_part(\'day\', req_latest_filled_date - req_first_fully_approved_date) AS time_to_fill, " +
        "date_part(\'day\', req_latest_filled_date - job_offer_accepted_date) AS time_to_accept, layer_2_manager, layer_3_manager, " +
        "layer_4_manager, layer_5_manager, layer_6_manager, layer_7_manager, layer_8_manager FROM resource_management_data " +
        "WHERE report_date = (SELECT MAX(report_date) FROM resource_management_data)) SELECT layer_2_manager, layer_3_manager, " +
        "layer_4_manager, layer_5_manager, layer_6_manager, layer_7_manager, layer_8_manager, req_type, requisition_number, " +
        "time_to_fill as time, \'fill\' as type, (date_trunc(\'week\', req_latest_filled_date) + \'4 days\'::interval)::DATE AS week_ending " +
        "FROM rm_data UNION ALL SELECT layer_2_manager, layer_3_manager, layer_4_manager, layer_5_manager, layer_6_manager, " +
        "layer_7_manager, layer_8_manager, req_type, requisition_number, time_to_accept as time, \'accept\' as type, " +
        "(date_trunc(\'week\', job_offer_accepted_date) + \'4 days\'::interval)::DATE AS week_ending FROM rm_data " +
        "UNION ALL SELECT layer_2_manager, layer_3_manager, layer_4_manager, layer_5_manager, layer_6_manager, layer_7_manager, " +
        "layer_8_manager, req_type, requisition_number, req_days_open as time, \'open\' as type, " +
        "(date_trunc(\'week\', report_date) + \'4 days\'::interval)::DATE AS week_ending FROM resource_management_data " +
        "WHERE req_current_parent_status IN (\'open\', \'approved\', \'sourcing\')) AS m")
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE requisition_time_to_fill_accept_open OWNER TO insighter;")
          .then(function () {
            async.each(['layer_2_manager', 'layer_3_manager', 'layer_4_manager', 'layer_5_manager',
              'layer_6_manager', 'layer_7_manager', 'layer_8_manager', 'req_type', 'week_ending'], function (column, done) {
              queryInterface.addIndex('requisition_time_to_fill_accept_open', [column]).then(function () {done();}).catch(done);
            }, function (err) {
              err ? reject(err): resolve();
            });
          })
          .catch(reject);
        })
        .catch(reject);
      })
      .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS requisition_time_to_fill_accept_open RESTRICT;")
      .then(resolve)
      .catch(reject);
    });
  }
};
