'use strict';
var Promise = require('bluebird');
var removeConstraints = [
  "ALTER TABLE program_heatmap_manual_entry DROP CONSTRAINT program_heatmap_manual_entry_created_by_fkey",
  "ALTER TABLE program_heatmap_manual_entry DROP CONSTRAINT program_heatmap_manual_entry_updated_by_fkey",
  "ALTER TABLE program_metadata_manual_entry DROP CONSTRAINT program_metadata_manual_entry_created_by_fkey",
  "ALTER TABLE program_metadata_manual_entry DROP CONSTRAINT program_metadata_manual_entry_updated_by_fkey"
];

var addConstraints = [
  "ALTER TABLE program_heatmap_manual_entry ADD CONSTRAINT program_heatmap_manual_entry_created_by_fkey FOREIGN KEY (created_by) REFERENCES users (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE ",
  "ALTER TABLE program_heatmap_manual_entry ADD CONSTRAINT program_heatmap_manual_entry_updated_by_fkey FOREIGN KEY (updated_by) REFERENCES users (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE ",
  "ALTER TABLE program_metadata_manual_entry ADD CONSTRAINT program_metadata_manual_entry_created_by_fkey FOREIGN KEY (created_by) REFERENCES users (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE ",
  "ALTER TABLE program_metadata_manual_entry ADD CONSTRAINT program_metadata_manual_entry_updated_by_fkey FOREIGN KEY (updated_by) REFERENCES users (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE "
];

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      Promise.each(removeConstraints, function(query) {
        queryInterface.sequelize.query(query);
      }).then(function() {
        Promise.each(addConstraints, function(query) {
          queryInterface.sequelize.query(query + "SET NULL");
        })
          .then(resolve)
          .catch(reject);
      })
    })
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      Promise.each(removeConstraints, function(query) {
        queryInterface.sequelize.query(query);
      }).then(function() {
        Promise.each(addConstraints, function(query) {
          queryInterface.sequelize.query(query + "RESTRICT");
        })
          .then(resolve)
          .catch(reject);
      })
    })
  }
};
