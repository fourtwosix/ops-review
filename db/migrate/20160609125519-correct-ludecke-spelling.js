'use strict';
var Promise = require('bluebird');
var now = new Date().toUTCString();
var _ = require('lodash');

var columns = [
  "program_manager",
  "pra",
  "l3_manager",
  "l2_manager"
];

var updates = [
  {
    type: 'pmrName',
    new: "John Ludecke",
    old: "John Ludeke"
  }
];

module.exports = {
  up: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.new + "', updated_at = '" + now + "' WHERE lower(" + column + ") = lower('" + update.old + "')");
      })
    });
  },

  down: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.old + "', updated_at = '" + now + "' WHERE lower(" + column + ") = lower('" + update.new + "')");
      })
    });
  }
};