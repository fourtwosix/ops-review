'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function (done) {
          console.log("Dropping Materialized View If Exists");
          queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_layer_4_org_names RESTRICT;")
            .then(function() {
              done();
            }).catch(function(err) {
              done(err);
            });
        },
        function (done) {
          console.log("Creating Materialized View");
          queryInterface.sequelize.query("CREATE MATERIALIZED VIEW permast_layer_3_org_names AS " +
          "SELECT DISTINCT layer_3_org_name FROM permast_data where layer_3_org_name is not null;")
            .then(function() {
              done();
            }).catch(function(err) {
              done(err);
            });
        },
        function (done) {
          console.log("Altering Table");
          queryInterface.sequelize.query("ALTER TABLE permast_layer_3_org_names OWNER TO insighter;")
            .then(function() {
              done();
            }).catch(function(err) {
              done(err);
            });
        }
      ], function (err) {
        err ? reject(err): resolve();
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function (done) {
          console.log("Dropping Materialized View If Exists");
          queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_layer_3_org_names RESTRICT;")
            .then(function() {
              done();
            }).catch(function(err) {
              done(err);
            });
        },
        function (done) {
          console.log("Creating Materialized View");
          queryInterface.sequelize.query("CREATE MATERIALIZED VIEW permast_layer_4_org_names AS " +
          "SELECT DISTINCT layer_4_org_name FROM permast_data;")
            .then(function() {
              done();
            }).catch(function(err) {
              done(err);
            });
        },
        function (done) {
          console.log("Altering Table");
          queryInterface.sequelize.query("ALTER TABLE permast_layer_4_org_names OWNER TO insighter;")
            .then(function() {
              done();
            }).catch(function(err) {
              done(err);
            });
        }
      ], function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
