'use strict';

var Promise = require('bluebird');
var _ = require('lodash');

var staticDataType = 'pmrDivision';

var oldDivisions = [
  '42Six',
  'ADI-R',
  'AF/MD',
  'Applied Technology',
  'Army & Defense Intel',
  'BITT',
  'CS - S&L',
  'CS',
  'DHS EO',
  'DHS IIP',
  'Eagle Alliance',
  'FDIC',
  'FedHlth',
  'Foreign Affairs',
  'GWAC',
  'ICS',
  'ITIP',
  'Justice/LE',
  'Missions Systems',
  'National Security',
  'Naval & Joint Ops',
  'NPS Internal',
  'SHD',
  'Technology/Industry SMEs',
  'Training',
  'VA/Military Health',
  'VIS',
  'Welkin'
];


var divisions = [
  'LPO/WPO',
  'MPO',
  'NGA/DIA',
  'ODNI',
  'Groundbreaker',
  'Financial Agencies',
  'Government Operations',
  'Foreign Affairs',
  'Science & Engineering',
  'Department of Transportation',
  'FDIC',
  'Veterans Administration',
  'Health and Human Services',
  'State and Local Health',
  'Military Health',
  'Naval Warfare & Systems Center',
  'Maritime Systems',
  'US Airforce & Joint Mission Systems',
  'Army Advanced Electronics & Ground',
  'Army Aviation and Missile Defense',
  'Warfighter Focus',
  'Flight School XXI',
  'Air Force',
  'Joint Commands & Defense Agencies',
  'Navy',
  'Army',
  'Logistics Modernization Program',
  'Integrated Personnel and Pay System - Navy',
  'DHS Division 1',
  'DHS Division 2',
  'Law Enforcement Division',
  'TSA/ITIP',
  'DC1/Stennis'
];

var now = new Date().toUTCString();

module.exports = {
  
  up: function (queryInterface, Sequelize) {
    var values = _.trimRight(_.reduce(divisions, function(result, division) {
      return result + "('" + division + "','" + staticDataType + "','" + now + "','" + now +"'), ";
    }, ""), ", ");
    return new Promise(function(resolve, reject) {
      queryInterface.sequelize.query(
        "DELETE FROM static_display_data " +
        "WHERE type = '" + staticDataType + "'"
      )
      .then( 
        function(resolve, reject) {
          queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES " + values)
        }
      )
      .then(resolve)
      .catch(reject);
    });
  },

  // NOTE: down identical to up with exception of oldDivisions used for inserts
  down: function (queryInterface, Sequelize) {
    var values = _.trimRight(_.reduce(oldDivisions, function(result, division) {
      return result + "('" + division + "','" + staticDataType + "','" + now + "','" + now +"'), ";
    }, ""), ", ");
    return new Promise(function(resolve, reject) {
      queryInterface.sequelize.query(
        "DELETE FROM static_display_data " +
        "WHERE type = '" + staticDataType + "'"
      )
      .then( 
        function(resolve, reject) {
          queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES " + values)
        }
      )
      .then(resolve)
      .catch(reject);
    });
  }
};
