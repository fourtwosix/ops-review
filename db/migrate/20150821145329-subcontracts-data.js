'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'subcontracts_data',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          report_date: {
            type: Sequelize.DATE
          },
          unique_id: {
            type: Sequelize.STRING,
            unique: true
          },
          total: {
            type: Sequelize.STRING
          },
          top_level_subcontract_num: {
            type: Sequelize.STRING
          },
          lower_level_order_num: {
            type: Sequelize.STRING
          },
          subcontractor_name: {
            type: Sequelize.STRING
          },
          size: {
            type: Sequelize.STRING
          },
          start_date: {
            type: Sequelize.DATE
          },
          end_date: {
            type: Sequelize.DATE
          },
          status: {
            type: Sequelize.STRING
          },
          ceiling_value: {
            type: Sequelize.FLOAT
          },
          value: {
            type: Sequelize.FLOAT
          },
          current_funding: {
            type: Sequelize.FLOAT
          },
          competition_type: {
            type: Sequelize.STRING
          },
          csc_project_num: {
            type: Sequelize.STRING
          },
          csc_contract_num: {
            type: Sequelize.STRING
          },
          subcontract_admin: {
            type: Sequelize.STRING
          },
          program_manager: {
            type: Sequelize.STRING
          },
          audit_file_received_by_compliance: {
            type: Sequelize.BOOLEAN
          },
          date_letter_subcontract_executed: {
            type: Sequelize.DATE
          },
          date_letter_subcontract_to_be_definitized: {
            type: Sequelize.DATE
          },
          created_at: {
            type: Sequelize.DATE
          },
          updated_at: {
            type: Sequelize.DATE
          }
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE subcontracts_data OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable("subcontracts_data")
        .then(resolve)
        .catch(reject);
    });
  }
};
