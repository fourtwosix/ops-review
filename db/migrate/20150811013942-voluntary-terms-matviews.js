'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("CREATE MATERIALIZED VIEW layer_" + layer + "_manager_voluntary_terms AS SELECT layer_" + layer + "_manager, date_trunc('month', termination_date)::DATE AS MONTH, count(DISTINCT personnel_number) AS voluntary_terminations FROM permast_data WHERE termination_type ~ '^(separation voluntary|us-termination voluntary)' GROUP BY layer_" + layer + "_manager, MONTH")
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });

    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS layer_" + layer + "_manager_voluntary_terms RESTRICT;")
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
