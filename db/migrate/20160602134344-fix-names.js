'use strict';
var Promise = require('bluebird');
var now = new Date().toUTCString();
var _ = require('lodash');

var columns = [
  "program_manager",
  "pra",
  "l3_manager",
  "l2_manager"
];

var updates = [
  {"old_name": "Brenda Jennings-Mcmahon", "new_name": "Brenda Jennings-McMahon"},
  {"old_name": "adam mazhar", "new_name": "Adam Mazhar"},
  {"old_name": "anthony t colyandro", "new_name": "Anthony Colyandro"},
  {"old_name": "anthony lisotta", "new_name": "Anthony Lisotta"},
  {"old_name": "barrie s smith", "new_name": "Barrie Smith"},
  {"old_name": "Barrier Smith", "new_name": "Barrie Smith"},
  {"old_name": "Jennings-Mcmahon Brenda", "new_name": "Brenda Jennings-McMahon"},
  {"old_name": "Carlyn Mccune", "new_name": "Carlyn McCune"},
  {"old_name": "david m tatum", "new_name": "David Tatum"},
  {"old_name": "deborah l miller", "new_name": "Deborah Miller"},
  {"old_name": "doug stephon", "new_name": "Doug Stephon"},
  {"old_name": "gary m calabrese", "new_name": "Gary Calabrese"},
  {"old_name": "juan holguin", "new_name": "Juan Holguin"},
  {"old_name": "Koerner Kathleen", "new_name": "Kathy Koerner"},
  {"old_name": "lawrence m stahl", "new_name": "Larry Stahl"},
  {"old_name": "marc s frere", "new_name": "Marc Frere"},
  {"old_name": "mary carr", "new_name": "Mary Carr"},
  {"old_name": "michael t hovey", "new_name": "Michael Hovey"},
  {"old_name": "mitchell c zocchi", "new_name": "Mitchell Zocchi"},
  {"old_name": "Peter Juhmerker", "new_name": "Peter Kuhmerker"},
  {"old_name": "Kuenzel Catherine", "new_name": "Sally Sullivan"},
  {"old_name": "stephen l proctor", "new_name": "Steve Proctor"},
  {"old_name": "Teven Averbach", "new_name": "Steven Averbach"},
  {"old_name": "timothy c groelinger", "new_name": "Timothy Groelinger"},
  {"old_name": "Murphy Brian", "new_name": "Brian Murphy"},
  {"old_name": "Massey Karen", "new_name": "Karen Massey"}
];


module.exports = {
  up: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.new_name + "', updated_at = '" + now + "' WHERE lower(" + column + ") = lower('" + update.old_name + "')");
      })
    }).then(function () {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE type = 'pmrName' AND lower(display_value) = lower('" + update.new_name + "')").then(function () {
          return queryInterface.sequelize.query("UPDATE static_display_data SET display_value = '" + update.new_name + "', updated_at = '" + now + "' WHERE lower(display_value) = lower('" + update.old_name + "')");
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return Promise.each(columns, function (column) {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + column + " = '" + update.old_name + "', updated_at = '" + now + "' WHERE lower(" + column + ") = lower('" + update.new_name + "')");
      })
    }).then(function () {
      return Promise.each(updates, function (update) {
        return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE type = 'pmrName' AND lower(display_value) = lower('" + update.old_name + "')").then(function () {
          return queryInterface.sequelize.query("UPDATE static_display_data SET display_value = '" + update.new_name + "', updated_at = '" + now + "' WHERE lower(display_value) = lower('" + update.old_name + "')");
        });
      });
    });
  }
};
