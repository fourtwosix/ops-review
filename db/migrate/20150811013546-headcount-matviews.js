'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("CREATE MATERIALIZED VIEW layer_" + layer + "_manager_headcount AS " +
          "SELECT layer_" + layer + "_manager, count(*) AS head_count, " +
          "date_trunc('month', report_date)::DATE AS MONTH FROM permast_data JOIN " +
          "(SELECT max(date_trunc('day', report_date))::DATE AS max_date FROM permast_data " +
          "GROUP BY date_trunc('month', report_date) ORDER BY date_trunc('month',report_date)) d " +
          "ON date_trunc('day', report_date) = d.max_date WHERE employment_status LIKE '%active' " +
          "GROUP BY layer_" + layer + "_manager, report_date ORDER BY report_date;")
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });

    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each([3, 4, 5, 6, 7], function (layer, done) {
        queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS layer_" + layer + "_manager_headcount RESTRICT;")
          .then(function () {
            done();
          })
          .catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
