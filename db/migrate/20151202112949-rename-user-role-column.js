'use strict';
var Promise = require('bluebird');
var tableName = 'users';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query(
        "ALTER TABLE " + tableName + " RENAME COLUMN role TO title;"
      )
        .then(resolve)
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query(
        "ALTER TABLE " + tableName + " RENAME COLUMN title TO role;"
      )
        .then(resolve)
        .catch(reject);
    });
  }
};
