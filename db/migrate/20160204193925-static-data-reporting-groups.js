'use strict';
var Promise = require('bluebird');
var _ = require('lodash');
var groups = [
  'nes-reporting',
  'ppmc-reporting',
  'mission-services',
  'staffing',
  'pmr-reporting'
];
var now = new Date().toUTCString();


module.exports = {
  up: function (queryInterface, Sequelize) {
    var values = _.trimRight(_.reduce(groups, function(result, group) {
      return result + "('" + group + "','groups','" + now + "','" + now +"'), ";
    }, ""), ", ");
    return new Promise(function(resolve, reject) {
        queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES " + values)
          .then(resolve)
          .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      var deletes = [];

      _.each(groups, function(group) {
        deletes.push(queryInterface.sequelize.query("DELETE FROM static_display_data WHERE type = 'groups' AND display_value = '" + group + "'"));
      });

      Promise.all(deletes)
        .then(resolve)
        .catch(reject);
    });
  }
};
