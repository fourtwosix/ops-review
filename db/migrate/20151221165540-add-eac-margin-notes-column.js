'use strict';
var Promise = require('bluebird');
var tableName = 'program_heatmap_manual_entry';
var columnName = 'eac_margin_percentage_notes';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.addColumn(tableName, columnName, Sequelize.TEXT)
        .then(resolve)
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.removeColumn(tableName, columnName)
        .then(resolve)
        .catch(reject);
    });
  }
};
