'use strict';
var _ = require('lodash');
var now = 'to_timestamp(' + new Date().getTime() / 1000 + ')';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var newNames = [
      "Savdy Keng",
      "Leon Thomas",
      "Gwen Frazier",
      "Philip Simon",
      "Don Hirsch",
      "Kanvasi Tejasen",
      "Kevin Brunner",
      "Christine Couch",
      "James S Johnson",
      "Takako Shimado",
      "Roger Sucharski",
      "Cynthia Davis",
      "Carlyn McCune",
      "Jenny Lewis",
      "Kevin French",
      "Jennifer Craft",
      "Sandra Nickerson"
    ];

    var values = _.trimRight(_.reduce(newNames, function(result, name) {
      return result + "('" + name + "','pmrName'," + now + "," + now +"), ";
    }, ""), ", ");

      return queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) SELECT DISTINCT initcap(layer_3_manager), 'pmrName', " + now + ", " + now + " FROM permast_l3_managers").then(function() {
        return queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES " + values)
          .then(function() {
          return queryInterface.sequelize.query("WITH names AS (SELECT DISTINCT names.name_values FROM (SELECT pra AS name_values FROM program_metadata_manual_entry UNION SELECT account_gm AS name_values FROM program_metadata_manual_entry UNION SELECT los_manager AS name_values FROM program_metadata_manual_entry UNION SELECT industry_gm AS name_values FROM program_metadata_manual_entry UNION SELECT l3_manager AS name_values FROM program_metadata_manual_entry UNION SELECT l2_manager AS name_values FROM program_metadata_manual_entry UNION SELECT program_manager AS name_values FROM program_metadata_manual_entry) names LEFT JOIN static_display_data ON names.name_values = lower(static_display_data.display_value) AND static_display_data.type = 'pmrName' WHERE static_display_data.display_value IS NULL) INSERT INTO static_display_data (display_value, type, created_at, updated_at) SELECT initcap(names.name_values), 'pmrName', " + now + ", " + now + " FROM names WHERE array_length(regexp_split_to_array(names.name_values, '\\s'),1) = 2 AND names.name_values IS NOT NULL;");
        });
      });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE type = 'pmrName'");
  }
};
