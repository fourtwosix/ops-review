'use strict';

var Promise = require('bluebird');
var async = require('async');

var updates =[
  {
    "column": "program_manager",
    "new": "Timothy Reeves",
    "old": "Reeves Timothy"
  },
  {
    "column": "pra",
    "new": "Timothy Reeves",
    "old": "Reeves Timothy"
  },
  {
    "column": "l3_manager",
    "new": "Timothy Reeves",
    "old": "Reeves Timothy"
  },
  {
    "column": "account_gm",
    "new": "Timothy Reeves",
    "old": "Reeves Timothy"
  },
  {
    "column": "los_manager",
    "new": "Timothy Reeves",
    "old": "Reeves Timothy"
  },
  {
    "column": "industry_gm",
    "new": "Timothy Reeves",
    "old": "Reeves Timothy"
  },
  {
    "column": "l2_manager",
    "new": "Timothy Reeves",
    "old": "Reeves Timothy"
  },
  {
    "column": "program_manager",
    "new": "Aaliyah Ford",
    "old": "Ford Aaliyah"
  },
  {
    "column": "pra",
    "new": "Aaliyah Ford",
    "old": "Ford Aaliyah"
  },
  {
    "column": "l3_manager",
    "new": "Aaliyah Ford",
    "old": "Ford Aaliyah"
  },
  {
    "column": "account_gm",
    "new": "Aaliyah Ford",
    "old": "Ford Aaliyah"
  },
  {
    "column": "los_manager",
    "new": "Aaliyah Ford",
    "old": "Ford Aaliyah"
  },
  {
    "column": "industry_gm",
    "new": "Aaliyah Ford",
    "old": "Ford Aaliyah"
  },
  {
    "column": "l2_manager",
    "new": "Aaliyah Ford",
    "old": "Ford Aaliyah"
  },
  {
    "column": "program_manager",
    "new": "Lewis Jimines",
    "old": "Jimines Lewis"
  },
  {
    "column": "pra",
    "new": "Lewis Jimines",
    "old": "Jimines Lewis"
  },
  {
    "column": "l3_manager",
    "new": "Lewis Jimines",
    "old": "Jimines Lewis"
  },
  {
    "column": "account_gm",
    "new": "Lewis Jimines",
    "old": "Jimines Lewis"
  },
  {
    "column": "los_manager",
    "new": "Lewis Jimines",
    "old": "Jimines Lewis"
  },
  {
    "column": "industry_gm",
    "new": "Lewis Jimines",
    "old": "Jimines Lewis"
  },
  {
    "column": "l2_manager",
    "new": "Lewis Jimines",
    "old": "Jimines Lewis"
  },
  {
    "column": "program_manager",
    "new": "Mitchell Zocchi",
    "old": "Mitchell C Zocchi"
  },
  {
    "column": "pra",
    "new": "Mitchell Zocchi",
    "old": "Mitchell C Zocchi"
  },
  {
    "column": "l3_manager",
    "new": "Mitchell Zocchi",
    "old": "Mitchell C Zocchi"
  },
  {
    "column": "account_gm",
    "new": "Mitchell Zocchi",
    "old": "Mitchell C Zocchi"
  },
  {
    "column": "los_manager",
    "new": "Mitchell Zocchi",
    "old": "Mitchell C Zocchi"
  },
  {
    "column": "industry_gm",
    "new": "Mitchell Zocchi",
    "old": "Mitchell C Zocchi"
  },
  {
    "column": "l2_manager",
    "new": "Mitchell Zocchi",
    "old": "Mitchell C Zocchi"
  },
  {
    "column": "program_manager",
    "new": "Lewis Jimines",
    "old": "Lewis Jiminez"
  },
  {
    "column": "pra",
    "new": "Lewis Jimines",
    "old": "Lewis Jiminez"
  },
  {
    "column": "l3_manager",
    "new": "Lewis Jimines",
    "old": "Lewis Jiminez"
  },
  {
    "column": "account_gm",
    "new": "Lewis Jimines",
    "old": "Lewis Jiminez"
  },
  {
    "column": "los_manager",
    "new": "Lewis Jimines",
    "old": "Lewis Jiminez"
  },
  {
    "column": "industry_gm",
    "new": "Lewis Jimines",
    "old": "Lewis Jiminez"
  },
  {
    "column": "l2_manager",
    "new": "Lewis Jimines",
    "old": "Lewis Jiminez"
  }
];



module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.waterfall([
        function (done) {
          async.eachSeries(updates, function (update, done) {
            var metaQuery = "UPDATE program_metadata_manual_entry SET " + update.column + " = '" + update.new + "' WHERE " + update.column + " = '" + update.old + "'";
            var staticQuery = "UPDATE static_display_data SET display_value = '" + update.new + "' WHERE type = 'pmrName' AND display_value = '" + update.old + "'";
            queryInterface.sequelize.query(staticQuery).then(function () {
              queryInterface.sequelize.query(metaQuery).then(function () {
                done();
              }).catch(done);
            }).catch(done);
          }, done);
        }], function (err) {
        err ? reject(err) : resolve();
      });
    });
  },


  /*Perform a reverse update on original update function*/
  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.waterfall([
        function (done) {
          async.eachSeries(updates, function (update, done) {
            var metaQuery = "UPDATE program_metadata_manual_entry SET " + update.column + " = '" + update.old + "' WHERE " + update.column + " = '" + update.new + "'";
            var staticQuery = "UPDATE static_display_data SET display_value = '" + update.old + "' WHERE type = 'pmrName' AND display_value = '" + update.new + "'";
            queryInterface.sequelize.query(staticQuery).then(function () {
              queryInterface.sequelize.query(metaQuery).then(function () {
                done();
              }).catch(done);
            }).catch(done);
          }, done);
        }], function (err) {
        err ? reject(err) : resolve();
      });
    });
  }
};
