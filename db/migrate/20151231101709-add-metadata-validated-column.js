'use strict';
var Promise = require('bluebird');
var async = require('async');
var tableName = 'program_metadata_manual_entry';
var columnName = 'validated';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function(done) {
          queryInterface.addColumn(tableName, columnName, Sequelize.BOOLEAN)
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        },
        function(done) {
          var queries = [
            "UPDATE program_metadata_manual_entry SET validated = true WHERE unique_id is not null",
            "CREATE INDEX program_heatmap_manual_entry_program_id_fiscal_month_end_index ON program_heatmap_manual_entry USING btree (program_id, fiscal_month_end DESC)"
          ];
          async.each(queries, function(query, done) {
            queryInterface.sequelize.query(query)
              .then(function() {
                done();
              })
              .catch(function(err) {
                done(err);
              })
          }, function(err) {
            err ? done(err) : done();
          });
        }
      ], function (err) {
        err ? reject(err): resolve();
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      async.series([
        function(done) {
          queryInterface.removeColumn(tableName, columnName)
            .then(function() {
              done();
            })
            .catch(function(err) {
              done(err);
            })
        },
        function(done) {
          var queries = [
            "DROP INDEX program_heatmap_manual_entry_program_id_fiscal_month_end_index"
          ];
          async.each(queries, function(query, done) {
            queryInterface.sequelize.query(query)
              .then(function() {
                done();
              })
              .catch(function(err) {
                done(err);
              })
          }, function(err) {
            err ? done(err) : done();
          });
        }
      ], function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
