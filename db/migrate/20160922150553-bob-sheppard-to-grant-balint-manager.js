'use strict';
var now = new Date().toUTCString();
var queries = [
  "UPDATE pmr_groups_divisions SET l3_manager = 'Grant Balint' WHERE lower(division_name) = 'dni'",
  "DELETE FROM static_display_data WHERE lower(display_value) = 'bob sheppard' OR lower(display_value) = 'robert sheppard'"
];

var metadataColumns = [
  "program_manager",
  "pra",
  "l3_manager",
  "l2_manager",
  "delivery_manager"
];


module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return Sequelize.Promise.map(queries, function (query) {
        return queryInterface.sequelize.query(query, {transaction: t});
      }).then(function () {
        return Sequelize.Promise.map(metadataColumns, function (col) {
          return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + col + " = 'Grant Balint' WHERE lower(" + col + ") = 'bob sheppard' OR lower(" + col + ") = 'robert sheppard'", {transaction: t});
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return queryInterface.sequelize.query("UPDATE pmr_groups_divisions SET l3_manager = 'Bob Sheppard' WHERE lower(division_name) = '42six'", {transaction: t}).then(function () {
        return queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) VALUES ('Bob Sheppard', 'pmrName', '" + now + "', '" + now + "')", {transaction: t}).then(function () {
          return Sequelize.Promise.map(metadataColumns, function (col) {
            return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET " + col + " = 'Bob Sheppard' WHERE lower(" + col + ") = 'grant balint'", {transaction: t});
          });
        });
      });
    });
  }
};
