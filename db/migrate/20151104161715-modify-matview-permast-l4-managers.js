'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.series([
        function (done) {
          queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_l4_managers RESTRICT;")
            .then(function() {
              done();
            }).catch(done);
        },
        function (done) {
          queryInterface.sequelize.query("CREATE MATERIALIZED VIEW permast_l4_managers AS " +
          "select distinct " +
          "(substring(layer_4_manager from position(',' in layer_4_manager)+2) || ' ' || substring(layer_4_manager for position(',' in layer_4_manager)-1)) as layer_4_manager, " +
          "layer_4_org_name from permast_data where layer_4_manager is not null AND layer_4_org_name is not null;")
            .then(function() {
              done();
            }).catch(done);
        },
        function (done) {
          queryInterface.sequelize.query("ALTER TABLE permast_l4_managers OWNER TO insighter;").then(function() {
            done();
          }).catch(done);
        },
        function (done) {
          queryInterface.sequelize.query("CREATE INDEX layer_4_manager_index ON permast_l4_managers(layer_4_manager);").then(function() {
            done();
          }).catch(done);
        },
        function (done) {
          queryInterface.sequelize.query("CREATE INDEX layer_4_org_name_index ON permast_l4_managers(layer_4_org_name);").then(function() {
            done();
          }).catch(done);
        }
      ], function (err) {
        err ? reject(err): resolve();
      });
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_l4_managers RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
