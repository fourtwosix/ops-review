'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable(
        'project_info_data',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          report_date: {
            type: Sequelize.DATE
          },
          unique_id: {
            type: Sequelize.STRING,
            unique: true
          },
          project_id: {
            type: Sequelize.STRING
          },
          project_name: {
            type: Sequelize.STRING
          },
          org_id: {
            type: Sequelize.STRING
          },
          project_start_date: {
            type: Sequelize.DATE
          },
          project_end_date: {
            type: Sequelize.DATE
          },
          prime_contractor_id: {
            type: Sequelize.STRING
          },
          created_at: {
            type: Sequelize.DATE
          },
          updated_at: {
            type: Sequelize.DATE
          }
        })
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE project_info_data OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.dropTable("project_info_data")
        .then(resolve)
        .catch(reject);
    });
  }
};
