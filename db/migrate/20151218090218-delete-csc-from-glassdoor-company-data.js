'use strict';

var GlassdoorCompanyData = require('../../server/models').GlassdoorCompanyData;
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolved, rejected) {
      GlassdoorCompanyData.destroy({
        where: {
          company_name: 'Computer Sciences Corp'
        }
      })
      .then(
        function(){resolved()},
        function(){rejected()}
        )
    });
  }
};
