'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("UPDATE users SET receives_emails = TRUE");
  },

  down: function (queryInterface, Sequelize) {
    return;
  }
};
