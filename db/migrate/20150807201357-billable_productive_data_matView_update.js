'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS billable_productive_data").then(function(){
          queryInterface.sequelize.query("CREATE MATERIALIZED VIEW billable_productive_data AS " +
          "SELECT labor_utilization_data.wtd_adjusted_capacity, labor_utilization_data.wtd_billable, " +
          "(((((labor_utilization_data.wtd_billable + labor_utilization_data.wtd_unbillable) + labor_utilization_data.wtd_bid_proposal) + labor_utilization_data.wtd_odo) " +
          "+ labor_utilization_data.wtd_internal_cost_recovery) + labor_utilization_data.wtd_service_center) AS wtd_productive, " +
          "labor_utilization_data.mtd_adjusted_capacity, labor_utilization_data.mtd_billable, " +
          "(((((labor_utilization_data.mtd_billable + labor_utilization_data.mtd_unbillable) + labor_utilization_data.mtd_bid_proposal) + " +
          "labor_utilization_data.mtd_odo) + labor_utilization_data.mtd_internal_cost_recovery) + " +
          "labor_utilization_data.mtd_service_center) AS mtd_productive, labor_utilization_data.ytd_adjusted_capacity, " +
          "labor_utilization_data.ytd_billable, (((((labor_utilization_data.ytd_billable + labor_utilization_data.ytd_unbillable) + " +
          "labor_utilization_data.ytd_bid_proposal) + labor_utilization_data.ytd_odo) + labor_utilization_data.ytd_internal_cost_recovery)" +
          " + labor_utilization_data.ytd_service_center) AS ytd_productive, labor_utilization_data.full_employee_name, " +
          "labor_utilization_data.week_ending_date, permast_data.layer_4_manager, permast_data.layer_3_manager, permast_data.layer_5_manager, " +
          "permast_data.layer_6_manager, permast_data.layer_7_manager, " +
          "CASE WHEN ((permast_data.job_description)::text ~ '(vp|mgr|dir|supv)'::text) THEN 'true'::text ELSE 'false'::text END AS is_manager " +
          "FROM (labor_utilization_data JOIN permast_data ON (((((permast_data.short_name)::text = (labor_utilization_data.short_name)::text) " +
          "AND ((date_trunc('month'::text, permast_data.report_date))::DATE = (date_trunc('month'::text, labor_utilization_data.week_ending_date))::date)) " +
          "AND (((date_trunc('day'::text, permast_data.report_date))::DATE >= ((date_trunc('week'::text, labor_utilization_data.week_ending_date) - '1 day'::interval))::date) " +
          "AND ((date_trunc('day'::text, permast_data.report_date))::DATE <= ((date_trunc('week'::text, labor_utilization_data.week_ending_date) + '5 days'::interval))::date)))));")
          .then(resolve)
          .catch(reject);
        }).catch(reject);
    });
  }
};


