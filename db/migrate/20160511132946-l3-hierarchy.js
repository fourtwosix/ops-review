'use strict';
var Promise = require('bluebird');
var readFile = Promise.promisify(require("fs").readFile);
var now = new Date().toUTCString();
var _ = require('lodash');

module.exports = {
  up: function (queryInterface, Sequelize) {

    var ops = [];

    return readFile("./db/migrate/pmr-794-l3-hierarchy-updates.json", "utf8").then(function (contents) {
      contents = JSON.parse(contents);
      return queryInterface.sequelize.transaction(function (t) {

        _.each(contents, function (entry) {

          _.each([entry.l2_manager, entry.new_l3_manager], function (name) {
            if (name) {
              ops.push(
                queryInterface.sequelize.query("INSERT INTO static_display_data (display_value, type, created_at, updated_at) SELECT '" + name + "', 'pmrName', '" + now + "', '" + now + "' WHERE NOT EXISTS (SELECT display_value FROM static_display_data WHERE display_value = '" + name + "' AND type = 'pmrName');", {transaction: t})
              );
            }
          });

          if (!entry.new_l3_manager) {
            entry.new_l3_manager = entry.old_l3_manager;
          }

          if (entry.old_division) {
            ops.push(
              queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET division = '" + entry.new_division + "', l3_manager = '" + entry.new_l3_manager + "', \"group\" = '" + entry.group + "', l2_manager = '" + entry.l2_manager + "' WHERE division = '" + entry.old_division + "' AND l3_manager = '" + entry.old_l3_manager + "';", {transaction: t}).then(function () {
                return queryInterface.sequelize.query("DELETE FROM pmr_groups_divisions WHERE division_name = '" + entry.old_division + "' AND EXISTS (SELECT division_name FROM pmr_groups_divisions WHERE division_name = '" + entry.new_division + "');", {transaction: t}).then(function () {
                  return queryInterface.sequelize.query("UPDATE pmr_divisions SET division_name = '" + entry.new_division + "', updated_at = '" + now + "' WHERE division_name = '" + entry.old_division + "' AND NOT EXISTS (SELECT division_name FROM pmr_divisions WHERE division_name = '" + entry.new_division + "');", {transaction: t}).then(function () {
                    return queryInterface.sequelize.query("DELETE FROM pmr_divisions WHERE division_name = '" + entry.old_division + "';", {transaction: t}).then(function () {
                      if (entry.new_l3_manager) {
                        return queryInterface.sequelize.query("UPDATE pmr_groups_divisions SET l3_manager = '" + entry.new_l3_manager + "', updated_at = '" + now + "' WHERE group_name = '" + entry.group + "' AND division_name = '" + entry.new_division + "';", {transaction: t});
                      } else {
                        return;
                      }
                    });
                  });
                });
              })
            );
          } else if (!entry.old_division && entry.new_division) {
            ops.push(
              queryInterface.sequelize.query("INSERT INTO pmr_divisions (division_name, created_at, updated_at) VALUES ('" + entry.new_division + "', '" + now + "', '" + now + "')", {transaction: t}).then(function () {
                return queryInterface.sequelize.query("INSERT INTO pmr_groups_divisions (group_name, division_name, l3_manager, created_at, updated_at) VALUES ('" + entry.group + "', '" + entry.new_division + "', '" + entry.new_l3_manager + "', '" + now + "', '" + now + "');", {transaction: t});
              })
            )
          }
        });

        return Promise.all(ops)
      });
    }).catch(function(err) {
      console.error(err);
    });
  },

  down: function (queryInterface, Sequelize) {
    var ops = [];

    return readFile("./db/migrate/pmr-794-l3-hierarchy-updates.json", "utf8").then(function (contents) {
      contents = JSON.parse(contents);
      return queryInterface.sequelize.transaction(function (t) {
        _.each(contents, function (entry) {

          if (!entry.new_l3_manager) {
            entry.new_l3_manager = entry.old_l3_manager;
          }

          if (entry.old_division && entry.new_division) {
            ops.push(

              queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET division = '" + entry.old_division + "', l3_manager = '" + entry.old_l3_manager + "', \"group\" = '" + entry.group + "', l2_manager = '" + entry.l2_manager + "' WHERE division = '" + entry.new_division + "' AND l3_manager = '" + entry.new_l3_manager + "';", {transaction: t}).then(function () {
                return queryInterface.sequelize.query("DELETE FROM pmr_divisions WHERE division_name = '" + entry.new_division + "';", {transaction: t}).then(function () {
                  return queryInterface.sequelize.query("INSERT INTO pmr_divisions (division_name, created_at, updated_at) SELECT '" + entry.old_division + "', '" + now + "', '" + now + "' WHERE NOT EXISTS (SELECT division_name FROM pmr_divisions WHERE division_name = '" + entry.old_division + "');", {transaction: t}).then(function () {
                    return queryInterface.sequelize.query("INSERT INTO pmr_groups_divisions (group_name, division_name, l3_manager, created_at, updated_at) VALUES ('" + entry.group + "', '" + entry.old_division + "', '" + entry.old_l3_manager + "', '" + now + "', '" + now + "');", {transaction: t});
                  });
                });
              })
            );
          } else if (!entry.old_division && entry.new_division) {
            ops.push(
              queryInterface.sequelize.query("DELETE FROM pmr_divisions WHERE division_name = '" + entry.new_division + "';")
            );
          }
        });
          return Promise.all(ops);
      });
    }).catch(function(err) {
      console.error(err);
    });
  }
};
