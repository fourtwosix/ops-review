'use strict';

var Promise = require('bluebird');

module.exports = {
  up: function(queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      return queryInterface.addColumn('resource_management_data', 'job_offer_accepted_date', {type: Sequelize.DATE})
        .then(resolve)
        .catch(reject)
    });
  },

  down: function(queryInterface) {
    return new Promise(function(resolve, reject) {
      queryInterface.removeColumn('resource_management_data', 'job_offer_accepted_date')
        .then(resolve)
        .catch(reject)
    });
  }
};


