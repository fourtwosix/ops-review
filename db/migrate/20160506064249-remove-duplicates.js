'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE id NOT IN (SELECT DISTINCT ON (type, display_value) id FROM static_display_data ORDER BY type, display_value, updated_at DESC)");
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("DELETE FROM static_display_data WHERE id NOT IN (SELECT DISTINCT ON (type, display_value) id FROM static_display_data ORDER BY type, display_value, updated_at DESC)");
  }
};