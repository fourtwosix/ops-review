'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.changeColumn('program_heatmap_manual_entry', 'notes', Sequelize.TEXT)
        .then(resolve)
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.changeColumn('program_heatmap_manual_entry', 'notes', Sequelize.STRING)
        .then(resolve)
        .catch(reject);
    });
  }
};
