'use strict';

var updates = [
  {
    group: "Homeland Security",
    division: "TSA SOC",
    oldL3Manager: "Steve Ashworth",
    newL3Manager: "Gordon Levy"
  },
  {
    group: "Defense",
    division: "Air Force",
    oldL3Manager: "David Rue",
    newL3Manager: "John Anderson"
  }
];

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function(t) {
      return Sequelize.Promise.map(updates, function(update) {
        return queryInterface.sequelize.query("UPDATE pmr_groups_divisions SET l3_manager = '" + update.newL3Manager + "' WHERE lower(group_name) = lower('" + update.group + "') AND lower(division_name) = lower('" + update.division + "')", {transaction: t}).then(function() {
          return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET l3_manager = '" + update.newL3Manager + "' WHERE lower(l3_manager) = lower('" + update.oldL3Manager + "')", {transaction: t});
        });
      })
    }).then(function() {
      return queryInterface.sequelize.query("REFRESH MATERIALIZED VIEW pmr_groups_divisions_l2_manager");
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function(t) {
      return Sequelize.Promise.map(updates, function(update) {
        return queryInterface.sequelize.query("UPDATE pmr_groups_divisions SET l3_manager = '" + update.oldL3Manager + "' WHERE lower(group_name) = lower('" + update.group + "') AND lower(division_name) = lower('" + update.division + "')", {transaction: t}).then(function() {
          return queryInterface.sequelize.query("UPDATE program_metadata_manual_entry SET l3_manager = '" + update.oldL3Manager + "' WHERE lower(l3_manager) = lower('" + update.newL3Manager + "')", {transaction: t});
        });
      })
    }).then(function() {
      return queryInterface.sequelize.query("REFRESH MATERIALIZED VIEW pmr_groups_divisions_l2_manager");
    });
  }
};
