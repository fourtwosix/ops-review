'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("CREATE MATERIALIZED VIEW candidate_data_recruiting_filters AS " +
        "SELECT DISTINCT candidate_data.client_program_name, NULL::text AS req_hiring_manager_name, NULL::text AS req_recruiter_name FROM candidate_data WHERE candidate_data. client_program_name IS NOT NULL " +
        "UNION " +
        "SELECT DISTINCT NULL::text AS client_program_name, candidate_data.req_hiring_manager_name, NULL::text AS req_recruiter_name FROM candidate_data WHERE candidate_data. req_hiring_manager_name IS NOT NULL " +
        "UNION " +
        "SELECT DISTINCT NULL::text AS client_program_name, NULL::text AS req_hiring_manager_name, candidate_data.req_recruiter_name FROM candidate_data WHERE candidate_data.req_recruiter_name IS NOT NULL;")
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE candidate_data_recruiting_filters OWNER TO insighter;")
            .then(resolve)
            .catch(reject);

        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS candidate_data_recruiting_filters RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
