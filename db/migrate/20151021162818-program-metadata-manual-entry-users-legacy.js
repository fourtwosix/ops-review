'use strict';
var Promise = require('bluebird');
var async = require('async');


module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.addColumn('program_metadata_manual_entry', 'legacy_modified_date', Sequelize.DATE).then(function () {
        queryInterface.addColumn('program_metadata_manual_entry', 'legacy_modified_by', Sequelize.STRING).then(function () {
          queryInterface.removeColumn('program_metadata_manual_entry', 'created_by').then(function () {
            queryInterface.removeColumn('program_metadata_manual_entry', 'updated_by').then(function () {
              queryInterface.addColumn('program_metadata_manual_entry', 'created_by', {
                type: Sequelize.INTEGER,
                references: 'users',
                referencesKey: 'id',
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT'
              }).then(function () {
                queryInterface.addColumn('program_metadata_manual_entry', 'updated_by', {
                  type: Sequelize.INTEGER,
                  references: 'users',
                  referencesKey: 'id',
                  onUpdate: 'CASCADE',
                  onDelete: 'RESTRICT'
                }).then(resolve)
                  .catch(reject);
              });
            });
          });
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.removeColumn('program_metadata_manual_entry', 'legacy_modified_date').then(function () {
        queryInterface.removeColumn('program_metadata_manual_entry', 'legacy_modified_by').then(function () {
          queryInterface.removeColumn('program_metadata_manual_entry', 'created_by').then(function () {
            queryInterface.removeColumn('program_metadata_manual_entry', 'updated_by').then(function () {
              queryInterface.addColumn('program_metadata_manual_entry', 'created_by', Sequelize.STRING).then(function () {
                queryInterface.addColumn('program_metadata_manual_entry', 'updated_by', Sequelize.STRING)
                  .then(resolve)
                  .catch(reject);
              });
            });
          });
        });
      });
    });
  }
};
