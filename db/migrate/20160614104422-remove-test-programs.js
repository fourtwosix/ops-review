'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("ALTER TABLE program_staffing DROP CONSTRAINT program_staffing_program_id_fkey").then(function() {
      return queryInterface.sequelize.query("ALTER TABLE program_staffing ADD CONSTRAINT program_staffing_program_id_fkey FOREIGN KEY (program_id) REFERENCES programs (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE").then(function() {
        return queryInterface.sequelize.query("DELETE FROM programs WHERE project_id SIMILAR TO 'test[0-9]*'");
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query("ALTER TABLE program_staffing DROP CONSTRAINT program_staffing_program_id_fkey").then(function() {
      return queryInterface.sequelize.query("ALTER TABLE program_staffing ADD CONSTRAINT program_staffing_program_id_fkey FOREIGN KEY (program_id) REFERENCES programs (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE").then(function() {
        return queryInterface.sequelize.query("DELETE FROM programs WHERE project_id SIMILAR TO 'test[0-9]*'");
      });
    });
  }
};