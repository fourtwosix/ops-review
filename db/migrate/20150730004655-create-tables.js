'use strict';
var async = require('async'),
    fs = require('fs'),
    Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    var db = queryInterface.sequelize;

    return new Promise(function (resolve, reject) {
      async.waterfall([
        function (cb) {
          fs.readFile(__dirname + '/initial-insight.sql', function (err, data) {
            if (err) throw err;
            cb(null, data.toString());
          });
        },

        function (initialSchema, cb) {
          // need to split on ';' to get the individual CREATE TABLE sql
          // as db.query can only execute one query at a time
          var tables = initialSchema.split(/;\s\n/g);

          function createTable (tableSql, doneCreate){
            if (tableSql.trim().length > 0) {
              db.query(tableSql.trim()).then(function (err) {
                doneCreate();
              }).catch(function (err) {
                doneCreate(err);
              })
            } else {
              doneCreate();
            }
          }

          async.eachSeries(tables, createTable, cb);
        }
      ], function (err) {
        err ? reject(err) : resolve();
      });
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.dropAllTables('SequelizeMeta')
        .then(resolve)
        .catch(reject);
    });
  }
};
