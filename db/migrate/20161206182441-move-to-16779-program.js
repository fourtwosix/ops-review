'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    var oldProgram = "16779.000";
    var newProgram = "16779";

    return queryInterface.sequelize.transaction(function (t) {
      return queryInterface.sequelize.query("DELETE FROM salesforce_financials WHERE program_id IN (SELECT id FROM programs WHERE project_id = '" + newProgram + "' OR project_id = '" + oldProgram + "')", {transaction: t}).then(function () {
        return queryInterface.sequelize.query("DELETE FROM salesforce_gap_projects WHERE program_id IN (SELECT id FROM programs WHERE project_id = '" + newProgram + "' OR project_id = '" + oldProgram + "')", {transaction: t}).then(function () {
          return queryInterface.sequelize.query("DELETE FROM programs WHERE project_id = '" + newProgram + "'", {transaction: t}).then(function () {
            return queryInterface.sequelize.query("UPDATE programs SET project_id = '" + newProgram + "' WHERE project_id = '" + oldProgram + "'", {transaction: t});
          });
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return;
  }
};
