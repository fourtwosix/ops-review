'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("CREATE MATERIALIZED VIEW staffing_level_fte AS WITH result AS (SELECT weekend_date, project_id AS program, SUM(COALESCE(total_hours, 0)) / 40 AS actuals, 'csc' AS type FROM costpoint_data GROUP BY project_id, weekend_date UNION ALL SELECT weekend_date, project AS program, SUM(COALESCE(total_hours, 0)) / 40 AS actuals, 'sub' AS type FROM steps_data GROUP BY project, weekend_date) SELECT weekend_date, program, SUM(actuals) AS actuals, 'total' as type FROM result GROUP BY weekend_date, program UNION ALL SELECT * FROM result;")
        .then(function () {
          queryInterface.sequelize.query("ALTER TABLE staffing_level_fte OWNER TO insighter;")
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS staffing_level_fte RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
