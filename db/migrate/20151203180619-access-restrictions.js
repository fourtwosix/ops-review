'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.createTable('auths',
        {
          name: {
            type: Sequelize.STRING,
            primaryKey: true
          },
          created_at: Sequelize.DATE,
          updated_at: Sequelize.DATE
        }).then(function () {
        queryInterface.createTable('users_auths', {
          user_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            references: 'users',
            referencesKey: 'id',
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
          },
          auth_name: {
            type: Sequelize.STRING,
            primaryKey: true,
            references: 'auths',
            referencesKey: 'name',
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
          },
          created_at: Sequelize.DATE,
          updated_at: Sequelize.DATE
        }).then(function () {
          var now = new Date().toUTCString();
          queryInterface.sequelize.query("INSERT INTO auths (name, created_at, updated_at) values ('dig','" + now + "', '" + now + "')")
            .then(function () {
              queryInterface.addColumn('contract_data', 'auth_name', {
                type: Sequelize.STRING,
                references: 'auths',
                referencesKey: 'name',
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT'
              }).then(function () {
                queryInterface.addColumn('programs', 'auth_name', {
                  type: Sequelize.STRING,
                  references: 'auths',
                  referencesKey: 'name',
                  onUpdate: 'CASCADE',
                  onDelete: 'RESTRICT'
                }).then(function () {
                  queryInterface.changeColumn('programs', 'project_id', {
                    type: Sequelize.STRING,
                    unique: true,
                    allowNull: false
                  }).then(function () {
                    var queries = [
                      "UPDATE contract_data SET auth_name = 'dig' WHERE division = 'national security' OR division = 'army & defense intel';",
                      "UPDATE programs SET auth_name = 'dig' FROM (SELECT program_id FROM program_metadata_manual_entry WHERE division = 'national security' OR division = 'army & defense intel') metadata WHERE programs.id = metadata.program_id;",
                      "CREATE VIEW contract_data_secure WITH (security_barrier) AS SELECT * FROM contract_data WHERE auth_name IS NULL OR auth_name = ANY(SELECT auth_name FROM users_auths WHERE user_id = current_setting('config.user_id')::int);",
                      "CREATE VIEW programs_secure WITH (security_barrier) AS SELECT * FROM programs WHERE auth_name IS NULL OR auth_name = ANY(SELECT auth_name FROM users_auths WHERE user_id = current_setting('config.user_id')::int);"
                    ];

                    async.each(queries, function (query, done) {
                      queryInterface.sequelize.query(query).then(function () {
                        done();
                      }).catch(done);
                    }, function (err) {
                      err ? reject(err) : resolve();
                    });
                  });
                });
              });
            });
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP VIEW contract_data_secure").then(function () {
        queryInterface.sequelize.query("DROP VIEW programs_secure").then(function () {
          queryInterface.removeColumn('programs', 'auth_name').then(function () {
            queryInterface.changeColumn('programs', 'project_id', {
              type: Sequelize.STRING,
              unique: false,
              allowNull: false
            }).then(function () {
              queryInterface.removeColumn('contract_data', 'auth_name').then(function () {
                queryInterface.dropTable('users_auths').then(function () {
                  queryInterface.dropTable('auths')
                    .then(resolve)
                    .catch(reject);
                });
              });
            });
          });
        });
      });
    });
  }
};
