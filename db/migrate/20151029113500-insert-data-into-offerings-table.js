/**
 * Created by cedam on 10/29/15.
 */

'use strict';
var Promise = require('bluebird');
var async = require('async');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      async.each(["MISSION SERVICES", "NBS", "NMS", "NES", "NIS", "BIG DATA", "CLOUD", "CYBERSECURITY", "N/A", "TBD"], function (offer_code, done) {
        queryInterface.sequelize.query("INSERT INTO OFFERINGS (UNIQUE_ID,OFFERING_NAME,LOS_NAME,OFFERING_CODE,OFFERING_DESCRIPTION) " +
        "VALUES (null,'" + offer_code + "',null,'" + offer_code + "'" + ",'" + offer_code + "');")
          .then(resolve).catch(done);
      }, function (err) {
        err ? reject(err): resolve();
      });
    });
  }
};
