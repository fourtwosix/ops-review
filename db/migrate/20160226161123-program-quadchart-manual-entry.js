'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.createTable(
        'program_quadchart_manual_entry',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          program_id: {
            type: Sequelize.INTEGER,
            references: 'programs',
            referencesKey: 'id',
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT'
          },
          fiscal_month_end: Sequelize.DATE,
          opportunities: Sequelize.TEXT,
          issues: Sequelize.TEXT,
          risks: Sequelize.TEXT,
          key_milestones: Sequelize.TEXT,
          noteworthy: Sequelize.TEXT,
          validated: Sequelize.BOOLEAN,
          created_by: {
            type: Sequelize.INTEGER,
            references: 'users',
            referencesKey: 'id',
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
          },
          updated_by: {
            type: Sequelize.INTEGER,
            references: 'users',
            referencesKey: 'id',
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL'
          },
          created_at: Sequelize.DATE,
          updated_at: Sequelize.DATE
        }
        )
        .then(resolve)
        .catch(reject);
    });
  },

  down: function (queryInterface, Sequelize) {
    return new Promise(function(resolve, reject) {
      queryInterface.dropTable('program_quadchart_manual_entry')
        .then(resolve)
        .catch(reject);
    });
  }
};
