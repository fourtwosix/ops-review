'use strict';
var Promise = require('bluebird');

module.exports = {
  up: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      // Because we are creating this view WITH NO DATA we need to refresh the view before we can use it (can be done nightly?)
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_l3_managers RESTRICT;")
        .then(function () {
          queryInterface.sequelize.query("CREATE MATERIALIZED VIEW permast_l3_managers AS " +
          "select distinct " +
          "(substring(layer_3_manager from position(',' in layer_3_manager)+2) || ' ' || substring(layer_3_manager for position(',' in layer_3_manager)-1)) as layer_3_manager, " +
          "layer_3_org_name from permast_data where layer_3_manager is not null AND layer_3_org_name is not null;")
            .then(function () {
              queryInterface.sequelize.query("ALTER TABLE permast_l3_managers OWNER TO insighter;")
                .then(resolve)
                .catch(reject);

            })
            .catch(reject);
        }).catch(reject);
    });
  },

  down: function (queryInterface) {
    return new Promise(function (resolve, reject) {
      queryInterface.sequelize.query("DROP MATERIALIZED VIEW IF EXISTS permast_l3_managers RESTRICT;")
        .then(resolve)
        .catch(reject);
    });
  }
};
