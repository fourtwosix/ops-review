/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
require('dotenv').config();
var config = require('./config/environment');

if (config.newrelic.enabled) {
  require('newrelic');
}

var express = require('express');
var kue = require('kue');
var redis = require('redis');
var dbConfig    = require(__dirname + '/config/database.json')[process.env.NODE_ENV];

// Setup server
var app = express();
var server = require('http').createServer(app);
var socketio = require('socket.io')(server, {
  serveClient: config.env !== 'production',
  path: '/socket.io-client'
});

require('./config/socketio')(socketio);
require('./config/express')(app);
require('./routes')(app);

if (!config.disableDatabases) {
  require('./tasks/scheduledTasks')();

  var jobs = kue.createQueue({
    redis: dbConfig.redis,
    prefix: config.redisOpts.prefix
  });

  jobs.watchStuckJobs();

  var mailer = require(__dirname + '/emails');

  if (config.autoMigrate) {
    require('./models').migrate();
  }

}

// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// Expose app
exports = module.exports = app;
