'use strict';

var _ = require('lodash');
var models = require('../../models');
var sequelize = models.sequelize;
var Contract = models.ContractData;
var async = require('async');
var moment = require('moment');

// Get a single program (by project ID)
exports.show = function(req, res) {

  sequelize.transaction(function(t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function() {
      return Contract.find({where: {project_num: req.params.id}}, {transaction: t});
    }).catch(function(err) {
      return handleError(res, err);
    });
  }).then(function(contractdata) {
    if(!contractdata) { return res.status(404).send('Not Found'); }
    return res.json(contractdata);
  }).catch(function(err) {
    return handleError(res, err);
  });
};

exports.index = function(req, res) {
  sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return sequelize.query("SELECT DISTINCT ON (project_num) * FROM " + Contract.getTableName() + " ORDER BY project_num ASC;", {type: sequelize.QueryTypes.SELECT, transaction: t});
    }).catch(function (err) {
      return handleError(res, err);
    });
  }).then(function (contracts) {
    return res.status(200).json(contracts);
  }).catch(function (err) {
    return handleError(res, err);
  });
};

function handleError(res, err) {
  console.error(err);
  return res.status(500).send(err);
}
