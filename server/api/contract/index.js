'use strict';

var express = require('express');
var controller = require('./contract.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/:id', auth.isAuthenticated(), controller.show);
router.get('/', auth.isAuthenticated(), controller.index);
module.exports = router;
