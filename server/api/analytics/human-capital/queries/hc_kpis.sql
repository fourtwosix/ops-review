SELECT *,
ROUND(100.0 * ((current_headcount - (current_headcount + voluntary_terminations + involuntary_terminations - hires))::NUMERIC /
(current_headcount + voluntary_terminations + involuntary_terminations - hires)), 2) AS headcount_growth

FROM
  (SELECT
    COUNT(DISTINCT (CASE WHEN date_trunc('day', hire_date)::DATE BETWEEN '<%-fiscalStart %>' AND '<%- fiscalEnd %>'
    AND report_date = (SELECT MAX(report_date) FROM permast_data) THEN personnel_number END)) AS hires,

    COUNT(CASE WHEN termination_type IS NULL AND report_date = (SELECT MAX(report_date) FROM permast_data) THEN personnel_number END) AS current_headcount,

    COUNT(DISTINCT (CASE WHEN termination_type LIKE '% voluntary' AND date_trunc('day', termination_date)::DATE
      BETWEEN '<%- fiscalStart %>' AND '<%- fiscalEnd %>' AND report_date = (SELECT MAX(report_date) FROM permast_data) THEN personnel_number END)) AS voluntary_terminations,

    COUNT(DISTINCT (CASE WHEN termination_type LIKE '%involuntary' AND date_trunc('day', termination_date)::DATE
      BETWEEN '<%- fiscalStart %>' AND '<%- fiscalEnd %>' AND report_date = (SELECT MAX(report_date) FROM permast_data) THEN personnel_number END)) AS involuntary_terminations
   FROM permast_data
   WHERE <%- removeCasualEmployeesClause %> AND <%- managerClause %>) p

CROSS JOIN

(SELECT COUNT(DISTINCT (req_can_id)) AS pending_hires FROM
   (SELECT DISTINCT ON (CONCAT(requisition_number, candidate_id)) CONCAT(requisition_number, candidate_id) AS req_can_id,
      application_cws_step, application_cws_status, application_cws_start_date FROM candidate_data
      WHERE <%- managerClause %> AND report_date = (SELECT MAX(report_date) FROM candidate_data
        WHERE date_trunc('day', report_date)::DATE BETWEEN '<%- fiscalStart %> ' AND '<%- fiscalEnd %>')
      ORDER BY req_can_id, application_cws_start_date DESC) c
WHERE application_cws_status = 'onboarding process initiated - waiting on new hire to start') c