SELECT * FROM (
SELECT DISTINCT personnel_number,
                last_name,
                first_name,
                layer_3_manager,
                layer_4_manager,
                to_char(termination_date, 'YYYY-MM-DD') AS termination_date,
                termination_type,
                to_char(age(termination_date, hire_date), 'fmYY"y" fmMM"m" fmDD"d"') AS tenure
FROM permast_data
WHERE termination_type LIKE '%involuntary'
  AND date_trunc('day', termination_date)::DATE BETWEEN '<%- fiscalStart %>' AND '<%- fiscalEnd %>'
  AND report_date =
    (SELECT MAX(report_date)
     FROM permast_data)
  AND <%- managerClause %>
  AND <%- removeCasualEmployeesClause %>
 <%- limit %> <%- offset %>
) table_query <%- order %>