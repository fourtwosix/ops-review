SELECT
  MAX(o.layer<%= layer_parent %>) parent_org,
  MAX(o.layer<%= layer %>)      layer_name,
  MIN(o.org_key) org_key,
  (SELECT o1.chief FROM organizations o1 WHERE o1.org_key = MIN(o.org_key)) chief,
  SUM(CASE WHEN  (employee_status = 'Active' OR employee_status = 'Inactive')
    THEN 1
      ELSE 0 END) AS head_count,
  SUM(CASE WHEN employee_status = 'Withdrawn'
    THEN 1
      ELSE 0 END) AS withdrawn,
  SUM(CASE WHEN termination_type = 'Involuntary'
    THEN 1
      ELSE 0 END) AS involuntary_terms,
  SUM(CASE WHEN termination_type = 'Voluntary'
    THEN 1
      ELSE 0 END) AS voluntary_terms,
  SUM(CASE WHEN hire_date > :startdate AND hire_date < :enddate
    THEN 1
      ELSE 0 END)    hires,
  COUNT(*)           total,
  (SELECT COUNT(*) AS pending_hires FROM
	  (SELECT DISTINCT ON (CONCAT(requisition_number, candidate_id)) CONCAT(requisition_number, candidate_id) AS req_can_id,
		application_cws_step, application_cws_status, application_cws_start_date FROM candidate_data
		WHERE layer_<%= layer %>_manager = (SELECT o1.chief FROM organizations o1 WHERE o1.org_key = MIN(o.org_key))
		AND report_date = (SELECT MAX(report_date) FROM candidate_data)
	ORDER BY req_can_id, application_cws_start_date DESC) c
	WHERE application_cws_status = 'onboarding process initiated - waiting on new hire to start')
FROM employees e
  INNER JOIN organizations o
    ON e.organization_id = o.id
WHERE e.employee_status IN ('Active', 'Inactive', 'Withdrawn')
      AND (
        e.termination_date IS NULL OR
        e.termination_date > :startdate
      )
      <%- casual_clause %>
      AND e.last_seen_report_date = (select MAX(last_seen_report_date) FROM employees )
      AND e.organization_id = o.id AND o.org_key LIKE :orgkey
GROUP BY o.layer<%= layer %>
ORDER BY o.layer<%= layer %>
