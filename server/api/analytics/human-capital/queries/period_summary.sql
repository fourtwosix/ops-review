SELECT date_trunc(:period, eh.date)::DATE AS <%= period %>,
SUM(CASE WHEN eh.employee_status IN ('Active', 'Inactive') THEN 1 ELSE 0 END) head_count,
SUM(CASE WHEN date_trunc(:period, e.hire_date)::DATE = <%= period %> THEN 1 ELSE 0 END) hires,
SUM(CASE WHEN termination_type = 'Involuntary' AND date_trunc(:period, termination_date)::DATE = <%= period %> THEN 1 ELSE 0 END) AS involuntary_terms,
SUM(CASE WHEN termination_type = 'Voluntary' AND date_trunc(:period, termination_date)::DATE = <%= period %> THEN 1 ELSE 0 END) AS voluntary_terms

FROM employee_history eh, 
	(SELECT MAX(eh1.date) AS max_date, date_trunc(:period, eh1.date) AS <%= period %> FROM employee_history eh1
		GROUP BY <%= period %>
		ORDER BY <%= period %> DESC) eh2, organizations o, employees e
WHERE eh.employee_id = e.id AND eh.date >= :startDate AND eh.date = eh2.max_date AND eh.organization_id = o.id AND o.org_key LIKE :orgKey
GROUP BY eh.date
ORDER BY eh.date DESC
