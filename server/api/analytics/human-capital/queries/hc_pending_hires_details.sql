SELECT * FROM (
SELECT c.requisition_number, c.layer_3_manager, c.req_hiring_manager_name AS hiring_manager, c.client_program_name AS client,
  (SELECT security_clearance
   FROM resource_management_data
   WHERE requisition_number = c.requisition_number LIMIT 1)
FROM
  (SELECT DISTINCT ON (CONCAT(requisition_number, candidate_id)) CONCAT(requisition_number, candidate_id) AS req_can_id,
     requisition_number,
     application_cws_status,
     candidate_name,
     layer_3_manager,
     req_hiring_manager_name,
     client_program_name
   FROM candidate_data
   WHERE <%- managerClause %>
     AND report_date =
       (SELECT MAX(report_date)
        FROM candidate_data
        WHERE date_trunc('day', report_date)::DATE BETWEEN '<%- fiscalStart %>' AND '<%- fiscalEnd %>')
   ORDER BY req_can_id,
            application_cws_start_date DESC) c
WHERE c.application_cws_status = 'onboarding process initiated - waiting on new hire to start'
 <%- limit %> <%- offset %>
) table_query <%- order %>