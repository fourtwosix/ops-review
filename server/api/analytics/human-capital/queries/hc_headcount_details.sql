SELECT * FROM (
SELECT DISTINCT
personnel_number,
last_name,
first_name,
layer_3_manager,
layer_4_manager
FROM permast_data
WHERE termination_type IS NULL
  AND report_date =
    (SELECT MAX(report_date)
     FROM permast_data)
  AND <%- managerClause %>
  AND <%- removeCasualEmployeesClause %>
   <%- limit %> <%- offset %>
) table_query <%- order %>