SELECT * FROM (
SELECT DISTINCT
short_name,
last_name,
first_name,
layer_3_manager,
layer_4_manager,
to_char(hire_date, 'YYYY-MM-DD') AS hire_date
FROM permast_data
WHERE date_trunc('day', hire_date)::DATE BETWEEN '<%- fiscalStart %>' AND '<%- fiscalEnd %>'
  AND report_date =
    (SELECT MAX(report_date)
     FROM permast_data)
  AND <%- managerClause %>
  AND <%- removeCasualEmployeesClause %>
   <%- limit %> <%- offset %>
) table_query <%- order %>