'use strict';
var fs = require('fs');
var models = require('../../../models');
var utils = require('../../../components/utils');
var config = require('../../../config/environment');
var path = require('path');
var sequelize = models.sequelize;
var Promise = require('bluebird');
var moment = require('moment');
var ejs = require('ejs');
var _ = require('lodash');

require('pg').defaults.parseInt8 = true;


var getPathFromRoot = function (p) {
  return path.join(config.root, p);
};

function readSql(cmd, data) {

  var file = fs.readFileSync("./server/api/analytics/human-capital/queries/" + cmd + ".sql", "utf8");
  var template = ejs.compile(file);
  return template(data);
}

function computeGrowth(layer) {
  var originalHeadcount = layer.head_count + layer.voluntary_terms + layer.involuntary_terms - layer.hires;
  var newHeadcount = layer.head_count;
  return 100 * ((newHeadcount - originalHeadcount ) / originalHeadcount);
}

function params(req, key, defaultValue) {
  if (key in req.query) {
    return req.query[key];
  }
  return defaultValue;
}

exports.humanCapital = function(req, res) {
  var startdate = params(req, 'startdate', '2015-04-01');
  var enddate = params(req, 'enddate', moment().format("YYYY-MM-DD") );
  var parent = params(req, 'parent', 'NPS Enterprise Servi') + '%';
  var layer = (parent.split("|").length - 1) + 2;
  var casual = JSON.parse(params(req, 'casual', 'true'));
  var casualClause = !casual ? "AND e.employee_group != 'Casual'" : "";

  Promise.all([
    sequelize.query(readSql('layer_n_summary', {layer:layer, layer_parent:layer>2 ? layer - 1: 2, casual_clause: casualClause}), {
      replacements: {
        startdate: startdate,
        enddate: enddate,
        orgkey: parent
      }
    }),
    sequelize.query(readSql('layer_n_summary', {layer:layer+1, layer_parent:layer, casual_clause: casualClause}), {
      replacements: {
        startdate: startdate,
        enddate: enddate,
        orgkey: parent
      }
    })
  ]).then(function(results){
    var layer2 = results[0][0];
    var i;

    for (i = 0; i < layer2.length; i++) {
      layer2[i].growth = computeGrowth(layer2[i]);
    }

    var layer3 = results[1][0];
    for (i = 0; i < layer3.length; i++) {
      layer3[i].growth = computeGrowth(layer3[i]);
    }

    res.json(200, {
      layer: parent,
      start_date : startdate,
      end_date : enddate,
      layer_counts : layer2,
      layer_children : layer3
    });
  });
};


exports.humanCapitalPeriod = function(req, res) {
  var parent = req.query.parent ? req.query.parent + '%' : 'NPS Enterprise%';
  var period = req.query.period || 'month';
  var startDate = moment(req.query.startDate || '2015-04-01').format('YYYY-MM-DD');
  var endDate = moment(req.query.endDate || new Date()).format('YYYY-MM-DD');

  sequelize.query(readSql('period_summary', {period: period}), {
    replacements: {
      orgKey: parent,
      period: period,
      startDate: startDate,
      endDate: endDate
    }
  }).then(function(results){
    res.json(200, {
      parent : parent,
      period: period,
      monthly : results[0]
    });
  });
};

exports.getLayerFilters = function (req, res) {
  var layer = req.query.layer || '2';
  sequelize.query("SELECT DISTINCT chief, org_key, layer" + layer + " FROM organizations WHERE layer = :layer", {
    type: sequelize.QueryTypes.SELECT,
    replacements: {
     layer: layer
    }
  }).then(function (results) {
    res.status(200).json(results);
  }).catch(function (err) {
    handleError(res, err);
  });
};

var getVariables = function (queryParams) {
  var variables = {
    endDate: utils.getPgDate(queryParams.endDate),
    fiscalStart: utils.getPgDate(queryParams.fiscalStart),
    fiscalEnd: utils.getPgDate(queryParams.fiscalEnd)
  };

  variables.managerClause = queryParams.l2_manager ? "layer_2_manager ~ $$" +
    queryParams.l2_manager + "$$" : "TRUE";

  variables.removeCasualEmployeesClause = JSON.parse(_.get(queryParams, 'casual', 'true')) ?
    'TRUE' : "employee_group != 'casual'";

  variables.order = queryParams.sort ? "ORDER BY \"" + queryParams.sort + "\" " + queryParams.order : "";

  variables.limit = queryParams.limit ? "LIMIT " + queryParams.limit : "";

  variables.offset = queryParams.offset ? "OFFSET " + queryParams.offset : "";

  return variables;
};

exports.getKpis = function (req, res) {
  var variables = getVariables(req.query);

  utils.readSQLFile(
    getPathFromRoot('/server/api/analytics/human-capital/queries/hc_kpis.sql'), variables)
  .then(function (query) {
    sequelize.query(query, {type: sequelize.QueryTypes.SELECT}).then(function (results) {
      res.json(results[0]);
    }).catch(function (err) {
      handleError(res, err);
    });
  }).catch(function (err) {
    handleError(res, err);
  });
};

exports.getKpiDetails = function (req, res) {
  var type = req.query.type;

  if (!type) return res.status(500).send('Must include type');

  var path = utils.getPathFromRoot('/server/api/analytics/human-capital/queries/hc_' + type  +  '_details.sql');
  utils.processGeneralRequest(path, getVariables(req.query), req, res);
};

function handleError (res, err) {
  console.error(err);
  return res.status(500).send(err);
}