var models = require('../../../models');
var Sequelize = models.sequelize;
var _ = require('lodash');
var utils = require('../../../components/utils');
var config = require('../../../config/environment');
var marginRules = {
  generalRules: {
    green: "(program_heatmap_manual_entry.eac_margin_percentage > .9 * program_metadata_manual_entry.baseline_margin_percentage)",
    yellow: "(program_heatmap_manual_entry.eac_margin_percentage <= .9 * program_metadata_manual_entry.baseline_margin_percentage AND program_heatmap_manual_entry.eac_margin_percentage >= .8 * program_metadata_manual_entry.baseline_margin_percentage)",
    red: "(program_heatmap_manual_entry.eac_margin_percentage < .8 * program_metadata_manual_entry.baseline_margin_percentage)"
  }
};

var getKpiClause = function (kpi, color) {
  if (kpi === "cpar_customer_satisfaction") {
    kpi = 'customer_satisfaction';
  } else if (kpi === "internal_support_suppliers") {
    kpi = 'supplier_status';
  } else if(kpi === "fy_revenue"){
    kpi = 'revenue_color';
  } else if(kpi === 'contract'){
    kpi = 'contractual';
  }

  var kpiClause = 'true';


  if(kpi === "eac_margin"){
      kpi = "program_heatmap_manual_entry.eac_margin_percentage";
      if (color === 'N/A') {
        kpiClause = "(" + kpi + " = 'N/A' OR " + kpi + " = '')";
      } else if (color === 'NOT SUBMITTED') {
        kpiClause = kpi + " IS NULL";
      } else if (color === 'GREEN') {
        kpiClause = "(" + "program_heatmap_manual_entry.margin =" +  " 'G'" + ")";
      } else if (color === 'YELLOW') {
        kpiClause = "(" + "program_heatmap_manual_entry.margin =" +  " 'Y'" + ")";
      } else if (color === 'RED') {
        kpiClause = "(" + "program_heatmap_manual_entry.margin =" +  " 'R'" + ")";
      }
    }
   else {
    kpi = "program_heatmap_manual_entry." + kpi;
    if (color === 'N/A') {
      kpiClause = "(" + kpi + " = 'N/A' OR " + kpi + " = '')";
    } else if (color === 'NOT SUBMITTED') {
      kpiClause = kpi + " IS NULL";
    } else {
      kpiClause = kpi + " = " + "'" + color.charAt(0) + "'";
    }
  }

  return kpiClause;
};

var getCyberKpiClause = function (kpi, color) {
  var cyberKpiClause = 'TRUE';
  kpi = "program_cyber_security." + kpi;
    if (color === 'N/A') {
      cyberKpiClause = "(" + kpi + " = 'N/A' OR " + kpi + " = '')";
    } else if (color === 'NOT SUBMITTED') {
      cyberKpiClause = kpi + " IS NULL";
    } else {
      cyberKpiClause = kpi + " = " + "'" + color.charAt(0) + "'";
    }
  return cyberKpiClause;
};

var getExportKpiClause = function (kpi, color) {
  var exportKpiClause = 'TRUE';
  if (kpi === "foreign_military_sale") {
    kpi = 'foreign_millitary_sale';
  } else if (kpi === "hardware_software") {
    kpi = 'h_s_ware';
  } else if(kpi === "foreign_hardware_software"){
    kpi = 'foreign_h_s_ware';
  }
  kpi = "program_export_international_entry." + kpi;

  if (color === 'N/A') {
    exportKpiClause = "(" + kpi + " = 'N/A' OR " + kpi + " = '')";
  } else if (color === 'NOT SUBMITTED') {
    exportKpiClause = kpi + " IS NULL";
  } else {
    exportKpiClause = kpi + " = " + "'" + color.charAt(0) + "'";
  }
  return exportKpiClause;
};
var getMarginCalc = function() {
  var colors = {
    "green": "'G'",
    "yellow": "'Y'",
    "red": "'R'",
    "not submitted": "'N/S'"
  };
  var marginCalc = "(CASE ";

  var getConditionString = function(condition, color) {
    return "WHEN " + condition + " THEN " + colors[color] + " ";
  };

  _.each(colors, function(abbr, color) {
    marginCalc += getConditionString(getKpiClause("eac_margin", color.toUpperCase()), color);
  });

  marginCalc += "ELSE " + "'N/S'" + " END) ";

  return marginCalc;
};

var getVariables = function (req) {
  if(req.query.staffingDate !== undefined){
    var staffingDates = [];
      staffingDates = req.query.staffingDate.split('-');
      var staffingDate = staffingDates[1]+"/"+staffingDates[2].substring(0,2)+"/"+staffingDates[0];
  }
  var variables = {
    endDate: utils.getPgDate(req.query.endDate),
    fiscalStart: utils.getPgDate(req.query.fiscalStart),
    fiscalEnd: utils.getPgDate(req.query.fiscalEnd),
    fiscalGDITEnd: utils.getPgDate(req.query.fiscalGDITEnd),
    staffingDate: utils.getPgDate(staffingDate),
    currentFYStart: req.query.currentFYStart,
    currentFYEnd: req.query.currentFYEnd
  };
  var exclusiveRole = false;
  var executiveRole = false;
  variables.salesforceUrl = config.salesforceUrl;
  variables.groupClause = req.query.group_name ? "program_metadata_manual_entry.group = '" +
    req.query.group_name + "'" : "TRUE";

  variables.divisionClause = req.query.division_name ? "program_metadata_manual_entry.division = '" +
    req.query.division_name + "'" : "TRUE";

  variables.programClause = req.query.project_id ? "programs_secure.project_id = '" +
    req.query.project_id + "'" : "TRUE";

  variables.programManagerClause = req.query.program_manager ?
    "program_metadata_manual_entry.program_manager = '" +  req.query.program_manager + "'" : "TRUE";

  variables.l4ManagerClause = req.query.l4_manager ?
    "program_metadata_manual_entry.l4_manager = '" +  req.query.l4_manager + "'" : "TRUE";

  variables.losClause = req.query.l3_portfolio ? "program_metadata_manual_entry.l3_portfolio = '" +
    req.query.l3_portfolio + "'" : "TRUE";

  variables.praClause = req.query.pra ?  "program_metadata_manual_entry.pra = '" +
    req.query.pra + "'" : "TRUE";

  variables.statusClause = "TRUE";
  if (req.query.program_status === undefined || req.query.program_status === 'false') {
    variables.statusClause = "program_metadata_manual_entry.program_status = 'open'"
  }
// Checks the current PM for the program. The query was picking up old PM's data and displaying wrong ribbon
  variables.metaPMClause = req.query.program_manager ?
    "program_metadata_manual_entry.program_manager = (SELECT tempMeta.program_manager FROM program_metadata_manual_entry as tempMeta WHERE tempMeta.program_id = program_metadata_manual_entry.program_id ORDER BY tempMeta.created_at DESC LIMIT 1)" : "TRUE";

  variables.l2ManagerClause = "TRUE";

  variables.l3ManagerClause = "TRUE";

  // This is extra l2,l3 clauses for program_financials_summary_bar.
  // It needs separate clauses for it because it effects on the other sql as well.
  variables.l2_ManagerClause = req.query.l2_manager ? "program_metadata_manual_entry.l2_manager = '" +
   req.query.l2_manager + "'" : "TRUE";

  variables.l3_ManagerClause = req.query.l3_manager ? "program_metadata_manual_entry.l3_manager = '" +
   req.query.l3_manager + "'" : "TRUE";

  variables.marginCalc = getMarginCalc();

  variables.kpiClause = req.query.program_status_kpi ?
    getKpiClause(req.query.program_status_kpi, req.query.program_status_kpi_color) : "TRUE";

  variables.cyberKpiClause = req.query.program_cyber_kpi ?
    getCyberKpiClause(req.query.program_cyber_kpi, req.query.program_cyber_kpi_color) : "TRUE";
  variables.exportKpiClause = req.query.program_export_kpi ?
      getExportKpiClause(req.query.program_export_kpi, req.query.program_export_kpi_color) : "TRUE";

  // TODO pmr_file_posted is a placeholder column in the future we should just disabled sorting on
  // certain columns on the frontend
  variables.order = req.query.sort && req.query.sort != 'pmr_file_posted' ? "ORDER BY \"" + req.query.sort + "\" " + req.query.order : "";

  variables.limit = req.query.limit ? "LIMIT " + req.query.limit : "";

  variables.offset = req.query.offset ? "OFFSET " + req.query.offset : "";

  for(var i = 0; i < req.user.roles.length; i++){
    if(req.user.roles[i].dataValues.name == 'Administrator'){
      exclusiveRole = true;
    }
    if(req.user.roles[i].dataValues.name == 'Executive'){
      executiveRole = true;
    }
  }
  variables.roleClause = "";
  variables.userClause = "TRUE";
  if(exclusiveRole == true){
    variables.executiveDivisionClause = "TRUE";
  }
  if(executiveRole == true && exclusiveRole !== true){
    var tempDivision = [];
    _.forEach(req.user.divisions, function(division){
      tempDivision.push("'"+division+"'");
    });
    _.forEach(req.user.programs, function(program){
  if(!_.includes(tempDivision, "'"+program.dataValues.division+"'") && program.dataValues.division != null){
    tempDivision.push("'"+ program.dataValues.division +"'");
      }
    });
    // variables.roleClause = "LEFT JOIN users_programs as user_check ON user_check.program_id = programs_secure.id";
    // variables.userClause = "user_check.user_id ='" + req.user.id+ "'";
    variables.executiveDivisionClause = req.query.l1 ? "program_metadata_manual_entry.division in (" + tempDivision + ")": "TRUE";
    if(req.query.project_id){
      variables.executiveDivisionClause = "TRUE";

    }
  }
  if(exclusiveRole !== true && executiveRole !== true){
      variables.roleClause = "LEFT JOIN users_programs as user_check ON user_check.program_id = programs_secure.id";
      variables.userClause = "user_check.user_id ='" + req.user.id+ "'";
      variables.executiveDivisionClause  = "TRUE";
  }
  return variables;
};
//This function is the one that changes the display data based on the input in the filter.
//
var getFilterQuery = function(table, column, query, dependsOn, req) {

  var adminBool = false;
  _.each(req.user.roles, function(role){
    if(role.name == "Administrator") adminBool = true;
  });
  var queryForJoining = '';
  var queryForDependingFields = '';
  _.forEach(dependsOn, function(dependingFilter) {
    if (dependingFilter.nameColumn === 'program_status') {
      if (dependingFilter.selectedFilter === 'open') {
        queryForJoining += ' INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) * ' +
                           ' FROM program_metadata_manual_entry ' +
                           ' WHERE program_metadata_manual_entry.validated = TRUE ' +
                           ' ORDER BY program_id, created_at DESC) program_metadata_manual_entry ' +
                           ' ON programs_secure.id = program_metadata_manual_entry.program_id ';
        queryForDependingFields += ' AND ' + dependingFilter.nameColumn + " = '" + dependingFilter.selectedFilter + "' ";
      }
    }
    else {

      queryForDependingFields += ' AND ' + dependingFilter.nameColumn + " ILIKE '" + dependingFilter.selectedFilter + "%' ";
    }
  });
if(column == 'project_id, pmr_group, division'){
 return "SELECT DISTINCT " + "project_id, pmr_group, programs_secure.division" + " FROM " + table + queryForJoining + " WHERE " + 'project_id' + " ILIKE '" +
    "%" + query + "%' " + queryForDependingFields + " ORDER BY " + column;
}
else if(column == 'l3_manager, division_name'){
 return "SELECT DISTINCT " + column + " FROM " + table + queryForJoining + " WHERE " + 'l3_manager' + " ILIKE '" +
    "%" + query + "%' " + queryForDependingFields + " ORDER BY " + column + " LIMIT 200";
}
else if(column == 'l4_manager, division, program_id'){
  if(adminBool == true){
      return "SELECT DISTINCT l4_manager"  + " FROM " + table + queryForJoining + " WHERE " + 'l4_manager' + " ILIKE '" +
       "%" + query + "%' " + queryForDependingFields + " ORDER BY l4_manager";
   }
   else{
     return "SELECT DISTINCT ON (program_id) " + column + ", created_at FROM " + table + queryForJoining + " WHERE " + 'l4_manager' + " ILIKE '" +
        "%" + query + "%' " + queryForDependingFields + " ORDER BY program_id, created_at DESC";
   }
 }
 else if(column == 'program_manager, division, program_id'){
   if(adminBool == true){
       return "SELECT DISTINCT program_manager"  + " FROM " + table + queryForJoining + " WHERE " + 'program_manager' + " ILIKE '" +
        "%" + query + "%' " + queryForDependingFields + " ORDER BY program_manager";
    }
    else{
      return "SELECT DISTINCT ON (program_id) " + column + ",created_at FROM " + table + queryForJoining + " WHERE " + 'program_manager' + " ILIKE '" +
         "%" + query + "%' " + queryForDependingFields + " ORDER BY program_id, created_at DESC";
    }
  }
  else if(column == 'pra, division, program_id'){
    if(adminBool == true){
        return "SELECT DISTINCT pra"  + " FROM " + table + queryForJoining + " WHERE " + 'pra' + " ILIKE '" +
         "%" + query + "%' " + queryForDependingFields + " ORDER BY pra";
     }
     else{
       return "SELECT DISTINCT ON (program_id) " + column + ", created_at FROM " + table + queryForJoining + " WHERE " + 'l4_manager' + " ILIKE '" +
          "%" + query + "%' " + queryForDependingFields + " ORDER BY program_id, created_at DESC";
     }
   }

else{
  return "SELECT DISTINCT " + column + " FROM " + table + queryForJoining + " WHERE " + column + " ILIKE '" +
    "%" + query + "%' " + queryForDependingFields + " ORDER BY " + column + " LIMIT 200";
}

};

var queriesDir = "/server/api/analytics/programs/queries/";
exports.quadChartOverview = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_quadchart_overview.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.kpiSummaryOverview = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_summary_kpi_overview.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.kpiSummary = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_summary_kpi.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.programsByStatusOverview = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_status_overview.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.cyberKpiSummaryOverview = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_cyber_kpi_overview.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.cyberKpiSummary = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_cyber_kpi.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.programCyberSecurityOverview = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_cyber_security_overview.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};


exports.programMetadata = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_metadata.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);

};

exports.exportKpiSummaryOverview = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_export_kpi_overview.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.exportKpiSummary = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_export_kpi.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.programExportInternationalOverview = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_export_international_overview.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.organizationDetail = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_organization_detail.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};
exports.financialDetail = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_financial_detail.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};
exports.contractDetail = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_contract_detail.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};
exports.staffingPosition = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "staffing_position.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};
exports.staffingVacancy = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "staffing_vacancy.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};


exports.programMetadataList = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_metadata_list.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.programMetadataCyber = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_metadata_cyber.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.programMetadataExport = function (req, res) {
  var path = utils.getPathFromRoot(queriesDir + "program_metadata_export.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.revenueCostsTimeline = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "revenue_costs_timeline.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.revenueCostsProfit = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "revenue_costs_profit.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.revenueCostsSummary = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "revenue_costs_summary.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.investmentSummary = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "investment_summary.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.fundingSummary = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "funding_summary.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.profitTimeline = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "profit_timeline.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.salesforceTaskList = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "salesforce_task_list.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.programFinancialsSummaryBar = function(req, res) {
  console.log("heaven",req.query);

  var path = utils.getPathFromRoot(queriesDir + "program_financials_summary_bar.sql");
  var variables = getVariables(req);


  // if(variables.programClause === "TRUE") {
  //   if(variables.programManagerClause != "TRUE"){
  //     variables.programClause = "TRUE";
  //   }
  //   else{
  //     variables.programClause = "FALSE";
  //   }
  // }

  utils.processGeneralRequest(path, variables, req, res);
};

exports.financialsDsoTrend = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "financials_dso_trend.sql");
  var variables = getVariables(req);

  // if(variables.programClause === "TRUE") {
  //   if(variables.programManagerClause != "TRUE"){
  //     variables.programClause = "TRUE";
  //   }
  //   else{
  //     variables.programClause = "FALSE";
  //   }
  // }

  utils.processGeneralRequest(path, variables, req, res);
};

exports.staffingChangeByMonth = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "staffing_change_by_month.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.staffingFullTimeEmployees = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "staffing_full_time_employees.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.getFinancialReport = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "financial_report.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};
exports.getRibbonReport = function(req, res) {
  console.log("hell",req.query);
  var path = utils.getPathFromRoot(queriesDir + "ribbon_report.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.getStaffingReport = function(req, res) {
  var path = utils.getPathFromRoot(queriesDir + "staffing_report.sql");
  utils.processGeneralRequest(path, getVariables(req), req, res);
};

exports.programFilters = function (req, res) {
  var column = req.query.column;
  var query = req.query.query || '';

  var statement = getFilterQuery('programs_secure', column, query, JSON.parse(req.query.dependsOn), req);

  if (!column) {
    res.status(500).send('No column selected');
  }

  Sequelize.transaction(function (t) {
    return Sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return Sequelize.query(statement,  {type: Sequelize.QueryTypes.SELECT, transaction: t});
    }).catch(function (err) {
      return handleError(res, err);
    });
  }).then(function (programs) {
    return res.status(200).json(programs);
  }).catch(function (err) {
    return handleError(res, err);
  });

};

var getFilters = function(req, res, table) {
  var column = req.query.column;
  var query = req.query.query || '';
  var dependsOn = JSON.parse(req.query.dependsOn);

  var statement = getFilterQuery(table, column, query, dependsOn, req);

  if (!column) {
    res.status(500).send('No column selected');
  }

  Sequelize.query(statement,  {
    type: Sequelize.QueryTypes.SELECT
  }).then(function (filters) {
    res.json(filters);
  }).catch(function (err) {
    handleError(res, err);
  });
};

exports.programMetadataFilters = function (req, res) {
  getFilters(req, res, 'program_metadata_manual_entry');
};

exports.programGroupFilters = function (req, res) {
  getFilters(req, res, 'pmr_groups');
};

exports.programGroupDivisionFilters = function (req, res) {
  getFilters(req, res, 'pmr_groups_divisions_l2_manager');
};

exports.permastManagerFilters = function (req, res) {

};

function handleError (res, err) {
  console.log(err);
  res.send(500, err);
}
