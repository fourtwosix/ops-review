SELECT
security_authorization,
scanning,
flaw_remediation,
inventory_status,
asset_status,
external_access,
device_hardening,
security_event_management,
identified_security_roles_responsibilities,
account_management,
contract_security_requirements,
overall
FROM programs_secure
  INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
              FROM program_metadata_manual_entry
              WHERE program_metadata_manual_entry.validated = TRUE
              ORDER BY program_id, created_at DESC) program_metadata_manual_entry
    ON programs_secure.id = program_metadata_manual_entry.program_id
  LEFT JOIN program_cyber_security
    ON program_cyber_security.program_id = programs_secure.id
    AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
        date_trunc('month', fiscal_month_end)
    AND program_cyber_security.validated = TRUE
    AND program_cyber_security.type = 'actual'
    <%- roleClause %>
WHERE <%-losClause %> AND <%- userClause %> AND <%-l4ManagerClause %> AND <%-programManagerClause %> AND <%-praClause %> AND <%-l3_ManagerClause %> AND <%-l2_ManagerClause %>  AND <%- divisionClause %> AND <%- groupClause %> AND <%- statusClause %>     AND <%- executiveDivisionClause %>
