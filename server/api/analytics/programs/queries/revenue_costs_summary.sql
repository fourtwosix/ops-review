WITH revenue_summary AS (
  SELECT DISTINCT
      program_financial_status.fiscal_month_end,
      program_financial_status.type,
  	SUM(program_financial_status.funded) as funded,
  	SUM(program_financial_status."fyForecast") as fy_forecast,
  	SUM(program_financial_status."fyBudget") as fy_budget,
  	CASE WHEN(program_financial_status.type = 'reserve') THEN NULL ELSE round(((SUM(COALESCE(program_financial_status."fyForecast", 0)) - SUM(program_financial_status."fyBudget")) /NULLIF(SUM(program_financial_status."fyBudget"),0))*100,1) END as fy_variance,
    SUM(program_financial_status."fyActualsToDate") as fy_actuals_to_date,
  	SUM(program_financial_status."fyBudgetToDate") as fy_budget_to_date,
  	SUM(program_financial_status.itdactuals) as itd_actuals,
  	SUM(program_financial_status.etc) as etc,
  	SUM(program_financial_status.eac) as eac,
  	SUM(program_financial_status.unexercised_options) as unexercised_options,
  	SUM(program_financial_status.final_mgmt_review) as final_mgmt_review

    FROM programs_secure


    INNER JOIN (SELECT distinct on (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id
    LEFT JOIN program_financial_status
      ON programs_secure.id = program_financial_status.program_id
         AND program_financial_status.type IN
             ('revenue', 'margin', 'reserve', 'hours')
         AND date_trunc('month', program_financial_status.fiscal_month_end) >=
             date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
         AND date_trunc('month', program_financial_status.fiscal_month_end) <=
             date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
<%- roleClause %>
  WHERE
    <%-programManagerClause %>
    AND <%- executiveDivisionClause %>
    AND <%-userClause %>
    AND <%- groupClause %>
    AND <%-metaPMClause %>
    AND <%-praClause%>
    AND <%- l2_ManagerClause %>
    AND <%- l3_ManagerClause%>
    AND <%- l4ManagerClause %>
    AND <%- divisionClause %>
    AND <%-programClause %>
    AND program_metadata_manual_entry.validated = TRUE
    AND <%- statusClause %>
       GROUP BY program_financial_status.type, program_financial_status.fiscal_month_end
)
SELECT revenue_total.type,
CASE WHEN revenue_total.funded IS NULL THEN NULL
ELSE concat('$',to_char(revenue_total.funded, '9,999,999,999,999,999')) END AS funded,

CASE WHEN revenue_total.fy_budget IS NULL THEN NULL
     WHEN (revenue_total.type = 'margin %') THEN concat(to_char(revenue_total.fy_budget, '99999999.9'), '%')
     WHEN (revenue_total.type = 'hours') THEN  to_char(revenue_total.fy_budget,  '9,999,999,999,999,999')
ELSE
concat('$',to_char(revenue_total.fy_budget, '9,999,999,999,999,999')) END AS fy_budget,

CASE WHEN revenue_total.fy_forecast IS NULL THEN NULL
     WHEN (revenue_total.type = 'margin %') THEN concat(to_char(revenue_total.fy_forecast, '99999999.9'), '%')
     WHEN (revenue_total.type = 'hours') THEN  to_char(revenue_total.fy_forecast,  '9,999,999,999,999,999')
ELSE
concat('$',to_char(revenue_total.fy_forecast, '9,999,999,999,999,999,999')) END AS fy_forecast,

CASE WHEN revenue_total.fy_variance IS NULL THEN NULL ELSE concat(to_char(revenue_total.fy_variance, '999999999.9'),'%') END AS fy_variance,

CASE WHEN revenue_total.fy_actuals_to_date IS NULL THEN NULL
     WHEN (revenue_total.type = 'margin %') THEN concat(to_char(revenue_total.fy_actuals_to_date, '99999999.9'), '%')
     WHEN (revenue_total.type = 'hours') THEN  to_char(revenue_total.fy_actuals_to_date,  '9,999,999,999,999,999')
ELSE
concat('$',to_char(revenue_total.fy_actuals_to_date,  '9,999,999,999,999,999')) END AS fy_actuals_to_date,


CASE WHEN revenue_total.fy_budget_to_date IS NULL THEN NULL
     WHEN (revenue_total.type = 'margin %') THEN concat(to_char(revenue_total.fy_budget_to_date, '99999999.9'), '%')
     WHEN (revenue_total.type = 'hours') THEN  to_char(revenue_total.fy_budget_to_date,  '9,999,999,999,999,999')
     WHEN (revenue_total.final_mgmt_review < 0) THEN concat('-$',to_char(revenue_total.final_mgmt_review * (-1),  '9,999,999,999,999,999'))

ELSE
concat('$',to_char(revenue_total.fy_budget_to_date,  '9,999,999,999,999,999')) END AS fy_budget_to_date,


CASE WHEN revenue_total.itd_actuals IS NULL THEN NULL
     WHEN (revenue_total.type = 'margin %') THEN concat(to_char(revenue_total.itd_actuals, '99999999.9'), '%')
     WHEN (revenue_total.type = 'hours') THEN  to_char(revenue_total.itd_actuals,  '9,999,999,999,999,999')
     WHEN (revenue_total.itd_actuals < 0) THEN concat('-$',to_char(revenue_total.itd_actuals * (-1),  '9,999,999,999,999,999'))

ELSE
concat('$',to_char(revenue_total.itd_actuals,  '9,999,999,999,999,999')) END AS itd_actuals,


CASE WHEN revenue_total.etc IS NULL THEN NULL
     WHEN (revenue_total.type = 'margin %') THEN concat(to_char(revenue_total.etc, '99999999.9'), '%')
     WHEN (revenue_total.type = 'hours') THEN  to_char(revenue_total.etc,  '9,999,999,999,999,999')
     WHEN (revenue_total.etc < 0) THEN concat('-$',to_char(revenue_total.etc * (-1),  '9,999,999,999,999,999'))

ELSE
concat('$',to_char(revenue_total.etc,  '9,999,999,999,999,999')) END AS _etc_,


CASE WHEN revenue_total.eac IS NULL THEN NULL
     WHEN (revenue_total.type = 'margin %') THEN concat(to_char(revenue_total.eac, '99999999.9'), '%')
     WHEN (revenue_total.type = 'hours') THEN  to_char(revenue_total.eac,  '9,999,999,999,999,999')
     WHEN (revenue_total.eac < 0) THEN concat('-$',to_char(revenue_total.eac * (-1),  '9,999,999,999,999,999'))

ELSE
concat('$',to_char(revenue_total.eac,  '9,999,999,999,999,999')) END AS _eac_,


CASE WHEN revenue_total.unexercised_options IS NULL THEN NULL
     WHEN (revenue_total.type = 'margin %') THEN concat(to_char(revenue_total.unexercised_options, '99999999.9'), '%')
     WHEN (revenue_total.type = 'hours') THEN  to_char(revenue_total.unexercised_options,  '9,999,999,999,999,999')
     WHEN (revenue_total.unexercised_options < 0) THEN concat('-$',to_char(revenue_total.unexercised_options * (-1),  '9,999,999,999,999,999'))

ELSE
concat('$',to_char(revenue_total.unexercised_options,  '9,999,999,999,999,999')) END AS unexercised_options,


CASE WHEN revenue_total.final_mgmt_review IS NULL THEN NULL
     WHEN (revenue_total.type = 'margin %') THEN concat(to_char(revenue_total.final_mgmt_review, '99999999.9'), '%')
     WHEN (revenue_total.type = 'hours') THEN  to_char(revenue_total.final_mgmt_review,  '9,999,999,999,999,999')
     WHEN (revenue_total.final_mgmt_review < 0) THEN concat('-$',to_char(revenue_total.final_mgmt_review * (-1),  '9,999,999,999,999,999'))
ELSE
concat('$',to_char(revenue_total.final_mgmt_review,  '9,999,999,999,999,999')) END AS "Final Management Review"
FROM (
SELECT revenue_summary.*
FROM revenue_summary
UNION
(SELECT
		revenue_summary.fiscal_month_end,
		'cost' as type,
		NULL as funded,
		COALESCE(revenue_values.fy_forecast,0) - COALESCE(margin_values.fy_forecast,0)as fy_forecast ,
		COALESCE(revenue_values.fy_budget,0) - COALESCE(margin_values.fy_budget,0) as fy_budget,
    round(((COALESCE(revenue_values.fy_forecast,0) - COALESCE(margin_values.fy_forecast,0)) -
    (COALESCE(revenue_values.fy_budget,0) - COALESCE(margin_values.fy_budget,0)))/
     NULLIF((COALESCE(revenue_values.fy_budget,0) - COALESCE(margin_values.fy_budget,0)),0) * 100 , 1) as fy_variance,
    COALESCE(revenue_values.fy_actuals_to_date,0) - COALESCE(margin_values.fy_actuals_to_date,0) as fy_actuals_to_date,
		COALESCE(revenue_values.fy_budget_to_date,0) - COALESCE(margin_values.fy_budget_to_date,0) as fy_budget_to_date,
		COALESCE(revenue_values.itd_actuals,0) - COALESCE(margin_values.itd_actuals,0) as itd_actuals,
		COALESCE(revenue_values.etc,0) - COALESCE(margin_values.etc,0) as etc,
		COALESCE(revenue_values.eac,0) - COALESCE(margin_values.eac,0) as eac,
		COALESCE(revenue_values.unexercised_options,0) - COALESCE(margin_values.unexercised_options,0) as unexercised_options,
		COALESCE(revenue_values.final_mgmt_review,0) - COALESCE(margin_values.final_mgmt_review,0) as final_mgmt_review

		FROM revenue_summary
		LEFT JOIN (SELECT revenue_summary.*
					FROM revenue_summary
					WHERE revenue_summary.type = 'margin'
					) as margin_values
          ON
						revenue_summary.fiscal_month_end = margin_values.fiscal_month_end
		LEFT JOIN (SELECT revenue_summary.*
				FROM revenue_summary
				WHERE revenue_summary.type = 'revenue'
				) as revenue_values
        ON
					revenue_summary.fiscal_month_end = revenue_values.fiscal_month_end
    WHERE revenue_summary.fiscal_month_end IS NOT NULL
)
UNION
(SELECT
		revenue_summary.fiscal_month_end,
		'margin %' as type,
		NULL as funded,
		round((margin_values.fy_forecast) / NULLIF(revenue_values.fy_forecast,0) * 100, 1)as fy_forecast ,
		round((margin_values.fy_budget) / NULLIF(revenue_values.fy_budget,0)*100, 1) as fy_budget,






    round((
		 (margin_values.fy_forecast) / NULLIF(revenue_values.fy_forecast,0) * 100
     -
     (margin_values.fy_budget) / NULLIF(revenue_values.fy_budget,0) * 100
    )
		/ NULLIF(
      (margin_values.fy_budget) / NULLIF(revenue_values.fy_budget,0)
      ,0), 1)
     as fy_variance,





    round((margin_values.fy_actuals_to_date) / NULLIF(revenue_values.fy_actuals_to_date,0) * 100, 1) as fy_actuals_to_date,

		round((margin_values.fy_budget_to_date) / NULLIF(revenue_values.fy_budget_to_date,0) * 100, 1) as fy_budget_to_date,
		round((margin_values.itd_actuals) / NULLIF(revenue_values.itd_actuals,0) * 100, 1) as itd_actuals,
		round((margin_values.etc) / NULLIF(revenue_values.etc,0) * 100, 1) as etc,
		round((margin_values.eac) / NULLIF(revenue_values.eac,0) * 100, 1) as eac,
		round((margin_values.unexercised_options) / NULLIF(revenue_values.unexercised_options,0) * 100, 1) as unexercised_options,
		round((margin_values.final_mgmt_review) / NULLIF(revenue_values.final_mgmt_review,0) * 100, 1) as final_mgmt_review

		FROM revenue_summary
		LEFT JOIN (SELECT revenue_summary.*
					FROM revenue_summary
					WHERE revenue_summary.type = 'margin'
					) as margin_values
          ON
						revenue_summary.fiscal_month_end = margin_values.fiscal_month_end
		LEFT JOIN (SELECT revenue_summary.*
				FROM revenue_summary
				WHERE revenue_summary.type = 'revenue'
				) as revenue_values
        ON
					revenue_summary.fiscal_month_end = revenue_values.fiscal_month_end
    WHERE revenue_summary.fiscal_month_end IS NOT NULL
))revenue_total
ORDER BY (CASE WHEN revenue_total.type = 'revenue' THEN 0
                WHEN revenue_total.type = 'cost' THEN 1
                WHEN revenue_total.type = 'margin' THEN 2
                WHEN revenue_total.type = 'margin %' THEN 3
                WHEN revenue_total.type = 'reserve' THEN 4
                WHEN revenue_total.type = 'hours' THEN 5 END)
