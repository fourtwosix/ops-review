SELECT *
FROM (
  SELECT
    programs_secure.project_id                         AS program_id,
    programs_secure.name                               AS program_name,
    '<a target="blank" href="<%- salesforceUrl %>/' || 'one/one.app#/alohaRedirect/apex/GAP_STD_Dashboard?id=' || gap_id ||
    '">' || gap_project_id || '</a>' AS url
  FROM salesforce_gap_projects
    INNER JOIN programs_secure ON salesforce_gap_projects.program_id = programs_secure.id
    INNER JOIN
    (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
     FROM program_metadata_manual_entry
     WHERE validated = TRUE
     ORDER BY program_id, created_at DESC)
    program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
     <%- roleClause %>
  WHERE <%-programClause %>
        AND <%- userClause %>
        AND <%-programManagerClause %>
        AND <%-losClause %>
        AND <%-praClause %>
        AND <%-l2_ManagerClause %>
        AND <%-groupClause %>
        AND <%- executiveDivisionClause %>
        AND <%-l3_ManagerClause %>
        AND <%-l4ManagerClause %>
        AND <%-divisionClause %>
        AND <%-statusClause %>
  ORDER BY programs_secure.project_id ASC

        <%- limit %> <%- offset %>
) table_query <%- order %>
