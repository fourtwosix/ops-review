SELECT * FROM (
  SELECT
    program_metadata_manual_entry.program_manager,
    program_metadata_manual_entry.pra,
    program_metadata_manual_entry.pm_email,
    program_metadata_manual_entry.group as "Division",
    program_metadata_manual_entry.division AS "Sector",
    program_metadata_manual_entry.business_area,
    program_metadata_manual_entry.l2_manager as "Division Manager",
    program_metadata_manual_entry.l3_manager as "Sector Manager",
    program_metadata_manual_entry.l4_manager as "Business Area Manager"
  FROM programs_secure
    INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id
    LEFT JOIN program_heatmap_manual_entry
      ON program_heatmap_manual_entry.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', fiscal_month_end)
        AND program_heatmap_manual_entry.validated = TRUE
        AND program_heatmap_manual_entry.type = 'actual'
     <%- roleClause %>
  WHERE <%- groupClause %>
    AND <%- executiveDivisionClause %>
    AND <%- userClause %>
    AND <%- l2_ManagerClause%>
    AND <%- l3_ManagerClause%>
    AND <%- l2ManagerClause %>
    AND <%- divisionClause %>
    AND <%- l3ManagerClause %>
    AND <%- programManagerClause %>
    AND <%- l4ManagerClause %>
    AND <%- programClause %>
    AND <%- losClause %>
    AND <%- praClause %>
    AND <%- statusClause %>
    AND <%-l2_ManagerClause %>
    AND <%-l3_ManagerClause %>
  ORDER BY program_metadata_manual_entry.program_id,
           program_heatmap_manual_entry.program_id,
           program_metadata_manual_entry.created_at DESC

  <%- limit %> <%- offset %>
) table_query <%- order %>
