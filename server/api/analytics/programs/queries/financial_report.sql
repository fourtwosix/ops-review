WITH metadata AS (
  SELECT
    metadata.program_id,
    programs.project_id,
    programs.name,
    metadata.group,
    metadata.division,
    metadata.contract_type,
    metadata.baseline_margin_percentage,
    metadata.program_status
  FROM
    programs
    INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) metadata
      ON programs.id = metadata.program_id
),
    heatmaps AS (
    SELECT DISTINCT ON (program_heatmap_manual_entry.program_id, fiscal_month_end)
      program_heatmap_manual_entry.program_id,
      fiscal_month_end,
      contractual,
      technical,
      cost,
      schedule,
      customer_satisfaction,
      CASE
      WHEN lower(contract_type) LIKE '%ffp%'
        THEN
          CASE
          WHEN (staffing_percentage >= 90 AND staffing_percentage <= 105)
            THEN 'G'
          WHEN (staffing_percentage >= 80 AND staffing_percentage < 90)
            THEN 'Y'
          WHEN (staffing_percentage > 105 OR staffing_percentage < 80)
            THEN 'R'
          END
      ELSE
        CASE
        WHEN (staffing_percentage > 95)
          THEN 'G'
        WHEN (staffing_percentage >= 85 AND staffing_percentage <= 95)
          THEN 'Y'
        WHEN (staffing_percentage < 85)
          THEN 'R'
        END
      END             AS staffing_plan_pct,
      (CASE WHEN ((program_heatmap_manual_entry.eac_margin_percentage >
                   .9 * program_metadata_manual_entry.baseline_margin_percentage) AND NOT ((
                                                                                             program_heatmap_manual_entry.eac_margin_percentage
                                                                                             <= .9 *
                                                                                                program_metadata_manual_entry.baseline_margin_percentage
                                                                                             AND
                                                                                             program_heatmap_manual_entry.eac_margin_percentage
                                                                                             >= .8 *
                                                                                                program_metadata_manual_entry.baseline_margin_percentage)
                                                                                           OR (
                                                                                             program_heatmap_manual_entry.eac_margin_percentage
                                                                                             > 3 AND
                                                                                             program_heatmap_manual_entry.eac_margin_percentage
                                                                                             <= 5) OR (
                                                                                             program_heatmap_manual_entry.eac_margin_percentage
                                                                                             < .8 *
                                                                                               program_metadata_manual_entry.baseline_margin_percentage)
                                                                                           OR (
                                                                                             program_heatmap_manual_entry.eac_margin_percentage
                                                                                             <= 3)))
        THEN 'G'
       WHEN (((program_heatmap_manual_entry.eac_margin_percentage <=
               .9 * program_metadata_manual_entry.baseline_margin_percentage AND
               program_heatmap_manual_entry.eac_margin_percentage >=
               .8 * program_metadata_manual_entry.baseline_margin_percentage) OR
              (program_heatmap_manual_entry.eac_margin_percentage > 3 AND
               program_heatmap_manual_entry.eac_margin_percentage <= 5)) AND NOT (
         (program_heatmap_manual_entry.eac_margin_percentage <
          .8 * program_metadata_manual_entry.baseline_margin_percentage) OR
         (program_heatmap_manual_entry.eac_margin_percentage <= 3)))
         THEN 'Y'
       WHEN ((program_heatmap_manual_entry.eac_margin_percentage <
              .8 * program_metadata_manual_entry.baseline_margin_percentage) OR
             (program_heatmap_manual_entry.eac_margin_percentage <= 3))
         THEN 'R'
       WHEN program_heatmap_manual_entry.eac_margin_percentage IS NULL
         THEN NULL
       ELSE NULL END) AS margin,
      supplier_status,
      growth,
      overall_project
    FROM
      program_heatmap_manual_entry
      INNER JOIN metadata program_metadata_manual_entry
        ON program_heatmap_manual_entry.program_id = program_metadata_manual_entry.program_id
    WHERE
      program_heatmap_manual_entry.validated = TRUE AND
      program_heatmap_manual_entry.type = 'actual'
    ORDER BY
      program_heatmap_manual_entry.program_id, fiscal_month_end DESC
  ),

    salesforce_financials AS (
    SELECT
      program_months.program_id,
      program_months.fiscal_month  AS fiscal_month_end,
      total_revenue_budget.value   AS total_revenue_budget,
      total_oi_budget.value        AS total_oi_budget,
      total_revenue_forecast.value AS total_revenue_forecast,
      total_oi_forecast.value      AS total_oi_forecast
    FROM
      (SELECT DISTINCT
         program_id,
         fiscal_month
       FROM salesforce_financials) program_months
      LEFT JOIN
      (SELECT salesforce_financials.*
       FROM salesforce_financials
       WHERE type = 'totalRevenueBudget') total_revenue_budget
        ON
          program_months.program_id = total_revenue_budget.program_id AND
          program_months.fiscal_month = total_revenue_budget.fiscal_month
      LEFT JOIN
      (SELECT salesforce_financials.*
       FROM salesforce_financials
       WHERE type = 'totalOiBudget') total_oi_budget
        ON
          program_months.program_id = total_oi_budget.program_id AND
          program_months.fiscal_month = total_oi_budget.fiscal_month
      LEFT JOIN
      (SELECT salesforce_financials.*
       FROM salesforce_financials
       WHERE type = 'totalRevenueForecast') total_revenue_forecast
        ON
          program_months.program_id = total_revenue_forecast.program_id AND
          program_months.fiscal_month = total_revenue_forecast.fiscal_month
      LEFT JOIN
      (SELECT salesforce_financials.*
       FROM salesforce_financials
       WHERE type = 'totalOiForecast') total_oi_forecast
        ON
          program_months.program_id = total_oi_forecast.program_id AND
          program_months.fiscal_month = total_oi_forecast.fiscal_month
    WHERE
      total_revenue_budget.value IS NOT NULL OR
      total_oi_budget.value IS NOT NULL OR
      total_revenue_forecast.value IS NOT NULL OR
      total_oi_forecast.value IS NOT NULL
  ),
    staffing AS (
    SELECT
      DISTINCT ON (program_id, fiscal_month_end) *
    FROM (
           SELECT
             COALESCE(csra_fte.type, sub_fte.type)                         AS type,
             COALESCE(csra_fte.program_id, sub_fte.program_id)             AS program_id,
             COALESCE(csra_fte.fiscal_month_end, sub_fte.fiscal_month_end) AS fiscal_month_end,
             COALESCE(csra_fte.fte_month, sub_fte.fte_month)               AS fte_month,
             csra_fte.fte                                                  AS csra_fte_count,
             sub_fte.fte                                                   AS sub_fte_count,
             csra_fte.fte / NULLIF(csra_fte.fte + sub_fte.fte, 0)          AS csra_fte_pct,
             sub_fte.fte / NULLIF(csra_fte.fte + sub_fte.fte, 0)           AS sub_fte_pct
           FROM
             (
               SELECT DISTINCT ON (program_id, fte_month, type) *
               FROM

                 (
                   (SELECT DISTINCT ON (program_id, fte_month) *
                    FROM
                      program_staffing
                    WHERE
                      fte IS NOT NULL AND
                      staffing_source = 'csra' AND
                      type = 'actual'
                    ORDER BY program_id, fte_month, fiscal_month_end DESC)

                   UNION

                   (SELECT DISTINCT ON (program_id, fte_month) *
                    FROM
                      program_staffing
                    WHERE
                      fte IS NOT NULL AND
                      staffing_source = 'csra' AND
                      type = 'forecast'
                    ORDER BY program_id, fte_month, fiscal_month_end DESC)
                 ) csra_fte
               ORDER BY program_id, fte_month, type ASC
             ) csra_fte
             FULL OUTER JOIN
             (
               SELECT DISTINCT ON (program_id, fte_month, type) *
               FROM
                 (
                   (SELECT DISTINCT ON (program_id, fte_month) *
                    FROM
                      program_staffing
                    WHERE
                      fte IS NOT NULL AND
                      staffing_source = 'sub' AND
                      type = 'actual'
                    ORDER BY program_id, fte_month, fiscal_month_end DESC)

                   UNION

                   (SELECT DISTINCT ON (program_id, fte_month) *
                    FROM
                      program_staffing
                    WHERE
                      fte IS NOT NULL AND
                      staffing_source = 'sub' AND
                      type = 'forecast'
                    ORDER BY program_id, fte_month, fiscal_month_end DESC)
                 ) sub_fte
               ORDER BY program_id, fte_month, type ASC
             ) sub_fte
               ON
                 csra_fte.program_id = sub_fte.program_id AND
                 csra_fte.fiscal_month_end = sub_fte.fiscal_month_end AND
                 csra_fte.fte_month = sub_fte.fte_month
         ) staffing_subquery
    ORDER BY program_id, fiscal_month_end, type DESC
  ),
    latest_forecasts AS (SELECT DISTINCT ON (s.program_id, s.type, f.forecast_month)
                           s.program_id,
                           f.forecast_month AS fiscal_month_end,
                           s.type :: VARCHAR,
                           f.forecast_value AS value
                         FROM
                           program_financial_forecast f INNER JOIN program_financial_status s
                             ON f.financial_status_id = s.id
                         WHERE f.forecast_value IS NOT NULL
                         ORDER BY s.program_id, s.type, f.forecast_month, s.fiscal_month_end DESC),
    forecast_results AS (SELECT DISTINCT
                           'forecast'                      AS type,
                           latest_forecasts.program_id,
                           latest_forecasts.fiscal_month_end,
                           contractRevenue.value           AS contract_revenue,
                           corpGeneralAdministrative.value AS corp_general_administrative,
                           directLaborBillable.value       AS direct_labor_billable,
                           directLaborFringe.value         AS direct_labor_fringe,
                           directLaborOverhead.value       AS direct_labor_overhead,
                           directLaborUnbillable.value     AS direct_labor_unbillable,
                           dso.value                       AS dso,
                           generalAdministrative.value     AS general_administrative,
                           gwacPmo.value                   AS gwac_pmo,
                           hours.value                     AS hours,
                           iff.value                       AS iff,
                           mhx.value                       AS mhx,
                           other.value                     AS other,
                           riskReserve.value               AS risk_reserve,
                           subcontractLabor.value          AS subcontract_labor,
                           travel.value                    AS travel
                         FROM latest_forecasts
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'contractRevenue') contractRevenue
                             ON latest_forecasts.program_id =
                                contractRevenue.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                contractRevenue.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'corpGeneralAdministrative') corpGeneralAdministrative
                             ON latest_forecasts.program_id =
                                corpGeneralAdministrative.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                corpGeneralAdministrative.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'directLaborBillable') directLaborBillable
                             ON latest_forecasts.program_id =
                                directLaborBillable.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                directLaborBillable.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'directLaborFringe') directLaborFringe
                             ON latest_forecasts.program_id =
                                directLaborFringe.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                directLaborFringe.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'directLaborOverhead') directLaborOverhead
                             ON latest_forecasts.program_id =
                                directLaborOverhead.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                directLaborOverhead.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'directLaborUnbillable') directLaborUnbillable
                             ON latest_forecasts.program_id =
                                directLaborUnbillable.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                directLaborUnbillable.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'dso') dso
                             ON latest_forecasts.program_id =
                                dso.program_id AND
                                latest_forecasts.fiscal_month_end
                                =
                                dso.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'generalAdministrative') generalAdministrative
                             ON latest_forecasts.program_id =
                                generalAdministrative.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                generalAdministrative.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'gwacPmo') gwacPmo
                             ON latest_forecasts.program_id =
                                gwacPmo.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                gwacPmo.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'hours') hours
                             ON latest_forecasts.program_id =
                                hours.program_id AND
                                latest_forecasts.fiscal_month_end
                                =
                                hours.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'iff') iff
                             ON latest_forecasts.program_id =
                                iff.program_id AND
                                latest_forecasts.fiscal_month_end
                                =
                                iff.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'mhx') mhx
                             ON latest_forecasts.program_id =
                                mhx.program_id AND
                                latest_forecasts.fiscal_month_end
                                =
                                mhx.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'other') other
                             ON latest_forecasts.program_id =
                                other.program_id AND
                                latest_forecasts.fiscal_month_end
                                =
                                other.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'riskReserve') riskReserve
                             ON latest_forecasts.program_id =
                                riskReserve.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                riskReserve.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'subcontractLabor') subcontractLabor
                             ON latest_forecasts.program_id =
                                subcontractLabor.program_id
                                AND
                                latest_forecasts.fiscal_month_end
                                =
                                subcontractLabor.fiscal_month_end
                           LEFT JOIN (SELECT
                                        program_id,
                                        fiscal_month_end,
                                        value
                                      FROM latest_forecasts
                                      WHERE type =
                                            'travel') travel
                             ON latest_forecasts.program_id =
                                travel.program_id AND
                                latest_forecasts.fiscal_month_end
                                =
                                travel.fiscal_month_end),

    actual_results AS (
    SELECT DISTINCT
      'actual'                                       AS type,
      jt.program_id,
      jt.fiscal_month_end,
      contractRevenue.current_month_actual           AS contract_revenue,
      corpGeneralAdministrative.current_month_actual AS corp_general_administrative,
      directLaborBillable.current_month_actual       AS direct_labor_billable,
      directLaborFringe.current_month_actual         AS direct_labor_fringe,
      directLaborOverhead.current_month_actual       AS direct_labor_overhead,
      directLaborUnbillable.current_month_actual     AS direct_labor_unbillable,
      dso.current_month_actual                       AS dso,
      generalAdministrative.current_month_actual     AS general_administrative,
      gwacPmo.current_month_actual                   AS gwac_pmo,
      hours.current_month_actual                     AS hours,
      iff.current_month_actual                       AS iff,
      mhx.current_month_actual                       AS mhx,
      other.current_month_actual                     AS other,
      riskReserve.current_month_actual               AS risk_reserve,
      subcontractLabor.current_month_actual          AS subcontract_labor,
      travel.current_month_actual                    AS travel
    FROM program_financial_status jt
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'contractRevenue') contractRevenue
        ON jt.program_id = contractRevenue.program_id AND jt.fiscal_month_end = contractRevenue.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'corpGeneralAdministrative') corpGeneralAdministrative
        ON jt.program_id = corpGeneralAdministrative.program_id AND
           jt.fiscal_month_end = corpGeneralAdministrative.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'directLaborBillable') directLaborBillable
        ON jt.program_id = directLaborBillable.program_id AND jt.fiscal_month_end = directLaborBillable.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'directLaborFringe') directLaborFringe
        ON jt.program_id = directLaborFringe.program_id AND jt.fiscal_month_end = directLaborFringe.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'directLaborOverhead') directLaborOverhead
        ON jt.program_id = directLaborOverhead.program_id AND jt.fiscal_month_end = directLaborOverhead.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'directLaborUnbillable') directLaborUnbillable
        ON jt.program_id = directLaborUnbillable.program_id AND
           jt.fiscal_month_end = directLaborUnbillable.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'dso') dso
        ON jt.program_id = dso.program_id AND jt.fiscal_month_end = dso.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'generalAdministrative') generalAdministrative
        ON jt.program_id = generalAdministrative.program_id AND
           jt.fiscal_month_end = generalAdministrative.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'gwacPmo') gwacPmo
        ON jt.program_id = gwacPmo.program_id AND jt.fiscal_month_end = gwacPmo.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'hours') hours
        ON jt.program_id = hours.program_id AND jt.fiscal_month_end = hours.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'iff') iff
        ON jt.program_id = iff.program_id AND jt.fiscal_month_end = iff.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'mhx') mhx
        ON jt.program_id = mhx.program_id AND jt.fiscal_month_end = mhx.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'other') other
        ON jt.program_id = other.program_id AND jt.fiscal_month_end = other.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'riskReserve') riskReserve
        ON jt.program_id = riskReserve.program_id AND jt.fiscal_month_end = riskReserve.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'subcontractLabor') subcontractLabor
        ON jt.program_id = subcontractLabor.program_id AND jt.fiscal_month_end = subcontractLabor.fiscal_month_end
      LEFT JOIN (SELECT
                   program_id,
                   fiscal_month_end,
                   current_month_actual
                 FROM program_financial_status
                 WHERE type = 'travel') travel
        ON jt.program_id = travel.program_id AND jt.fiscal_month_end = travel.fiscal_month_end),


    papertrail_financials AS (
    SELECT DISTINCT ON (program_id, date_trunc('month', fiscal_month_end))
      program_id,
      fiscal_month_end,
      type,
      COALESCE(direct_labor_billable, 0) + COALESCE(direct_labor_unbillable, 0)                  AS dl_total,
      direct_labor_billable                                                                      AS dl_billable,
      subcontract_labor                                                                          AS sub_labor,
      COALESCE(contract_revenue, 0) - (
        COALESCE(direct_labor_billable, 0) + COALESCE(direct_labor_unbillable, 0) + COALESCE(subcontract_labor, 0) +
        COALESCE(travel, 0) + COALESCE(gwac_pmo, 0) + COALESCE(iff, 0) + COALESCE(other, 0) + COALESCE(risk_reserve, 0)
        + COALESCE(direct_labor_fringe, 0))                                                      AS gc,
      (COALESCE(contract_revenue, 0) - (
        COALESCE(direct_labor_billable, 0) + COALESCE(direct_labor_unbillable, 0) + COALESCE(subcontract_labor, 0) +
        COALESCE(travel, 0) + COALESCE(gwac_pmo, 0) + COALESCE(iff, 0) + COALESCE(other, 0) + COALESCE(risk_reserve, 0)
        + COALESCE(direct_labor_fringe, 0))) :: NUMERIC / NULLIF(contract_revenue, 0) :: NUMERIC AS pct_gc,
      COALESCE(contract_revenue, 0) - (
        COALESCE(direct_labor_billable, 0) + COALESCE(direct_labor_unbillable, 0) + COALESCE(subcontract_labor, 0) +
        COALESCE(travel, 0) + COALESCE(gwac_pmo, 0) + COALESCE(iff, 0) + COALESCE(other, 0) + COALESCE(risk_reserve, 0)
        + COALESCE(direct_labor_fringe, 0) + COALESCE(direct_labor_overhead, 0) + COALESCE(mhx, 0) +
        COALESCE(general_administrative, 0))                                                     AS oi,
      (COALESCE(contract_revenue, 0) - (
        COALESCE(direct_labor_billable, 0) + COALESCE(direct_labor_unbillable, 0) + COALESCE(subcontract_labor, 0) +
        COALESCE(travel, 0) + COALESCE(gwac_pmo, 0) + COALESCE(iff, 0) + COALESCE(other, 0) + COALESCE(risk_reserve, 0)
        + COALESCE(direct_labor_fringe, 0) + COALESCE(direct_labor_overhead, 0) + COALESCE(mhx, 0) +
        COALESCE(general_administrative, 0))) :: NUMERIC /
      NULLIF(contract_revenue, 0) :: NUMERIC                                                     AS oi_pct,
      contract_revenue                                                                           AS revenue,
      (COALESCE(direct_labor_billable, 0) + COALESCE(direct_labor_unbillable, 0)) :: NUMERIC / NULLIF(
        COALESCE(contract_revenue, 0) - (
          COALESCE(direct_labor_billable, 0) + COALESCE(direct_labor_unbillable, 0) + COALESCE(subcontract_labor, 0) +
          COALESCE(travel, 0) + COALESCE(gwac_pmo, 0) + COALESCE(iff, 0) + COALESCE(other, 0) +
          COALESCE(risk_reserve, 0) + COALESCE(direct_labor_fringe, 0) + COALESCE(direct_labor_overhead, 0) +
          COALESCE(mhx, 0) + COALESCE(general_administrative, 0)),
        0) :: NUMERIC                                                                            AS dl_oi_pct,
      subcontract_labor :: NUMERIC / NULLIF(COALESCE(contract_revenue, 0) - (
        COALESCE(direct_labor_billable, 0) + COALESCE(direct_labor_unbillable, 0) + COALESCE(subcontract_labor, 0) +
        COALESCE(travel, 0) + COALESCE(gwac_pmo, 0) + COALESCE(iff, 0) + COALESCE(other, 0) + COALESCE(risk_reserve, 0)
        + COALESCE(direct_labor_fringe, 0) + COALESCE(direct_labor_overhead, 0) + COALESCE(mhx, 0) +
        COALESCE(general_administrative, 0)), 0) :: NUMERIC                                      AS sublabor_oi_pct,
      direct_labor_unbillable                                                                    AS dl_unbillable,
      direct_labor_unbillable :: NUMERIC / NULLIF(COALESCE(contract_revenue, 0) - (
        COALESCE(direct_labor_billable, 0) + COALESCE(direct_labor_unbillable, 0) + COALESCE(subcontract_labor, 0) +
        COALESCE(travel, 0) + COALESCE(gwac_pmo, 0) + COALESCE(iff, 0) + COALESCE(other, 0) + COALESCE(risk_reserve, 0)
        + COALESCE(direct_labor_fringe, 0) + COALESCE(direct_labor_overhead, 0) + COALESCE(mhx, 0) +
        COALESCE(general_administrative, 0)), 0) :: NUMERIC                                      AS dl_unbillable_oi_pct
    FROM
      (
        SELECT *
        FROM actual_results
        UNION
        SELECT *
        FROM forecast_results
      ) results
    --GROUP BY program_id, fiscal_month_end, type
    ORDER BY program_id, date_trunc('month', fiscal_month_end), type ASC
  ),
    report_months AS (
    SELECT DISTINCT
      date_trunc('month', fiscal_month_end) :: DATE AS fiscal_month_end,
      program_id,
      CASE
      WHEN
        date_trunc('month', fiscal_month_end) :: DATE BETWEEN '2017-04-01' :: DATE AND '2017-06-01' :: DATE
        THEN 'FY181Q'
      WHEN
        date_trunc('month', fiscal_month_end) :: DATE BETWEEN '2017-07-01' :: DATE AND '2017-09-01' :: DATE
        THEN 'FY182Q'
      WHEN
        date_trunc('month', fiscal_month_end) :: DATE BETWEEN '2017-10-01' :: DATE AND '2017-12-01' :: DATE
        THEN 'FY183Q'
      WHEN
        date_trunc('month', fiscal_month_end) :: DATE BETWEEN '2018-01-01' :: DATE AND '2018-03-01' :: DATE
        THEN 'FY184Q'
      WHEN
        date_trunc('month', fiscal_month_end) :: DATE BETWEEN '2018-04-01' :: DATE AND '2018-06-01' :: DATE
        THEN 'FY191Q'
      END AS fy_quarter
    FROM
      (
        SELECT
          fiscal_month_end,
          program_id
        FROM heatmaps
        UNION
        SELECT
          fte_month AS fiscal_month_end,
          program_id
        FROM staffing
        UNION
        SELECT
          fiscal_month_end,
          program_id
        FROM papertrail_financials
        UNION
        SELECT
          fiscal_month_end,
          program_id
        FROM salesforce_financials
      ) months
    WHERE fiscal_month_end IS NOT NULL
          AND date_trunc('month', fiscal_month_end) :: DATE BETWEEN '2016-04-01' :: DATE AND '2017-06-01' :: DATE
  ),

    report_months_actuals AS (
    SELECT DISTINCT
      MAX(date_trunc('month', fiscal_month_end)) :: DATE AS fiscal_month_end,
      program_id
    FROM
      (
        SELECT
          fiscal_month_end,
          program_id
        FROM heatmaps
        UNION
        SELECT
          fiscal_month_end,
          program_id
        FROM staffing
        WHERE date_trunc('month', staffing.fiscal_month_end) = date_trunc('month', staffing.fte_month)
        UNION
        SELECT
          fiscal_month_end,
          program_id
        FROM papertrail_financials
        WHERE type = 'actual'
      ) months
    GROUP BY program_id
  )


(SELECT
   metadata.program_id,
   metadata.project_id                          AS "Program ID",
   metadata.name                                AS "Program Name",
   metadata.program_status                      AS "Program Status",
   metadata.group                               AS "Group",
   metadata.division                            AS "Business Unit",
   metadata.contract_type                       AS "Contract Type",
   now()                                        AS "Report Generated",
   report_months_actuals.fiscal_month_end       AS "Latest Actuals",
   report_months.fiscal_month_end :: VARCHAR    AS "FY Month",
   salesforce_financials.total_revenue_budget   AS "FY Revenue B/T",
   salesforce_financials.total_oi_budget        AS "FY OI B/T",
   salesforce_financials.total_revenue_forecast AS "FY Revenue A/F",
   salesforce_financials.total_oi_forecast      AS "FY OI A/F",
   heatmaps.contractual                         AS "Contractual",
   heatmaps.technical                           AS "Technical",
   heatmaps.cost                                AS "Cost/CPI",
   heatmaps.schedule                            AS "Schedule/SPI",
   heatmaps.customer_satisfaction               AS "CPAR/Customer Satisfaction",
   heatmaps.staffing_plan_pct                   AS "Staffing to Plan %",
   heatmaps.supplier_status                     AS "Supplier Status",
   heatmaps.growth                              AS "Growth",
   heatmaps.margin                              AS "EAC ROS",
   heatmaps.overall_project                     AS "Overall",
   staffing.type                                AS "Staffing Values Type",
   staffing.csra_fte_count                      AS "CSRA FTE count",
   staffing.sub_fte_count                       AS "Sub FTE count",
   staffing.csra_fte_pct                        AS "% of CSRA FTE to total for the program",
   staffing.sub_fte_pct                         AS "% of Sub FTE to total for the program",
   papertrail_financials.type                   AS "Financial Values Type",
   papertrail_financials.dl_total               AS "$DL Total",
   papertrail_financials.dl_billable            AS "$DL Billable",
   papertrail_financials.sub_labor              AS "$Sub Labor",
   papertrail_financials.gc                     AS "$GC",
   papertrail_financials.pct_gc                 AS "% GC",
   papertrail_financials.oi                     AS "$OI",
   papertrail_financials.oi_pct                 AS "OI %",
   papertrail_financials.revenue                AS "$Revenue",
   papertrail_financials.dl_oi_pct              AS "% of $DL to total $OI",
   papertrail_financials.sublabor_oi_pct        AS "% of $Sub Labor to total $OI",
   papertrail_financials.dl_unbillable          AS "$DL Unbillable",
   papertrail_financials.dl_unbillable_oi_pct   AS "% of DL Unbillable to total $OI"
 FROM
   metadata
   INNER JOIN report_months_actuals
     ON metadata.program_id = report_months_actuals.program_id
   INNER JOIN report_months
     ON
       metadata.program_id = report_months.program_id
   LEFT JOIN heatmaps
     ON
       metadata.program_id = heatmaps.program_id AND
       report_months.fiscal_month_end = date_trunc('month', heatmaps.fiscal_month_end) :: DATE
   LEFT JOIN salesforce_financials
     ON
       metadata.program_id = salesforce_financials.program_id AND
       report_months.fiscal_month_end = date_trunc('month', salesforce_financials.fiscal_month_end) :: DATE
   LEFT JOIN staffing
     ON
       metadata.program_id = staffing.program_id AND
       report_months.fiscal_month_end = date_trunc('month', staffing.fte_month) :: DATE
   LEFT JOIN papertrail_financials
     ON
       metadata.program_id = papertrail_financials.program_id AND
       report_months.fiscal_month_end = date_trunc('month', papertrail_financials.fiscal_month_end) :: DATE)


UNION ALL

(
  SELECT
    metadata.program_id,
    metadata.project_id            AS "Program ID",
    metadata.name                  AS "Program Name",
    metadata.program_status        AS "Program Status",
    metadata.group                 AS "Group",
    metadata.division              AS "Business Unit",
    metadata.contract_type         AS "Contract Type",
    now()                          AS "Report Generated",
    subtotals.fiscal_month_end     AS "Latest Actuals",
    subtotals.fy_quarter           AS "FY Month",
    NULL                           AS "FY Revenue B/T",
    NULL                           AS "FY OI B/T",
    NULL                           AS "FY Revenue A/F",
    NULL                           AS "FY OI A/F",
    NULL                           AS "Contractual",
    NULL                           AS "Technical",
    NULL                           AS "Cost/CPI",
    NULL                           AS "Schedule/SPI",
    NULL                           AS "CPAR/Customer Satisfaction",
    NULL                           AS "Staffing to Plan %",
    NULL                           AS "Supplier Status",
    NULL                           AS "Growth",
    NULL                           AS "EAC ROS",
    NULL                           AS "Overall",
    NULL                           AS "Staffing Values Type",
    subtotals.csra_fte_count       AS "CSRA FTE Count",
    subtotals.sub_fte_count        AS "Sub FTE Count",
    subtotals.csra_fte_pct         AS "% of CSRA FTE to total for the program",
    subtotals.sub_fte_pct          AS "% of Sub FTE to total for the program",
    NULL                           AS "Financial Values Type",
    subtotals.dl_total             AS "$DL Total",
    subtotals.dl_billable          AS "$DL Billable",
    subtotals.sub_labor            AS "$Sub Labor",
    subtotals.gc                   AS "%GC",
    subtotals.pct_gc               AS "% GC",
    subtotals.oi                   AS "$OI",
    subtotals.oi_pct               AS "OI %",
    subtotals.revenue              AS "$Revenue",
    subtotals.dl_oi_pct            AS "% of $DL to total $OI",
    subtotals.sublabor_oi_pct      AS "% of $Sub Labor to total $OI",
    subtotals.dl_unbillable        AS "$DL Unbillable",
    subtotals.dl_unbillable_oi_pct AS "% of DL Unbillable to total $OI"
  FROM
    (SELECT
       metadata.project_id,
       report_months.fy_quarter,
       report_months_actuals.fiscal_month_end,
       SUM(COALESCE(staffing.csra_fte_count,
                    0))                                                  AS csra_fte_count,
       SUM(COALESCE(staffing.sub_fte_count,
                    0))                                                  AS sub_fte_count,
       SUM(COALESCE(staffing.csra_fte_count, 0)) /
       NULLIF(SUM(COALESCE(staffing.csra_fte_count, 0)) + SUM(COALESCE(staffing.sub_fte_count, 0)),
              0)                                                         AS csra_fte_pct,
       SUM(COALESCE(staffing.sub_fte_count, 0)) /
       NULLIF(SUM(COALESCE(staffing.csra_fte_count, 0)) + SUM(COALESCE(staffing.sub_fte_count, 0)),
              0)                                                         AS sub_fte_pct,
       SUM(COALESCE(papertrail_financials.dl_total,
                    0))                                                  AS dl_total,
       SUM(COALESCE(papertrail_financials.dl_billable,
                    0))                                                  AS dl_billable,
       SUM(COALESCE(papertrail_financials.sub_labor,
                    0))                                                  AS sub_labor,
       SUM(COALESCE(papertrail_financials.gc,
                    0))                                                  AS gc,
       SUM(COALESCE(papertrail_financials.gc, 0)) / NULLIF(SUM(COALESCE(papertrail_financials.revenue, 0)),
                                                           0)            AS pct_gc,
       SUM(COALESCE(papertrail_financials.oi,
                    0))                                                  AS oi,
       SUM(COALESCE(papertrail_financials.oi,
                    0)) / NULLIF(SUM(COALESCE(papertrail_financials.revenue,
                                              0)), 0)                    AS oi_pct,
       SUM(COALESCE(papertrail_financials.revenue,
                    0))                                                  AS revenue,
       SUM(COALESCE(papertrail_financials.oi, 0)) / NULLIF(SUM(COALESCE(papertrail_financials.revenue,
                                                                        0)),
                                                           0)            AS dl_oi_pct,
       SUM(COALESCE(papertrail_financials.sub_labor, 0)) / NULLIF(SUM(COALESCE(papertrail_financials.oi, 0)),
                                                                  0)     AS sublabor_oi_pct,
       SUM(COALESCE(papertrail_financials.dl_unbillable,
                    0))                                                  AS dl_unbillable,
       SUM(COALESCE(papertrail_financials.dl_unbillable, 0)) / NULLIF(SUM(COALESCE(papertrail_financials.oi, 0)),
                                                                      0) AS dl_unbillable_oi_pct
     FROM
       metadata
       INNER JOIN report_months_actuals
         ON metadata.program_id = report_months_actuals.program_id
       INNER JOIN report_months
         ON
           metadata.program_id = report_months.program_id
       LEFT JOIN salesforce_financials
         ON
           metadata.program_id = salesforce_financials.program_id AND
           report_months.fiscal_month_end = date_trunc('month', salesforce_financials.fiscal_month_end) :: DATE
       LEFT JOIN staffing
         ON
           metadata.program_id = staffing.program_id AND
           report_months.fiscal_month_end = date_trunc('month', staffing.fte_month) :: DATE
       LEFT JOIN papertrail_financials
         ON
           metadata.program_id = papertrail_financials.program_id AND
           report_months.fiscal_month_end = date_trunc('month', papertrail_financials.fiscal_month_end) :: DATE
     GROUP BY metadata.project_id, report_months_actuals.fiscal_month_end, fy_quarter
    ) subtotals
    INNER JOIN metadata
      ON subtotals.project_id = metadata.project_id
)

ORDER BY program_id, "FY Month" ASC
