SELECT * FROM (
  SELECT
    to_char(program_heatmap_manual_entry.updated_at, 'MM-DD-YYYY') AS date_submitted,
    to_char('<%- endDate %>'::DATE, 'MM-YYYY') AS fiscal_month,
    program_metadata_manual_entry.customer,
    CASE WHEN program_metadata_manual_entry.export_international = 'true' THEN 'Yes'
      WHEN program_metadata_manual_entry.export_international = 'false' THEN 'No'
      ELSE NULL END as export_international,
    program_metadata_manual_entry.program_description,
    ' ' as "Organization Detail",
    program_metadata_manual_entry.program_manager,
    program_metadata_manual_entry.pra,
    program_metadata_manual_entry.pm_email,
    ' ' as "Financial Detail",
    -- FINANCIAL
    CASE WHEN revenue_financial.final_mgmt_review IS NULL THEN 'N/A'
         WHEN revenue_financial.final_mgmt_review < 0 THEN concat('-$',to_char(revenue_financial.final_mgmt_review* (-1), '9,999,999,999,999,999'))
    ELSE concat('$',to_char(revenue_financial.final_mgmt_review, '9,999,999,999,999,999')) END AS "Contract Ceiling",

    CASE WHEN revenue_financial."fyBudget"  IS NULL THEN 'N/A'
    ELSE concat('$',to_char(revenue_financial."fyBudget", '9,999,999,999,999,999')) END AS "FY Budget Revenue",

    CASE WHEN program_investment_totals.value  IS NULL THEN 'N/A'
    ELSE concat('$',to_char(program_investment_totals.value, '9,999,999,999,999,999')) END AS "Total Investment",
    -- STAFFING
    ' ' as "Staffing Detail",
    CASE WHEN gdit_current.value IS NULL THEN 'N/A'
    ELSE to_char(gdit_current.value, '999999') END as "GDIT Current",
    -- CONTRACT
    ' ' as "Contract Detail",
    program_metadata_manual_entry.small_business_goal_percentage as "Small Business Goal %",
-- 1st Column Ended

    upper(programs_secure.project_id) AS program_number,
    programs_secure.name AS program_name,
    program_metadata_manual_entry.required_credentials as "Contractual Requirements and Credentials",
    program_metadata_manual_entry.lead_for_export_international,
    program_metadata_manual_entry.scope_of_services,
    ' '  as _______________,
    program_metadata_manual_entry.group as "Division",
    program_metadata_manual_entry.l2_manager as "Division Manager",
    ' '  as ____,
    ' '  as ____________,
    -- FINANCIAL
    CASE WHEN revenue_financial.eac IS NULL THEN 'N/A'
         WHEN revenue_financial.eac < 0 THEN concat('-$',to_char(revenue_financial.eac* (-1), '9,999,999,999,999,999'))
    ELSE concat('$',to_char(revenue_financial.eac, '9,999,999,999,999,999')) END AS "Contract Value",

    CASE WHEN ((revenue_financial."fyBudget" IS NULL) AND (margin_financial."fyBudget" IS NULL))
      THEN 'N/A'
      ELSE  concat('$', to_char(COALESCE(revenue_financial."fyBudget",0) - COALESCE(margin_financial."fyBudget",0), '9,999,999,999,999,999')) END  as "FY Budget Cost",
    CASE WHEN program_metadata_manual_entry.use_cpi_spi = 'true' THEN 'Yes'
      WHEN program_metadata_manual_entry.use_cpi_spi = 'false' THEN 'No'
      ELSE NULL END as "CPI/SPI Indicator",
    -- STAFFING
    ' '  as ________________,
    CASE WHEN sub_current.value IS NULL THEN 'N/A'
    ELSE to_char(sub_current.value,'999999') END as "Sub Current",
    -- CONTRACT
    ' '  as ________,
    gdit_actual.value as "Small Business Actual %",

-- 2nd Column Ended
    program_metadata_manual_entry.contract_type,
    program_metadata_manual_entry.fee_structure,
    CASE WHEN program_metadata_manual_entry.fee_value IS NULL THEN 'N/A'
    ELSE program_metadata_manual_entry.fee_value END as fee_value,
    program_metadata_manual_entry.foreign_sourced_hw_sw,
    ' '  as ___________________,
    ' '  as ___,
    program_metadata_manual_entry.division AS "Sector",
    program_metadata_manual_entry.l3_manager as "Sector Manager",
    ' '  as ______,
    ' '  as __,
    -- FINANCIAL
    CASE WHEN revenue_financial.funded IS NULL THEN 'N/A'
    ELSE concat('$',to_char(revenue_financial.funded, '9,999,999,999,999,999')) END AS "Contract Value Funded",

    CASE WHEN margin_financial."fyBudget" IS NULL THEN 'N/A'
    ELSE concat('$',to_char(margin_financial."fyBudget", '9,999,999,999,999,999')) END AS "FY Budget Margin",

    CASE WHEN reserve_financial."fyBudget" IS NULL THEN 'N/A'
    ELSE concat('$',to_char(reserve_financial."fyBudget", '9,999,999,999,999,999')) END AS "Reserve",

    -- STAFFING
    ' '  as _____________________,
    CASE WHEN gdit_funded.value IS NULL THEN 'N/A'
    ELSE to_char(gdit_funded.value,'999999') END as "GDIT Funded",
    -- CONTRACT
    ' '  as ______________________,
    CASE WHEN program_metadata_manual_entry.current_month IS NULL THEN 'N/A'
    ELSE program_metadata_manual_entry.current_month END as current_month,
-- 3rd Column Ended

    CASE WHEN program_metadata_manual_entry.oci = 'true' THEN 'Yes'
      WHEN program_metadata_manual_entry.oci = 'false' THEN 'No'
      ELSE NULL END as oci,
    CASE WHEN program_metadata_manual_entry.labor_cat_cert_required = 'true' THEN 'Yes'
      WHEN program_metadata_manual_entry.labor_cat_cert_required = 'false' THEN 'No'
      ELSE NULL END as labor_cat_cert_required,
      CASE WHEN program_metadata_manual_entry.gfe_or_property = 'true' THEN 'Yes'
        WHEN program_metadata_manual_entry.gfe_or_property = 'false' THEN 'No'
        ELSE NULL END as gfe_or_property,
    ' '  as __________________,
    '' as _____________,
    ' '  as __________,
    program_metadata_manual_entry.business_area,
    program_metadata_manual_entry.l4_manager as "Business Area Manager",
    ' '  as ___________,
    '' as ______________,
    -- FINANCIAL
    CASE WHEN ((revenue_financial.eac IS NULL) OR (margin_financial.eac IS NULL))
    THEN NULL ELSE
	  concat(to_char(round(cast(margin_financial.eac as numeric(36,2))  / NULLIF(cast(revenue_financial.eac  as numeric(36,2)),0) * 100, 1),'9999.9'),'%') END AS "Contract Value Margin %",
    CASE WHEN ((revenue_financial."fyBudget" IS NULL) AND (margin_financial."fyBudget" IS NULL))
    THEN NULL ELSE
    concat(to_char(round(cast(margin_financial."fyBudget" as numeric(36,2)) / NULLIF(cast(revenue_financial."fyBudget" as numeric(36,2)),0) * 100, 1), '999999.9'), '%') END AS "FY Budget Margin %",
    program_metadata_manual_entry.payment_terms,
    -- STAFFING
    ' '  as _________,
    CASE WHEN sub_funded.value IS NULL THEN 'N/A'
    ELSE to_char(sub_funded.value, '9999999') END as "Sub Funded",
    -- CONTRACT
    ' '  as _____,
    CASE WHEN program_metadata_manual_entry.months_to_complete IS NULL THEN 'N/A'
    ELSE program_metadata_manual_entry.months_to_complete END as months_to_complete
-- 4th Column Ended


  FROM programs_secure
    INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id
    LEFT JOIN program_heatmap_manual_entry
      ON program_heatmap_manual_entry.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', fiscal_month_end)
        AND program_heatmap_manual_entry.validated = TRUE
        AND program_heatmap_manual_entry.type = 'actual'
    LEFT JOIN
      program_financial_status AS "revenue_financial"
      ON revenue_financial.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', revenue_financial.fiscal_month_end)
        AND revenue_financial.type = 'revenue'
        AND revenue_financial.validated = TRUE

    LEFT JOIN program_financial_status AS "margin_financial"
      ON margin_financial.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', margin_financial.fiscal_month_end)
        AND margin_financial.type = 'margin'
        AND margin_financial.validated = TRUE
    LEFT JOIN program_financial_status AS "reserve_financial"
      ON reserve_financial.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', reserve_financial.fiscal_month_end)
        AND reserve_financial.type = 'reserve'
        AND reserve_financial.validated = TRUE

    LEFT JOIN program_staffing_positions AS "gdit_funded"
      ON gdit_funded.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', gdit_funded.fiscal_month_end)
        AND gdit_funded.staffing_source = 'gdit'
        AND gdit_funded.type = 'total_funded'
    LEFT JOIN program_staffing_positions AS "gdit_current"
      ON gdit_current.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', gdit_current.fiscal_month_end)
        AND gdit_current.staffing_source = 'gdit'
        AND gdit_current.type = 'total_current'
    LEFT JOIN program_staffing_positions AS "sub_funded"
      ON sub_funded.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', sub_funded.fiscal_month_end)
        AND sub_funded.staffing_source = 'sub'
        AND sub_funded.type = 'total_funded'
    LEFT JOIN program_staffing_positions AS "sub_current"
      ON sub_current.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', sub_current.fiscal_month_end)
        AND sub_current.staffing_source = 'sub'
        AND sub_current.type = 'total_current'
    LEFT JOIN program_staffing_positions AS "gdit_actual"
      ON gdit_actual.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', gdit_actual.fiscal_month_end)
        AND gdit_actual.staffing_source = 'gdit'
        AND gdit_actual.type = 'small_business_actual'
  LEFT JOIN (SELECT SUM(program_investment_totals.value) as value,
              program_investment_totals.program_id,
              program_investment_totals.fiscal_month
              FROM program_investment_totals
              GROUP BY program_investment_totals.program_id, program_investment_totals.fiscal_month
            )program_investment_totals
          ON program_investment_totals.program_id = programs_secure.id
            AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
                date_trunc('month', program_investment_totals.fiscal_month)


     <%- roleClause %>
  WHERE <%- groupClause %>
    AND <%- executiveDivisionClause %>
    AND <%- userClause %>
    AND <%- l2_ManagerClause%>
    AND <%- l3_ManagerClause%>
    AND <%- l2ManagerClause %>
    AND <%- divisionClause %>
    AND <%- l3ManagerClause %>
    AND <%- programManagerClause %>
    AND <%- l4ManagerClause %>
    AND <%- programClause %>
    AND <%- losClause %>
    AND <%- praClause %>
    AND <%- statusClause %>
    AND <%-l2_ManagerClause %>
    AND <%-l3_ManagerClause %>
  ORDER BY program_metadata_manual_entry.program_id,
           program_heatmap_manual_entry.program_id,
           program_metadata_manual_entry.created_at DESC

  <%- limit %> <%- offset %>
) table_query <%- order %>
