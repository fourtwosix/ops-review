WITH program_fy_financials_temp AS (
  SELECT DISTINCT
  program_metadata_manual_entry.program_id as program_id,
    program_fy_financials.fiscal_month,
    program_fy_financials.type,
    program_fy_financials.value,
    program_financial_status.current_month_actual AS dso
  FROM programs_secure

    INNER JOIN (SELECT distinct on (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
    ON programs_secure.id = program_metadata_manual_entry.program_id

    LEFT JOIN program_fy_financials
    ON programs_secure.id = program_fy_financials.program_id
      AND program_fy_financials.type IN
          ('oiForecast', 'oiBudget', 'revenueForecast', 'revenueBudget')
      AND date_trunc('month', program_fy_financials.fiscal_month) >=
          date_trunc('month', to_date('<%- currentFYStart %>', 'YYYY-MM-DD'))
      AND date_trunc('month', program_fy_financials.fiscal_month) <=
          date_trunc('month', to_date('<%- currentFYEnd %>', 'YYYY-MM-DD'))
    LEFT JOIN
    program_financial_status
    ON programs_secure.id = program_financial_status.program_id
      AND program_financial_status.type = 'fundingColor'
      AND program_financial_status.validated = TRUE
      AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
          date_trunc('month', program_financial_status.fiscal_month_end)
  WHERE program_metadata_manual_entry.program_status = 'open'
  AND program_metadata_manual_entry.validated = TRUE
),


ribbon_table AS (
  SELECT DISTINCT ON (program_fy_financials_temp.program_id)
  program_fy_financials_temp.program_id,
  (program_fy_financials_temp.dso)  AS dso,
  (CASE WHEN(revenue_forecast.value IS NOT NULL) THEN revenue_forecast.value
		ELSE (CASE WHEN( fy_revenue_forecast.value IS NULL) THEN NULL
				ELSE fy_revenue_forecast.value END) END) AS revenue_forecast,

    (CASE WHEN(revenue_budget.value IS NOT NULL) THEN revenue_budget.value
		ELSE (CASE WHEN( fy_revenue_budget.value IS NULL) THEN NULL
				ELSE fy_revenue_budget.value END) END)
  AS revenue_budget,

    (CASE WHEN(oi_forecast.value IS NOT NULL) THEN oi_forecast.value
		ELSE (CASE WHEN( fy_oi_forecast.value IS NULL) THEN NULL
				ELSE fy_oi_forecast.value END) END)
  AS oi_forecast,

    (CASE WHEN(oi_budget.value IS NOT NULL) THEN oi_budget.value
		ELSE (CASE WHEN( fy_oi_budget.value IS NULL) THEN NULL
				ELSE fy_oi_budget.value END) END)
  AS oi_budget
  FROM program_fy_financials_temp
    LEFT JOIN (SELECT program_fy_financials_temp.program_id, program_fy_financials_temp.type, SUM(program_fy_financials_temp.value) as value
               FROM program_fy_financials_temp
               WHERE program_fy_financials_temp.type = 'revenueForecast'
               GROUP BY program_id, program_fy_financials_temp.type
              ) revenue_forecast
      ON
        program_fy_financials_temp.program_id = revenue_forecast.program_id


    LEFT JOIN (SELECT program_fy_financials_temp.program_id, program_fy_financials_temp.type, SUM(program_fy_financials_temp.value) as value
               FROM program_fy_financials_temp
               WHERE program_fy_financials_temp.type = 'revenueBudget'
               GROUP BY program_id, program_fy_financials_temp.type
             ) revenue_budget
      ON
        program_fy_financials_temp.program_id = revenue_budget.program_id


    LEFT JOIN (SELECT program_fy_financials_temp.program_id, program_fy_financials_temp.type, SUM(program_fy_financials_temp.value) as value
               FROM program_fy_financials_temp
               WHERE program_fy_financials_temp.type = 'oiForecast'
               GROUP BY program_id, program_fy_financials_temp.type
             ) oi_forecast
      ON
        program_fy_financials_temp.program_id = oi_forecast.program_id


    LEFT JOIN (SELECT program_fy_financials_temp.program_id, program_fy_financials_temp.type, SUM(program_fy_financials_temp.value) as value
               FROM program_fy_financials_temp
               WHERE program_fy_financials_temp.type = 'oiBudget'
               GROUP BY program_id, program_fy_financials_temp.type
             ) oi_budget
      ON
        program_fy_financials_temp.program_id = oi_budget.program_id


        LEFT JOIN (SELECT program_fy_financials.program_id, program_fy_financials.type, SUM(program_fy_financials.value) as value
                     FROM program_fy_financials
                     WHERE program_fy_financials.type = 'oiBudget'
        	          AND date_trunc('month', program_fy_financials.fiscal_month) >=
        	             date_trunc('month', to_date('<%- currentFYStart %>', 'YYYY-MM-DD'))
        	         AND date_trunc('month', program_fy_financials.fiscal_month) <=
        	             date_trunc('month', to_date('<%- currentFYEnd %>', 'YYYY-MM-DD'))
        	             GROUP BY program_id, program_fy_financials.type
                   ) fy_oi_budget
           ON
              program_fy_financials_temp.program_id = fy_oi_budget.program_id
              	  LEFT JOIN (SELECT program_fy_financials.program_id, program_fy_financials.type, SUM(program_fy_financials.value) as value
                     FROM program_fy_financials
                     WHERE program_fy_financials.type = 'oiForecast'
        	          AND date_trunc('month', program_fy_financials.fiscal_month) >=
        	             date_trunc('month', to_date('<%- currentFYStart %>', 'YYYY-MM-DD'))
        	         AND date_trunc('month', program_fy_financials.fiscal_month) <=
        	             date_trunc('month', to_date('<%- currentFYEnd %>', 'YYYY-MM-DD'))
        	             GROUP BY program_id, program_fy_financials.type
                   ) fy_oi_forecast
           ON
              program_fy_financials_temp.program_id = fy_oi_forecast.program_id
              	  LEFT JOIN (SELECT program_fy_financials.program_id, program_fy_financials.type, SUM(program_fy_financials.value) as value
                     FROM program_fy_financials
                     WHERE program_fy_financials.type = 'revenueForecast'
        	          AND date_trunc('month', program_fy_financials.fiscal_month) >=
        	             date_trunc('month', to_date('<%- currentFYStart %>', 'YYYY-MM-DD'))
        	         AND date_trunc('month', program_fy_financials.fiscal_month) <=
        	             date_trunc('month', to_date('<%- currentFYEnd %>', 'YYYY-MM-DD'))
        	             GROUP BY program_id, program_fy_financials.type
                   ) fy_revenue_forecast
           ON
              program_fy_financials_temp.program_id = fy_revenue_forecast.program_id
              	  LEFT JOIN (SELECT program_fy_financials.program_id, program_fy_financials.type, SUM(program_fy_financials.value) as value
                     FROM program_fy_financials
                     WHERE program_fy_financials.type = 'revenueBudget'
        	          AND date_trunc('month', program_fy_financials.fiscal_month) >=
        	             date_trunc('month', to_date('<%- currentFYStart %>', 'YYYY-MM-DD'))
        	         AND date_trunc('month', program_fy_financials.fiscal_month) <=
        	             date_trunc('month', to_date('<%- currentFYEnd %>', 'YYYY-MM-DD'))
        	             GROUP BY program_id, program_fy_financials.type
                   ) fy_revenue_budget
           ON
              program_fy_financials_temp.program_id = fy_revenue_budget.program_id
)


SELECT DISTINCT ON (ribbon_table.program_id)
  ribbon_table.program_id as "Program ID",
  programs.project_id as "Program Number",
  programs.name as "Program Name",
  program_metadata_manual_entry.program_manager as "Program Manager",
  program_metadata_manual_entry.group as "Division",
  program_metadata_manual_entry.division as "Sector",
  program_metadata_manual_entry.l3_manager as "Sector Manager",
  program_metadata_manual_entry.l4_manager as "Business Area Manager",
  program_metadata_manual_entry.pra as "PRA",
  date_trunc('month', to_date('<%- currentFYEnd %>', 'YYYY-MM')) as "Fiscal End",
  ribbon_table.dso as "DSO",
  ribbon_table.revenue_forecast   AS "FY Forecast Revenue",
  ribbon_table.revenue_budget     AS "FY Budget Revenue",
  ribbon_table.oi_forecast         AS "FY Forecast OI",
  ribbon_table.oi_budget           AS "FY Budget OI",
  round(ribbon_table.oi_forecast/NULLIF(ribbon_table.revenue_forecast,0)*100,1) AS"FY Forecast OI%",
  round(ribbon_table.oi_budget/NULLIF(ribbon_table.revenue_budget,0)*100,1)       AS "FY Budget OI%"
FROM ribbon_table, program_metadata_manual_entry, programs
WHERE programs.id = ribbon_table.program_id AND ribbon_table.program_id = program_metadata_manual_entry.program_id
ORDER BY ribbon_table.program_id, program_metadata_manual_entry.created_at DESC
