WITH investment_total AS (
  SELECT DISTINCT
    program_metadata_manual_entry.program_id,
    programs_secure.project_id,
    program_financial_status.fiscal_month_end,
    program_financial_status.current_month_actual as dso,
    program_financial_status.funding,
    program_financial_status."awardFee" as award_fee
  FROM programs_secure
    INNER JOIN (SELECT distinct on (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id
    LEFT JOIN program_financial_status
      ON programs_secure.id = program_financial_status.program_id
         AND date_trunc('month', program_financial_status.fiscal_month_end) >=
             date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
         AND date_trunc('month', program_financial_status.fiscal_month_end) <=
             date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
        AND type = 'fundingColor'

<%- roleClause %>
  WHERE
    <%-programManagerClause %>
    AND <%- executiveDivisionClause %>
    AND <%-userClause %>
    AND <%- groupClause %>
    AND <%-metaPMClause %>
    AND <%-praClause%>
    AND <%- l2_ManagerClause %>
    AND <%- l3_ManagerClause%>
    AND <%- l4ManagerClause %>
    AND <%- divisionClause %>
    AND <%-programClause %>
    AND program_metadata_manual_entry.validated = TRUE
    AND <%- statusClause %>
)
SELECT
investment_total.project_id,
CASE WHEN (investment_total.dso IS NULL) THEN 'N/S' ELSE to_char(investment_total.dso, '9999999') END AS dso,
CASE WHEN (investment_total.funding IS NULL) THEN 'N/S' ELSE investment_total.funding END AS funding,
CASE WHEN (investment_total.award_fee IS NULL) THEN 'N/S' ELSE investment_total.award_fee END AS award_fee
FROM investment_total
