with tempReport AS(

SELECT DISTINCT ON(program_metadata_manual_entry.program_id,program_staffing.fte_month, program_staffing.type,program_staffing.staffing_source,program_staffing.fiscal_month_end)
program_metadata_manual_entry.program_id,
programs.project_id,
program_metadata_manual_entry.program_status,
program_metadata_manual_entry.pmr_status,
program_metadata_manual_entry.group,
program_metadata_manual_entry.division,
program_metadata_manual_entry.contract_type,
program_staffing.type,
program_staffing.fte_month,
program_staffing.staffing_source,
program_staffing.fte,
program_staffing.fiscal_month_end


FROM (SELECT DISTINCT ON (program_id)* FROM program_metadata_manual_entry order by program_id ) program_metadata_manual_entry
INNER JOIN program_staffing on  program_staffing.program_id = program_metadata_manual_entry.program_id
LEFT JOIN programs on programs.id = program_metadata_manual_entry.program_id

WHERE date_trunc('month', program_staffing.fte_month) = date_trunc('month', to_date('<%- staffingDate %>', 'YYYY-MM-DD'))
AND (program_metadata_manual_entry.program_status = 'open' and program_metadata_manual_entry.pmr_status in ('Required','Waived','Not Required')) and program_staffing.type in ('actual','forecast')
AND date_trunc('month', program_staffing.fiscal_month_end) = date_trunc('month', to_date('<%- staffingDate %>', 'YYYY-MM-DD'))
and program_staffing.type = 'actual'

order by program_metadata_manual_entry.program_id ASC,program_staffing.fte_month, program_staffing.type, program_staffing.fiscal_month_end desc
),
program_financial_status_temp AS(
	SELECT program_financial_status.*

	FROM (SELECT DISTINCT ON (program_id)* FROM program_metadata_manual_entry order by program_id, created_at DESC) program_metadata_manual_entry
		 inner join program_financial_status on program_financial_status.program_id = program_metadata_manual_entry.program_id

	WHERE program_metadata_manual_entry.program_status = 'open'
	 			AND program_financial_status.program_id = program_metadata_manual_entry.program_id
				AND date_trunc('month', program_financial_status.fiscal_month_end) = date_trunc('month', to_date('<%- staffingDate %>', 'YYYY-MM-DD'))
),
ODC AS(
	SELECT DISTINCT ON (program_financial_status_temp.program_id, program_financial_status_temp.fiscal_month_end)
	programs.project_id,
	programs.name,
	program_financial_status_temp.program_id,
	program_financial_status_temp.fiscal_month_end,
	bill.current_month_actual as bill,
	unbill.current_month_actual as unbill,
	sub.current_month_actual as sub,
	(COALESCE(sub.current_month_actual, 0)+ COALESCE(unbill.current_month_actual,0) + COALESCE(bill.current_month_actual, 0)) as total_labor,
	(COALESCE(sub.current_month_actual, 0)+ COALESCE(unbill.current_month_actual,0) + COALESCE(bill.current_month_actual, 0) + COALESCE(dlfringe.current_month_actual,0) +COALESCE(other.current_month_actual,0) + COALESCE(travel.current_month_actual,0)) as total_cost,
	(COALESCE(unbill.current_month_actual,0) + COALESCE(bill.current_month_actual, 0)) as total_dl,
	dlfringe.current_month_actual as dlfringe,
	COALESCE(other.current_month_actual,0) + COALESCE(travel.current_month_actual,0) as odc,
	revenue.current_month_actual as revenue

	FROM program_financial_status_temp
	LEFT JOIN (SELECT program_financial_status_temp.* FROM program_financial_status_temp WHERE program_financial_status_temp.type = 'directLaborBillable') bill
	on program_financial_status_temp.program_id = bill.program_id and program_financial_status_temp.fiscal_month_end = bill.fiscal_month_end

	LEFT JOIN (SELECT program_financial_status_temp.* FROM program_financial_status_temp WHERE program_financial_status_temp.type = 'directLaborUnbillable') unbill
	on program_financial_status_temp.program_id = unbill.program_id and program_financial_status_temp.fiscal_month_end = unbill.fiscal_month_end

	LEFT JOIN (SELECT program_financial_status_temp.* FROM program_financial_status_temp WHERE program_financial_status_temp.type = 'subcontractLabor') sub
	on program_financial_status_temp.program_id = sub.program_id and program_financial_status_temp.fiscal_month_end = sub.fiscal_month_end

	LEFT JOIN (SELECT program_financial_status_temp.* FROM program_financial_status_temp WHERE program_financial_status_temp.type = 'directLaborFringe') dlfringe
	on program_financial_status_temp.program_id = dlfringe.program_id and program_financial_status_temp.fiscal_month_end = dlfringe.fiscal_month_end

	LEFT JOIN (SELECT program_financial_status_temp.* FROM program_financial_status_temp WHERE program_financial_status_temp.type = 'contractRevenue') revenue
	on program_financial_status_temp.program_id = revenue.program_id and program_financial_status_temp.fiscal_month_end = revenue.fiscal_month_end

	LEFT JOIN (SELECT program_financial_status_temp.* FROM program_financial_status_temp WHERE program_financial_status_temp.type = 'other') other
	on program_financial_status_temp.program_id = other.program_id and program_financial_status_temp.fiscal_month_end = other.fiscal_month_end

	LEFT JOIN (SELECT program_financial_status_temp.* FROM program_financial_status_temp WHERE program_financial_status_temp.type = 'travel') travel
	on program_financial_status_temp.program_id = travel.program_id and program_financial_status_temp.fiscal_month_end = travel.fiscal_month_end

	LEFT JOIN programs on program_financial_status_temp.program_id = programs.id
)

SELECT DISTINCT ON(tempReport.program_id, tempReport.fte_month, tempReport.type)
tempReport.project_id as "Program Number",
ODC.name as "Program Name",
tempReport.program_status as "Heatmap Status",
tempReport.pmr_status as "PMR Status",
tempReport.group as "Group",
tempReport.division as "Division",
tempReport.fte_month as "FTE Month",
tempReport.contract_type as "Contract Type",
tempReport.type as "Type",
csraStaff.fte as CSRA_FTE,
subStaff.fte as SUB_FTE,
(coalesce(csraStaff.fte,0) + coalesce(subStaff.fte,0)) as "Total FTE",
ODC.bill as "DL Billable",
ODC.unbill as "DL Unbillable",
ODC.sub as "Sub Labor",
ODC.dlfringe as "DL Fringe",
ODC.odc as "ODC's",
ODC.revenue as "Revenue",
ODC.total_labor as "Total Labor",
round(ODC.total_dl/ NULLIF(ODC.total_labor,0)::numeric*100,1) as "DL as % of Total",
round(ODC.sub/NULLIF(ODC.total_labor,0)::numeric*100,1) as "Sub Labor as % of Total",
round(ODC.dlfringe/NULLIF(ODC.total_dl,0)::numeric*100,1) as "DL Fringe as % of DL",
ODC.total_cost as "Total Direct Cost",
(COALESCE(ODC.revenue,0) - COALESCE(ODC.total_cost,0)) as "GC",
round((COALESCE(ODC.revenue,0) - COALESCE(ODC.total_cost,0))/NULLIF(ODC.revenue, 0)::numeric*100, 1) as "GC%"




FROM tempReport
LEFT JOIN (SELECT * from tempReport WHERE tempReport.staffing_source = 'csra' order by tempReport.fiscal_month_end DESC
) csraStaff on csraStaff.program_id = tempReport.program_id and csraStaff.fte_month = tempReport.fte_month  and csraStaff.type = tempReport.type and  csraStaff.fiscal_month_end = tempReport.fiscal_month_end

LEFT JOIN (SELECT * from tempReport WHERE tempReport.staffing_source = 'sub' order by tempReport.fiscal_month_end DESC
) subStaff on subStaff.program_id = tempReport.program_id and subStaff.fte_month = tempReport.fte_month and subStaff.type = tempReport.type and subStaff.fiscal_month_end = tempReport.fiscal_month_end

LEFT JOIN ODC on ODC.project_id = tempReport.project_id
order by tempReport.program_id DESC,tempReport.fte_month ASC, tempReport.type, tempReport.project_id, tempReport.fiscal_month_end DESC
