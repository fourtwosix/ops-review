SELECT * FROM (
  WITH contract_detail AS(
  SELECT
    programs_secure.project_id,
    ROW_NUMBER() OVER (PARTITION BY programs_secure.project_id ORDER BY programs_secure.project_id, program_contract_option_entry.start_date ASC) AS RowNumber,

    to_char(program_contract_option_entry.start_date, 'MM-DD-YYYY') as start_date,
    to_char(program_contract_option_entry.end_date, 'MM-DD-YYYY') as end_date,
    program_contract_option_entry.exercised,
    program_contract_option_entry.funded
  FROM programs_secure
    INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id
    LEFT JOIN program_contract_option_entry
      ON program_contract_option_entry.program_id = programs_secure.id

     <%- roleClause %>
  WHERE <%- groupClause %>
    AND <%- executiveDivisionClause %>
    AND <%- userClause %>
    AND <%- l2_ManagerClause%>
    AND <%- l3_ManagerClause%>
    AND <%- l2ManagerClause %>
    AND <%- divisionClause %>
    AND <%- l3ManagerClause %>
    AND <%- programManagerClause %>
    AND <%- l4ManagerClause %>
    AND <%- programClause %>
    AND <%- losClause %>
    AND <%- praClause %>
    AND <%- statusClause %>
    AND program_contract_option_entry.start_date IS NOT NULL
    ORDER BY programs_secure.project_id,
             program_contract_option_entry.start_date ASC
   <%- limit %> <%- offset %>
 )
 SELECT
     contract_detail.project_id,
     Case WHEN(RowNumber = 1) THEN 'Base Option' ELSE concat('Option', to_char(RowNumber-1, '999'))  END as option,

     contract_detail.start_date,
     contract_detail.end_date,
     contract_detail.exercised,
     contract_detail.funded
     FROM contract_detail
  ) table_query
