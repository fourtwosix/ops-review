WITH program_staffing AS (SELECT
                            CASE WHEN program_staffing.staffing_source :: TEXT = 'csra'
                              THEN 'GDIT'
                            ELSE program_staffing.staffing_source :: TEXT END AS staffing_source,
                            program_staffing.type,
                            program_staffing.fte,
                            program_staffing.fiscal_month_end,
                            program_staffing.program_id
                          FROM program_staffing
                            INNER JOIN
                            programs_secure ON programs_secure.id = program_staffing.program_id
                            INNER JOIN
                            (
                              SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
                              FROM program_metadata_manual_entry
                              WHERE
                                validated = TRUE
                                AND program_metadata_manual_entry.program_status = 'open'
                              ORDER BY program_id,
                                created_at DESC
                            ) program_metadata_manual_entry
                              ON programs_secure.id = program_metadata_manual_entry.program_id
                             <%- roleClause %>
                          WHERE <%-programClause %>
                                AND <%- userClause %>
                                AND <%-programManagerClause %>
                                AND <%-losClause %>
                                AND <%-praClause %>
                                AND <%-l2_ManagerClause %>
                                AND <%-groupClause %>
                                AND <%- executiveDivisionClause %>
                                AND <%-l3_ManagerClause %>
                                AND <%-l4ManagerClause %>
                                AND <%-divisionClause %>
                                AND program_staffing.validated = TRUE
                                AND program_staffing.type = 'actual'
),

    current_month AS
  (
    SELECT program_staffing.*
    FROM program_staffing
    WHERE
      date_trunc('month', program_staffing.fiscal_month_end) =
      date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
  ),

    previous_month AS
  (
    SELECT DISTINCT ON (program_staffing.program_id, program_staffing.staffing_source) program_staffing.*
    FROM program_staffing
    WHERE
      date_trunc('month', program_staffing.fiscal_month_end) <
      date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
    ORDER BY program_staffing.program_id, program_staffing.staffing_source, program_staffing.fiscal_month_end DESC
  ),
    fy_end AS
  (
    SELECT
      DISTINCT ON (program_staffing.program_id, program_staffing.staffing_source) program_staffing.*
    FROM program_staffing
    WHERE
      date_trunc('month', program_staffing.fiscal_month_end) >=
      CASE WHEN
        date_part('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) = 4
        THEN
          date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '1 month'
      ELSE
        date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
      END
    ORDER BY program_staffing.program_id, program_staffing.staffing_source, program_staffing.fiscal_month_end ASC
  ),
    fy_change AS
  (
    SELECT
      SUM(current_month.fte - fy_end.fte) AS change,
      current_month.staffing_source
    FROM current_month
      LEFT JOIN fy_end
        ON current_month.program_id = fy_end.program_id AND current_month.staffing_source = fy_end.staffing_source
    GROUP BY current_month.staffing_source

    UNION ALL

    SELECT
      SUM(current_month.fte - fy_end.fte) AS change,
      'total'                             AS staffing_source
    FROM current_month
      LEFT JOIN fy_end
        ON current_month.program_id = fy_end.program_id AND current_month.staffing_source = fy_end.staffing_source
  ),
    prev_change AS
  (
    SELECT
      SUM(current_month.fte - previous_month.fte) AS change,
      current_month.staffing_source
    FROM current_month
      LEFT JOIN
      previous_month
        ON current_month.program_id = previous_month.program_id AND
           current_month.staffing_source = previous_month.staffing_source
    GROUP BY current_month.staffing_source

    UNION ALL

    SELECT
      SUM(current_month.fte - previous_month.fte) AS change,
      'total'                                     AS staffing_source
    FROM current_month
      LEFT JOIN previous_month
        ON current_month.program_id = previous_month.program_id AND
           current_month.staffing_source = previous_month.staffing_source
  )

SELECT
  fy_change.change   AS "Change from FY Start",
  prev_change.change AS "Change from Previous Period",
  prev_change.staffing_source
FROM prev_change
  LEFT JOIN fy_change
    ON prev_change.staffing_source = fy_change.staffing_source
ORDER BY
  lower(prev_change.staffing_source) = 'total' DESC,
  lower(prev_change.staffing_source) = 'csra' DESC,
  lower(prev_change.staffing_source) = 'sub' DESC
