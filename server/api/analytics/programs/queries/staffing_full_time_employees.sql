WITH program_staffing_filtered AS
(SELECT
   program_staffing.program_id,
   date_trunc('month', program_staffing.fiscal_month_end) :: DATE AS fiscal_month_end,
   program_staffing.staffing_source,
   program_staffing.type,
   program_staffing.fte,
   date_trunc('month', program_staffing.fte_month) :: DATE        AS fte_month
 FROM programs_secure


   INNER JOIN
   (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
    FROM program_metadata_manual_entry
    ORDER BY program_id, created_at DESC)
   program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
   INNER JOIN program_staffing ON programs_secure.id = program_staffing.program_id
   <%- roleClause %>
 WHERE <%-programClause %>
       AND            <%- userClause %>
       AND <%- executiveDivisionClause %>
       AND <%-programManagerClause %>
       AND <%-losClause %>
       AND <%-praClause %>
       AND <%-l2_ManagerClause %>
       AND <%-groupClause %>
       AND <%-l3_ManagerClause %>
       AND <%-l4ManagerClause %>
       AND <%-divisionClause %>
       AND <%-statusClause %>
       AND date_trunc('month', program_staffing.fiscal_month_end) >=
           date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
       AND date_trunc('month', program_staffing.fiscal_month_end) <=
           date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
       AND program_staffing.validated IS TRUE),
    program_staffing_csra AS (SELECT DISTINCT ON (program_id, type, fte_month)
                                program_id,
                                fte,
                                fte_month,
                                type
                              FROM program_staffing_filtered
                              WHERE program_staffing_filtered.staffing_source = 'csra'
                              ORDER BY program_id, type, fte_month, fiscal_month_end DESC
  ),
    program_staffing_sub AS (SELECT DISTINCT ON (program_id, type, fte_month)
                               program_id,
                               fte,
                               fte_month,
                               type
                             FROM program_staffing_filtered
                             WHERE program_staffing_filtered.staffing_source = 'sub'
                             ORDER BY program_id, type, fte_month, fiscal_month_end DESC
  )


SELECT
  program_staffing_filtered.fte_month                            AS month,
  SUM(COALESCE(program_staffing_filtered.baseline_fte, 0))       AS "Baseline",
  SUM(COALESCE(program_staffing_filtered.csra_forecasts_fte, 0)) AS "GDIT Forecasts",
  SUM(COALESCE(program_staffing_filtered.sub_forecasts_fte, 0))  AS "Subcontractor Forecasts",
  SUM(COALESCE(program_staffing_filtered.csra_actuals_fte, 0))   AS "GDIT Actuals",
  SUM(COALESCE(program_staffing_filtered.sub_actuals_fte, 0))    AS "Subcontractor Actuals"
FROM
  (SELECT DISTINCT ON (program_staffing_filtered.fte_month)
     program_staffing_filtered.fte_month,
     COALESCE(csra_baseline.fte, 0) + COALESCE(sub_baseline.fte, 0) AS baseline_fte,
     csra_forecasts.fte                                             AS csra_forecasts_fte,
     sub_forecasts.fte                                              AS sub_forecasts_fte,
     csra_actuals.fte                                               AS csra_actuals_fte,
     sub_actuals.fte                                                AS sub_actuals_fte
   FROM program_staffing_filtered
     LEFT JOIN (SELECT
                  fte_month,
                  SUM(COALESCE(fte, 0)) AS fte
                FROM program_staffing_csra
                WHERE program_staffing_csra.type = 'baseline'
                GROUP BY fte_month
               ) csra_baseline
       ON
         program_staffing_filtered.fte_month = csra_baseline.fte_month
     LEFT JOIN (SELECT
                  fte_month,
                  SUM(COALESCE(fte, 0)) AS fte
                FROM program_staffing_csra
                WHERE program_staffing_csra.type = 'actual'
                GROUP BY fte_month
               ) csra_actuals
       ON
         program_staffing_filtered.fte_month = csra_actuals.fte_month

     LEFT JOIN (SELECT
                  fte_month,
                  SUM(COALESCE(fte, 0)) AS fte
                FROM program_staffing_csra
                WHERE program_staffing_csra.type = 'forecast'
                GROUP BY fte_month
               ) csra_forecasts
       ON
         program_staffing_filtered.fte_month = csra_forecasts.fte_month
     LEFT JOIN (SELECT
                  fte_month,
                  SUM(COALESCE(fte, 0)) AS fte
                FROM program_staffing_sub
                WHERE program_staffing_sub.type = 'baseline'
                GROUP BY fte_month
               ) sub_baseline
       ON
         program_staffing_filtered.fte_month = sub_baseline.fte_month

     LEFT JOIN (SELECT
                  fte_month,
                  SUM(COALESCE(fte, 0)) AS fte
                FROM program_staffing_sub
                WHERE program_staffing_sub.type = 'actual'
                GROUP BY fte_month
               ) sub_actuals
       ON
         program_staffing_filtered.fte_month = sub_actuals.fte_month

     LEFT JOIN (SELECT
                  fte_month,
                  SUM(COALESCE(fte, 0)) AS fte
                FROM program_staffing_sub
                WHERE program_staffing_sub.type = 'forecast'
                GROUP BY fte_month
               ) sub_forecasts
       ON
         program_staffing_filtered.fte_month = sub_forecasts.fte_month
   WHERE
     program_staffing_filtered.fte_month >=
     date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD')) :: DATE
     AND
     program_staffing_filtered.fte_month <=
     date_trunc('month', to_date('<%- fiscalEnd %>', 'YYYY-MM-DD')) :: DATE
  ) program_staffing_filtered
GROUP BY program_staffing_filtered.fte_month
ORDER BY program_staffing_filtered.fte_month
