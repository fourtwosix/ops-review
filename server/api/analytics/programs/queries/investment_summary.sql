WITH investment_total AS (
  SELECT DISTINCT
    program_investment_totals.program_id,
    program_investment_totals.fiscal_month,
    program_investment_totals.type,
    program_investment_totals.value
  FROM programs_secure
    INNER JOIN (SELECT distinct on (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id
    LEFT JOIN program_investment_totals
      ON programs_secure.id = program_investment_totals.program_id
         AND date_trunc('month', program_investment_totals.fiscal_month) >=
             date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
         AND date_trunc('month', program_investment_totals.fiscal_month) <=
             date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))

<%- roleClause %>
  WHERE
    <%-programManagerClause %>
    AND <%- executiveDivisionClause %>
    AND <%-userClause %>
    AND <%- groupClause %>
    AND <%-metaPMClause %>
    AND <%-praClause%>
    AND <%- l2_ManagerClause %>
    AND <%- l3_ManagerClause%>
    AND <%- l4ManagerClause %>
    AND <%- divisionClause %>
    AND <%-programClause %>
    AND program_metadata_manual_entry.validated = TRUE
    AND <%- statusClause %>
)
SELECT
	CASE WHEN (investment_summary.type = 'AR30') THEN 'A/R < 30'
      WHEN (investment_summary.type = 'AR3060') THEN 'A/R 30 - 60'
      WHEN (investment_summary.type = 'AR6190') THEN 'A/R 61 - 90'
      WHEN (investment_summary.type = 'AR90') THEN 'A/R > 90'
      WHEN (investment_summary.type = 'unbilled') THEN 'Unbilled'
      WHEN (investment_summary.type = 'retained_wip') THEN 'Retained WIP'
      WHEN (investment_summary.type = 'Investment Total') THEN 'Investment Total'
      END AS type,
	    CASE WHEN (investment_summary.type IS NULL) THEN NULL ELSE concat('$', to_char(COALESCE(investment_summary.value,0), '9,999,999,999,999,999')) END AS value
FROM(
SELECT
	investment_total.fiscal_month,
	investment_total.type,
	  SUM(investment_total.value) as value
      FROM investment_total
      GROUP BY investment_total.type, investment_total.fiscal_month

  UNION
  (SELECT
  		investment_total.fiscal_month,
      'Investment Total' as type,
      SUM(investment_total.value) as value
    FROM  investment_total
    WHERE investment_total.fiscal_month IS NOT NULL
    GROUP BY investment_total.fiscal_month
  )
)investment_summary
      ORDER BY (CASE WHEN investment_summary.type = 'unbilled' THEN 0
                      WHEN investment_summary.type = 'AR30' THEN 1
                      WHEN investment_summary.type = 'AR3060' THEN 2
                      WHEN investment_summary.type = 'AR6190' THEN 3
                      WHEN investment_summary.type = 'AR90' THEN 4
                      WHEN investment_summary.type = 'retained_wip' THEN 5
                      WHEN investment_summary.type = 'Investment Total' THEN 6 END)
