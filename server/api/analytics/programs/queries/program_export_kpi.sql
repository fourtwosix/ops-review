SELECT programs_secure.project_id,
program_export_international_entry.id,
COALESCE(program_export_international_entry.fiscal_month_end, '<%- endDate %>') AS fiscal_month_end,
program_export_international_entry.h_s_ware AS "Hardware/Software",
program_export_international_entry.performance_services,
program_export_international_entry.us_subcontractor,
program_export_international_entry.foreign_subcontractor,
program_export_international_entry.foreign_h_s_ware   AS "Foreign Hardware/Software",
program_export_international_entry.personal_protective_equipment,
program_export_international_entry.foreign_millitary_sale AS "Foreign Military Sale",
program_export_international_entry.export_license,
program_export_international_entry.overall_project,
program_export_international_entry.type
FROM programs_secure
INNER JOIN
  (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
   FROM program_metadata_manual_entry WHERE
     validated = true
     AND <%- statusClause %>
   ORDER BY program_id,
            created_at DESC) program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
LEFT JOIN program_export_international_entry ON program_export_international_entry.program_id = programs_secure.id
AND (((date_trunc('month', program_export_international_entry.fiscal_month_end) > date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '1 year')
      AND (date_trunc('month', program_export_international_entry.fiscal_month_end) < date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
      AND program_export_international_entry.type = 'actual')
     OR (date_trunc('month', program_export_international_entry.fiscal_month_end) = date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))))
<%- roleClause %>
WHERE <%- programClause %>
AND <%- userClause %>
AND <%- executiveDivisionClause %>
AND <%- l4ManagerClause %>
AND <%- programManagerClause %>
AND <%- losClause %>
AND <%- praClause %>
AND <%- l2_ManagerClause %>
AND <%- groupClause %>
AND <%- l3_ManagerClause %>
AND <%- divisionClause %>
AND (date_trunc('month', program_export_international_entry.fiscal_month_end) > date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '1 year')
AND (date_trunc('month', program_export_international_entry.fiscal_month_end) <= date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
AND program_export_international_entry.type = 'actual'
ORDER BY program_export_international_entry.fiscal_month_end
