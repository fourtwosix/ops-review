SELECT
  programs_secure.project_id,
  program_cyber_security.id,
  COALESCE(program_cyber_security.fiscal_month_end, '<%- endDate %>') AS fiscal_month_end,
  COALESCE(program_cyber_security.security_authorization, 'N/S') AS security_authorization,
  COALESCE(program_cyber_security.scanning, 'N/S')         AS scanning,
  COALESCE(program_cyber_security.flaw_remediation, 'N/S')  AS flaw_remediation,
  COALESCE(program_cyber_security.inventory_status, 'N/S')           AS inventory_status,
  COALESCE(program_cyber_security.asset_status, 'N/S')                 AS asset_status,
  COALESCE(program_cyber_security.external_access, 'N/S')                  AS external_access,
  COALESCE(program_cyber_security.device_hardening, 'N/S')                    AS device_hardening,
  COALESCE(program_cyber_security.security_event_management, 'N/S')       AS security_event_management,
  COALESCE(program_cyber_security.identified_security_roles_responsibilities, 'N/S')                 AS identified_security_roles_responsibilities,
  COALESCE(program_cyber_security.account_management, 'N/S')             AS account_management,
  COALESCE(program_cyber_security.contract_security_requirements, 'N/S')                    AS contract_security_requirements,
  COALESCE(program_cyber_security.overall, 'N/S')             AS overall,
  program_cyber_security.type
FROM programs_secure
  INNER JOIN
  (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
   FROM program_metadata_manual_entry
   WHERE validated = TRUE
   ORDER BY program_id, created_at DESC)
  program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
  LEFT JOIN program_cyber_security
    ON
      program_cyber_security.program_id = programs_secure.id
      AND
          (date_trunc('month', program_cyber_security.fiscal_month_end) >
           date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '6 months')
          AND (date_trunc('month', program_cyber_security.fiscal_month_end) <=
               date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
          AND program_cyber_security.type = 'actual'
      AND program_cyber_security.validated = TRUE
      <%- roleClause %>
WHERE <%-programClause %>
      AND <%- userClause %>
      AND <%- executiveDivisionClause %>
      AND <%- l4ManagerClause %>
      AND <%-programManagerClause %>
      AND <%-losClause %>
      AND <%-praClause %>
      AND <%-l2_ManagerClause %>
      AND <%-groupClause %>
      AND <%-l3_ManagerClause %>
      AND <%-divisionClause %>
      AND <%-statusClause %>
ORDER BY fiscal_month_end
