WITH program_metadata_manual_entry AS
(SELECT DISTINCT ON (program_metadata_manual_entry.program_id) program_metadata_manual_entry.*,programs_secure.*
 FROM program_metadata_manual_entry
   INNER JOIN programs_secure ON programs_secure.id = program_metadata_manual_entry.program_id
  <%- roleClause %>
 WHERE
   <%-programClause %>
   AND <%-userClause %>
   AND <%-programManagerClause %>
   AND <%-l4ManagerClause %>
   AND <%-losClause %>
   AND <%-praClause %>
   AND <%-l2_ManagerClause %>
   AND <%-groupClause %>
   AND <%- executiveDivisionClause %>
   AND <%-metaPMClause %>
   AND <%- statusClause %>
   AND <%-l3_ManagerClause %>
   AND <%-divisionClause %>
   AND validated = TRUE
 ORDER BY program_metadata_manual_entry.program_id,
   program_metadata_manual_entry.created_at DESC),
    financials_filtered AS (
    SELECT
      id,
      program_id,
      type,
      date_trunc('month', fiscal_month) :: DATE AS fiscal_month_end,
      value                                     AS current_month_actual
    FROM program_fy_financials
    WHERE
      date_trunc('month', fiscal_month) >=
       date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
      AND date_trunc('month', fiscal_month) <=
           date_trunc('month', to_date('<%- fiscalGDITEnd %>', 'YYYY-MM-DD'))
           UNION
     SELECT
       id,
       program_id,
       type,
       date_trunc('month', fiscal_month) :: DATE AS fiscal_month_end,
       value                                     AS current_month_actual
     FROM program_fy_financials
     WHERE
     date_trunc('month', fiscal_month) >=
      date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
     AND date_trunc('month', fiscal_month) <=
          date_trunc('month', to_date('<%- fiscalGDITEnd %>', 'YYYY-MM-DD'))
  )
SELECT *
FROM
  (SELECT DISTINCT
     financials_filtered.fiscal_month_end                 AS month,
     SUM(SUM(revenue.current_month_actual))
     OVER (
       ORDER BY financials_filtered.fiscal_month_end ASC) AS cumulative_revenue,
     SUM(SUM(revenue.current_month_actual) - SUM(oi_forecast.current_month_actual))
     OVER (
       ORDER BY financials_filtered.fiscal_month_end ASC) AS cumulative_operating_costs,
     SUM(SUM(revenue_budget.current_month_actual))
     OVER (
       ORDER BY financials_filtered.fiscal_month_end)     AS target_revenue
   FROM
     financials_filtered
     INNER JOIN program_metadata_manual_entry
       ON financials_filtered.program_id = program_metadata_manual_entry.program_id
     LEFT JOIN LATERAL (SELECT
                          COALESCE(financials_filtered.current_month_actual, 0) AS current_month_actual,
                          financials_filtered.type,
                          financials_filtered.fiscal_month_end
                        WHERE financials_filtered.type = 'revenueForecast') revenue
       ON financials_filtered.type = revenue.type AND
          date_trunc('month', financials_filtered.fiscal_month_end) =
          date_trunc('month', revenue.fiscal_month_end)
     LEFT JOIN LATERAL (SELECT
                          COALESCE(financials_filtered.current_month_actual, 0) AS current_month_actual,
                          financials_filtered.type,
                          financials_filtered.fiscal_month_end
                        WHERE financials_filtered.type = 'oiForecast') oi_forecast
       ON financials_filtered.type = oi_forecast.type AND
          date_trunc('month', financials_filtered.fiscal_month_end) =
          date_trunc('month', oi_forecast.fiscal_month_end)
     LEFT JOIN LATERAL (SELECT
                          COALESCE(financials_filtered.current_month_actual, 0) AS current_month_actual,
                          financials_filtered.type,
                          financials_filtered.fiscal_month_end
                        WHERE financials_filtered.type = 'revenueBudget') revenue_budget
       ON financials_filtered.type = revenue_budget.type AND
          date_trunc('month', financials_filtered.fiscal_month_end) =
          date_trunc('month', revenue_budget.fiscal_month_end)

   GROUP BY financials_filtered.fiscal_month_end
   ORDER BY financials_filtered.fiscal_month_end
  ) financials_filtered
