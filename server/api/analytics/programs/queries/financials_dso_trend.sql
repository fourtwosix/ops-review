SELECT
  program_financial_status.fiscal_month_end,
  program_financial_status.current_month_actual AS "DSO"
FROM programs_secure
  INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
              FROM program_metadata_manual_entry
              WHERE program_metadata_manual_entry.validated = TRUE
              ORDER BY program_id, created_at DESC) program_metadata_manual_entry
    ON programs_secure.id = program_metadata_manual_entry.program_id
  INNER JOIN
  program_financial_status
    ON programs_secure.id = program_financial_status.program_id
       AND program_financial_status.type = 'fundingColor'
       AND program_financial_status.validated = TRUE
       AND (date_trunc('month', program_financial_status.fiscal_month_end) >=
            date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '1 year')
       AND (date_trunc('month', program_financial_status.fiscal_month_end) <=
            date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
WHERE
  <%-programClause %>
  AND program_metadata_manual_entry.validated = TRUE
  AND program_metadata_manual_entry.program_status = 'open'
ORDER BY program_financial_status.fiscal_month_end ASC
