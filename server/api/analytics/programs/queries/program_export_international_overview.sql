SELECT
h_s_ware as "Hardware/Software",
performance_services,
us_subcontractor,
foreign_subcontractor,
foreign_h_s_ware as "Foreign Hardware/Software",
personal_protective_equipment,
foreign_millitary_sale AS "Foreign Military Sale",
export_license,
overall_project
FROM programs_secure
  INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
              FROM program_metadata_manual_entry
              WHERE program_metadata_manual_entry.validated = TRUE
              ORDER BY program_id, created_at DESC) program_metadata_manual_entry
    ON programs_secure.id = program_metadata_manual_entry.program_id
  LEFT JOIN program_export_international_entry
    ON program_export_international_entry.program_id = programs_secure.id
    AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
        date_trunc('month', fiscal_month_end)
    AND program_export_international_entry.validated = TRUE
    AND program_export_international_entry.type = 'actual'
    <%- roleClause %>
WHERE <%-losClause %> AND <%- userClause %> AND <%-l4ManagerClause %> AND <%-programManagerClause %> AND <%-praClause %> AND <%-l3_ManagerClause %> AND <%-l2_ManagerClause %>  AND <%- divisionClause %> AND <%- groupClause %> AND <%- statusClause %>     AND <%- executiveDivisionClause %>
