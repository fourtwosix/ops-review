SELECT * FROM (
WITH program_staffing_filtered AS
(SELECT
   program_staffing_positions.program_id,
   date_trunc('month', program_staffing_positions.fiscal_month_end) :: DATE AS fiscal_month_end,
   program_staffing_positions.staffing_source,
   program_staffing_positions.type,
   program_staffing_positions.value,
   program_metadata_manual_entry.small_business_goal_percentage as cgv
 FROM programs_secure
   INNER JOIN
   (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
    FROM program_metadata_manual_entry
    ORDER BY program_id, created_at DESC)
   program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
   INNER JOIN program_staffing_positions ON programs_secure.id = program_staffing_positions.program_id
   <%- roleClause %>
 WHERE <%-programClause %>
       AND <%- userClause %>
       AND <%- executiveDivisionClause %>
       AND <%-programManagerClause %>
       AND <%-losClause %>
       AND <%-praClause %>
       AND <%-l2_ManagerClause %>
       AND <%-groupClause %>
       AND <%-l3_ManagerClause %>
       AND <%-l4ManagerClause %>
       AND <%-divisionClause %>
       AND <%-statusClause %>
       AND date_trunc('month', program_staffing_positions.fiscal_month_end) =
           date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
  ),
  staffing_position AS(

  SELECT DISTINCT ON(  program_staffing_filtered.program_id,  program_staffing_filtered.fiscal_month_end,  program_staffing_filtered.staffing_source) program_staffing_filtered.program_id,
    program_staffing_filtered.staffing_source,
    program_staffing_filtered.fiscal_month_end,
    gdit_current.value AS ccv,
    gdit_funded.value AS cfv,
    gdit_thirty.value AS ctv,
    gdit_thirty_sixty.value AS ctsv,
    gdit_sixty.value AS csv,
    gdit_actual.value AS cav,
    gdit_goal.cgv

  FROM program_staffing_filtered
    LEFT JOIN (SELECT program_staffing_filtered.*
             FROM program_staffing_filtered
             WHERE program_staffing_filtered.type = 'total_current'
            ) gdit_current
    ON
      program_staffing_filtered.program_id = gdit_current.program_id AND
      program_staffing_filtered.fiscal_month_end = gdit_current.fiscal_month_end AND
      program_staffing_filtered.staffing_source = gdit_current.staffing_source

          LEFT JOIN (SELECT program_staffing_filtered.*
             FROM program_staffing_filtered
             WHERE program_staffing_filtered.type = 'total_funded'
            ) gdit_funded
    ON
      program_staffing_filtered.program_id = gdit_funded.program_id AND
      program_staffing_filtered.fiscal_month_end = gdit_funded.fiscal_month_end AND
      program_staffing_filtered.staffing_source = gdit_funded.staffing_source

          LEFT JOIN (SELECT program_staffing_filtered.*
             FROM program_staffing_filtered
             WHERE program_staffing_filtered.type = 'openThirty'
            ) gdit_thirty
    ON
      program_staffing_filtered.program_id = gdit_thirty.program_id AND
      program_staffing_filtered.fiscal_month_end = gdit_thirty.fiscal_month_end AND
      program_staffing_filtered.staffing_source = gdit_thirty.staffing_source

          LEFT JOIN (SELECT program_staffing_filtered.*
             FROM program_staffing_filtered
             WHERE program_staffing_filtered.type = 'openThirtySixty'
            ) gdit_thirty_sixty
    ON
      program_staffing_filtered.program_id = gdit_thirty_sixty.program_id AND
      program_staffing_filtered.fiscal_month_end = gdit_thirty_sixty.fiscal_month_end AND
      program_staffing_filtered.staffing_source = gdit_thirty_sixty.staffing_source

  LEFT JOIN (SELECT program_staffing_filtered.*
     FROM program_staffing_filtered
     WHERE program_staffing_filtered.type = 'openSixty'
    ) gdit_sixty
    ON
      program_staffing_filtered.program_id = gdit_sixty.program_id AND
      program_staffing_filtered.fiscal_month_end = gdit_sixty.fiscal_month_end AND
      program_staffing_filtered.staffing_source = gdit_sixty.staffing_source
  LEFT JOIN (SELECT program_staffing_filtered.*
     FROM program_staffing_filtered
     WHERE program_staffing_filtered.type = 'small_business_actual'
   ) gdit_actual
    ON
      program_staffing_filtered.program_id = gdit_actual.program_id AND
      program_staffing_filtered.fiscal_month_end = gdit_actual.fiscal_month_end AND
      program_staffing_filtered.staffing_source = gdit_actual.staffing_source
  LEFT JOIN (SELECT program_staffing_filtered.*
     FROM program_staffing_filtered
     WHERE program_staffing_filtered.staffing_source = 'gdit') gdit_goal
     ON
       program_staffing_filtered.program_id = gdit_goal.program_id AND
       program_staffing_filtered.fiscal_month_end = gdit_goal.fiscal_month_end AND
       program_staffing_filtered.staffing_source = gdit_goal.staffing_source

    )
    SELECT DISTINCT ON (staffing_position.staffing_source)
              CASE WHEN staffing_position.staffing_source = 'gdit' THEN 'GDIT Positions'
                   WHEN  staffing_position.staffing_source = 'sub' THEN 'Sub Positions'  END AS "Staffing Source",
              SUM(staffing_position.ccv) AS "Total Current",
              SUM(staffing_position.cfv) AS "Total Funded",
              SUM(staffing_position.ctv) AS "Open < 30 Days",
              SUM(staffing_position.ctsv) AS "Open 30 to 60 Days",
              SUM(staffing_position.csv) AS "Open > 60 Days",
              SUM(staffing_position.csv) AS "Open > 60 Days",
              CASE WHEN staffing_number.pnum > 1 THEN 'N/A' WHEN SUM(staffing_position.cav) IS NULL THEN NULL ELSE concat(to_char(SUM(staffing_position.cav), '9999999.9'), '%') END AS "Small Business Actual %",
              CASE WHEN staffing_number.pnum > 1 THEN 'N/A' WHEN SUM(staffing_position.cgv) IS NULL THEN NULL ELSE concat(to_char(SUM(staffing_position.cgv), '9999999.9'), '%') END AS "Small Business Goal %",
              CASE WHEN staffing_number.pnum > 1 THEN 'N/A' WHEN SUM(staffing_position.cav) IS NULL THEN NULL ELSE concat(to_char(round((SUM(staffing_position.cav) - SUM(staffing_position.cgv)) / SUM(NULLIF(staffing_position.cav,0)) * 100,1),'999999.9'), '%') END AS "Small Business Variance"

    				  FROM staffing_position
             , (SELECT COUNT(*) OVER() AS pnum   FROM staffing_position GROUP BY  program_id LIMIT 1) staffing_number

    				  GROUP BY staffing_position.staffing_source,staffing_position.fiscal_month_end, staffing_number.pnum

  )table_query
  ORDER BY (CASE WHEN table_query."Staffing Source" = 'GDIT Positions' THEN 0
                WHEN table_query."Staffing Source" = 'Sub Positions'  THEN 1
                  ELSE 2 END)
