SELECT * FROM (
WITH program_staffing_filtered AS
(SELECT
   program_staffing_positions.program_id,
   date_trunc('month', program_staffing_positions.fiscal_month_end) :: DATE AS fiscal_month_end,
   program_staffing_positions.staffing_source,
   program_staffing_positions.type,
   program_staffing_positions.value
 FROM programs_secure
   INNER JOIN
   (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
    FROM program_metadata_manual_entry
    ORDER BY program_id, created_at DESC)
   program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
   INNER JOIN program_staffing_positions ON programs_secure.id = program_staffing_positions.program_id
   <%- roleClause %>
 WHERE <%-programClause %>
       AND <%- userClause %>
       AND <%- executiveDivisionClause %>
       AND <%-programManagerClause %>
       AND <%-losClause %>
       AND <%-praClause %>
       AND <%-l2_ManagerClause %>
       AND <%-groupClause %>
       AND <%-l3_ManagerClause %>
       AND <%-l4ManagerClause %>
       AND <%-divisionClause %>
       AND <%-statusClause %>
       AND date_trunc('month', program_staffing_positions.fiscal_month_end) >=
           date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
       AND date_trunc('month', program_staffing_positions.fiscal_month_end) <=
           date_trunc('month', to_date('<%- fiscalGDITEnd %>', 'YYYY-MM-DD'))
  ),
  staffing_position AS(

    SELECT DISTINCT ON(  program_staffing_filtered.program_id,  program_staffing_filtered.fiscal_month_end,  program_staffing_filtered.staffing_source)
  	program_staffing_filtered.program_id,
    program_staffing_filtered.staffing_source,
    program_staffing_filtered.fiscal_month_end,
    gdit_thirty.value AS ctv,
    gdit_thirty_sixty.value AS ctsv,
    gdit_sixty.value AS csv

  FROM program_staffing_filtered

  LEFT JOIN (SELECT program_staffing_filtered.*
             FROM program_staffing_filtered
             WHERE program_staffing_filtered.type = 'openThirty'
            ) gdit_thirty
    ON
      program_staffing_filtered.program_id = gdit_thirty.program_id AND
      program_staffing_filtered.fiscal_month_end = gdit_thirty.fiscal_month_end AND
      program_staffing_filtered.staffing_source = gdit_thirty.staffing_source

          LEFT JOIN (SELECT program_staffing_filtered.*
             FROM program_staffing_filtered
             WHERE program_staffing_filtered.type = 'openThirtySixty'
            ) gdit_thirty_sixty
    ON
      program_staffing_filtered.program_id = gdit_thirty_sixty.program_id AND
      program_staffing_filtered.fiscal_month_end = gdit_thirty_sixty.fiscal_month_end AND
      program_staffing_filtered.staffing_source = gdit_thirty_sixty.staffing_source

  LEFT JOIN (SELECT program_staffing_filtered.*
     FROM program_staffing_filtered
     WHERE program_staffing_filtered.type = 'openSixty'
    ) gdit_sixty
    ON
      program_staffing_filtered.program_id = gdit_sixty.program_id AND
      program_staffing_filtered.fiscal_month_end = gdit_sixty.fiscal_month_end AND
      program_staffing_filtered.staffing_source = gdit_sixty.staffing_source

      -- GROUP BY program_staffing_filtered.staffing_source, program_staffing_filtered.fiscal_month_end
    )
    SELECT DISTINCT ON (staffing_position.fiscal_month_end)
			   staffing_position.fiscal_month_end as month,
              SUM(COALESCE(staffing_position.ctv,0)) AS "Open < 30",
              SUM(COALESCE(staffing_position.ctsv,0)) AS "Open 30 - 60",
              SUM(COALESCE(staffing_position.csv,0)) AS "Open > 60",
              SUM(COALESCE(staffing_position.ctv,0)) AS "Open < 30 ",
              SUM(COALESCE(staffing_position.ctsv,0)) AS "Open 30 - 60 ",
              SUM(COALESCE(staffing_position.csv,0)) AS "Open > 60 "

			  FROM staffing_position
			  WHERE NOT staffing_position.staffing_source = 'small' AND
              staffing_position.fiscal_month_end >=
             date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD')) :: DATE
             AND
             staffing_position.fiscal_month_end <=
             date_trunc('month', to_date('<%- fiscalGDITEnd %>', 'YYYY-MM-DD')) :: DATE
				GROUP BY staffing_position.fiscal_month_end
        ORDER BY staffing_position.fiscal_month_end
  )table_query
