SELECT programs_secure.project_id,
program_heatmap_manual_entry.id,
COALESCE(program_heatmap_manual_entry.fiscal_month_end, '<%- endDate %>') AS fiscal_month_end,
program_heatmap_manual_entry.revenue_color                  AS "FY Revenue $",
program_heatmap_manual_entry.margin_dollar_color       AS "FY Margin $",
program_heatmap_manual_entry.fyoi as "FY Margin %",
program_heatmap_manual_entry.investment,
program_heatmap_manual_entry.technical,
program_heatmap_manual_entry.schedule,
program_heatmap_manual_entry.customer_satisfaction AS "Customer Satisfaction",
program_heatmap_manual_entry.contractual as "Contract",
program_heatmap_manual_entry.supplier_status as "Internal Support & Suppliers",
program_heatmap_manual_entry.staffing,
program_heatmap_manual_entry.cyber_security,
program_heatmap_manual_entry.overall_project,
program_heatmap_manual_entry.type
FROM programs_secure
INNER JOIN
  (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
   FROM program_metadata_manual_entry WHERE
     validated = true
     AND <%- statusClause %>
   ORDER BY program_id,
            created_at DESC) program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
LEFT JOIN program_heatmap_manual_entry ON program_heatmap_manual_entry.program_id = programs_secure.id
AND (((date_trunc('month', program_heatmap_manual_entry.fiscal_month_end) > date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '1 year')
      AND (date_trunc('month', program_heatmap_manual_entry.fiscal_month_end) < date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
      AND program_heatmap_manual_entry.type = 'actual')
     OR (date_trunc('month', program_heatmap_manual_entry.fiscal_month_end) = date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))))
<%- roleClause %>
WHERE <%- programClause %>
AND <%- userClause %>
AND <%- executiveDivisionClause %>
AND <%- l4ManagerClause %>
AND <%- programManagerClause %>
AND <%- losClause %>
AND <%- praClause %>
AND <%- l2_ManagerClause %>
AND <%- groupClause %>
AND <%- l3_ManagerClause %>
AND <%- divisionClause %>
AND (date_trunc('month', program_heatmap_manual_entry.fiscal_month_end) > date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '1 year')
AND (date_trunc('month', program_heatmap_manual_entry.fiscal_month_end) <= date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
AND program_heatmap_manual_entry.type = 'actual'
ORDER BY program_heatmap_manual_entry.fiscal_month_end
