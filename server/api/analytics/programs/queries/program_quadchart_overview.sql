SELECT *
FROM (
       SELECT DISTINCT ON (program_quadchart_manual_entry.program_id, program_quadchart_manual_entry.fiscal_month_end)
         upper(programs_secure.project_id)                               AS program_number,
         programs_secure.name                                            AS program_name,
         program_metadata_manual_entry.program_manager,
         upper((programs_secure.project_id || ' -- ' || programs_secure.name || ' -- ' ||
                program_metadata_manual_entry.program_manager))          AS quad_title,
         program_metadata_manual_entry.pra,
         program_metadata_manual_entry.key_subs,
         program_metadata_manual_entry.contract_type,
         to_char(program_metadata_manual_entry.start_date, 'MM-DD-YYYY') AS start_date,
         to_char(program_metadata_manual_entry.end_date, 'MM-DD-YYYY')   AS end_date,
         program_metadata_manual_entry.payment_terms,
         program_metadata_manual_entry.project_scope,
         program_metadata_manual_entry.program_status,
         program_quadchart_manual_entry.program_id,
         program_quadchart_manual_entry.fiscal_month_end,
         program_quadchart_manual_entry.opportunities,
         program_quadchart_manual_entry.issues,
         program_quadchart_manual_entry.risks,
         program_quadchart_manual_entry.key_milestones,
         program_quadchart_manual_entry.noteworthy,
         program_quadchart_manual_entry.created_at,
         program_quadchart_manual_entry.created_by,
         program_quadchart_manual_entry.updated_at,
         program_quadchart_manual_entry.updated_by,
         program_quadchart_manual_entry.validated
       FROM programs_secure
         LEFT JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
                    FROM program_metadata_manual_entry
                    WHERE validated = TRUE
                    ORDER BY program_id, created_at DESC)
                   program_metadata_manual_entry
           ON programs_secure.id = program_metadata_manual_entry.program_id
         LEFT JOIN program_quadchart_manual_entry
           ON program_metadata_manual_entry.program_id = program_quadchart_manual_entry.program_id
           <%- roleClause %>
       WHERE TRUE
            AND <%- userClause %>
            AND <%- executiveDivisionClause %>
             AND <%-groupClause %>
             AND <%-l2_ManagerClause %>
             AND <%-divisionClause %>
             AND <%-l3_ManagerClause %>
             AND <%-l4ManagerClause %>
             AND <%-programManagerClause %>
             AND <%-programClause %>
             AND <%-losClause %>
             AND <%-praClause %>
             AND <%-statusClause %>
             AND program_quadchart_manual_entry.validated = TRUE
             AND date_trunc('month', program_quadchart_manual_entry.fiscal_month_end) =
                 date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
       ORDER BY program_quadchart_manual_entry.program_id ASC,
         program_quadchart_manual_entry.fiscal_month_end DESC
     ) program_quadchart_manual_entry
ORDER BY program_number ASC;
