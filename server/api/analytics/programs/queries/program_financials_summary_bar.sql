WITH program_fy_financials_temp AS (
  SELECT DISTINCT
  program_metadata_manual_entry.program_id as program_id,
    program_fy_financials.fiscal_month,
    program_fy_financials.type,
    program_fy_financials.value,
    program_financial_status.current_month_actual AS dso
  FROM programs_secure


    INNER JOIN (SELECT distinct on (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id
    LEFT JOIN program_fy_financials
      ON programs_secure.id = program_fy_financials.program_id
         AND program_fy_financials.type IN
             ('oiForecast', 'oiBudget', 'revenueForecast', 'revenueBudget')
         AND date_trunc('month', program_fy_financials.fiscal_month) >=
             date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
         AND date_trunc('month', program_fy_financials.fiscal_month) <=
             date_trunc('month', to_date('<%- fiscalGDITEnd %>', 'YYYY-MM-DD'))
    LEFT JOIN
    program_financial_status
      ON programs_secure.id = program_financial_status.program_id AND
      program_financial_status.type = 'fundingColor' AND
         program_financial_status.validated = TRUE
         AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
             date_trunc('month', program_financial_status.fiscal_month_end)
<%- roleClause %>
  WHERE
    <%-programManagerClause %>
    AND <%- executiveDivisionClause %>
    AND <%-userClause %>
    AND <%- groupClause %>
    AND <%-metaPMClause %>
    AND <%-praClause%>
    AND <%- l2_ManagerClause %>
    AND <%- l3_ManagerClause%>
    AND <%- l4ManagerClause %>
    AND <%- divisionClause %>
    AND <%-programClause %>
    AND program_metadata_manual_entry.validated = TRUE
    AND <%- statusClause %>
),


ribbon_table AS (
  SELECT DISTINCT ON (program_fy_financials_temp.program_id)
  program_fy_financials_temp.program_id,
  (program_fy_financials_temp.dso)  AS dso,
  (CASE WHEN(revenue_forecast.value IS NOT NULL) THEN revenue_forecast.value
		ELSE NULL END) AS revenue_forecast,

    (CASE WHEN(revenue_budget.value IS NOT NULL) THEN revenue_budget.value
		ELSE NULL END) AS revenue_budget,

    (CASE WHEN(oi_forecast.value IS NOT NULL) THEN oi_forecast.value
		ELSE NULL END) AS oi_forecast,

    (CASE WHEN(oi_budget.value IS NOT NULL) THEN oi_budget.value
		ELSE NULL END) AS oi_budget

FROM program_fy_financials_temp
  LEFT JOIN (SELECT program_fy_financials_temp.program_id, program_fy_financials_temp.type, SUM(program_fy_financials_temp.value) AS value
             FROM program_fy_financials_temp
             WHERE program_fy_financials_temp.type = 'revenueForecast'
             GROUP BY program_id, program_fy_financials_temp.type
            ) revenue_forecast
    ON
      program_fy_financials_temp.program_id = revenue_forecast.program_id


  LEFT JOIN (SELECT program_fy_financials_temp.program_id, program_fy_financials_temp.type, SUM(program_fy_financials_temp.value) AS value
             FROM program_fy_financials_temp
             WHERE program_fy_financials_temp.type = 'revenueBudget'
             GROUP BY program_id, program_fy_financials_temp.type
           ) revenue_budget
    ON
      program_fy_financials_temp.program_id = revenue_budget.program_id


  LEFT JOIN (SELECT program_fy_financials_temp.program_id, program_fy_financials_temp.type, SUM(program_fy_financials_temp.value) AS value
             FROM program_fy_financials_temp
             WHERE program_fy_financials_temp.type = 'oiForecast'
             GROUP BY program_id, program_fy_financials_temp.type
           ) oi_forecast
    ON
      program_fy_financials_temp.program_id = oi_forecast.program_id


  LEFT JOIN (SELECT program_fy_financials_temp.program_id, program_fy_financials_temp.type, SUM(program_fy_financials_temp.value) AS value
             FROM program_fy_financials_temp
             WHERE program_fy_financials_temp.type = 'oiBudget'
             GROUP BY program_id, program_fy_financials_temp.type
           ) oi_budget
    ON
      program_fy_financials_temp.program_id = oi_budget.program_id

)

SELECT
  SUM(ribbon_table.dso)					AS dso,
  SUM(ribbon_table.revenue_forecast)    AS revenue_forecast,
  SUM(ribbon_table.revenue_budget)      AS revenue_budget,
  SUM(ribbon_table.oi_forecast)         AS oi_forecast,
  SUM(ribbon_table.oi_budget)           AS oi_budget,
  round(SUM(ribbon_table.oi_forecast)/SUM(NULLIF(ribbon_table.revenue_forecast,0))*100,1) AS oi_forecast_percent,
  round(SUM(ribbon_table.oi_budget)/SUM(NULLIF(ribbon_table.revenue_budget,0))*100,1)       AS oi_budget_percent
FROM ribbon_table
