SELECT * FROM (
  SELECT
    to_char(program_cyber_security.updated_at, 'MM-DD-YYYY') AS date_submitted,
    to_char('<%- endDate %>'::DATE, 'MM-YYYY') AS fiscal_month,
    upper(programs_secure.project_id) AS program_number,
    programs_secure.name AS program_name,
    program_metadata_manual_entry.program_manager,
    program_metadata_manual_entry.pm_email,
    program_metadata_manual_entry.group AS "Division",
    program_metadata_manual_entry.l2_manager AS "Division Manager",
    program_metadata_manual_entry.division AS "Sector",
    program_metadata_manual_entry.l3_manager AS "Sector Manager",
    program_metadata_manual_entry.business_area,
    program_metadata_manual_entry.l4_manager AS "Business Area Manager",
    program_metadata_manual_entry.pra,
    program_metadata_manual_entry.program_status AS "Heatmap Status",
    program_metadata_manual_entry.customer,
    program_metadata_manual_entry.contract_type,
    program_metadata_manual_entry.fee_structure,
    CASE WHEN program_metadata_manual_entry.fee_value IS NULL THEN 'N/A'
    ELSE program_metadata_manual_entry.fee_value END as fee_value,
    program_metadata_manual_entry.pmr_status,
    program_metadata_manual_entry.pmr_frequency,
    CASE WHEN program_metadata_manual_entry.labor_cat_cert_required = 'true' THEN 'Yes'
      WHEN program_metadata_manual_entry.labor_cat_cert_required = 'false' THEN 'No'
      ELSE NULL END as labor_cat_cert_required,
    CASE WHEN program_metadata_manual_entry.export_international = 'true' THEN 'Yes'
      WHEN program_metadata_manual_entry.export_international = 'false' THEN 'No'
      ELSE NULL END as export_international,
    CASE WHEN program_metadata_manual_entry.gfe_or_property = 'true' THEN 'Yes'
      WHEN program_metadata_manual_entry.gfe_or_property = 'false' THEN 'No'
      ELSE NULL END as gfe_or_property,
    CASE WHEN program_metadata_manual_entry.oci = 'true' THEN 'Yes'
      WHEN program_metadata_manual_entry.oci = 'false' THEN 'No'
      ELSE NULL END as oci,
    program_metadata_manual_entry.lead_for_export_international,
    program_metadata_manual_entry.foreign_sourced_hw_sw,
    program_metadata_manual_entry.required_credentials,
    CASE WHEN program_metadata_manual_entry.scope_of_services IS NULL THEN 'N/A'
    ELSE program_metadata_manual_entry.scope_of_services END as scope_of_services,
    program_metadata_manual_entry.program_description,
    program_metadata_manual_entry.small_business_goal_percentage,
    program_metadata_manual_entry.current_month,
    program_metadata_manual_entry.months_to_complete,
    program_metadata_manual_entry.baseline_margin_percentage AS "Baseline EAC Margin %",
    program_heatmap_manual_entry.eac_margin_percentage AS "EAC Margin %",
    program_heatmap_manual_entry.margin AS "EAC Margin % Color",
    (CASE WHEN (program_metadata_manual_entry.baseline_margin_percentage = 0 OR
                program_metadata_manual_entry.baseline_margin_percentage IS NULL OR
                program_heatmap_manual_entry.eac_margin_percentage IS NULL)
      THEN NULL
     WHEN (program_metadata_manual_entry.baseline_margin_percentage < 0)
       THEN concat(to_char(round((((program_heatmap_manual_entry.eac_margin_percentage -
                                    program_metadata_manual_entry.baseline_margin_percentage) /
                                   program_metadata_manual_entry.baseline_margin_percentage) * 100) * (-1), 1),
                           '999999.9'),'%')
     ELSE concat(to_char(round((((program_heatmap_manual_entry.eac_margin_percentage -
                                  program_metadata_manual_entry.baseline_margin_percentage) /
                                 program_metadata_manual_entry.baseline_margin_percentage) * 100), 1),
                         '999999.9'),'%')
     END) AS "EAC Margin Variance",
     (CASE WHEN (program_metadata_manual_entry.fyoi_budget_percentage IS NOT NULL)
                       THEN concat(to_char(round(program_metadata_manual_entry.fyoi_budget_percentage ),'9999.9'),'%')
           ELSE (CASE WHEN (fyoi_percentage.oi_budget_value IS NULL)
                       THEN NULL
                 ELSE concat(to_char(round(fyoi_percentage.oi_budget_value , 1), '999999.9'), '%')  END)
     END) AS "Baseline FY Margin %",
    (CASE WHEN (program_heatmap_manual_entry.fyoi_percentage  IS NOT NULL)
                      THEN concat(to_char(round(program_heatmap_manual_entry.fyoi_percentage, 1),'9999.9'),'%')
          ELSE (CASE WHEN (fyoi_percentage.oi_forecast_value IS NULL)
                      THEN NULL
              ELSE concat(to_char(round(fyoi_percentage.oi_forecast_value, 1), '999999.9'), '%')  END)
    END) AS "FY Margin %",
    program_heatmap_manual_entry.fyoi AS "FY Margin % Color",
    (CASE 	WHEN (program_heatmap_manual_entry.fyoi_percentage IS NOT NULL AND program_metadata_manual_entry.fyoi_budget_percentage IS NOT NULL)
        	THEN concat(to_char(round((((round(program_heatmap_manual_entry.fyoi_percentage, 1) - round(program_metadata_manual_entry.fyoi_budget_percentage, 1) ) /NULLIF(round(program_metadata_manual_entry.fyoi_budget_percentage, 1),0)) * 100), 1), '999999.9'),'%')

     	   WHEN (program_heatmap_manual_entry.fyoi_percentage IS NULL AND program_metadata_manual_entry.fyoi_budget_percentage IS NOT NULL AND fyoi_percentage.oi_forecast_value IS NOT NULL)
       	   THEN  concat(to_char(round((((round(fyoi_percentage.oi_forecast_value, 1) - round(program_metadata_manual_entry.fyoi_budget_percentage, 1) ) / NULLIF(round(program_metadata_manual_entry.fyoi_budget_percentage, 1),0)) * 100), 1), '999999.9'),'%')
    ELSE 	(CASE WHEN (	fyoi_percentage.oi_budget_value = 0 OR 	fyoi_percentage.oi_budget_value IS NULL OR 	fyoi_percentage.oi_forecast_value IS NULL)
          		  THEN NULL

            	  WHEN (	fyoi_percentage.oi_budget_value < 0)
            	  THEN concat(to_char(round((((round(fyoi_percentage.oi_forecast_value, 1) - round(fyoi_percentage.oi_budget_value, 1)) /
                                   NULLIF(round(fyoi_percentage.oi_budget_value, 1), 0)) * 100) * (-1), 1), '999999.9'),'%')
     ELSE concat(to_char(round((((round(fyoi_percentage.oi_forecast_value, 1) - round(fyoi_percentage.oi_budget_value, 1)) / NULLIF(round(fyoi_percentage.oi_budget_value, 1), 0)) * 100), 1), '999999.9'),'%') END)
     END) AS "FY Margin Variance",

     program_cyber_security.security_authorization,
     program_cyber_security.scanning,
     program_cyber_security.flaw_remediation,
     program_cyber_security.inventory_status,
     program_cyber_security.asset_status,
     program_cyber_security.external_access,
     program_cyber_security.device_hardening,
     program_cyber_security.security_event_management,
     program_cyber_security.identified_security_roles_responsibilities,
     program_cyber_security.account_management,
     program_cyber_security.contract_security_requirements,
     program_cyber_security.overall,
     program_cyber_security.security_authorization_notes,
     program_cyber_security.scanning_notes,
     program_cyber_security.flaw_remediation_notes,
     program_cyber_security.inventory_status_notes,
     program_cyber_security.asset_status_notes,
     program_cyber_security.external_access_notes,
     program_cyber_security.device_hardening_notes,
     program_cyber_security.security_event_management_notes,
     program_cyber_security.identified_security_roles_responsibilities_notes,
     program_cyber_security.account_management_notes,
     program_cyber_security.contract_security_requirements_notes,
     program_cyber_security.overall_notes as "Cyber Security Overall Notes"
  FROM programs_secure
    INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id

    LEFT JOIN program_heatmap_manual_entry AS "forecast_next_overall"
      ON forecast_next_overall.program_id = programs_secure.id
         AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) + interval '1 month'  =
             date_trunc('month', forecast_next_overall.fiscal_month_end)
         AND forecast_next_overall.validated = TRUE
         AND forecast_next_overall.type = 'forecast'

    LEFT JOIN program_heatmap_manual_entry
      ON program_heatmap_manual_entry.program_id = programs_secure.id
         AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
             date_trunc('month', program_heatmap_manual_entry.fiscal_month_end)
         AND program_heatmap_manual_entry.validated = TRUE
         AND program_heatmap_manual_entry.type = 'actual'

    LEFT JOIN program_heatmap_manual_entry pra
      ON pra.program_id = programs_secure.id
         AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
             date_trunc('month', pra.fiscal_month_end)
         AND pra.validated = TRUE
         AND pra.type = 'pra'

     LEFT JOIN program_cyber_security
       ON program_cyber_security.program_id = programs_secure.id
          AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
              date_trunc('month', program_cyber_security.fiscal_month_end)
          AND program_cyber_security.validated = TRUE
          AND program_cyber_security.type = 'actual'

    LEFT JOIN (SELECT program_metadata_manual_entry.program_id,
    						round(fyoi_forecast.value/NULLIF(fyre_forecast.value,0)*100,1) as oi_forecast_value,
    						round(fyoi_budget.value/NULLIF(fyre_budget.value,0)*100,1) as oi_budget_value
    						FROM
    						   (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
 			                FROM program_metadata_manual_entry
 			                WHERE program_metadata_manual_entry.validated = TRUE
 			                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
 		   				LEFT JOIN (SELECT
 		   				fyoi_forecast_calculation.program_id,
 		   				 fyoi_forecast_calculation.type,
 		   				  (SUM(fyoi_forecast_calculation.value)) as value
 		   							FROM (
 						              SELECT program_fy_financials.program_id, program_fy_financials.type,program_fy_financials.value
 				                      FROM program_fy_financials
 						              WHERE program_fy_financials.type = 'oiForecast'
 						              AND date_trunc('month', program_fy_financials.fiscal_month) >=
                          date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
 						              AND date_trunc('month', program_fy_financials.fiscal_month) <=
                          date_trunc('month', to_date('<%- fiscalGDITEnd %>', 'YYYY-MM-DD'))

 					            		) fyoi_forecast_calculation
 		            	 GROUP BY fyoi_forecast_calculation.program_id, fyoi_forecast_calculation.type
 		            	 )fyoi_forecast
          				   ON	fyoi_forecast.program_id = program_metadata_manual_entry.program_id


 		   				LEFT JOIN (SELECT
 		   				fyoi_budget_calculation.program_id,
 		   				 fyoi_budget_calculation.type,
 		   				  (SUM(fyoi_budget_calculation.value)) as value
 		   							FROM (
 						              SELECT program_fy_financials.program_id, program_fy_financials.type,program_fy_financials.value
 				                      FROM program_fy_financials
 						              WHERE program_fy_financials.type = 'oiBudget'
 						              AND date_trunc('month', program_fy_financials.fiscal_month) >=
                          date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
 						              AND date_trunc('month', program_fy_financials.fiscal_month) <=
                          date_trunc('month', to_date('<%- fiscalGDITEnd %>', 'YYYY-MM-DD'))

 					            		) fyoi_budget_calculation
 		            	 GROUP BY fyoi_budget_calculation.program_id, fyoi_budget_calculation.type
 		            	 )fyoi_budget
          				   ON	fyoi_budget.program_id = program_metadata_manual_entry.program_id


 		   				LEFT JOIN (SELECT
 					   				fyre_forecast_calculation.program_id,
 					   				fyre_forecast_calculation.type,
 				   				    (SUM(fyre_forecast_calculation.value)) as value
 		   							FROM (
 						              SELECT program_fy_financials.program_id, program_fy_financials.type,program_fy_financials.value
 				                      FROM program_fy_financials
 						              WHERE program_fy_financials.type = 'revenueForecast'
 						              AND date_trunc('month', program_fy_financials.fiscal_month) >=
                          date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
 						              AND date_trunc('month', program_fy_financials.fiscal_month) <=
                          date_trunc('month', to_date('<%- fiscalGDITEnd %>', 'YYYY-MM-DD'))

 					            		) fyre_forecast_calculation
 		            	 GROUP BY fyre_forecast_calculation.program_id, fyre_forecast_calculation.type
 		            	 )fyre_forecast
          				   ON	fyre_forecast.program_id = program_metadata_manual_entry.program_id


 		   				LEFT JOIN (SELECT
 		   				fyre_budget_calculation.program_id,
 		   				 fyre_budget_calculation.type,
 		   				  (SUM(fyre_budget_calculation.value)) as value
 		   							FROM (
 						              SELECT program_fy_financials.program_id, program_fy_financials.type,program_fy_financials.value
 				                      FROM program_fy_financials
 						              WHERE program_fy_financials.type = 'revenueBudget'
 						              AND date_trunc('month', program_fy_financials.fiscal_month) >=
                          date_trunc('month', to_date('<%- fiscalStart %>', 'YYYY-MM-DD'))
 						              AND date_trunc('month', program_fy_financials.fiscal_month) <=
                          date_trunc('month', to_date('<%- fiscalGDITEnd %>', 'YYYY-MM-DD'))

 					            		) fyre_budget_calculation
 		            	 GROUP BY fyre_budget_calculation.program_id, fyre_budget_calculation.type
 		            	 )fyre_budget
          				   ON	fyre_budget.program_id = program_metadata_manual_entry.program_id
 					)fyoi_percentage
 					ON fyoi_percentage.program_id = program_metadata_manual_entry.program_id

   LEFT JOIN program_export_international_entry
      ON program_export_international_entry.program_id = programs_secure.id
         AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
             date_trunc('month', program_export_international_entry.fiscal_month_end)
         AND program_export_international_entry.validated = TRUE
         AND program_export_international_entry.type = 'actual'

    LEFT JOIN program_financial_status fin
      ON fin.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', fin.fiscal_month_end)
        AND fin.validated = TRUE
        AND fin.type = 'dso'
     <%- roleClause %>
  WHERE TRUE
    AND <%- groupClause %>
    AND <%- executiveDivisionClause %>
    AND <%- userClause %>
    AND <%- l2ManagerClause %>
    AND <%- l2_ManagerClause%>
    AND <%- divisionClause %>
    AND <%- l3ManagerClause %>
    AND <%- l3_ManagerClause%>
    AND <%- l4ManagerClause %>
    AND <%- programManagerClause %>
    AND <%- programClause %>
    AND <%- losClause %>
    AND <%- praClause %>
    AND <%- kpiClause %>
    AND <%- exportKpiClause %>
    AND <%- cyberKpiClause %>
    AND <%- statusClause %>
    AND program_metadata_manual_entry.validated = TRUE
  ORDER BY program_metadata_manual_entry.program_id, program_heatmap_manual_entry.program_id,
    program_metadata_manual_entry.created_at DESC

<%- limit %> <%- offset %>
) table_query <%- order %>
