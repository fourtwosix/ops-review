SELECT
  revenue_color as "FY Revenue $",
  margin_dollar_color as "FY Margin $",
  fyoi as "FY Margin %",
  margin as "EAC Margin %",
  investment,
  technical,
  schedule,
  customer_satisfaction AS "Customer Satisfaction",
  contractual as "Contract",
  supplier_status as "Internal Support & Suppliers",
  staffing,
  cyber_security,
  overall_project
FROM programs_secure
  INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
              FROM program_metadata_manual_entry
              WHERE program_metadata_manual_entry.validated = TRUE
              ORDER BY program_id, created_at DESC) program_metadata_manual_entry
    ON programs_secure.id = program_metadata_manual_entry.program_id
  LEFT JOIN program_heatmap_manual_entry
    ON program_heatmap_manual_entry.program_id = programs_secure.id
    AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
        date_trunc('month', fiscal_month_end)
    AND program_heatmap_manual_entry.validated = TRUE
    AND program_heatmap_manual_entry.type = 'actual'
    <%- roleClause %>
WHERE <%-losClause %> AND <%- userClause %> AND <%-l4ManagerClause %> AND <%-programManagerClause %> AND <%-praClause %> AND <%-l3_ManagerClause %> AND <%-l2_ManagerClause %>  AND <%- divisionClause %> AND <%- groupClause %> AND <%- statusClause %>     AND <%- executiveDivisionClause %>
