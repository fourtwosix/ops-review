SELECT * FROM (
  SELECT
    program_metadata_manual_entry.small_business_goal_percentage,
    program_metadata_manual_entry.current_month,
    program_metadata_manual_entry.months_to_complete
  FROM programs_secure
    INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
                FROM program_metadata_manual_entry
                WHERE program_metadata_manual_entry.validated = TRUE
                ORDER BY program_id, created_at DESC) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id
    LEFT JOIN program_heatmap_manual_entry
      ON program_heatmap_manual_entry.program_id = programs_secure.id
        AND date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) =
            date_trunc('month', fiscal_month_end)
        AND program_heatmap_manual_entry.validated = TRUE
        AND program_heatmap_manual_entry.type = 'actual'
     <%- roleClause %>
  WHERE <%- groupClause %>
    AND <%- executiveDivisionClause %>
    AND <%- userClause %>
    AND <%- l2_ManagerClause%>
    AND <%- l3_ManagerClause%>
    AND <%- l2ManagerClause %>
    AND <%- divisionClause %>
    AND <%- l3ManagerClause %>
    AND <%- programManagerClause %>
    AND <%- l4ManagerClause %>
    AND <%- programClause %>
    AND <%- losClause %>
    AND <%- praClause %>
    AND <%- statusClause %>
    AND <%-l2_ManagerClause %>
    AND <%-l3_ManagerClause %>
  ORDER BY program_metadata_manual_entry.program_id,
           program_heatmap_manual_entry.program_id,
           program_metadata_manual_entry.created_at DESC

  <%- limit %> <%- offset %>
) table_query <%- order %>
