SELECT programs_secure.project_id,
program_cyber_security.id,
COALESCE(program_cyber_security.fiscal_month_end, '<%- endDate %>') AS fiscal_month_end,
program_cyber_security.security_authorization,
program_cyber_security.scanning,
program_cyber_security.flaw_remediation,
program_cyber_security.inventory_status,
program_cyber_security.asset_status,
program_cyber_security.external_access,
program_cyber_security.device_hardening,
program_cyber_security.security_event_management,
program_cyber_security.identified_security_roles_responsibilities,
program_cyber_security.account_management,
program_cyber_security.contract_security_requirements,
program_cyber_security.overall,
program_cyber_security.type
FROM programs_secure
INNER JOIN
  (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
   FROM program_metadata_manual_entry WHERE
     validated = true
     AND <%- statusClause %>
   ORDER BY program_id,
            created_at DESC) program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
LEFT JOIN program_cyber_security ON program_cyber_security.program_id = programs_secure.id
AND (((date_trunc('month', program_cyber_security.fiscal_month_end) > date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '1 year')
      AND (date_trunc('month', program_cyber_security.fiscal_month_end) < date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
      AND program_cyber_security.type = 'actual')
     OR (date_trunc('month', program_cyber_security.fiscal_month_end) = date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))))
<%- roleClause %>
WHERE <%- programClause %>
AND <%- userClause %>
AND <%- executiveDivisionClause %>
AND <%- l4ManagerClause %>
AND <%- programManagerClause %>
AND <%- losClause %>
AND <%- praClause %>
AND <%- l2_ManagerClause %>
AND <%- groupClause %>
AND <%- l3_ManagerClause %>
AND <%- divisionClause %>
AND (date_trunc('month', program_cyber_security.fiscal_month_end) > date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '1 year')
AND (date_trunc('month', program_cyber_security.fiscal_month_end) <= date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
AND program_cyber_security.type = 'actual'
ORDER BY program_cyber_security.fiscal_month_end
