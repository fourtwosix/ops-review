WITH program_financial_status AS
(
  SELECT
    program_financial_status.type,
    program_financial_status.contract_baseline,
    program_financial_status.current_month_actual
  FROM program_financial_status
    INNER JOIN
    programs_secure ON programs_secure.id = program_financial_status.program_id
    INNER JOIN
    (
      SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
      FROM program_metadata_manual_entry
      WHERE
        validated = TRUE
        AND <%-statusClause %>
      ORDER BY program_id,
        created_at DESC
    ) program_metadata_manual_entry
      ON programs_secure.id = program_metadata_manual_entry.program_id
           <%- roleClause %>
  WHERE <%-programClause %>
        AND <%- userClause %>
        AND <%-l4ManagerClause %>
        AND <%- executiveDivisionClause %>
        AND <%-programManagerClause %>
        AND <%-losClause %>
        AND <%-praClause %>
        AND <%-l2_ManagerClause %>
        AND <%-groupClause %>
        AND <%-l3_ManagerClause %>
        AND <%-divisionClause %>
        AND program_financial_status.validated = true
        AND (date_trunc('month', program_financial_status.fiscal_month_end) =
             date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
),
    total_revenue AS

  (SELECT
     'total_revenue'::TEXT AS type,
     SUM(COALESCE(current_month_actual, 0)) AS actual,
     SUM(COALESCE(contract_baseline, 0)) AS baseline
   FROM program_financial_status
   WHERE type = 'contractRevenue'),

    total_cost AS

  (SELECT
     'total_cost'::TEXT AS type,
     SUM(COALESCE(current_month_actual, 0)) AS actual,
     SUM(COALESCE(contract_baseline, 0)) AS baseline
   FROM program_financial_status
   WHERE type IN (
     'directLaborBillable',
     'directLaborUnbillable',
     'subcontractLabor',
     'travel',
     'gwacPmo',
     'iff',
     'other',
     'riskReserve',
     'directLaborFringe',
     'directLaborOverhead',
     'mhx',
     'generalAdministrative',
     'corpGeneralAdministrative'
   )
  )


SELECT
  total_revenue.* FROM total_revenue

UNION ALL

SELECT total_cost.* FROM total_cost

UNION ALL

SELECT
  'profit'::TEXT AS type,
  total_revenue.actual - total_cost.actual AS actual,
  total_revenue.baseline - total_cost.baseline AS baseline
FROM total_revenue CROSS JOIN total_cost;
