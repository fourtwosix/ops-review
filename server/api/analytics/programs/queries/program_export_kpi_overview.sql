SELECT
  programs_secure.project_id,
  program_export_international_entry.id,
  COALESCE(program_export_international_entry.fiscal_month_end, '<%- endDate %>')    AS fiscal_month_end,
  COALESCE(program_export_international_entry.h_s_ware, 'N/S')                       AS "Hardware/Software",
  COALESCE(program_export_international_entry.performance_services, 'N/S')           AS performance_services,
  COALESCE(program_export_international_entry.us_subcontractor, 'N/S')               AS us_subcontractor,
  COALESCE(program_export_international_entry.foreign_subcontractor, 'N/S')          AS foreign_subcontractor,
  COALESCE(program_export_international_entry.foreign_h_s_ware, 'N/S')               AS "Foreign Hardware/Software",
  COALESCE(program_export_international_entry.personal_protective_equipment, 'N/S')  AS personal_protective_equipment,
  COALESCE(program_export_international_entry.foreign_millitary_sale, 'N/S')         AS "Foreign Military Sale",
  COALESCE(program_export_international_entry.export_license, 'N/S')                  AS export_license,
  COALESCE(program_export_international_entry.overall_project, 'N/S')                 AS overall_project,
  program_export_international_entry.type
FROM programs_secure
  INNER JOIN
  (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
   FROM program_metadata_manual_entry
   WHERE validated = TRUE
   ORDER BY program_id, created_at DESC)
  program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
  LEFT JOIN program_export_international_entry
    ON
      program_export_international_entry.program_id = programs_secure.id
      AND
          (date_trunc('month', program_export_international_entry.fiscal_month_end) >
           date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '6 months')
          AND (date_trunc('month', program_export_international_entry.fiscal_month_end) <=
               date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
          AND program_export_international_entry.type = 'actual'
      AND program_export_international_entry.validated = TRUE
      <%- roleClause %>
WHERE <%-programClause %>
      AND <%- userClause %>
      AND <%- executiveDivisionClause %>
      AND <%- l4ManagerClause %>
      AND <%-programManagerClause %>
      AND <%-losClause %>
      AND <%-praClause %>
      AND <%-l2_ManagerClause %>
      AND <%-groupClause %>
      AND <%-l3_ManagerClause %>
      AND <%-divisionClause %>
      AND <%-statusClause %>
ORDER BY fiscal_month_end
