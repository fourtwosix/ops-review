SELECT
  programs_secure.project_id,
  program_heatmap_manual_entry.id,
  COALESCE(program_heatmap_manual_entry.fiscal_month_end, '<%- endDate %>') AS fiscal_month_end,
  COALESCE(program_heatmap_manual_entry.revenue_color, 'N/S')                     AS "FY Revenue $",
  COALESCE(program_heatmap_manual_entry.margin_dollar_color, 'N/S')         AS "FY Margin $",
  COALESCE(program_heatmap_manual_entry.fyoi, 'N/S')  as "FY Margin %",
  (CASE WHEN program_heatmap_manual_entry.type = 'pra'
    THEN program_heatmap_manual_entry.margin
   ELSE COALESCE(program_heatmap_manual_entry.margin, 'N/S') END)           AS "EAC Margin %",
  COALESCE(program_heatmap_manual_entry.investment, 'N/S')                 AS investment,
  COALESCE(program_heatmap_manual_entry.technical, 'N/S')                  AS technical,
  COALESCE(program_heatmap_manual_entry.schedule, 'N/S')                    AS schedule,
  COALESCE(program_heatmap_manual_entry.customer_satisfaction, 'N/S')       AS "Customer Satisfaction",
  COALESCE(program_heatmap_manual_entry.contractual, 'N/S')                 AS "Contract",
  COALESCE(program_heatmap_manual_entry.supplier_status, 'N/S')             AS "Internal Support & Suppliers",
  COALESCE(program_heatmap_manual_entry.staffing, 'N/S')                    AS "Staffing",
  COALESCE(program_heatmap_manual_entry.cyber_security, 'N/S')              AS cyber_security,
  COALESCE(program_heatmap_manual_entry.overall_project, 'N/S')             AS overall_project,
  program_heatmap_manual_entry.type
FROM programs_secure
  INNER JOIN
  (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) *
   FROM program_metadata_manual_entry
   WHERE validated = TRUE
   ORDER BY program_id, created_at DESC)
  program_metadata_manual_entry ON programs_secure.id = program_metadata_manual_entry.program_id
  LEFT JOIN program_heatmap_manual_entry
    ON
      program_heatmap_manual_entry.program_id = programs_secure.id
      AND
      (
        (
          (date_trunc('month', program_heatmap_manual_entry.fiscal_month_end) >
           date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) - INTERVAL '6 months')
          AND (date_trunc('month', program_heatmap_manual_entry.fiscal_month_end) <=
               date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')))
          AND program_heatmap_manual_entry.type = 'actual'
        )
        OR (date_trunc('month', program_heatmap_manual_entry.fiscal_month_end) =
            date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD'))
            AND program_heatmap_manual_entry.type = 'pra')
        OR (
          date_trunc('month', program_heatmap_manual_entry.fiscal_month_end) =
          (date_trunc('month', to_date('<%- endDate %>', 'YYYY-MM-DD')) +
           INTERVAL '1 month')
          AND program_heatmap_manual_entry.type = 'forecast'))
      AND program_heatmap_manual_entry.validated = TRUE
      <%- roleClause %>
WHERE <%-programClause %>
      AND <%- userClause %>
      AND <%- executiveDivisionClause %>
      AND <%- l4ManagerClause %>
      AND <%-programManagerClause %>
      AND <%-losClause %>
      AND <%-praClause %>
      AND <%-l2_ManagerClause %>
      AND <%-groupClause %>
      AND <%-l3_ManagerClause %>
      AND <%-divisionClause %>
      AND <%-statusClause %>
ORDER BY fiscal_month_end
