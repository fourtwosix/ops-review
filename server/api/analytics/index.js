'use strict';

var express = require('express');
var hcCtrl = require('./human-capital');
var programsCtrl = require('./programs');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get("/human_capital", hcCtrl.humanCapital);
router.get("/human_capital_period", hcCtrl.humanCapitalPeriod);
router.get("/human_capital_kpis", hcCtrl.getKpis);
router.get("/human_capital_kpi_details", hcCtrl.getKpiDetails);
router.get("/manager_filters", hcCtrl.getLayerFilters);

// Programs
router.get("/program_quadchart_overview", auth.isAuthenticated(), programsCtrl.quadChartOverview);
router.get("/program_summary_kpi_overview", auth.isAuthenticated(), programsCtrl.kpiSummaryOverview);
router.get("/program_summary_kpi", auth.isAuthenticated(), programsCtrl.kpiSummary);
router.get("/programs_by_status_overview", auth.isAuthenticated(), programsCtrl.programsByStatusOverview);
router.get("/program_cyber_kpi_overview", auth.isAuthenticated(), programsCtrl.cyberKpiSummaryOverview);
router.get("/program_cyber_kpi", auth.isAuthenticated(), programsCtrl.cyberKpiSummary);
router.get("/program_cyber_security_overview", auth.isAuthenticated(), programsCtrl.programCyberSecurityOverview);
router.get("/program_metadata", auth.isAuthenticated(), programsCtrl.programMetadata);
router.get("/program_export_kpi", auth.isAuthenticated(), programsCtrl.exportKpiSummary);
router.get("/program_export_kpi_overview", auth.isAuthenticated(), programsCtrl.exportKpiSummaryOverview);
router.get("/program_export_international_overview", auth.isAuthenticated(), programsCtrl.programExportInternationalOverview);
router.get("/program_financial_detail", auth.isAuthenticated(), programsCtrl.financialDetail);
router.get("/program_detail_detail", auth.isAuthenticated(), programsCtrl.organizationDetail);
router.get("/program_contract_detail", auth.isAuthenticated(), programsCtrl.contractDetail);
router.get("/staffing_position", auth.isAuthenticated(), programsCtrl.staffingPosition);
router.get("/staffing_vacancy", auth.isAuthenticated(), programsCtrl.staffingVacancy);

router.get("/program_metadata_filters", auth.isAuthenticated(), programsCtrl.programMetadataFilters);
router.get("/program_group_filters", auth.isAuthenticated(), programsCtrl.programGroupFilters);
router.get("/program_group_division_filters", auth.isAuthenticated(), programsCtrl.programGroupDivisionFilters);
router.get("/program_filters", auth.isAuthenticated(), programsCtrl.programFilters);
router.get("/program_metadata_list", auth.isAuthenticated(), programsCtrl.programMetadataList);
router.get("/program_metadata_cyber", auth.isAuthenticated(), programsCtrl.programMetadataCyber);
router.get("/program_metadata_export", auth.isAuthenticated(), programsCtrl.programMetadataExport);
router.get("/revenue_costs_timeline", auth.isAuthenticated(), programsCtrl.revenueCostsTimeline);
router.get("/profit_timeline", auth.isAuthenticated(), programsCtrl.profitTimeline);
router.get("/revenue_costs_profit", auth.isAuthenticated(), programsCtrl.revenueCostsProfit);
router.get("/revenue_costs_summary", auth.isAuthenticated(), programsCtrl.revenueCostsSummary);
router.get("/investment_summary", auth.isAuthenticated(), programsCtrl.investmentSummary);
router.get("/funding_summary", auth.isAuthenticated(), programsCtrl.fundingSummary);


router.get("/salesforce_task_list", auth.isAuthenticated(), programsCtrl.salesforceTaskList);
router.get("/program_financials_summary_bar", auth.isAuthenticated(), programsCtrl.programFinancialsSummaryBar);
router.get("/financials_dso_trend", auth.isAuthenticated(), programsCtrl.financialsDsoTrend);
router.get("/staffing_change_by_month", auth.isAuthenticated(), programsCtrl.staffingChangeByMonth);
router.get("/staffing_full_time_employees", auth.isAuthenticated(), programsCtrl.staffingFullTimeEmployees);
router.get("/financial_report", auth.hasRole('Administrator'), programsCtrl.getFinancialReport);
router.get("/staffing_report", auth.hasRole('Administrator'), programsCtrl.getStaffingReport);
router.get("/ribbon_report", auth.hasRole('Administrator'), programsCtrl.getRibbonReport);

module.exports = router;
