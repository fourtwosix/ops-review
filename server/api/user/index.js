'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('Administrator'), controller.index);
router.delete('/:id', auth.hasRole('Administrator'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);
router.get('/:uid/decks', auth.isAuthenticated(),controller.getMyDecks);
router.put('/:id', auth.hasRole('Administrator'), controller.update);

module.exports = router;
