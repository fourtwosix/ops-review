'use strict';

var models = require('../../models');
var sequelize = models.sequelize;
var Promise = require('bluebird');
var User = models.User;
var Program = models.Program;
var PermastData = models.PermastData;
var emails = require('../../emails');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var _ = require('lodash');

var validationError = function(res, err) {
  return res.json(422, err);
};

var populateOrgInfo = function (user, callback) {
  PermastData.getOrgInfo(user).then(function (info) {
    PermastData.getManagerInfo(info.first_name, info.last_name).then(function (managerInfo) {
      user = user.toJSON();
      user.orgInfo = _.merge(info, managerInfo);

      // If this is a manager they are their own manager at their level
      if (user.orgInfo.isManager) {
        user.orgInfo['layer_' + user.orgInfo.managerLevel + '_manager'] = info.last_name.toLowerCase() +
          ', ' + info.first_name.toLowerCase();
      }

      callback(null, user);
    }).catch(function (err) {
      callback(err, user);
    });
  }).catch(function (err) {
    callback(err, user);
  })
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return User.findAll({attributes: ['name', 'email', 'title', 'provider', 'id', 'groups', 'pmr_group','divisions'],
        include: [{model: models.Roles, as: 'roles'}, {model: models.Program, as: 'programs'}, {model: models.Auths, as: 'auths'}],
        transaction: t});
    })
  }).then(function(users) {
    res.json(200, users);
  }).catch(function(err) {
    res.send(500, err);
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res) {
  console.log("\n\n\n\nINSIGHT: CREATING NEW USER!\n\n");
  var newUser = User.build(req.body);
  newUser.provider = 'local';
  newUser.title = newUser.title || 'user';

  console.log("\n\nINSIGHT: ABOUT TO SAVE USER: ", _.get(newUser, 'email', 'INVALID'), "\n\n");
  newUser.save().then(function(user) {
    console.log("\n\nINSIGHT: USER SAVED: ", _.get(newUser, 'email', 'INVALID'), "\n\n");
    emails.sendNewUserAdminNotificationEmail(user, function(err) {
      console.log("\n\nINSIGHT EMAIL SENT: ", _.get(newUser, 'email', 'INVALID'), "\n\n");
      if (err) {
        console.error("INSIGHT - Error while sending new user notification email to admins: " + err);
      }
    });

    var token = jwt.sign({id: user.id }, config.secrets.session, { expiresInMinutes: 60*5 });
    res.json({ token: token });
  }).catch(function(err) {
    validationError(res, err);
  });
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId).then(function (user) {
    if (!user){
      return res.send(401);
    }
    res.json(user.profile);
  }).catch(function(err) {
    next(err);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.destroy({
    where: {
      id: req.params.id
    }
  }).then(function() {
    return res.send(204);
  }).catch(function(err) {
    handleError(res, err);
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res) {
  var userId = req.user.id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId).then(function (user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save().then(function() {
        res.send(200);
      }).catch(function(err) {
        validationError(res, err);
      });
    } else {
      res.send(403);
    }
  }).catch(function(err) {
    validationError(res, err);
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user.id;
  sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return User.findById(userId, {attributes: ['name', 'email', 'title', 'provider', 'id', 'groups', 'receives_emails', 'pmr_group','divisions'],
        include: [{model: models.Roles, as: 'roles'}, {model: models.Program, as: 'programs'}, {model: models.Auths, as: 'auths'}],
        transaction: t
      });
    }).catch(function(err) {
      next(err);
    });
  }).then(function(user) {
    if (!user) {
      return res.json(401);
    }
    populateOrgInfo(user, function (err, user) {
      // Only return enabled groups
      user.groups = _.filter(user.groups, function (group) {
        return _.includes(config.deckGroups, group);
      });

      res.json(user);
    });
  }).catch(function(err) {
    next(err);
  });
};


/**
 * Get list of owned decks for this user (returns every column)
 */
exports.getMyDecks = function(req,res, next) {
  var userId = req.params.uid;
  User.findById(userId).then(function (user){
    if(!user) {
      return res.send(401);
    }
    user.getOwnedDecks().then(function(decks) {
      res.send(decks);
    });
  }).catch(function(err) {
    next(err);
  });
};

/**
 * Compare the the user's original permissions to their updated permissions.
 * Added permission changes are added to the added array
 * Removed permission changes are added to the removed array
 * (Do this for each different permission)
 *
 * @param orig
 * @param updates
 * @returns {{added: Array, removed: Array}}
 */
function getChanges (orig, updates) {
  var result = {added: [], removed: []};
  var values = _.union(orig, updates);

  _.each(values, function (val) {
    var inOrig = _.contains(orig, val);
    var inUpdates = _.contains(updates, val);

    if (inUpdates && !inOrig) {
      result.added.push(val);
    } else if(inOrig && !inUpdates) {
      result.removed.push(val);
    }
  });

  return result;
}

exports.update = function (req, res) {
  var userId = req.params.id;

  sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return User.findById(userId, {
        include: [
          {model: models.Roles, as: 'roles'},
          {model: models.Auths, as: 'auths'},
          {model: models.Program, as: 'programs'}
        ],
        transaction: t
      }).then(function (user) {
        var origRoles = _.pluck(user.roles, 'name');
        var origAuths = _.pluck(user.auths, 'name');
        var origPmrGroup = _.pluck(user.pmr_group, 'pmr_group');
        var origDivisions = _.pluck(user.divisions, 'divisions');
        var origPrograms = _.pluck(user.programs, 'project_id');
        var programUpdates = _.pluck(req.body.programs, 'project_num');

        var roleChanges = getChanges(origRoles, req.body.roles);
        var authChanges = getChanges(origAuths, req.body.auths);
        var pmrGroupChanges = getChanges(origPmrGroup, req.body.pmr_group);
        var groupsChanges = getChanges(user.groups, req.body.groups);
        var divisionChanges = getChanges(origDivisions, req.body.divisions);
        var programChanges = getChanges(origPrograms, programUpdates);

        user.groups = req.body.groups;
        user.pmr_group = req.body.pmr_group;
        user.divisions = req.body.divisions;

        return Promise.each(programUpdates, function (projectId, i) {
          return Program.findOne({
            where: {
              project_id: projectId
            },
            transaction: t
          }).then(function (program) {
            programUpdates[i] = program;
            return;
          });
        }).then(function () {
          return user.save({
            include: [
              {model: models.Roles, as: 'roles'},
              {model: models.Auths, as: 'auths'},
              {model: models.Program, as: 'programs'}
            ],
            transaction: t
          }).then(function (user) {
            return user.setRoles(req.body.roles, {transaction: t}).then(function () {
              return user.setAuths(req.body.auths, {transaction: t}).then(function () {
                return user.setPrograms(programUpdates, {transaction: t}).then(function () {
                  res.header('Content-Type', 'application/json');
                  res.status(200).send();

                  emails.sendUserPermissionChangeNotificationEmail(user, {
                    roles: roleChanges,
                    auths: authChanges,
                    programs: programChanges,
                    groups: groupsChanges,
                    pmr_group : pmrGroupChanges,
                    divisions : divisionChanges
                  }, function (err) {
                    if (err) {
                      return console.error(err);
                    }
                  });
                }).catch(function (err) {
                  handleError(res, err);
                });
              });
            });
          });
        });
      });
    });
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res) {
  res.redirect('/');
};

function handleError(res, err) {
  console.error(err);
  return res.status(500).send(err);
}
