'use strict';

var models = require('../../models');
var PmrGroups = models.PmrGroups;

// Get list of PMR Groups
exports.getGroups = function (req, res) {
  PmrGroups.findAll({
    attributes: ['group_name']
  }).then(function (divisions) {
    return res.status(200).json(divisions);
  }).catch(function (err) {
    return handleError(res, err);
  });
};
