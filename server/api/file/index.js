'use strict';

var express = require('express');
var controller = require('./file.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/templates/:name', auth.isAuthenticated(), controller.downloadTemplate);
router.post('/upload', auth.isAuthenticated(), controller.uploadToJson);

module.exports = router;
