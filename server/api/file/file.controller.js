'use strict';

var path = require('path');
var multiparty = require('multiparty');
var fs = require('fs');
var xlsx = require('xlsx');
var baby = require('babyparse');

/**
 * Retrieves a template file for downloading
 */
exports.downloadTemplate = function(req, res) {
  var fileName = req.params.name;
  var templateFile = path.join(__dirname, '../../templates', fileName + '.xlsx');
  res.download(templateFile);
};

/**
 * Takes a a CSV or Excel file and returns the contents of the file as JSON
 */
exports.uploadToJson = function (req, res) {
  var form = new multiparty.Form();

  form.parse(req, function (err, fields, files) {
    var file = files.fileUpload[0]; //TODO May need to change 'fileUpload' to something else depending on how the frontend portion of the upload is handled
    var type = path.extname(file.path); //get the file extension
    var json = {};
    if (type === '.csv') {
      json = convertCSV(file.path);
    }
    else if (type === '.xls' || type === '.xlsx') {
      json = convertXLS(file.path);
    }

    if (!json) {
      return res.send(500);
    }

    return res.json(200,json);
  });
}

/**
 * Converts a given file (using its path) from Excel XLS / XLSX to JSON
 * @param filePath The full file path of the uploaded file
 */
function convertXLS(filePath) {
  var workbook = xlsx.readFile(filePath);

  if (!workbook) {
    console.error("Error: XLSX workbook was not properly initialized")
  }

  var result = {};
  workbook.SheetNames.forEach(function (sheetName) {
    var json = xlsx.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
    if (json.length > 0) {
      result[sheetName] = json;
    }
  });

  //log any errors encountered when converting to JSON, but still return the JSON data in case the data is still (partially) valid
  if (Object.keys(result).length === 0) {
    console.error("Error: XLSX worksheet was not properly converted to JSON");
  }
  return result;
}

/**
 * Converts a given file from CSV to JSON
 * @param filePath The full file path of the uploaded file
 */
function convertCSV(filePath) {
  //read the csv file and put its contents into buffer
  var buffer = fs.readFileSync(filePath, {encoding: 'utf-8'},
    function (err) {
      console.log(err);
    });

  if (!buffer) {
    return res.send(500);
  }
  buffer = buffer.split("\n");

  //convert the csv data to JSON
  var json = baby.parse(buffer.toString());

  //log any errors encountered when converting to JSON, but still return the JSON data in case the data is still valid (despite the error(s))
  if (Object.keys(json.errors).length > 0) {
    console.error(json.errors);
  }
  return json.data;
}
