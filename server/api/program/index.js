'use strict';

var express = require('express');
var controller = require('./program.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/groups', auth.isAuthenticated(), controller.getGroups);
router.get('/groups/divisions', auth.isAuthenticated(), controller.getGroupsDivisions);

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isUserAllowedToEditProgram(), controller.create);
router.put('/:id', auth.isUserAllowedToEditProgram(), controller.update);
router.patch('/:id', auth.isUserAllowedToEditProgram(), controller.update);
router.delete('/:id', auth.isUserAllowedToEditProgram(), controller.destroy);

router.get('/:id/heatmaps', auth.isAuthenticated(), controller.getProgramHeatmaps);

module.exports = router;
