/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Program = require('./program.model');

exports.register = function(socket) {
  Program.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Program.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('program:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('program:remove', doc);
}