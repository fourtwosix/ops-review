'use strict';

var _ = require('lodash');
var models = require('../../models');
var Program = models.Program;
var ProgramHeatmap = models.ProgramHeatmapManualEntry;
var PMRGroup = models.PMRGroup;
var PMRGroupDivision = models.PMRGroupDivision;
var sequelize = models.sequelize;
var async = require('async');
var moment = require('moment');
var auth = require('../../auth/auth.service');


// Get list of programs
exports.index = function (req, res) {

  sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return Program.findAll({transaction: t});
    }).catch(function (err) {
      return handleError(res, err);
    });
  }).then(function (programs) {
    return res.status(200).json(programs);
  }).catch(function (err) {
    return handleError(res, err);
  });
};

// Get a single program (by project ID)
exports.show = function (req, res) {

  sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return Program.find({where: {project_id: req.params.id}}, {transaction: t})
    }).catch(function (err) {
      return handleError(res, err);
    })
  }).then(function (program) {
    if (!program) {
      return res.status(404).send('Not Found');
    }
    return res.json(program);
  }).catch(function (err) {
    return handleError(res, err);
  });
};

// Creates a new program in the DB.
exports.create = function (req, res) {
  Program.create(req.body).then(function (program) {
    return res.status(201).json(program);
  }).catch(function (err) {
    return handleError(res, err);
  });
};

// Updates an existing program in the DB.
exports.update = function (req, res) {
  if (req.body.id) {
    delete req.body.id;
  }

  function isUserAllowedToEditPreviousHeatmap(heatmapFiscalMonthEnd) {
    var heatmapFiscalEndMoment = moment.utc(new Date(heatmapFiscalMonthEnd));
    var fiscalEndMoment = moment.utc(new Date()).subtract(1, 'months').endOf('month').date(1);

    return heatmapFiscalEndMoment.isSame(fiscalEndMoment, 'month') ||
      (heatmapFiscalEndMoment.isBefore(fiscalEndMoment, 'month') &&
      (auth.checkRolesArray(req.user.toJSON(), 'Administrator') ||
      auth.checkRolesArray(req.user.toJSON(), 'Executive') ||
      auth.checkRolesArray(req.user.toJSON(), 'Program Manager')));
  }

  async.waterfall([
    function (done) {
      async.map(req.body.programHeatmaps || [], function (heatmap, done) {
        if (!isUserAllowedToEditPreviousHeatmap(heatmap.fiscal_month_end)) {
          return done('User is not allowed to edit this previous heatmap .');
        }

        if (heatmap.id) {
          var heatmapId = heatmap.id;
          delete heatmap.id;

          ProgramHeatmap.findById(heatmapId).then(function (heatmapInstance) {
            if (!heatmapInstance) {
              return done(new Error('No heatmap found.'))
            }
            heatmap.updated_by = req.user.id;
            heatmap.created_by = heatmapInstance.created_by;
            heatmapInstance.update(heatmap).then(function (heatmap) {
              done(null, heatmap);
            }).catch(function (err) {
              done(err);
            })
          }).catch(function (err) {
            done(err);
          });
        } else {
          heatmap.updated_by = req.user.id;
          heatmap.created_by = req.user.id;
          ProgramHeatmap.create(heatmap).then(function (heatmap) {
            return done(null, heatmap);
          }).catch(function (err) {
            return done(err);
          })
        }
      }, function (err, programHeatmap) {
        done(err, programHeatmap);
      });
    },
    function (programHeatmap, done) {
      delete req.body.programHeatmap;

      sequelize.transaction(function (t) {
        return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
          return Program.find({where: {id: req.params.id}}, {transaction: t});
        })
      }).then(function (program) {
        if (!program) {
          return res.status(404).send('Not Found');
        }

        sequelize.transaction(function (t) {
          return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
            return program.update(req.body, {transaction: t});
          })
        }).then(function (program) {
          var programJSON = program.toJSON();
          programJSON.programHeatmap = programHeatmap;
          return done(null, programJSON);
        })
      }).catch(function (err) {
        return handleError(res, err);
      });
    }
  ], function (err, program) {
    if (err) {
      return handleError(res, err)
    }
    return res.status(200).json(program);
  });
};

// Deletes a program from the DB.
exports.destroy = function (req, res) {

  sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return Program.findById(req.params.id, {transaction: t});
    }).catch(function (err) {
      return handleError(res, err);
    })
  }).then(function (program) {
    if (!program) {
      return res.status(404).send('Not Found');
    }

    sequelize.transaction(function (t) {
      return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
        return program.destroy({transaction: t}).then(function () {
          return res.status(204).send('No content');
        })
      }).catch(function (err) {
        return handleError(err);
      })
    });
  }).catch(function (err) {
    return handleError(res, err);
  });
};

exports.getProgramHeatmaps = function (req, res) {
  var month = new Date(req.query.fiscalEnd);
  var whereClause = {
    where: {
      program_id: req.params.id,
      fiscal_month_end: {
        $gte: new Date(month.getFullYear(), month.getMonth(), 1),
        $lt: new Date(month.getFullYear(), month.getMonth() + 1) // TODO double check if this is correct
      }
    }
  };

  sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return ProgramHeatmap.findAll(whereClause, {transaction: t});
    })
  }).then(function (programHeatmaps) {
    res.status(200).json(programHeatmaps);
  }).catch(function (err) {
    return handleError(res, err);
  });
};

// Get list of PMR Groups
exports.getGroups = function (req, res) {
  PMRGroup.findAll({order: 'group_name'}).then(function (groups) {
    return res.status(200).json(groups);
  }).catch(function (err) {
    return handleError(res, err);
  });
};

// Get list of PMR Divisions based on Group name
exports.getGroupsDivisions = function (req, res) {
  PMRGroupDivision.findAll({
    attributes: ['group_name', 'division_name', 'l3_manager'],
    where: {
      group_name: req.query.groupName
    },
    order: 'division_name'
  }).then(function (divisions) {
    return res.status(200).json(divisions);
  }).catch(function (err) {
    return handleError(res, err);
  });
};

function handleError(res, err) {
  console.error(err);
  return res.status(500).send(err);
}
