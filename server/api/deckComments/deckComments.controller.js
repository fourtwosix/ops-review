var _ = require('lodash');
var models = require('../../models');
var sequelize = models.sequelize;
var Comments = models.ProgramDeckComments;
var Program = models.Program;
var utils = require('../../components/utils');
var auth = require('../../auth/auth.service');

function getProgramDeckComments(deckId, programId, fiscalMonthEnd, user, res) {
  return sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + user.id + ";", {transaction: t}).then(function () {
      return Program.find({
        transaction: t,
        where: {
          project_id: programId
        }
      }).then(function(program) {
        return Comments.findAll({
          where: {
            deck: deckId,
            program_id: program.id,
            fiscal_month_end: {
              $gte: utils.getGTEDate(fiscalMonthEnd, 1),
              $lte: utils.getLTEDate(fiscalMonthEnd)
            },
            is_deleted: false
          },
          include: [
            {
              model: models.User,
              as: 'deckCommentCreatedBy',
              attributes: ['name', 'email']
            },
            {
              model: models.User,
              as: 'deckCommentUpdatedBy',
              attributes: ['name', 'email']
            }
          ],
          order: "updated_at DESC"
        })
      })
    })
  }).then(function (comments) {
    return res.status(200).send(comments);
  }).catch(function (err) {
    handleError(res, err);
  });
};

exports.getProgramDeckComments = function (req, res) {
  var deckId = req.params.deckId;
  var programId = req.params.programId;
  var fiscalMonthEnd = new Date(req.params.fiscalMonthEnd);


  getProgramDeckComments(deckId, programId, fiscalMonthEnd, req.user, res);
};

exports.saveComment = function (req, res) {
  var deckId = req.body.deckId;
  var programId = req.body.programId;
  var fiscalMonthEnd = req.body.fiscalMonthEnd;
  var comment = req.body.comment;


if(!comment.id) {
  comment.deck = deckId;
  comment.fiscal_month_end = fiscalMonthEnd;
  comment.created_by = comment.created_by || req.user.id;
}
  comment.updated_by = req.user.id;
  delete comment.updated_at;
  delete comment.created_at;
  sequelize.transaction(function (t) {
    return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
      return Program.find({
        transaction: t,
        where: {
          project_id: programId
        }
      }).then(function (program) {
        if (comment.id) {
          return Comments.findById(comment.id, {transaction: t}).then(function (dbComment) {
            if (!dbComment) {
              throw new Error("No existing comment found!");
            } else if (comment.created_by && comment.created_by != dbComment.created_by && !auth.checkRolesArray(req.user, "Administrator")) {
              throw new Error("The user attempting to edit this comment does not match the user who created it.");
            } else if (comment.program_id != program.id) {
              throw new Error("The program id for the comment being updated does not match the program id of the original comment");
            } else if(comment.is_deleted) {
              throw new Error("This comment cannot be updated since it is pending deletion");
            }

            comment.program_id = program.id;

            return dbComment.update(comment, {transaction: t});
          });
        } else {
          comment.is_deleted = false;
          comment.program_id = program.id;
          return Comments.create(comment, {transaction: t});
        }
      })
    })
  }).then(function () {
    return getProgramDeckComments(deckId, programId, fiscalMonthEnd, req.user, res);
  }).catch(function (err) {
    handleError(res, err);
  })
};

exports.delete = function (req, res) {
  var id = req.params.id;
  var user = req.user;

  return Comments.findById(id).then(function (comment) {
    if (!comment) {
      return handleError(res, new Error("No existing comment found!"));
    } else if (comment.created_by != user.id && !auth.checkRolesArray(user, "Administrator")) {
      return handleError(res, new Error("The user attempting to delete this comment does not match the user who created it and is not an administrator."));
    } else {
      // return comment.destroy().then(function () {
      //   return res.status(204).end();
      // });
      return comment.update({
        deleted_at: new Date(),
        deleted_by: req.user.id,
        is_deleted: true
      }).then(function() {
        return res.status(204).end();
      });
    }
  }).catch(function (err) {
    handleError(res, err);
  });
};

function handleError(res, err) {
  console.error(err);
  return res.status(500).send(err);
}
