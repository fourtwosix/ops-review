'use strict';

var express = require('express');
var controller = require('./deckComments.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get("/:deckId/:programId/:fiscalMonthEnd", auth.isAuthenticated(), controller.getProgramDeckComments);
router.post("/", auth.isAuthenticated(), controller.saveComment);
router.delete("/:id", auth.isAuthenticated(), controller.delete);

module.exports = router;
