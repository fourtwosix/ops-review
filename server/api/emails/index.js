'use strict';

var express = require('express');
var controller = require('./emails.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.post('/adminNotificationEmail', auth.isAuthenticated(), controller.sendNewUserAdminNotificationEmail);
router.post('/blast', controller.blast);

module.exports = router;
