'use strict';

var emails = require('../../emails');
var models = require('../../models');
var sequelize = models.sequelize;
var User = models.User;

exports.sendNewUserAdminNotificationEmail = function(req, res) {
  console.log("\n\nINSIGHT - BEGINNING OF sendNewUserAdminNotificationEmail\n\n");

        emails.sendNewUserAdminNotificationEmail(req.body.from, function (err) {
          if (err) {
            return res.status(500).send(err);
          } else {

            console.log("\n\nINSIGHT - SENT EMAIL, NOW UPDATING RECEIVES_EMAILS\n\n");
            sequelize.query("UPDATE users SET receives_emails = true WHERE id = " + req.user.id).then(function(){
              res.status(200).end();
            }).catch(function(err){
              handleError(res, err);
            })
          }
        });
};

exports.blast = function (req, res) {

  console.log("\nEXECUTING emails.sendEmailAnnouncement\n");

  emails.sendEmailAnnouncement(req.body.subject, req.body.data, req.body.PMR, function (err) {
    if (err) {
      console.log("\nERROR RETURNED FROM EMAIL SERVER: ", err, "\n");
      return console.error(err);
    } else {
      console.log("\nSUCCESS RETURNED FROM EMAIL SERVER\n");
      res.status(200).end();
    }

  });
};

function handleError(res, err) {
  console.error(err);
  return res.status(500).send(err);
}

