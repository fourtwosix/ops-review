'use strict';

var _ = require('lodash');
var emails = require('../../emails');

exports.sendFeedback = function (req, res) {
  var fromUser = req.body.fromUser;
  var feedbackSubject = req.body.feedbackSubject;
  var feedbackText = req.body.feedbackText;

  emails.sendFeedbackEmail(fromUser, feedbackSubject, feedbackText, function(err) {
    if (err) {
      return handleError(res, err);
    } else {
      res.status(200).end();
    }

  });
};

function handleError(res, err) {
  console.error(err);
  return res.status(500).send(err);
}
