'use strict';

var express = require('express');
var auth = require('../../auth/auth.service');
var controller = require('./feedback.controller');

var router = express.Router();

router.post("/", auth.isAuthenticated(), controller.sendFeedback);

module.exports = router;
