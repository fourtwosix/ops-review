'use strict';

var models = require('../../models');
var StaticDisplayData = models.StaticDisplayData;
var utils = require('../../components/utils');

exports.get = function(req, res) {
  StaticDisplayData.findAll(
    {
      where: {type: req.params.type},
      order: req.query.order ? req.query.order : 'id'
    })
    .then(function(staticDisplayData) {
      if(!staticDisplayData) {
        return utils.handleError(res, new Error('No static display data found for type: ' + req.params.type));
      } else {
        return res.json(staticDisplayData);
      }
    }).catch(function (err) {
    return utils.handleError(res, err);
  });
};

exports.add = function (req, res) {
  if (!req.body.type || !req.body.display_value) {
    utils.handleError(res, new Error("Not a valid type or display_value!"));
  }

  return utils.validateModel(StaticDisplayData, req.body).then(function (errors) {
    if (errors) {
      return utils.handleError(res, errors);
    }

    return StaticDisplayData.findOne({
      where: {
        type: req.body.type,
        display_value: req.body.display_value
      }
    }).then(function (found) {
      if (found) {
        return utils.handleError(res, new Error("A row already exists with type: '" + req.body.type + "' and display_value: '" + req.body.display_value + "'"));
      }

      return StaticDisplayData.create({
        type: req.body.type,
        display_value: req.body.display_value,
        description: req.body.description,
        created_by: req.user.id,
        updated_by: req.user.id
      }).then(function () {
        return res.status(200).end();
      });
    })
  }).catch(function (err) {
    return utils.handleError(res, err);
  });
};

exports.update = function(req, res) {
  if(!req.params.id) {
    return utils.handleError(res, new Error('Invalid id!'));
  }

  return utils.validateModel(StaticDisplayData, req.body).then(function(errors) {
    if (errors) {
      return utils.handleError(res, errors);
    }

    return StaticDisplayData.findById(req.params.id).then(function(row) {
      if(!row) {
        return utils.handleError(res, new Error("Could not find a row with an id of '" + req.params.id + "'"));
      }

      return row.update({
        display_value: req.body.display_value,
        description: req.body.description
      }).then(function() {
        return res.status(200).end();
      });
    });
  }).catch(function(err) {
    return utils.handleError(res, err);
  });
};

exports.delete = function (req, res) {
  if (!req.params.id) {
    return utils.handleError(res, new Error('Invalid id!'));
  }

  return StaticDisplayData.destroy({
    where: {
      id: req.params.id
    }
  }).then(function () {
    return res.status(200).end();
  }).catch(function (err) {
    return utils.handleError(res, err);
  });
};
