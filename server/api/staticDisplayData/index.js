'use strict';

var express = require('express');
var controller = require('./staticDisplayData.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/:type', auth.isAuthenticated(), controller.get);
router.post('/', auth.hasRole('Administrator'), controller.add);
router.put('/:id', auth.hasRole('Administrator'), controller.update);
router.delete('/:id', auth.hasRole('Administrator'), controller.delete);

module.exports = router;
