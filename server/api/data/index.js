'use strict';

var express = require('express');
var controller = require('./data.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.post("/query", auth.isAuthenticated(), controller.query);
router.get("/csv", auth.isAuthenticated(), controller.csv);
router.get("/checkDownload", auth.isAuthenticated(), controller.checkDownload);
router.post("/queryColumnMetadata", controller.queryColumnMetadata);
router.post("/filters", auth.isAuthenticated(), controller.getFilters);
router.post("/card", auth.isAuthenticated(), controller.getCard);
router.post("/decks", auth.isAuthenticated(), controller.getDecks);
router.post("/lastUpdated", auth.isAuthenticated(), controller.getLastUpdated);
router.post("/externalAPI", auth.isAuthenticated(), controller.externalAPI);

module.exports = router;
