'use strict';

var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var mongoSql = require('mongo-sql');
var async = require('async');
var models = require('../../models');
var sequelize = models.sequelize;
var qs = require('querystring');
var json2csv = require('json2csv');
var http = require('http');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var env       = process.env.NODE_ENV || 'development';
var dbConfig    = require(__dirname + '/../../config/database.json')[env];
var redis = require("redis");

var PG_DATA_TYPES = {
  'double precision': 'NUMBER',
  'integer': 'NUMBER',
  'character varying': 'STRING',
  'timestamp with time zone': 'DATE'
};

var redisClient = redis.createClient(dbConfig.redis);

mongoSql.conditionalHelpers.add("$similarTo", function(col, val) {
  return col + ' ~ ' + val;
});


mongoSql.conditionalHelpers.add("$notLike", function(col, val) {
  return col + ' NOT LIKE ' + val;
});

var generateQuery = function (jsonQuery) {
  var sqlQuery = mongoSql.sql(jsonQuery);
  var values = sqlQuery.values;
  var qString = sqlQuery.toString();

  // Replace mongo-sql placeholders with ? so sequelize can read it
  qString = qString.replace(/\$\d+([^\w]+|$)/g, ' ? ');

  return {json: jsonQuery, values: values, sql: qString};
};

// TODO this query should be cached in redis
exports.queryColumnMetadata = function (req, res) {
  var jsonQuery = {
    type: 'select',
    columns: ['column_name', 'data_type', 'table_name'],
    table: 'information_schema.columns'
  };

  var query = generateQuery(jsonQuery);

  sequelize.query(query.sql, {replacements: query.values, type: sequelize.QueryTypes.SELECT})
    .then(function (results) {
      var info = {};
      _.each(results, function (column) {
        info[column.table_name] = info[column.table_name] || {};

        info[column.table_name][column.column_name] = {
          name: column.column_name,
          displayName: _.startCase(column.column_name),
          type: PG_DATA_TYPES[column.data_type] || column.data_type
        };
      });

      return res.json(200, info);
    }).catch(function (err) {
      console.error(err);
      return res.send(500, err);
    })
};

exports.csv = function (req, res) {
  var jsonQuery = req.query.query;
  var token = JSON.parse(req.cookies.token);
  var filename = req.query.filename;

  if (_.isUndefined(jsonQuery)) {
    return res.send(400, new Error('Must specify query'));
  }

  jwt.verify(token, config.secrets.session, function (err) {
    if (err) {
      console.error(err);
      return res.send(500, err);
    }
    var query = generateQuery(jsonQuery);
    redisClient.set(req.cookies.token + filename, "true");
    redisClient.expire(req.cookies.token + filename, 120);

    query.sql = "SET LOCAL config.user_id = " + req.user.id + "; " + query.sql;

    sequelize.query(query.sql, {
      replacements: query.values, type: sequelize.QueryTypes.SELECT
    }).then(function(data) {
      var fields = data.length > 0 ? _.keys(data[0]) : [];

      json2csv({ data: data, fields: fields }, function(err, csv) {
        if (err) {
          console.error(err);
          res.send(500, err);
        } else {
          res.attachment(req.query.filename);
          res.send(csv);
          redisClient.del(req.cookies.token + filename);
        }
      });

    }).catch(function(err) {
      console.error(err);
      res.send(500, err);
    });
  });

};

exports.checkDownload = function (req, res) {

  redisClient.exists(req.cookies.token + req.query.filename, function(err, reply) {
    if (reply === 1) {
      res.json(true);
    } else {
      res.json(false);
    }
  });
};

exports.query = function (req, res) {
  var jsonQuery = req.body.json;
  if (_.isUndefined(jsonQuery)) {
    return res.send(400, new Error('Must specify query'));
  }

  var paging = !!req.body.limit;
  var limit = req.body.limit || 100;
  var offset = req.body.offset;
  var order = {};
  req.body.sort ? order[req.body.sort] = req.body.order : order = null;
  // orderBy parameter uses array-syntax (i.e. ?orderBy[1]=0&orderBy[3]=1).  the code below does special parsing since body-parser does not handle array-syntax parameters
  var queryParams = order ? order : qs.parse(req._parsedUrl.query);
  var orderBy = [];
  _.forOwn(queryParams, function(value, key) {
    if (_.startsWith(key, 'orderBy[')) {
      orderBy.push({
        column: parseInt(key.substring(8)) + 1,  // substring after "orderBy[" yields column number
        direction: (value == 0)  ? 'asc' : 'desc'
      });
    } else {
      orderBy.push({
        column: key,
        direction: value
      })
    }
  });

  async.waterfall([
    // wrap query in "count(*)" to get number of results (for use with paging)
    function(done) {
      if (paging) {
        var query = generateQuery(jsonQuery);
        sequelize.query("SET LOCAL config.user_id = " + req.user.id + "; select count(*) from (" + query.sql + ") as total_count_subquery", {
          replacements: query.values, type: sequelize.QueryTypes.SELECT
        }).then(function(count) {
          done(null, count);
        }).catch(function(err) {
          done(err);
        });
      } else {
        done(null, null);
      }
    },

    // execute actual query to get results (with limit, offset, and order by from paging params)
    function(count, done) {
      var query = generateQuery(jsonQuery);

      //wrap the original query, allowing it to be ordered internally, and apply the "front-end" column sorting to the outer query (user clicks on a column in the UI to sort by that column)
      if (!_.isEmpty(orderBy)) {
        var orderByClause = " order by " + _.map(orderBy, function(col) {
          return "tableizedQuery.\"" + col.column + "\" " + col.direction;
        }). join(',');
        query.sql = "SELECT * FROM (" + query.sql + ") AS tableizedQuery " + orderByClause;
      }

      if (paging) {
        query.sql += " limit " + limit + " offset " + offset;
      }

      query.sql = "SET LOCAL config.user_id = " + req.user.id + "; " + query.sql;

        sequelize.query(query.sql, {
        replacements: query.values, type: sequelize.QueryTypes.SELECT
      }).then(function (result) {
        var returnVal = paging ? {total: count[0].count, rows: result} : result;
        done(null, returnVal);
      }).catch(function (err) {
        done(err);
      })
    }
  ], function(err, response) {
    if (err) {
      console.error(err);
      res.send(500, err);
    } else {
      res.json(200, response);
    }
  });
};

exports.getFilters = function (req, res) {
  var table = req.body.table;
  var filter = req.body.filter;
  var query = req.body.query || "";
  var useWhitespace = req.body.useWhitespace;
  var dependsOn = req.body.dependsOn;
  var jsonQuery;

  if (_.isUndefined(filter)) {
    return res.send(401, 'Missing filter');
  }

  jsonQuery  = filter.autocomplete.query;
  jsonQuery.table = table;
  jsonQuery.type = 'select';
  jsonQuery.limit = 100;
  jsonQuery.where = jsonQuery.where || {};
  _.forEach(dependsOn, function(dependingFilter) {
    jsonQuery.where[dependingFilter.name] = dependingFilter.selectedFilter;
  });


  // Build where query for auto complete
  query = query.trim().toLowerCase();
  if (useWhitespace !== 'true') {
    query = query.replace(/\s+/g, '|');
  }
  query =' \'%' + query + '%\'';

  var matches = _.map(filter.matchColumns, function (column) {
    return 'LOWER(' + column + ') similar to ' + query;
  });

  var matchesQuery = '(' + matches.join(' or ') + ')';
  jsonQuery.where.$custom = [matchesQuery];

  var statement = generateQuery(jsonQuery);

  statement.sql = "SET LOCAL config.user_id = " + req.user.id + "; " + statement.sql;
  sequelize.query(statement.sql, {replacements: statement.values, type: sequelize.QueryTypes.SELECT})
    .then(function (results) {
      var result = _.map(results, function (value) {
        value.displayName = _.template(filter.displayName)(value);
        return value;
      });
      res.json(result);
    }).catch(function () {
      res.json([]);//
    });
};


var DEFAULT_DECKS = {
  'nes-reporting': ['humanCapital', 'utilization', 'laborHourReporting', 'requisition', 'candidate'],
  'ppmc-reporting': ['humanCapital', 'utilization', 'laborHourReporting', 'requisition', 'candidate'],
  'mission-services': ['humanCapital', 'utilization', 'laborHourReporting', 'requisition', 'candidate'],
  'staffing': ['requisition', 'candidate'],
  'pmr-reporting': ['programOverview', 'programStatus','financialStatus', 'staffing', 'programCyberSecurity', 'programExportInternational', 'salesforceLinks'] // TODO Temp disabled programs deck PMR-285 'programs'
  //'pmr-reporting': ['programOverview', 'programStatus'] // 'financialStatus'  TODO Temp disabled programs deck PMR-285 'programs'
};

var getPath = function(type, file) {
  return path.join(__dirname, '../../', type, file + '.json');
};
var getDeckPath = function(file) {
  return getPath('decks', file);
};
var getCardPath = function(file) {
  return getPath('cards', file);
};

var parseJsonFile = function(filePath) {
  return JSON.parse(fs.readFileSync(filePath, {encoding: 'utf-8'}));
};

var parseDeckFile = function(deckName) {
  var deck = parseJsonFile(getDeckPath(deckName));
  deck.deckName = deckName;

  deck.cards = _.map(deck.cards, function(cardName) {
    var cardJson = parseJsonFile(getCardPath(cardName));
    cardJson.cardName = cardName;
    return cardJson;
  });

  return deck;
};

exports.getCard = function(req, res) {
  var cardId = req.body.cardId;
  var cardJson = parseJsonFile(getCardPath(cardId));
  cardJson.cardName = cardId;
  return res.json(cardJson);
};

exports.getDecks = function(req, res) {
  var deckGroup = req.body.group;

  if (deckGroup && _.has(DEFAULT_DECKS, deckGroup) && _.includes(config.deckGroups, deckGroup)) {
    // TODO in the future we want to add group to the deck and then do a query for decks that match the group
    var decks = _.map(DEFAULT_DECKS[deckGroup], function(defaultDeckName) {
      return parseDeckFile(defaultDeckName);
    });

    return res.json(decks);
  } else {
    return res.json([]);
  }
};

exports.getLastUpdated = function(req, res) {
  var lastUpdatedConfig = req.body;

  var selectClauses = _.map(lastUpdatedConfig, function(column, table) {
    return "(SELECT MAX(" + column + ") FROM " + table + ")";
  });
  var selectStatement = "SELECT LEAST(" + selectClauses.join(", ") + " )";
  sequelize.query(selectStatement, {type: sequelize.QueryTypes.SELECT})
    .then(function (results) {
      res.json(results[0].least);
    }).catch(function () {
      res.json(null);
    });

};

exports.externalAPI = function (req, res) {
  var method = req.body.method.toUpperCase();
  var url = req.body.url;
  var parameters = req.body.parameters;
  switch(method) {
    case "GET":
      var getURL = url + "?" + qs.stringify(parameters);
      http.get(getURL, function (results) {
        var resultStream = "";
        results.on('data', function (resultChunk) {
          resultStream += resultChunk;
        });
        results.on('end', function () {
          res.json(JSON.parse(resultStream));
        });
      }).on('error', function (e) {
        res.json(null);
      });
      break;
    //TODO: Setup http.request case to handle POST requests: https://nodejs.org/api/http.html#http_http_request_options_callback
    default:
      var err = new Error('Method ' + method + " not supported.");
      console.log(err);
      return res.send(501, err);
      break;
  }
};
