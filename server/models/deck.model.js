/**
 * Created by iancampbell on 7/13/15.
 */

'use strict';

module.exports = function(sequelize, DataTypes) {
  var Deck = sequelize.define('Deck',
    {
      config: DataTypes.JSON
    }, 
    {
      tableName: 'decks',
      underscored: true,
      classMethods: {
        associate: function(models) {
          Deck.belongsToMany(models.User, {
            as: "viewers",
            through: 'deck_viewers'
          });
          Deck.belongsToMany(models.User, {
            as: "editors",
            through: 'deck_editors'
          });
          Deck.belongsTo(models.User, {
            as: "owner",
            constraints: false
          }); //the creator of the card
          Deck.hasMany(models.Card, {
            as: "cards",
            foreignKey: "deck_id",
            constraints: false
          });
        }

      }
    }
  );
  return Deck;
};

