module.exports = function(sequelize, DataTypes) {
  return sequelize.define("ProgramHeatmapManualEntry", {
    program_id: {
      type: DataTypes.INTEGER,
      unique: 'program_heatmap_manual_entry_unique_program_id_fiscal_month_end'
    },
    unique_id: {type: DataTypes.STRING, unique: true},
    fiscal_month_end: {
      type: DataTypes.DATE,
      unique: 'program_heatmap_manual_entry_unique_program_id_fiscal_month_end'
    },
    contractual: DataTypes.STRING,
    technical: DataTypes.STRING,
    investment: DataTypes.STRING,
    revenue_color: DataTypes.STRING,
    margin_dollar_color: DataTypes.STRING,
    cpi: DataTypes.DECIMAL(14,2),
    cost: DataTypes.STRING,
    spi: DataTypes.DECIMAL(14,2),
    schedule: DataTypes.STRING,
    eac_margin_percentage: DataTypes.DECIMAL(14,1),
    fyoi_percentage: DataTypes.DECIMAL(14,1),
    customer_satisfaction: DataTypes.STRING,
    staffing_percentage: DataTypes.DECIMAL(14,1),
    dl_percentage: DataTypes.DECIMAL(14,1),
    dl_percentage_actual: DataTypes.DECIMAL(14,1),
    staffing: DataTypes.STRING,
    supplier_status: DataTypes.STRING,
    offerings: DataTypes.STRING,
    acquisition_stability: DataTypes.STRING,
    overall_project: DataTypes.STRING,
    growth: DataTypes.STRING,
    growth_notes: DataTypes.TEXT,
    cyber_security: DataTypes.STRING,
    cyber_security_notes: DataTypes.TEXT,
    created_by: {
      type: DataTypes.INTEGER,
      references: 'users',
      referencesKey: 'id',
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    },
    updated_by: {
      type: DataTypes.INTEGER,
      references: 'users',
      referencesKey: 'id',
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    },
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    contractual_notes: DataTypes.TEXT,
    technical_notes: DataTypes.TEXT,
    cost_notes: DataTypes.TEXT,
    schedule_notes: DataTypes.TEXT,
    customer_satisfaction_notes: DataTypes.TEXT,
    staffing_notes: DataTypes.TEXT,
    dl_percentage_notes: DataTypes.TEXT,
    supplier_status_notes: DataTypes.TEXT,
    offerings_notes: DataTypes.TEXT,
    acquisition_stability_notes: DataTypes.TEXT,
    overall_project_notes: DataTypes.TEXT,
    eac_margin_percentage_notes: DataTypes.TEXT,
    margin_notes: DataTypes.TEXT,
    fyoi_percentage_notes: DataTypes.TEXT,
    fyoi: DataTypes.TEXT,
    dl_color: DataTypes.TEXT,
    validated: DataTypes.BOOLEAN,
    type: {
      type: DataTypes.ENUM,
      values: ['actual', 'forecast', 'pra'],
      defaultValue: 'actual'
    }
  }, {
    underscored: true,
    tableName: 'program_heatmap_manual_entry'
  })
};
