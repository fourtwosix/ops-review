module.exports = function(sequelize, DataTypes) {
  var DeckComment = sequelize.define("ProgramDeckComments", {
    program_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'programs',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    },
    fiscal_month_end: DataTypes.DATEONLY,
    deck: DataTypes.STRING,
    comment: DataTypes.TEXT,
    created_by: {
      type: DataTypes.INTEGER,
      references: {
        model: 'users',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    },
    updated_by: {
      type: DataTypes.INTEGER,
      references: {
        model: 'users',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    },
      updated_by: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
      deleted_by: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL'
      },
    is_deleted: DataTypes.BOOLEAN,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    delete_at: DataTypes.DATE
  },
    {
      underscored: true,
      tableName: 'program_deck_comments',
      classMethods: {
        associate: function(models) {
          DeckComment.belongsTo(models.User, {
            as: 'deckCommentCreatedBy',
            foreignKey: 'created_by'
          });
          DeckComment.belongsTo(models.User, {
            as: 'deckCommentUpdatedBy',
            foreignKey: 'updated_by'
          });
        }
      }
    });

  return DeckComment;
};
