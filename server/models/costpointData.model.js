module.exports = function(sequelize, DataTypes) {
  return sequelize.define("CostpointData", {
    report_date: DataTypes.DATE,
    unique_id: {type: DataTypes.STRING, unique: true},
    org_id: DataTypes.STRING,
    last_name: DataTypes.STRING,
    first_name: DataTypes.STRING,
    mi: DataTypes.STRING,
    tes_id: DataTypes.STRING,
    project_id: DataTypes.STRING,
    project_task: DataTypes.STRING,
    account_id: DataTypes.STRING,
    account_name: DataTypes.STRING,
    weekend_date: DataTypes.DATE,
    total_hours: DataTypes.FLOAT
  }, {
    underscored: true,
    tableName: 'costpoint_data'
  })
};
