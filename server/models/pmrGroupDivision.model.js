module.exports = function(sequelize, DataTypes) {
  var PMRGroupDivision = sequelize.define("PMRGroupDivision", {
    group_name: {type: DataTypes.STRING, primaryKey: true},
    division_name: {type: DataTypes.STRING, primaryKey: true},
    l3_manager: DataTypes.STRING,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE
  }, {
    underscored: true,
    tableName: 'pmr_groups_divisions'
  });

  return PMRGroupDivision;
};
