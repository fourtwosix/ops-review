module.exports = function(sequelize, DataTypes) {
  var Roles = sequelize.define("Roles", {
    name: {type: DataTypes.STRING, primaryKey: true},
    notes: DataTypes.STRING
  }, {
    underscored: true,
    tableName: 'roles',
    classMethods: {
      associate: function(models) {
        Roles.belongsToMany(models.User, {
          as: 'users',
          through: 'UserRolesAssociation'
        });
      }
    }
  });

  return Roles;
};
