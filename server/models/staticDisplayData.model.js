module.exports = function (sequelize, DataTypes) {
  return sequelize.define("StaticDisplayData", {
    // id: {type: DataTypes.INTEGER, primaryKey: true},
    display_value: {
      type: DataTypes.STRING,
      allowNull: false
    },
    type: {
      type: DataTypes.STRING,
      validate: {
        isIn: [['pmrName', 'pmrContractType', 'groups']]
      }
    },
    description: DataTypes.STRING,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER
  }, {
    underscored: true,
    tableName: 'static_display_data'
  })
};
