var moment = require('moment');

module.exports = function (sequelize, DataTypes) {
  var RequestToken = sequelize.define('RequestToken', {
    user_id: DataTypes.INTEGER,
    type: DataTypes.STRING,
    token: DataTypes.STRING,
    expires: DataTypes.DATE
  }, {
    underscored: true,
    tableName: 'request_tokens',
    indexes: [{fields: ['user_id']}, {fields: ['token']}],
    classMethods: {
      createToken: function (user, token, type) {
        return RequestToken.create({
          user_id: user.id,
          type: type,
          token: token,
          expires: moment().utc().add(5, 'hours').toDate()
        });
      },
      getToken: function (token, type) {
        return RequestToken.findOne({
          where: {
            token: token,
            type: type,
            expires: {$gte: moment().utc().toDate()}
          },
          order: 'created_at DESC'
        });
      },
      cleanUpTokens: function (user, type) {
        return RequestToken.destroy({where: {user_id: user.id, type: type}})
      }
    }
  });

  return RequestToken;
};
