module.exports = function(sequelize, DataTypes) {
  var UserRolesAssociation = sequelize.define("UserRolesAssociation", {
    user_id: {type: DataTypes.INTEGER, primaryKey: true},
    role_name: {type: DataTypes.STRING, primaryKey: true}
  }, {
    underscored: true,
    tableName: 'users_roles'
  });

  return UserRolesAssociation;
};
