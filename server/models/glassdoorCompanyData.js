module.exports = function(sequelize, DataTypes) {
  return sequelize.define("GlassdoorCompanyData", {
    company_name: DataTypes.STRING,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
    glassdoor_id: DataTypes.INTEGER,
    glassdoor_industry: DataTypes.STRING,
    numberOfRatings: DataTypes.INTEGER,
    overallRating: DataTypes.FLOAT,
    cultureAndValuesRating: DataTypes.FLOAT,
    seniorLeadershipRating: DataTypes.FLOAT,
    compensationAndBenefitsRating: DataTypes.FLOAT,
    careerOpportunitiesRating: DataTypes.FLOAT,
    workLifeBalanceRating: DataTypes.FLOAT,
    recommendToFriendRating: DataTypes.FLOAT
  }, {
    underscored: true,
    tableName: 'glassdoor_company_data'
  })
};
