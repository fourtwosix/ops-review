module.exports = function(sequelize, DataTypes) {
  return sequelize.define("ResourceHeatmap", {
    technical_performance: DataTypes.STRING,
    schedule_performance: DataTypes.STRING,
    quality_performance: DataTypes.STRING,
    management_performance: DataTypes.STRING,
    cost: DataTypes.STRING,
    customer_satisfaction: DataTypes.STRING,
    staffing: DataTypes.STRING,
    process: DataTypes.STRING,
    communication: DataTypes.STRING,
    invoicing: DataTypes.STRING,
    date: DataTypes.DATE
  }, {
    underscored: true,
    tableName: 'resource_heatmap'
  })
};
