module.exports = function(sequelize, DataTypes) {
  return sequelize.define("SubcontractsData", {
    report_date: DataTypes.DATE,
    unique_id: {type: DataTypes.STRING, unique: true},
    total: DataTypes.STRING,
    top_level_subcontract_num: DataTypes.STRING,
    lower_level_order_num: DataTypes.STRING,
    subcontractor_name: DataTypes.STRING,
    size: DataTypes.STRING,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    status: DataTypes.STRING,
    ceiling_value: DataTypes.FLOAT,
    value: DataTypes.FLOAT,
    current_funding: DataTypes.FLOAT,
    competition_type: DataTypes.STRING,
    csc_project_num: DataTypes.STRING,
    csc_contract_num: DataTypes.STRING,
    subcontract_admin: DataTypes.STRING,
    program_manager: DataTypes.STRING,
    audit_file_received_by_compliance: DataTypes.BOOLEAN,
    date_letter_subcontract_executed: DataTypes.DATE,
    date_letter_subcontract_to_be_definitized: DataTypes.DATE
  }, {
    underscored: true,
    tableName: 'subcontracts_data'
  })
};
