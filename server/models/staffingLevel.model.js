'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('StaffingLevel', {
    resource: DataTypes.STRING,
    staffing_fte: DataTypes.STRING,
    year: DataTypes.INTEGER,
    jan_2015: DataTypes.INTEGER,
    feb_2015: DataTypes.INTEGER,
    mar_2015: DataTypes.INTEGER,
    apr_2015: DataTypes.INTEGER,
    may_2015: DataTypes.INTEGER,
    jun_2015: DataTypes.INTEGER,
    jul_2015: DataTypes.INTEGER,
    aug_2015: DataTypes.INTEGER,
    sep_2015: DataTypes.INTEGER,
    oct_2015: DataTypes.INTEGER,
    nov_2015: DataTypes.INTEGER,
    dec_2015: DataTypes.INTEGER
  }, {
    underscored: true,
    tableName: 'staffing_level'
  });
};
