'use strict';

module.exports = function(sequelize, DataTypes) {
  var PmrGroups = sequelize.define('PmrGroups', {
    group_name: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    description: DataTypes.TEXT,
    l2_manager: DataTypes.STRING,
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE
    },
    {
      tableName: 'pmr_groups',
      underscored: true,
    });

  return PmrGroups;
};
