var _ = require('lodash');
var Promise = require('bluebird');

module.exports = function(sequelize, DataTypes) {
  var PermastData = sequelize.define('PermastData', {
    report_date: DataTypes.DATE,
    unique_id: {type: DataTypes.STRING, unique: true},
    personnel_number: DataTypes.STRING,
    last_name: DataTypes.STRING,
    first_name: DataTypes.STRING,
    short_name: DataTypes.STRING,
    company_code: DataTypes.STRING,
    company_name: DataTypes.STRING,
    business_unit_area: DataTypes.STRING,
    employment_status: DataTypes.STRING,
    cost_center_code: DataTypes.STRING,
    cost_center_name: DataTypes.STRING,
    employee_group: DataTypes.STRING,
    employee_subgroup: DataTypes.STRING,
    work_schedule: DataTypes.STRING,
    hours_per_week: {
      type: DataTypes.INTEGER,
      set: function(val) {
        this.setDataValue('hours_per_week', parseInt(val));
      }
    },
    exempt_status: DataTypes.STRING,
    job_code: DataTypes.STRING,
    job_description: DataTypes.STRING,
    short_job_description: DataTypes.STRING,
    position_number: DataTypes.STRING,
    position_name: DataTypes.STRING,
    ps_grp: DataTypes.STRING,
    hire_date: DataTypes.DATE,
    termination_date: DataTypes.DATE,
    termination_type: DataTypes.STRING,
    leave_accrual_date: DataTypes.DATE,
    service_date: DataTypes.DATE,
    supervisor_personnel_number: DataTypes.STRING,
    superior_short_name: DataTypes.STRING,
    superior: DataTypes.STRING,
    layer_2_org_name: DataTypes.STRING,
    layer_2_manager: DataTypes.STRING,
    layer_3_org_name: DataTypes.STRING,
    layer_3_manager: DataTypes.STRING,
    layer_4_org_name: DataTypes.STRING,
    layer_4_manager: DataTypes.STRING,
    layer_5_org_name: DataTypes.STRING,
    layer_5_manager: DataTypes.STRING,
    layer_6_org_name: DataTypes.STRING,
    layer_6_manager: DataTypes.STRING,
    layer_7_org_name: DataTypes.STRING,
    layer_7_manager: DataTypes.STRING,
    location: DataTypes.STRING,

    // Ingest fields
    latitude: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('latitude', parseFloat(val));
      }
    },
    longitude: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('longitude', parseFloat(val));
      }
    }
  }, {
    underscored: true,
    tableName: 'permast_data',
    indexes: [
      {fields: ['report_date']},
      {fields: ['personnel_number']},
      {fields: ['short_name']},
      {fields: ['employment_status']},
      {fields: ['hire_date']},
      {fields: ['termination_date']},
      {fields: ['termination_type']},
      {fields: ['layer_3_manager']},
      {fields: ['layer_4_manager']},
      {fields: ['layer_5_manager']}
    ],
    classMethods: {
      getOrgInfo: function (user) {
        var deferred = Promise.pending();
        var re = /(.*)@(csgov.com|csc.com)/ig;
        var shortName = (re.exec(user.email) || [])[1];

        if (shortName) {
          PermastData.findOne({
            where: {short_name: shortName.toLowerCase()},
            order: 'report_date DESC'
          }).then(function (info) {
            deferred.fulfill({
              first_name: info.first_name,
              last_name: info.last_name,
              layer_2_org_name: info.layer_2_org_name,
              layer_2_manager: info.layer_2_manager,
              layer_3_org_name: info.layer_3_org_name,
              layer_3_manager: info.layer_3_manager,
              layer_4_org_name: info.layer_4_org_name,
              layer_4_manager: info.layer_4_manager,
              layer_5_org_name: info.layer_5_org_name,
              layer_5_manager: info.layer_5_manager,
              layer_6_org_name: info.layer_6_org_name,
              layer_6_manager: info.layer_6_manager,
              layer_7_org_name: info.layer_7_org_name,
              layer_7_manager: info.layer_7_manager
            });
          }).catch(function (err) {
            deferred.reject(err);
          });
        } else {
          deferred.reject();
        }
        return deferred.promise;
      },
      getManagerInfo: function (firstName, lastName) {
        var managerName = lastName.toLowerCase() + ', ' + firstName.toLowerCase();
        var deferred = Promise.pending();

        PermastData.findOne({
          where: {
            $or: [
              {layer_2_manager: managerName},
              {layer_3_manager: managerName},
              {layer_4_manager: managerName},
              {layer_5_manager: managerName},
              {layer_6_manager: managerName},
              {layer_7_manager: managerName}
            ]
          },
          attributes: [
            'layer_2_org_name',
            'layer_2_manager',
            'layer_3_org_name',
            'layer_3_manager',
            'layer_4_org_name',
            'layer_4_manager',
            'layer_5_org_name',
            'layer_5_manager',
            'layer_6_org_name',
            'layer_6_manager',
            'layer_7_org_name',
            'layer_7_manager'
          ],
          order: 'report_date DESC'
        }).then(function (info) {
          if (info) {
            var managerLevel = _(info.toJSON()).omit(_.isUndefined).omit(_.isNull).invert().value();
            managerLevel = parseInt(managerLevel[managerName].split('_')[1]);
            deferred.fulfill({
              isManager: true,
              managerLevel: managerLevel
            });
          } else {
            deferred.fulfill({isManager: false});
          }
        }).catch(function (err) {
          deferred.reject(err);
        });

        return deferred.promise;
      }
    }
  });

  return PermastData;
};
