module.exports = function(sequelize, DataTypes) {
  return sequelize.define("CandidateData", {
    report_date: DataTypes.DATE,
    unique_id: {type: DataTypes.STRING, unique: true},
    requisition_number: DataTypes.STRING,
    requisition_title: DataTypes.STRING,
    req_creation_date: DataTypes.DATE,
    req_first_fully_approved_date: DataTypes.DATE,
    req_first_sourcing_date: DataTypes.DATE,
    job_title: DataTypes.STRING,
    candidate_name: DataTypes.STRING,
    candidate_id: DataTypes.STRING,
    application_cws_step: DataTypes.STRING,
    application_cws_status: DataTypes.STRING,
    application_cws_start_date: DataTypes.DATE,
    client_program_name: DataTypes.STRING,
    candidate_is_internal: DataTypes.BOOLEAN,
    job_code: DataTypes.STRING,
    req_current_parent_status: DataTypes.STRING,
    req_current_detail_status: DataTypes.STRING,
    req_type: DataTypes.STRING,
    work_country: DataTypes.STRING,
    work_state: DataTypes.STRING,
    work_city: DataTypes.STRING,
    req_hiring_manager_name: DataTypes.STRING,
    req_recruiter_name: DataTypes.STRING,
    org_layer_2: DataTypes.STRING,
    org_layer_3: DataTypes.STRING,
    org_layer_4: DataTypes.STRING,
    org_layer_5: DataTypes.STRING,
    org_layer_6: DataTypes.STRING,
    org_layer_7: DataTypes.STRING,
    layer_2_manager: DataTypes.STRING,
    layer_3_manager: DataTypes.STRING,
    layer_4_manager: DataTypes.STRING,
    layer_5_manager: DataTypes.STRING,
    layer_6_manager: DataTypes.STRING,
    layer_7_manager: DataTypes.STRING,

    latitude: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('latitude', parseFloat(val));
      }
    },
    longitude: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('longitude', parseFloat(val));
      }
    }
  }, {
    underscored: true,
    tableName: 'candidate_data',
    indexes: [
      {fields: ['report_date']},
      {fields: ['requisition_number']},
      {fields: ['application_cws_step']},
      {fields: ['application_cws_status']},
      {fields: ['client_program_name']},
      {fields: ['layer_3_manager']},
      {fields: ['layer_4_manager']},
      {fields: ['layer_5_manager']},
      {fields: ['req_creation_date']},
      {fields: ['req_first_fully_approved_date']},
      {fields: ['req_first_sourcing_date']},
      {name: 'req_by_candidate', fields: ['requisition_number', 'candidate_id']}
    ]
  })
};
