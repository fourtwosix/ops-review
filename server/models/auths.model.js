module.exports = function(sequelize, DataTypes) {
  var Auths = sequelize.define("Auths", {
    name: {
      type: DataTypes.STRING,
      primaryKey: true
    }
  }, {
    underscored: true,
    tableName: 'auths',
    classMethods: {
      associate: function(models) {
        Auths.belongsToMany(models.User, {
          through: 'users_auths',
          foreignKey: 'auth_name'
        });
      }
    }
  });

  return Auths;
};
