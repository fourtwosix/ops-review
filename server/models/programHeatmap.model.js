module.exports = function(sequelize, DataTypes) {
  return sequelize.define("ProgramHeatmap", {
    contractual: DataTypes.STRING,
    technical: DataTypes.STRING,
    cost: DataTypes.STRING,
    schedule: DataTypes.STRING,
    margin: DataTypes.STRING,
    customer_satisfaction: DataTypes.STRING,
    dl_color: DataTypes.STRING,
    fyoi: DataTypes.STRING,
    staffing: DataTypes.STRING,
    supplier: DataTypes.STRING,
    offerings: DataTypes.STRING,
    acquisition_stability: DataTypes.STRING,
    overall: DataTypes.STRING,
    date: DataTypes.DATE,
    project_id: DataTypes.STRING
  }, {
    underscored: true,
    tableName: 'program_heatmap'
  })
};
