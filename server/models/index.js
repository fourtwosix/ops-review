'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(module.filename);
var env       = process.env.NODE_ENV || 'development';
var dbConfig  = require(__dirname + '/../config/database.json')[env];
var config    = require(__dirname + '/../config/environment');

// If databases are disabled don't initialize them
if (config.disableDatabases) return;

var sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, dbConfig);
var async     = require('async');
var _         = require('lodash');
var db        = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename);
  })
  .forEach(function(file) {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

_.forEach(_.values(db), function(model) {
  model.associate && model.associate(db);
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.migrate = function() {
  var Umzug = require('umzug');

  var umzug = new Umzug({
    storage: 'sequelize',
    logging: function (message) {
      console.log(message);
    },
    storageOptions: {
      sequelize: sequelize
    },
    migrations: {
      params: [sequelize.getQueryInterface(), sequelize.constructor, function() {
        throw new Error('Migration tried to use old style "done" callback. Please upgrade to "umzug" and return a promise instead.');
      }],
      path: path.join(__dirname + '../../../db/migrate')
    }
  });

  return umzug.up();
}


module.exports = db;
