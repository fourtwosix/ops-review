'use strict';

module.exports = function(sequelize, DataTypes) {
  var PMRDivision = sequelize.define('PMRDivision', {
      division_name: {
        type: DataTypes.STRING,
        primaryKey: true
      },
      description: DataTypes.TEXT,
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE
    },
    {
      tableName: 'pmr_divisions',
      underscored: true,
      classMethods: {
        associate: function(models) {
          PMRDivision.belongsToMany(models.PMRGroup, {
            as: 'groups',
            through: 'PMRGroupDivision',
            foreignKey: 'division_name'
          });
        }
      }
    });

  return PMRDivision;
};
