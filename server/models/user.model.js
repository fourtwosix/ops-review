'use strict';

var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
      name: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        unique: true,
        validate: {
          isEmail: true
        },
        set: function(val) {
          this.setDataValue('email', val.toLowerCase());
        }
      },
      title: {type: DataTypes.STRING, defaultValue: 'user'},
      hashedPassword: {
        type: DataTypes.STRING,
        field: "hashed_password",
        validate: {
          notEmpty: true
        }
      },
      provider: DataTypes.STRING,
      groups: DataTypes.ARRAY(DataTypes.STRING),
      salt: DataTypes.STRING,
      password: {
        type: DataTypes.VIRTUAL,
        set: function(val) {
          this.setDataValue('_password', val);
          this.setDataValue('salt', this.makeSalt());
          this.setDataValue('hashedPassword', this.encryptPassword(val));
        },
        get: function() {
          return this.getDataValue('_password');
        }
      },
      profile: {
        type: DataTypes.VIRTUAL,
        get: function() {
          return {
            'name': this.getDataValue('name'),
            'title': this.getDataValue('title'),
            'email': this.getDataValue('email')
          };
        }
      },
      token: {
        type: DataTypes.VIRTUAL,
        get: function() {
          return {
            'id': this.getDataValue('id'),
            'title': this.getDataValue('title')
          };
        }
      },
      receives_emails: {
        type: DataTypes.BOOLEAN
      },
      pmr_group: DataTypes.ARRAY(DataTypes.STRING),
      divisions: DataTypes.ARRAY(DataTypes.STRING)
    },
    {
      tableName: 'users',
      underscored: true,
      classMethods: {
        associate: function(models) {
          User.belongsToMany(models.Roles, {
            as: 'roles',
            through: 'UserRolesAssociation'
          });

          User.belongsToMany(models.Program, {
            as: 'programs',
            through: 'users_programs'
          });

          User.belongsToMany(models.Deck, {
            as: 'editableDecks',
            through: 'deck_editors'
          });

          User.belongsToMany(models.Deck, {
            as: 'viewableDecks',
            through: 'deck_viewers'
          });
          User.hasMany(models.Deck, {
            as: "ownedDecks",
            foreignKey: "owner_id",
            constraints: false
          });
          User.belongsToMany(models.Auths, {
            as: 'auths',
            through: 'users_auths',
            foreignKey: 'user_id'
          });
          User.hasMany(models.ProgramDeckComments, {
            foreignKey: "created_by"
          });
        }
      },
      instanceMethods: {
        /**
         * Authenticate - check if the passwords are the same
         *
         * @param {String} plainText
         * @return {Boolean}
         * @api public
         */
        authenticate: function(plainText) {
          return this.encryptPassword(plainText) === this.hashedPassword;
        },

        /**
         * Make salt
         *
         * @return {String}
         * @api public
         */
        makeSalt: function() {
          return crypto.randomBytes(16).toString('base64');
        },

        /**
         * Encrypt password
         *
         * @param {String} password
         * @return {String}
         * @api public
         */
        encryptPassword: function(password) {
          if (!password || !this.salt) return '';
          var salt = new Buffer(this.salt, 'base64');
          return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
        }
      }
    });

  return User;
};
