module.exports = function(sequelize, DataTypes) {
  return sequelize.define('LaborUtilizationData', {
    report_date: DataTypes.DATE,
    week_ending_date: DataTypes.DATE,
    unique_id: {type: DataTypes.STRING, unique: true},
    line_of_service: DataTypes.STRING,
    industry: DataTypes.STRING,
    offering: DataTypes.STRING,
    pool: DataTypes.STRING,
    group: DataTypes.STRING,
    cp_org: DataTypes.STRING,
    personnel_number: DataTypes.STRING,
    full_employee_name: DataTypes.STRING,
    short_name: DataTypes.STRING,
    employee_type_code: DataTypes.STRING,
    employee_salary_code: DataTypes.STRING,
    hire_date: DataTypes.DATE,
    termination_date: DataTypes.DATE,
    full_time_weeks_worked: DataTypes.INTEGER,
    wtd_adjusted_capacity: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('wtd_adjusted_capacity', parseFloat(val));
      }
    },
    mtd_adjusted_capacity: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('mtd_adjusted_capacity', parseFloat(val));
      }
    },
    ytd_adjusted_capacity: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('ytd_adjusted_capacity', parseFloat(val));
      }
    },
    ytd_capacity: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('ytd_capacity', parseFloat(val));
      }
    },
    wtd_billable: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('wtd_billable', parseFloat(val));
      }
    },
    wtd_unbillable: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('wtd_unbillable', parseFloat(val));
      }
    },
    wtd_bid_proposal: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('wtd_bid_proposal', parseFloat(val));
      }
    },
    wtd_odo: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('wtd_odo', parseFloat(val));
      }
    },
    wtd_internal_cost_recovery: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('wtd_internal_cost_recovery', parseFloat(val));
      }
    },
    wtd_general_overhead: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_general_overhead', parseFloat(val));
      }
    },
    wtd_service_center: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_service_center', parseFloat(val));
      }
    },
    wtd_available_bench: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_available_bench', parseFloat(val));
      }
    },
    wtd_training: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_training', parseFloat(val));
      }
    },
    wtd_unallowable_indirect: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_unallowable_indirect', parseFloat(val));
      }
    },
    wtd_holiday: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_holiday', parseFloat(val));
      }
    },
    wtd_vacation: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_vacation', parseFloat(val));
      }
    },
    wtd_sick_leave: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_sick_leave', parseFloat(val));
      }
    },
    wtd_emergency: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_emergency', parseFloat(val));
      }
    },
    wtd_lwop: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_lwop', parseFloat(val));
      }
    },
    wtd_other_leave: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_other_leave', parseFloat(val));
      }
    },
    wtd_suspense: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_suspense', parseFloat(val));
      }
    },
    wtd_total_hours: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_total_hours', parseFloat(val));
      }
    },
    mtd_billable: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('mtd_billable', parseFloat(val));
      }
    },
    mtd_unbillable: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('mtd_unbillable', parseFloat(val));
      }
    },
    mtd_bid_proposal: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('mtd_bid_proposal', parseFloat(val));
      }
    },
    mtd_odo: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('mtd_odo', parseFloat(val));
      }
    },
    mtd_internal_cost_recovery: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('mtd_internal_cost_recovery', parseFloat(val));
      }
    },
    mtd_general_overhead: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_general_overhead', parseFloat(val));
      }
    },
    mtd_service_center: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_service_center', parseFloat(val));
      }
    },
    mtd_available_bench: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_available_bench', parseFloat(val));
      }
    },
    mtd_training: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_training', parseFloat(val));
      }
    },
    mtd_unallowable_indirect: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_unallowable_indirect', parseFloat(val));
      }
    },
    mtd_holiday: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_holiday', parseFloat(val));
      }
    },
    mtd_vacation: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_vacation', parseFloat(val));
      }
    },
    mtd_sick_leave: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_sick_leave', parseFloat(val));
      }
    },
    mtd_emergency: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_emergency', parseFloat(val));
      }
    },
    mtd_lwop: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_lwop', parseFloat(val));
      }
    },
    mtd_other_leave: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_other_leave', parseFloat(val));
      }
    },
    mtd_suspense: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_suspense', parseFloat(val));
      }
    },
    mtd_total_hours: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_total_hours', parseFloat(val));
      }
    },
    ytd_billable: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('ytd_billable', parseFloat(val));
      }
    },
    ytd_unbillable: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('ytd_unbillable', parseFloat(val));
      }
    },
    ytd_bid_proposal: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('ytd_bid_proposal', parseFloat(val));
      }
    },
    ytd_odo: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('ytd_odo', parseFloat(val));
      }
    },
    ytd_internal_cost_recovery: {
      type: DataTypes.FLOAT,
      set: function(val) {
        this.setDataValue('ytd_internal_cost_recovery', parseFloat(val));
      }
    },
    ytd_general_overhead: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_general_overhead', parseFloat(val));
      }
    },
    ytd_service_center: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_service_center', parseFloat(val));
      }
    },
    ytd_available_bench: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_available_bench', parseFloat(val));
      }
    },
    ytd_training: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_training', parseFloat(val));
      }
    },
    ytd_unallowable_indirect: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_unallowable_indirect', parseFloat(val));
      }
    },
    ytd_holiday: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_holiday', parseFloat(val));
      }
    },
    ytd_vacation: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_vacation', parseFloat(val));
      }
    },
    ytd_sick_leave: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_sick_leave', parseFloat(val));
      }
    },
    ytd_emergency: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_emergency', parseFloat(val));
      }
    },
    ytd_lwop: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_lwop', parseFloat(val));
      }
    },
    ytd_other_leave: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_other_leave', parseFloat(val));
      }
    },
    ytd_suspense: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_suspense', parseFloat(val));
      }
    },
    ytd_total_hours: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_total_hours', parseFloat(val));
      }
    },
    wtd_investments: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('wtd_investments', parseFloat(val));
      }
    },
    mtd_investments: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('mtd_investments', parseFloat(val));
      }
    },
    ytd_investments: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('ytd_investments', parseFloat(val));
      }
    },
    direct_billable_utilization_wtd: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('direct_billable_utilization_wtd', parseFloat(val));
      }
    },
    cost_recovery_utilization_wtd: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('cost_recovery_utilization_wtd', parseFloat(val));
      }
    },
    total_productive_utilization_wtd: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('total_productive_utilization_wtd', parseFloat(val));
      }
    },
    direct_billable_utilization_mtd: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('direct_billable_utilization_mtd', parseFloat(val));
      }
    },
    cost_recovery_utilization_mtd: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('cost_recovery_utilization_mtd', parseFloat(val));
      }
    },
    total_productive_utilization_mtd: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('total_productive_utilization_mtd', parseFloat(val));
      }
    },
    direct_billable_utilization_ytd: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('direct_billable_utilization_ytd', parseFloat(val));
      }
    },
    cost_recovery_utilization_ytd: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('cost_recovery_utilization_ytd', parseFloat(val));
      }
    },
    total_productive_utilization_ytd: {
      type: DataTypes.FLOAT, set: function (val) {
        this.setDataValue('total_productive_utilization_ytd', parseFloat(val));
      }
    }
  },
  {
    underscored: true,
    tableName: 'labor_utilization_data',
    indexes: [
      {fields: ['report_date']},
      {fields: ['week_ending_date']},
      {fields: ['personnel_number']},
      {fields: ['short_name']}
    ]
  });
};
