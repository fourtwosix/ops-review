module.exports = function(sequelize, DataTypes) {
  return sequelize.define("ProjectInfoData", {
    report_date: DataTypes.DATE,
    unique_id: {type: DataTypes.STRING, unique: true},
    project_id: DataTypes.STRING,
    project_name: DataTypes.STRING,
    org_id: DataTypes.STRING,
    project_start_date: DataTypes.DATE,
    project_end_date: DataTypes.DATE,
    prime_contractor_id: DataTypes.STRING
  }, {
    underscored: true,
    tableName: 'project_info_data'
  })
};
