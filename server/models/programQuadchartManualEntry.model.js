module.exports = function(sequelize, DataTypes) {
  return sequelize.define("ProgramQuadchartManualEntry", {
    fiscal_month_end: DataTypes.DATE,
    opportunities: DataTypes.TEXT,
    issues: DataTypes.TEXT,
    risks: DataTypes.TEXT,
    key_milestones: DataTypes.TEXT,
    noteworthy: DataTypes.TEXT,
    validated: DataTypes.BOOLEAN,
    created_by: {
      type: DataTypes.INTEGER,
      references: 'users',
      referencesKey: 'id',
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    },
    updated_by: {
      type: DataTypes.INTEGER,
      references: 'users',
      referencesKey: 'id',
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL'
    },
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE
  }, {
    underscored: true,
    tableName: 'program_quadchart_manual_entry'
  })
};
