'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('StaffingCompositionLos', {
    los: DataTypes.STRING,
    program: DataTypes.STRING,
    resource: DataTypes.STRING,
    jan: DataTypes.INTEGER,
    feb: DataTypes.INTEGER,
    mar: DataTypes.INTEGER,
    apr: DataTypes.INTEGER,
    may: DataTypes.INTEGER,
    jun: DataTypes.INTEGER,
    jul: DataTypes.INTEGER,
    aug: DataTypes.INTEGER,
    sep: DataTypes.INTEGER,
    oct: DataTypes.INTEGER,
    nov: DataTypes.INTEGER,
    dec: DataTypes.INTEGER
  }, {
    underscored: true,
    tableName: 'staffing_composition_los'
  });
};
