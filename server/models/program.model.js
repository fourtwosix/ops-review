module.exports = function(sequelize, DataTypes) {
  var Program = sequelize.define("Program", {
    id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
    name: DataTypes.STRING,
    project_id: {type: DataTypes.STRING, uniqueKey: true},
    pmr_group: DataTypes.STRING,
    division: DataTypes.STRING
  }, {
    underscored: true,
    tableName: 'programs_secure',
    classMethods: {
      associate: function(models) {
        Program.hasMany(models.ProgramQuadchartManualEntry, {as: 'quadchartManualEntries', foreignKey: 'program_id'});
        Program.hasMany(models.ProgramHeatmapManualEntry, {as: 'heatmapManualEntries', foreignKey: 'program_id'});
        Program.hasMany(models.ProgramMetadataManualEntry, {as: 'metadataManualEntries', foreignKey: 'program_id'});
        Program.belongsToMany(models.Offerings, {through: models.ProgramOfferingAssociation, foreignKey: 'program_id'});
        Program.belongsToMany(models.User, {
          as: 'users',
          through: 'users_programs',
          foreignKey: 'program_id'
        });
        Program.belongsTo(models.Auths, {
          as: 'auth'
        });
      }
    }
  });

  return Program;
};
