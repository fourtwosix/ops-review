/**
 * Created by iancampbell on 7/13/15.
 */

'use strict';

module.exports = function(sequelize, DataTypes) {
  var Card = sequelize.define('Card', {
    config: DataTypes.JSON
  },
    {
      tableName: 'cards',
      underscored: true,
      classMethods: {
        associate: function (models) {
          Card.belongsTo(models.Deck, {
            onDelete: 'cascade'
          });
        }
      }
    });

  return Card;
};
