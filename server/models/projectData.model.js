module.exports = function(sequelize, DataTypes) {
  return sequelize.define("ProjectData", {
    report_date: DataTypes.DATE,
    unique_id: {type: DataTypes.STRING, unique: true},
    personnel_number: DataTypes.STRING,
    short_name: DataTypes.STRING,
    full_employee_name: DataTypes.STRING,
    employee_type_code: DataTypes.STRING,
    timesheet_type: DataTypes.STRING,
    home_org_id: DataTypes.STRING,
    business_organization: DataTypes.STRING,
    home_org_name: DataTypes.STRING,
    level_3_org_segment_id: DataTypes.STRING,
    line_of_service: DataTypes.STRING,
    line_of_service_code: DataTypes.STRING,
    level_3_org_name: DataTypes.STRING,
    project_classification: DataTypes.STRING,
    project_id: DataTypes.STRING,
    project_name: DataTypes.STRING,
    labor_type_id: DataTypes.STRING,
    labor_type: DataTypes.STRING,
    hours: {
      type: DataTypes.INTEGER,
      set: function(val) {
        this.setDataValue('hours_per_week', parseInt(val));
      }
    },
    timesheet_date: DataTypes.DATE,
    employee_layer: DataTypes.STRING,
    layer_4_manager: DataTypes.STRING,
    layer_5_manager: DataTypes.STRING,
    layer_6_manager: DataTypes.STRING,
    layer_7_manager: DataTypes.STRING
  }, {
    underscored: true,
    tableName: 'project_data'
  })
};
