module.exports = function(sequelize, DataTypes) {
  var Offerings = sequelize.define("Offerings", {
    unique_id: {type: DataTypes.STRING, unique: true},
    offering_name: DataTypes.STRING,
    los_name: DataTypes.STRING,
    offering_code: DataTypes.STRING,
    offering_description: DataTypes.STRING
  }, {
    underscored: true,
    tableName: 'offerings',
    classMethods: {
      associate: function(models) {
        Offerings.belongsToMany(models.Program, {through: models.ProgramOfferingAssociation});
        Offerings.hasMany(models.Offerings, {foreignKey: 'parent_offering_id', as: 'parent'});
      }
    }
  });

  return Offerings;
};
