module.exports = function(sequelize, DataTypes) {
  return sequelize.define("StepsData", {
    report_date: DataTypes.DATE,
    unique_id: {type: DataTypes.STRING, unique: true},
    org_id: DataTypes.STRING,
    vendor: DataTypes.STRING,
    vendor_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    first_name: DataTypes.STRING,
    mi: DataTypes.STRING,
    tes_id: DataTypes.STRING,
    project: DataTypes.STRING,
    project_task: DataTypes.STRING,
    weekend_date: DataTypes.DATE,
    total_hours: DataTypes.FLOAT,
    run_date: DataTypes.DATE
  }, {
    underscored: true,
    tableName: 'steps_data'
  })
};
