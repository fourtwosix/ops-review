module.exports = function(sequelize, DataTypes) {
  var Resource = sequelize.define("Resource", {
    name: DataTypes.STRING,
    type: DataTypes.STRING,
    project_id: DataTypes.STRING
  }, {
    underscored: true,
    tableName: 'resources',
    classMethods: {
      associate: function(models) {
        Resource.hasMany(models.ResourceHeatmap, {as: 'heatmaps'});
      }
    }
  });

  return Resource;
};
