module.exports = function(sequelize, DataTypes) {
  var ProgramOfferingAssociation = sequelize.define("ProgramOfferingAssociation", {
    type: DataTypes.STRING,
    program_id:DataTypes.INTEGER,
    offering_id:DataTypes.INTEGER
  }, {
    underscored: true,
    tableName: 'program_offering_association'
  });

  return ProgramOfferingAssociation;
};
