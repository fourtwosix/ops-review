'use strict';

module.exports = function(sequelize, DataTypes) {
  var PMRGroup = sequelize.define('PMRGroup', {
      group_name: {
        type: DataTypes.STRING,
        primaryKey: true
      },
      description: DataTypes.TEXT,
      l2_manager: DataTypes.STRING,
      created_at: DataTypes.DATE,
      updated_at: DataTypes.DATE
    },
    {
      tableName: 'pmr_groups',
      underscored: true,
      classMethods: {
        associate: function(models) {
          PMRGroup.belongsToMany(models.PMRDivision, {
            as: 'divisions',
            through: 'PMRGroupDivision',
            foreignKey: 'group_name'
          });
        }
      }
    });

  return PMRGroup;
};
