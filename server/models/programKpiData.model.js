module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ProgramKpiData', {
    program: DataTypes.STRING,
    pra: DataTypes.STRING,
    staffing_source: DataTypes.STRING,
    sub: DataTypes.BOOLEAN,
    mgt: DataTypes.STRING,
    tech: DataTypes.STRING,
    sched: DataTypes.STRING,
    cost: DataTypes.STRING,
    quality: DataTypes.STRING,
    cust_sat: DataTypes.STRING,
    staff: DataTypes.STRING,
    process: DataTypes.STRING,
    invoicing: DataTypes.STRING
  }, {
    underscored: true,
    tableName: 'program_kpi_data'
  });
};