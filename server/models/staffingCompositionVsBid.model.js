'use strict';

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('StaffingCompositionVsBid', {
      program: DataTypes.STRING,
      sub: DataTypes.STRING,
      month: DataTypes.STRING,
      percent_workshare: DataTypes.DECIMAL,
      percent_promised: DataTypes.DECIMAL,
      variance: DataTypes.DECIMAL
    }, {
    underscored: true,
    tableName: 'staffing_composition_vs_bid'
  });
};

