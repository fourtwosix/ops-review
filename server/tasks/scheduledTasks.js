var _ = require('lodash');
var models = require('../models');
var sequelize = models.sequelize;
var qs = require('querystring');
var http = require('http');
var schedule = require('node-schedule');
var async = require('async');
var moment = require('moment');
var emails = require('../emails');
var config = require('../config/environment');

module.exports = function() {

  var refreshMaterializedViews = function () {
    console.log("Executing scheduled task: scheduled_refreshMaterializedViews");

    sequelize.query("select RefreshAllMaterializedViews()", {type: sequelize.QueryTypes.SELECT}).then(function () {
      console.log("Successfully finished refreshing materialized views!")
    }).catch(function (err) {
      console.log("Error refreshing materialized views:", err);
    });
  };

  var callAPI = function (method, url, parameters, callback) {
    var apiReturn = {
      error: undefined,
      data: {}
    };
    switch (method.toUpperCase()) {
      case "GET":
        var getURL = url + "?" + qs.stringify(parameters);
        http.get(getURL, function (results) {
          var resultStream = "";
          results.on('data', function (resultChunk) {
            resultStream += resultChunk;
          });
          results.on('end', function () {
            apiReturn.data = JSON.parse(resultStream);
            callback(apiReturn);
          });
        }).on('error', function (e) {
          apiReturn.error = e;
          console.log(apiReturn.error);
          callback(apiReturn);
        });
        break;
      //TODO: Setup http.request case to handle POST requests: https://nodejs.org/api/http.html#http_http_request_options_callback
      default:
        apiReturn.error = new Error('Method ' + method + " not supported.");
        console.log(apiReturn.error);
        callback(apiReturn);
        break;
    }
  };

  var insertGlassdoorRecord = function(record) {
    if (!record) return;
    var updateColumns = [
      'company_name',
      'glassdoor_id',
      'glassdoor_industry',
      'number_of_ratings',
      'overall_rating',
      'culture_and_values_rating',
      'senior_leadership_rating',
      'compensation_and_benefits_rating',
      'career_opportunities_rating',
      'work_life_balance_rating',
      'recommend_to_friend_rating'
    ];

    var insertColumns = _.cloneDeep(updateColumns);
    insertColumns.push([
      'created_at',
      'updated_at'
    ]);

    var companyName = "'" + record.name + "'";

    var updateValues = [
      companyName,
      record.id,
      "'" + record.industry + "'",
      record.numberOfRatings,
      record.overallRating,
      record.cultureAndValuesRating,
      record.seniorLeadershipRating,
      record.compensationAndBenefitsRating,
      record.careerOpportunitiesRating,
      record.workLifeBalanceRating,
      record.recommendToFriendRating
    ];

    var insertValues = _.cloneDeep(updateValues);
    insertValues.push([
      "now()",
      "now()"
    ]);

    var updateSetters = [];
    _.forEach(updateColumns, function(col, i) {
      updateSetters.push(col + " = " + updateValues[i])
    });

    // Select the most recent record for the given company and with the same data values we are passing in.
    var selectSQL = "SELECT company_name, created_at " +
      "FROM glassdoor_company_data " +
      "WHERE created_at::DATE = now()::DATE AND company_name = " + companyName + ";";
    sequelize.query(selectSQL, {type: sequelize.QueryTypes.SELECT})
      .then(function (results) {
        var recordCount = results.length;
        if(recordCount == 0) {
          // If any of the values passed in were different than the most recent record, then insert a new record.
          var insertSQL = "INSERT INTO glassdoor_company_data (" + insertColumns.join(', ') + ") VALUES (" + insertValues.join(', ') + ");";
          sequelize.query(insertSQL)
            .then(function (results) {
            }).catch(function (e) {
              console.log(e);
            });
        }
        else {
          // Otherwise, the values passed in were the same as the most recent records, and we simply update the updated_at field.
          var updateSQL = "UPDATE glassdoor_company_data SET " + updateSetters.join(', ') + ", updated_at = now() WHERE created_at::DATE = now()::DATE AND company_name = " + companyName + ";";
          sequelize.query(updateSQL)
            .then(function (results) {
            }).catch(function (e) {
              console.log(e);
            });
        }
      }).catch(function (e) {
        console.log(e);
      });
  };

  var glassdoorAPICall = function() {
    callAPI('get',
      'http://api.glassdoor.com/api/api.htm',
      {
        "v" : 1,
        "format": "json",
        "t.p": 42801,
        "t.k": "fZSPeGDMTmm",
        "action": "employers",
        "q": "CSRA"
      }, function(data) {
        if(_.isUndefined(data.error)) {
          var cscRecord = _.find(data.data.response.employers, {"exactMatch": true});
          if(!_.isUndefined(cscRecord)) {
            insertGlassdoorRecord(cscRecord);
          }
        }
      });
  };

  var sendPMRreminderEmails = function() {
    if (!moment.utc().isSame(config.pmrReminderEmailSendDate(), 'day')) {
      console.log("sendPMRreminderEmails: Today(" + moment.utc().toDate() + ") is " +
        "not the day to send PMR reminder emails(" + config.pmrReminderEmailSendDate().toDate() +
        "), so none will be sent.");
      return;
    }

    var fiscalEnd = moment.utc().subtract(1, 'months').endOf('month').date(1).format('YYYY-MM-DD');

    /* Find outstanding PMRs for Programs with:
     1) Validated metadata, but open & unvalidated current fiscal month heatmap
     2) Validated metadata, but no current fiscal month heatmap -> get the last heatmap entry's updated user
     3) Validated metadata, but no heatmaps
      */
    function getIncompleteHeatmapsQuery() {
      var openUnvalidatedHeatmapQuery =
        "SELECT program_metadata_manual_entry.program_id, programs_secure.project_id, program_heatmap_manual_entry.updated_by, users.email " +
        "FROM programs_secure " +
        "INNER JOIN (SELECT * from program_heatmap_manual_entry " +
        "WHERE date_trunc('month', fiscal_month_end) = date_trunc('month', to_date('" + fiscalEnd + "', 'YYYY-MM-DD'))) AS program_heatmap_manual_entry " +
        "ON programs_secure.id = program_heatmap_manual_entry.program_id " +
        "INNER JOIN users " +
        "ON users.id = program_heatmap_manual_entry.updated_by " +
        "INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) program_id, id " +
        "FROM program_metadata_manual_entry " +
        "WHERE program_metadata_manual_entry.validated is true " +
        "ORDER BY program_metadata_manual_entry.program_id, program_metadata_manual_entry.created_at DESC) AS program_metadata_manual_entry " +
        "ON program_metadata_manual_entry.program_id = programs_secure.id " +
        "WHERE programs_secure.id = program_metadata_manual_entry.program_id " +
        "AND (program_heatmap_manual_entry.validated is null or program_heatmap_manual_entry.validated = false)";

      var previousHeatmapQuery =
        "SELECT program_metadata_manual_entry.program_id, programs_secure.project_id, program_heatmap_manual_entry.updated_by, users.email " +
        "FROM programs_secure " +
        "INNER JOIN (SELECT DISTINCT ON (program_heatmap_manual_entry.program_id) * " +
        "FROM program_heatmap_manual_entry " +
        "ORDER BY program_heatmap_manual_entry.program_id, program_heatmap_manual_entry.fiscal_month_end DESC) AS program_heatmap_manual_entry " +
        "ON programs_secure.id = program_heatmap_manual_entry.program_id " +
        "INNER JOIN users " +
        "ON users.id = program_heatmap_manual_entry.updated_by " +
        "INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) program_id, id " +
        "FROM program_metadata_manual_entry " +
        "WHERE program_metadata_manual_entry.validated is true " +
        "ORDER BY program_metadata_manual_entry.program_id, program_metadata_manual_entry.created_at DESC) AS program_metadata_manual_entry " +
        "ON program_metadata_manual_entry.program_id = programs_secure.id " +
        "WHERE programs_secure.id = program_metadata_manual_entry.program_id " +
        "AND date_trunc('month', fiscal_month_end) < date_trunc('month', to_date('" + fiscalEnd + "', 'YYYY-MM-DD'))";

      var metadataOnlyQuery =
        "SELECT program_metadata_manual_entry.program_id, programs_secure.project_id, COALESCE(program_metadata_manual_entry.updated_by, program_metadata_manual_entry.created_by) AS updated_by, users.email " +
        "FROM programs_secure " +
        "INNER JOIN (SELECT DISTINCT ON (program_metadata_manual_entry.program_id) program_id, id, updated_by, created_by " +
        "FROM program_metadata_manual_entry " +
        "WHERE program_metadata_manual_entry.validated is true " +
        "ORDER BY program_metadata_manual_entry.program_id, program_metadata_manual_entry.created_at DESC) AS program_metadata_manual_entry " +
        "ON program_metadata_manual_entry.program_id = programs_secure.id " +
        "INNER JOIN users " +
        "ON users.id = COALESCE(program_metadata_manual_entry.updated_by, program_metadata_manual_entry.created_by) " +
        "WHERE programs_secure.id = program_metadata_manual_entry.program_id " +
        "AND NOT EXISTS (SELECT 1 " +
        "FROM program_heatmap_manual_entry " +
        "WHERE program_heatmap_manual_entry.program_id = program_metadata_manual_entry.program_id)";

      return openUnvalidatedHeatmapQuery + " UNION " + previousHeatmapQuery + " UNION " + metadataOnlyQuery;
    }

    sequelize.transaction(function (t) {
      return sequelize.query("SET LOCAL config.user_id = 1;", {transaction: t}).then(function () {
        return sequelize.query(getIncompleteHeatmapsQuery(), {transaction: t})
      }).catch(function (err) {
        return console.error("sendPMRreminderEmails: Error while running getIncompleteHeatmapsQuery, " + err);
      })
    }).then(function (results) {
      if (!results) {
        return console.error("sendPMRreminderEmails: No getIncompleteHeatmapsQuery results found.");
      }
      var userProgramIds = {};

      // Parse the results. The query result array is in the first element of the "results" array.
      _.each(results[0], function(result) {
        if (userProgramIds[result.email] === undefined) {
          userProgramIds[result.email] = [];
        }

        userProgramIds[result.email].push(result.project_id.toUpperCase());
      });

      if (!_.isEmpty(userProgramIds)) {
        console.log("sendPMRreminderEmails: userProgramIds to send PMR reminder emails to: " + JSON.stringify(userProgramIds));
        emails.sendPMRreminderEmail(userProgramIds, null, function(err) {
          if (err) {
            console.error("sendPMRreminderEmails: Error while sending PMR reminder emails, " + err);
          } else {
            console.log("sendPMRreminderEmails: Emails sent.");
          }
        });
      } else {
        console.log("sendPMRreminderEmails: userProgramIds is empty - there are no reminders to send.");
      }
    }).catch(function (err) {
      console.error("sendPMRreminderEmails: Error while attempting to send PMR reminder emails, " + err);
    });
  };

  // Now, on server start
  // PMR Reminder Emails
  if (config.pmrReminderEmailsEnabled) {
    var scheduled_PMRreminderEmails = new schedule.scheduleJob(config.pmrReminderEmailCronSchedule, function () {
      console.log("Executing scheduled task: scheduled_PMRreminderEmails");
      sendPMRreminderEmails();
    });
  }

  // Loads Glassdoor data into database
  var serverStart_GlassdoorDataLoad = schedule.scheduleJob(new Date(Date.now()), function() {
    console.log("Executing scheduled task: serverStart_GlassdoorDataLoad");
    glassdoorAPICall();
  });

  // Each day at midnight
  // Loads Glassdoor data into database
  var scheduled_GlassdoorDataLoad = new schedule.scheduleJob(config.glassDoorDataCronSchedule, function () {
    console.log("Executing scheduled task: scheduled_GlassdoorDataLoad");
    glassdoorAPICall();
  });

  // Each day at 12:30AM run the matviews
  var scheduled_RefreshMatViews = new schedule.scheduleJob(config.refreshMatViewsCronSchedule, refreshMaterializedViews);

};
