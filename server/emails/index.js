var kue = require('kue');
var path = require('path');
var nodemailer = require('nodemailer');
var templatesDir = path.resolve(__dirname, 'templates');
var emailTemplates = require('email-templates').EmailTemplate;
var async = require('async');
var _ = require('lodash');
var models = require('../models');
var User = models.User;
var dbConfig = require(__dirname + '/../config/database.json')[process.env.NODE_ENV];
var mConfig = require(__dirname + '/../config/mailer.json')[process.env.NODE_ENV];
var config = require(__dirname + '/../config/environment');
var sequelize = require('sequelize');
var json2csv = require('json2csv');
var fs = require('fs');

var jobs = kue.createQueue({redis: dbConfig.redis, prefix: config.redisOpts.prefix});

jobs.on( 'error', function( err ) {
  console.error( '\n\nKUE ERROR: ', err, "\n\n");
});

var tConfig = _.omit({
  service: mConfig.service,
  auth: _.omit({
    user: mConfig.username,
    pass: mConfig.password
  }, _.isUndefined),
  host: mConfig.host,
  port: mConfig.port,
  tls: {
    rejectUnauthorized: false
  },
  pool: true
}, _.isEmpty);

var transporter = nodemailer.createTransport(tConfig);
var sent = [];
var length;
var attempt;


// Render used by email-templates for bulk emails
var Render = function (data, locals) {
  this.send = function (err, html, text) {
    if (err) {
      console.error(err);
    } else {
      sendEmailJob(data.from, locals.email, data.subject, null, locals, text, html);
    }
  };
  this.batch = function (batch) {
    batch(locals, templatesDir, this.send);
  };
};


var _sendEmail = function (data, done) {
  var sendIt = function () {
    if (config.testEmail) {
      console.log("\n\n========= ", data.to, " =============", "\n\n============= BEGIN TEST EMAIL CONTENT =============\n\n", data.html,
        '\n\n============= TEXT =============\n\n', data.text, "\n\n============= END TEST EMAIL CONTENT =============\n\n");
      done();
    } else {
      console.log("\n\nINSIGHT - INSIDE sendIt, ABOUT TO SEND!!\n\n");
      transporter.verify(function (error, success) {
        if (error) {
          console.log("\n\n\n\n\nVERIFY ERROR: ", error, "\n\n\n\n\n\n");
        } else {
          console.log('\n\n\n\n\n\n\n\nVERIFY: Server is ready to take our messages: ', success, "\n\n\n\n\n\n\n\n");
        }

        transporter.sendMail(data, function (err) {
          attempt++;
          if (err) {
            console.log("NODEMAILER FAILED TO SEND TO ", data.to);
            console.log(err);
            return done(err);
          } else {
            console.log("SENDING to ", data.to);

            sent.push({"email": data.to});

            return done();

            if (attempt == length) {
              console.log("INSIGHT - SENT: ", sent);
            } else {
              done(err);
            }
          }
          done(err);
        });
      });
    }
  };

  // Get template if one is specified and it already hasn't been rendered
  if (!_.isEmpty(data.template) && !_.isEmpty(data.locals) && _.isEmpty(data.html) && _.isEmpty(data.text)) {

    var template = new emailTemplates(path.join(templatesDir, data.template));
    template.render(data.locals, function (err, results) {
      if (err) {
        console.log(err);
        done(err);
      } else {
        data.html = results.html;
        data.text = results.text;
        sendIt();
      }
    });

  } else {
    console.log("\n\nINSIGHT - BEFORE SENDIT\n\n");
    sendIt(data, done);
  }
};

var _sendBulkEmail = function (data, done) {

  console.log("\nSENDBULKEMAIL JOB CREATED\n");

  var template = new emailTemplates(path.join(templatesDir, data.template));
  var locals = _.merge({}, data.locals);

  console.log("\n\n===== MAILING LIST =====\n\n");
  _.each(data.users, function (user) {
    console.log(user.email);
  });


  if(config.env === 'production' && !config.bulkEmailRecipients) {
    async.eachSeries(data.users, function(user, callback) {
      template.render(locals, function(err, results) {
        if(err) return done(err);
        sendEmailJob(data.from, user.email, data.subject, data.template, locals, results.text, results.html, done);
        callback();
      }, function (err) {
        done(err);
      });
    });
  } else {
    var emails = config.bulkEmailRecipients;

    async.eachSeries(emails, function (email, callback) {
      template.render(locals, function (err, results) {
        if (err) return done(err);
        sendEmailJob(data.from, email, data.subject, data.template, locals, results.text, results.html, done);
        callback();
      }, function (err) {
        done(err);
      });
    });
  }
};

var sendEmailJob = function (from, to, subject, template, locals, text, html, callback) {
  var metadata = {
    from: from,
    to: to,
    subject: subject,
    template: template,
    locals: locals,
    text: text,
    html: html
  };

  // Set local constants
  metadata.locals.domain = config.domain;

  console.log("\n\nINSIGHT: ABOUT TO SEND EMAIL VIA _sendEmail\n\n");

  if (mConfig.bypassRedis) {
    console.log("\n\nBYPASSING REDIS!!\n\n");
    _sendEmail(metadata, callback);
  } else {
    console.log("\n\nUSING REDIS!!\n\n");
    jobs.create('send email', metadata)
      .priority('medium')
      .removeOnComplete(true)
      .save(function (err) {
        callback && callback(err);
      });
  }
};

var sendBulkEmailJob = function (from, subject, template, users, delay, locals, callback) {

  console.log("\nIN SEND BULK EMAIL JOB\n");

  if(config.bulkEmailRecipients) {
    users = _.map(config.bulkEmailRecipients, function(email) {
      return {email: email};
    });
  }

  async.each(users, function (user, cb) {

    var metadata = {
      from: from,
      to: user.email,
      subject: subject,
      template: template,
      locals: locals,
      text: null,
      html: null
    };
    // Set local constants
    metadata.locals.domain = config.domain;

    if (mConfig.bypassRedis) {
      console.log("BYPASSING REDIS");
      // _sendBulkEmail(metadata, callback);
      // sendEmailJob(from, user.email, subject, template, locals, null, null, cb);
      _sendEmail(metadata, cb);
    } else {
      var job = jobs.create('send bulk email', metadata).priority('low').removeOnComplete(true);

      if (delay) {
        job.delay(delay);
      }

      job.save(function (err) {
        cb && cb(err);
      });
    }

  }, callback);
};

//Need to re-create jobs due to issue 514 (https://github.com/Automattic/kue/issues/514)
async.waterfall([
  function (callback) {
    var states = ['inactive', 'active'];

    async.each(states, function(state, cb) {
      console.log("\n\n\n\nRECREATING JOBS FOR STATE: ", state);
      kue.Job.rangeByState(state, 0, 5000, 'asc', function(err, queue) {
        console.log("\n\n\n\n", state, " QUEUE: ", queue, "\n\n\n\n\n");
        queue.forEach(function(job) {
          job.complete();
          jobs.create(job.type, job.data).priority('medium').removeOnComplete(true).save();
        });
        console.log("\n\n\n\n", state, " CALLBACK\n\n\n\n");
        cb && cb();
      })}, function(err) {
        if(err) {
          console.error("\n\nKUE JOB RECREATION ERROR: ", err);
        }
        console.log("\n\n\n\n\n\n\n\n\nCALLBACK\n\n\n\n\n\n\n\n\n");
        callback && callback();
      })
  },
  function (callback) {
    console.log("\n\n\n\nPROCESSING SEND EMAIL\n\n\n\n")
    jobs.process('send email', 20, function (job, done) {
      // console.log("\n\n\n\nALL JOBS: ", jobs, "\n\n\n\n");
      console.log("\n\nJOBS PROCESS SEND EMAIL: ", job, "\n\n");
      _sendEmail(job.data, done);
    });
    callback && callback();
  },
  function (callback) {
    console.log("\n\n\n\nPROCESSING BULK EMAIL\n\n\n\n");
    jobs.process('send bulk email', 20, function (job, done) {
      console.log("\n\nJOBS PROCESS SEND BULK EMAIL: ", job, "\n\n");
      // _sendBulkEmail(job.data, done);
      _sendEmail(job.data, done);
    });
    callback && callback();
  },
  function (callback) {
    console.log("\n\n\n\nWATCH STUCK JOBS\n\n\n\n");
    jobs.watchStuckJobs(1000); //look for stuck jobs every 30 seconds
    callback && callback();
  }
]);


/**
 * Send basic email to one user using the users info as the local template variables.
 *
 * @param template
 * @param from
 * @param subject
 * @param userId
 */
exports.sendEmail = function (template, from, subject, userId) {
  User.findById(userId).then(function (user) {
    var locals = {};
    locals.user = user;
    sendEmailJob(from, user.email, subject, template, locals);
  }).catch(function (err) {
    console.log(err);
  });
};

/**
 * Send an email to multiple users
 *
 * @param template
 * @param from
 * @param subject
 * @param userIds
 * @param delay
 */
exports.sendBulkEmails = function (template, from, subject, userIds, delay) {
  sendBulkEmailJob(from, subject, template, userIds, delay);
};

/**
 * Send out a reset password link to user
 */
exports.passwordReset = function (user, token, hostname, callback) {
  var protocol = process.env.NODE_ENV === 'development' ? 'http' : 'https';
  var locals = {
    user: user,
    resetUrl: protocol + '://' + hostname + '/password/reset/' + token
  };
  sendEmailJob(mConfig.from, user.email, 'Insight Password Reset', 'password-reset', locals, null, null, callback);
};

/**
 * Send out new user registration email
 */
exports.sendNewUserAdminNotificationEmail = function (fromUser, callback) {
  console.log("\n\nINSIGHT: BEGINNING OF emails/index/sendNewUserAdminNotificationEmail - \n", fromUser, "\n\n");

  var locals = {
    fromUser: fromUser
  };

  if (_.isEmpty(config.adminEmails)) {
    return callback("There are no adminEmails defined.");
  }

  _.forEach(config.adminEmails, function (toEmail) {
    console.log("\n\nINSIGHT: TO EMAIL LOOP - ", toEmail, "\n\n");
    sendEmailJob(mConfig.from, toEmail, 'Insight - New User Registration', 'new-user', locals, null, null, callback);
  });
};

/**
 * Send out PMR feedback email
 */
exports.sendFeedbackEmail = function (fromUser, feedbackSubject, feedbackText, callback) {
  var locals = {
    fromUser: fromUser,
    feedbackSubject: feedbackSubject + " - Insight Feedback",
    feedbackText: feedbackText
  };

  if (_.isEmpty(config.adminEmails)) {
    return callback("There are no adminEmails defined.");
  }

  _.forEach(config.adminEmails, function (toEmail) {
    sendEmailJob(mConfig.from, toEmail, locals.feedbackSubject, 'pmr-feedback', locals, null, null, callback);
  });
};

/**
 * Send out a PMR reminder email to user
 */
exports.sendPMRreminderEmail = function (userProgramIds, hostname, callback) {
  _.forOwn(userProgramIds, function (value, key) {
    var locals = {
      email: key,
      programIds: value,
      pmrUrl: 'http://' + hostname + '/program'
    };
    sendEmailJob(mConfig.from, key, 'PMR Reminder', 'pmr-reminder', locals, null, null, callback);
  });
};

/**
 * Send out a permission change notification to the user
 */
exports.sendUserPermissionChangeNotificationEmail = function (user, permissionChanges, callback) {
  var locals = {
    user: user,
    permissionChanges: permissionChanges
  };
  sendEmailJob(mConfig.from, user.email, 'PMR User Permission Change Notification', 'pmr-permission-notification', locals, null, null, callback);
};

/**
 * Send out a validation link to user
 */
exports.validateUser = function (user, token, hostname) {
  var locals = {
    user: user,
    validateUrl: 'https://' + hostname + '/validate/' + token
  };
  sendEmailJob(mConfig.from, user.email, 'Insight Email Validation', 'validate-user', locals);
};

exports.sendEmailAnnouncement = function (subject, body, PMR, callback) {
  console.log("\nIN sendEmailAnnouncement\n");

  var locals = {
    body: body,
    subject: subject
  };

  sent = [];
  attempt = 0;

  if (!PMR) {

    User.findAll({order: 'name'}).then(function (users) {
      length = users.length;
      console.log("\nSUCCESSFULLY FOUND ALL USER OBJECTS\n");
      sendBulkEmailJob(mConfig.from, subject, 'announcement', users, null, locals, callback);
    }).catch(function (err) {
      console.log("\nERROR FINDING USER OBJECTS: ", err, "\n");
      console.log(err);
    });

  } else {
    var usersPMR = [];
    User.findAll({order: 'name'}).then(function (users) {
      _.forEach(users, function (user) {
        if (user != null) {
          var found = false;
          _.forEach(user.groups, function (group) {
            if (group == "pmr-reporting") {
              found = true;
            }
          });
          if (found) {
            usersPMR.push(user);
          }
        }
      });
      length = usersPMR.length;

      //sendEmailJob(mConfig.from, usersPMR[1].email, subject, 'announcement', locals, null, null, callback);

      sendBulkEmailJob(mConfig.from, subject, 'announcement', usersPMR, null, locals, callback);
    }).catch(function (err) {
      console.log(err);
    });
  }
};
