'use strict';

var path = require('path');
var _ = require('lodash');
var moment = require('moment');
var log4js = require('log4js');

log4js.replaceConsole();

function requiredProcessEnv (name) {
  if(!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../..'),

  // Server port
  port: process.env.PORT || 9000,

  // Should we populate the DB with sample data?
  seedDB: false,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: 'YYCskv85LD9Eg0bptM8ym1pTw930T72u'
  },

  // List of user titles
  userTitles: ['guest', 'user', 'admin'],

  // MongoDB connection options
  mongo: {
    options: {
      db: {
        safe: true
      }
    }
  },

  redisOpts: {
    prefix: 'insight-' + process.env.NODE_ENV
  },

  glassDoorDataCronSchedule: process.env.GLASSDOOR_DATA_CRON || '0 0 0 * * *', // 12:00:00 AM every day

  refreshMatViewsCronSchedule: process.env.REFRESH_MAT_VEIWS_CRON || '0 30 0 * * *', // 12:30:00 AM every day

  // List of email addresses to send PMR feedback to.
  // production.js will have its own emails.
  adminEmails: process.env.ADMIN_EMAILS ? JSON.parse(process.env.ADMIN_EMAILS) : [],

  // PMR Reminder email schedule - what time of day & what date to send them.
  pmrReminderEmailsEnabled: process.env.PMR_REMINDER_EMAILS_ENABLED === 'true', // Set to false by default, to not spam email users accidentally. Set it to true to turn on the schedule.

  pmrReminderEmailCronSchedule: process.env.PMR_REMINDER_EMAILS_CRON || new Date(), // Run at startup by default

  pmrReminderEmailSendDate: function() {
    return moment().utc();  // today, so it will run right away
  },

  deckGroups: process.env.INSIGHT_DECK_GROUPS ? JSON.parse(process.env.INSIGHT_DECK_GROUPS) :
    ["nes-reporting", "ppmc-reporting", "mission-services", "staffing", "pmr-reporting"],

  newrelic: {
    enabled: process.env.NEW_RELIC_ENABLED === 'true' || false,
    key: process.env.NEW_RELIC_KEY || '',
    appNames: [process.env.NEW_RELIC_APP_NAME || 'Insight-Develop'],
    logging: {
      level: process.env.NEW_RELIC_LOG || 'info'
    }
  },

  domain: process.env.DOMAIN_NAME || 'csra.com',

  redirectUrl: process.env.REDIRECT_URL || false,

  disableDatabases: !!process.env.REDIRECT_URL || process.env.MAINTENANCE_MODE === "true",

  okta: {
    enabled: process.env.OKTA_ENABLED === 'true' || false,
    callbackUrl: process.env.OKTA_CALLBACK_URL,
    entryPoint: process.env.OKTA_ENTRY_POINT,
    issuer: process.env.OKTA_ISSUER
  },

  salesforceUrl: process.env.SALESFORCE_URL || 'https://csra.lightning.force.com/one/one.app#/alohaRedirect/apex', //this will change later

  bulkEmailRecipients: process.env.BULK_EMAIL_RECIPIENTS ? JSON.parse(process.env.BULK_EMAIL_RECIPIENTS) : []
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});
