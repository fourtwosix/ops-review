'use strict';

// Development specific configuration
// ==================================
module.exports = {
  seedDB: true,
  testEmail: true
};
