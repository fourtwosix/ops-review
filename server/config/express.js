/**
 * Express configuration
 */

'use strict';

var express = require('express');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var path = require('path');
var config = require('./environment');
var passport = require('passport');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var dbConfig    = require(__dirname + '/database.json')[process.env.NODE_ENV];

module.exports = function(app) {
  var env = app.get('env');

  var viewFolder = (('development' === env || 'test' === env) ? '/client' : '/public');
  app.set('views', [config.root + viewFolder, config.root + '/server/views']);
  app.engine('html', require('ejs').__express);
  app.set('view engine', 'html');
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());

  if (!config.disableDatabases) {
    app.use(session({
      store: new RedisStore({
        url: dbConfig.redis,
        ttl: 60 * 60 * 5 // 5 hours
      }),
      secret: config.secrets.session
    }));
  }

  app.use(passport.initialize());
  app.use(passport.session());

  if ('production' === env || 'staging' === env) {
    app.use(favicon(path.join(config.root, 'public', 'favicon.ico')));
    app.use(express.static(path.join(config.root, 'public')));
    app.set('appPath', config.root + '/public');
    app.use(morgan('dev'));
  }

  if ('development' === env || 'test' === env) {
    app.use(require('connect-livereload')());
    app.use(express.static(path.join(config.root, '.tmp')));
    app.use(express.static(path.join(config.root, 'client')));
    app.set('appPath', 'client');
    app.use(morgan('dev'));
    app.use(errorHandler()); // Error handler - has to be last
  }
};
