'use strict';

var models = require('../../models');
var moment = require('moment');
var Promise = require("bluebird");

module.exports = {
  seedUsers: function () {
    var User = models.User;
    var UserRolesAssociation = models.UserRolesAssociation;
    return Promise.join(
      User.findOrCreate({
        where: {
          name: "Test PMR User",
          email: "pmr-tester@42six.com",
          title: "user",
          provider: "local",
          "groups": ['pmr-reporting']
        }, defaults: {password: "_42six"}
      }),
      User.findOrCreate({
        where: {
          "name": "Test OPS User",
          "email": "ops-tester@42six.com",
          "title": "user",
          "provider": "local",
          "groups": ['nes-reporting']
        }, defaults: {password: "_42six"}
      }),
      User.findOrCreate({
        where: {
          "name": "Admin",
          "email": "admin@admin.com",
          "title": "admin",
          "provider": "local"
        }, defaults: {password: "admin"}
      })
        .spread(function(user) {
          UserRolesAssociation.findOrCreate({
            where: {
              "user_id": user.id,
              "role_name": "Administrator"
            }
          });
        })
    );
  }
};
