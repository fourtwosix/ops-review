#!/usr/bin/env bash

DBUSER=insighter
DATABASE="insight_development"
DBHOST=db-insight

read -s -p "Postgres Password For $DBUSER@$DBHOST: " PGPASSWORD

export PGPASSWORD

declare -a arr=(
  "\copy (SELECT * FROM candidate_data ORDER BY report_date DESC LIMIT 50000) to candidate_data.csv With CSV"
  "\copy (SELECT * FROM contract_data) to contract_data.csv With CSV"
  "\copy (SELECT * FROM costpoint_data ORDER BY report_date DESC LIMIT 50000) to costpoint_data.csv With CSV"
  "\copy (SELECT * FROM labor_utilization_data ORDER BY report_date DESC LIMIT 50000) to labor_utilization_data.csv With CSV"
  "\copy (SELECT * FROM permast_data ORDER BY report_date DESC LIMIT 50000) to permast_data.csv With CSV"
  "\copy (SELECT * FROM programs) to program.csv With CSV"
  "\copy (SELECT * FROM project_data ORDER BY report_date DESC LIMIT 50000) to project_data.csv With CSV"
  "\copy (SELECT * FROM resource_management_data ORDER BY report_date DESC LIMIT 50000) to resource_management_data.csv With CSV"
  "\copy (SELECT * FROM steps_data ORDER BY report_date DESC LIMIT 50000) to steps_data.csv With CSV"
  "\copy (SELECT * FROM subcontracts_data ORDER BY report_date DESC LIMIT 50000) to subcontracts_data.csv With CSV"
)

for i in "${arr[@]}"
do
  echo "$i"
  psql -d $DATABASE -t -A -F"," -c "$i" --user $DBUSER -h $DBHOST
done

