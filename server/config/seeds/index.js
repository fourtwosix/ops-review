'use strict';

var fs = require('fs');
var path = require('path');
var models = require('../../models');
var sequelize = models.sequelize;
var change_case = require('change-case');
var Promise = require("bluebird");

module.exports = {
  seedData: function() {

    function seedCsvData(modelName, filepath) {
      console.log("seedCsvData: ",modelName, filepath);
      fs.chmodSync(filepath, '755');
      return models.sequelize.query("copy " + models[modelName].tableName + " from ? with CSV", {
        replacements: [filepath]
      });
    }

    function getModelName(filename) {
      return change_case.upperCaseFirst(change_case.camel(filename.substring(0, filename.indexOf('.'))));
    }

    function seedFile(filename) {
      var filetype = filename.substring(filename.lastIndexOf('.') + 1);
      if (filetype !== 'csv') {
        return Promise.resolve();
      }

      return new Promise(function (resolve, reject) {
        var modelName = getModelName(filename);

        // check if data exists for this model (only seed data if this model has no data)
        models[modelName].findAll({limit: 1}).then(function (data) {
          if (data.length === 0) {
            var filepath = path.join(__dirname, filename);
            seedCsvData(modelName, filepath).then(resolve).catch(reject);
          } else {
            console.log("Skipping CSV seed data for " + modelName);
            resolve();
          }
        }).catch(reject);
      });
    }

    var seedFiles = Promise.map(fs.readdirSync(__dirname), seedFile, {
      concurrency:1
    });

    //seed js data (using sequelize in a more manual fashion)
    var seedUsers = require('./seeds.js').seedUsers();

    return new Promise(function (resolve, reject) {
      Promise.all([seedUsers,seedFiles]).then(function(){
        sequelize.query("select RefreshAllMaterializedViews()", {type: sequelize.QueryTypes.SELECT})
          .then(resolve)
          .catch(reject);
      }).catch(reject);
    });
  }
};



