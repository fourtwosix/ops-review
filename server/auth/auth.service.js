'use strict';

var config = require('../config/environment');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var compose = require('composable-middleware');
var models = require('../models');
var sequelize = models.sequelize;
var User = models.User;
var validateJwt = expressJwt({ secret: config.secrets.session });
var async = require('async');
var _ = require('lodash');
var moment = require('moment');


function getToken(req) {
  if (req.query && req.query.hasOwnProperty('access_token')) {
    return req.query.access_token;
  } else if (req.cookies.token) {
    return req.cookies.token.substring(1, req.cookies.token.length-1);
  }
}

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated() {
  return compose()
  // Validate jwt
  .use(function(req, res, next) {
    // If using okta use embedded isAuthenticated() function
    if (config.okta.enabled) {
      if (req.isAuthenticated()) {
        next();
      } else {
        var error = new Error('Invalid Okta Session');
        error.status = 401;
        error.name = "UnauthorizedError";
        next(error);
      }

      // If using local auth verify the existence of a valid jwt token
    } else {
      var token = getToken(req);

      if (token) {
        req.headers.authorization = 'Bearer ' + token;
      }

      validateJwt(req, res, next);
    }
  })
  // Refresh token
  .use(function (jwtErr, req, res, next) {
    if (config.okta.enabled) return next(jwtErr);

    if (jwtErr.message === 'jwt expired') {

      // If this an expired token issued within the last 14 days, refresh the token
      // if the user still exists
      var token = getToken(req);

      if (token) {
        jwt.verify(token, config.secrets.session, {ignoreExpiration: true}, function (err, profile) {
          if (moment(profile.iat * 1000).isBefore(moment().subtract(14, 'days'))) {
            return next(jwtErr);
          }

          // Ensure that user still exists
          User.findById(profile.id).then(function (user) {
            if (!user) return res.send(401);
            var token = signToken(user.id);

            res.cookie('token', JSON.stringify(token));
            req.user = user;
            next();
          }).catch(function () {
            next(jwtErr);
          });
        });
      } else {
        next(jwtErr);
      }
    } else {
      next(jwtErr);
    }
  })
  // Attach user to request
  .use(function(req, res, next) {
    sequelize.transaction(function (t) {
      return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
        return User.findById(req.user.id,
        {include: [{model: models.Roles, as: 'roles'}, {model: models.Program, as: 'programs'}, {model: models.Auths, as: 'auths'}],
          transaction: t});
      }).catch(function(err) {
        next(err);
      });
    }).then(function(user) {
      if (!user) return res.send(401);

      req.user = user;
      next();
    }).catch(function(err) {
      next(err);
    });
  });
}


var checkRolesArray = function(user, roleName) {
  return _.includes(_.pluck(user.roles, 'name'), roleName);
};

var hasProgram = function(user, projectId) {
  return _.includes(_.pluck(user.programs, 'project_id'), projectId.toLowerCase());
};

/**
 * Checks if the user role meets the minimum requirements of the route
 */
function hasRole(roleRequired) {
  if (!roleRequired) throw new Error('Required role needs to be set');

  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements(req, res, next) {
      if (checkRolesArray(req.user.toJSON(), roleRequired)) {
        next();
      } else {
        res.send(403);
      }
    });
}

function isUserAllowedToEditProgram() {
  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements(req, res, next) {
      if (checkRolesArray(req.user.toJSON(), 'Administrator') || (
          checkRolesArray(req.user.toJSON(), 'Program Manager') && hasProgram(req.user.toJSON(), req.body.project_id)) || (
        checkRolesArray(req.user.toJSON(), 'Executive') && hasProgram(req.user.toJSON(), req.body.project_id))) {
        next();
      } else {
        res.send(403);
      }
    });
}

/**
 * Returns a jwt token signed by the app secret
 */
function signToken(id) {
  return jwt.sign({ id: id }, config.secrets.session, { expiresInMinutes: 60*5 });
}

/**
 * Set token cookie directly for oAuth strategies
 */
function setTokenCookie(req, res) {
  if (!req.user) return res.json(404, { message: 'Something went wrong, please try again.'});
  var token = signToken(req.user.id, req.user.title);
  res.cookie('token', JSON.stringify(token));
  res.redirect('/');
}

exports.isAuthenticated = isAuthenticated;
exports.hasRole = hasRole;
exports.checkRolesArray = checkRolesArray;
exports.isUserAllowedToEditProgram = isUserAllowedToEditProgram;
exports.signToken = signToken;
exports.setTokenCookie = setTokenCookie;
