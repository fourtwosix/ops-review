'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');

var router = express.Router();

router
.get('/', passport.authenticate('saml', {
  failureRedirect: '/',
  failureFlash: true
}),
function (req, res) {
  res.redirect('/');
})

.post('/callback', passport.authenticate('saml', {
  failureRedirect: '/',
  failureFlash: true
}),
function (req, res) {
  var url = JSON.parse(req.cookies.redirectUrl || '"/"');
  res.cookie('redirectUrl', JSON.stringify('/'));
  res.cookie('token', JSON.stringify("okta"));
  res.redirect(url || '/');
});

module.exports = router;
