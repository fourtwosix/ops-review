var passport = require('passport');
var SamlStrategy = require('passport-saml').Strategy;
var _ = require('lodash');
var emails = require('../../emails');

exports.setup = function (User, config) {
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id).then(function (user) {
      done(null, user);
    }).catch(done);
  });

  passport.use(new SamlStrategy({
    callbackUrl: config.okta.callbackUrl,
    entryPoint: config.okta.entryPoint,
    issuer: config.okta.issuer
  },
  function (profile, done) {
    // TODO employeeID will be stored in profile.nameID

    console.log("\n\nINSIGHT - RECEIVED OKTA PROFILE: ", profile, "\n\n");
    var email = profile.emailaddress.toLowerCase();
    var name = (profile.FirstName + ' ' + profile.LastName).trim();

    if (!email) return done(new Error("No email provided."));

    User.findOne({where: {email: email}}).then(function (user) {
      if (!user) {
        user = User.build({
          email: email,
          name: name,
          provider: 'okta',
          receives_email: null
        });

        console.log("\n\nINSIGHT - FOUND USER: ", user, "\n\n");

        user.save().then(function (user) {
          console.log("\n\nINSIGHT - SAVED USER: ", user, "\n\n");
          // Assume if user is validating through CSRA okta they are valid CSRA users
          user.setRoles(_.union(user.roles, ['CSRA Validated'])).then(function () {
            console.log("\n\nINSIGHT - USER ROLES SET\n\n");
            done(null, user);
          })
        }).catch(done);
      } else {
        done(null, user);
      }
    }).catch(done);
  }));
};
