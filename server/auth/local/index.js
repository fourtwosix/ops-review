'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');
var crypto = require('crypto');
var async = require('async');
var models = require('../../models');
var RequestToken = models.RequestToken;
var User = models.User;
var emails = require('../../emails');
var _ = require('lodash');

var router = express.Router();

router.post('/', function(req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    var error = err || info;
    if (error) return res.json(401, error);
    if (!user) return res.json(404, {message: 'Something went wrong, please try again.'});

    var token = auth.signToken(user.id, user.title);
    res.json({token: token});
  })(req, res, next)
});

router.post('/forgot', function (req, res) {
  var email = (req.body.email || "").toLowerCase();

  async.waterfall([
    function (done) {
      crypto.randomBytes(20, function (err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function (token, done) {
      User.findOne({where: {email: email}}).then(function (user) {
        if (!user) {
          return done({message: 'No account with that email address exists.'});
        }

        RequestToken.createToken(user, token, 'password-reset').then(function () {
          done(null, token, user);
        }).catch(done);

      }).catch(function () {
        done(err);
      });
    },
    function (token, user, done) {
      emails.passwordReset(user, token, req.headers.host);
      done();
    }
  ], function (err) {
    if (err) {
      console.error(err);
      return res.send(500, err);
    }

    res.send(200);
  });
});

router.get('/reset/:token?',  function (req, res) {
  var token = req.params.token;

  RequestToken.getToken(token, 'password-reset').then(function (request) {
    if (request) {
      User.findById(request.user_id, {attributes: ['name', 'email', 'title', 'provider', 'id', 'groups', 'pmr_group', 'divisions'],
        include: [{model: models.Roles, as: 'roles'}, {model: models.Auths, as: 'auths'}]}).then(function (user) {
        if (!user) {
          return res.send(500, 'User not found.');
        }
        res.json(user);
      }).catch(function (err) {
        console.error(err);
        res.send(500, 'Error retrieving user.');
      });
    } else {
      res.send(500, 'Reset token not found. Please try sending a new email.')
    }
  });
});

router.post('/reset/:token', function (req, res) {
  var token = req.params.token;
  var user = req.body;
  var newPass = String(req.body.newPassword);

  RequestToken.getToken(token, 'password-reset').then(function (request) {
    if (request) {
      User.findById(user.id).then(function (user) {
        if (!user) {
          return res.send(500, 'User not found.');
        }
        user.password = newPass;
        user.save().then(function () {
          RequestToken.cleanUpTokens(user, 'password-reset').then(function () {
            res.send(200);
          });
        }).catch(function (err) {
          console.error(err);
          res.send(500, 'Error updating password.');
        })
      }).catch(function (err) {
        console.error(err);
        res.send(500, 'Error retrieving user.');
      });
    } else {
      res.send(500, 'Reset token not found. Please try sending a new email.')
    }
  });
});

router.post('/validate', function (req, res) {
  var email = (req.body.email || "").toLowerCase();

  async.waterfall([
    function (done) {
      crypto.randomBytes(20, function (err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function (token, done) {
      User.findOne({where: {email: email}}).then(function (user) {
        if (!user) {
          return done({message: 'No account with that email address exists.'});
        }

        RequestToken.createToken(user, token, 'email-validation').then(function () {
          done(null, token, user);
        }).catch(done);

      }).catch(done);
    },
    function (token, user, done) {
      emails.validateUser(user, token, req.headers.host);
      done();
    }
  ], function (err) {
    if (err) {
      console.error(err);
      return res.send(500, err);
    }

    res.send(200);
  });
});

router.get('/validate/:token', function (req, res) {
  var token = req.params.token;

  RequestToken.getToken(token, 'email-validation').then(function (request) {
    if (request) {
      User.findById(request.user_id, {attributes: ['name', 'email', 'title', 'provider', 'id', 'groups', 'pmr_group', 'divisions'],
        include: [{model: models.Roles, as: 'roles'}, {model: models.Auths, as: 'auths'}]}).then(function (user) {
        if (!user) {
          return res.send(500, 'User not found.');
        }

        // If user is found add CSRA Validated role
        user.setRoles(_.union(user.roles, ['CSRA Validated'])).then(function () {
          RequestToken.cleanUpTokens(user, 'email-validation').then(function () {
            res.json(user);
          });
        }).catch(function (err) {
          console.log(err);
          res.send(500, 'Error validating user.');
        });
      }).catch(function (err) {
        console.error(err);
        res.send(500, 'Error retrieving user.');
      });
    } else {
      res.send(500, 'Validation token not found. Please try sending a new email.')
    }
  })
  .catch(function (err) {
    console.error(err);
    res.send(500, 'Error validating user.');
  });
});

router.get('/roles', function(req, res) {
  models.Roles.findAll({attributes: ['name']}).then(function(roles) {
    res.json(200, roles);
  }).catch(function(err) {
    res.send(500, err);
  });
});

router.get('/auths', function(req, res) {
  models.Auths.findAll({attributes: ['name']}).then(function(roles) {
    res.json(200, roles);
  }).catch(function(err) {
    res.send(500, err);
  });
});

module.exports = router;
