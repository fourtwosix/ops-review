var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var models = require('../../models');

exports.setup = function (User, config) {
  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password' // this is the virtual field on the model
  },
  function(email, password, done) {
    User.find({
      where: {
        email: email.toLowerCase()
      },
      include: [{model: models.Roles, as: 'roles'}, {model: models.Auths, as: 'auths'}]
    }).then(function(user) {
      if (!user) {
        return done(null, false, { message: 'This email is not registered.' });
      }
      if (!user.authenticate(password)) {
        return done(null, false, { message: 'This password is not correct.' });
      }
      return done(null, user);
    }).catch(function(err) {
      done(err);
    });
  }
  ));
};
