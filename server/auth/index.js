'use strict';

var express = require('express');
var passport = require('passport');
var config = require('../config/environment');
var models = require('../models');
var User = models.User;

// Passport Configuration
require('./local/passport').setup(User, config);

var router = express.Router();

if (config.okta.enabled) {
  require('./okta/passport').setup(User, config);
  router.use('/okta', require('./okta'));
}

router.use('/local', require('./local'));

router.get('/login', function (req, res) {
  // If okta is enabled send the user to the okta login page
  if (config.okta.enabled) {
    res.redirect('/auth/okta');

    // If local auth is enabled send user to our login page
  } else {
    res.redirect('/login');
  }
});

router.get('/logout', function (req, res) {
  req.logout();

  // If okta is enabled send them to our static logout page
  if (config.okta.enabled) {
    res.redirect('/logout.html');

    // If local auth is enabled send user to our login page
  } else {
    res.redirect('/');
  }
});


module.exports = router;
