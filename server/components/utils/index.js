'use strict';

var fs = require('fs');
var ejs = require('ejs');
var Promise = require('bluebird');
var config = require('../../config/environment');
var path = require('path');
var moment = require('moment');
var json2csv = require('json2csv');
var _ = require('lodash');
var models = require('../../models');
var sequelize = models.sequelize;

exports.getPgDate = function (dateStr) {
  return moment(dateStr).format("YYYY-MM-DD")
};

exports.getPathFromRoot = function (p) {
  return path.join(config.root, p);
};

exports.readSQLFile = function (path, data) {
  return new Promise(function (resolve, reject) {
    fs.readFile(path, "utf8", function (err, file) {
      if (err) return reject(err);
      resolve(ejs.compile(file)(data));
    });
  });
};

exports.readSQLFileSync = function (path, data) {
  var file = fs.readFileSync(path, "utf8");
  var template = ejs.compile(file);
  return template(data);
};

exports.downloadCSV = function (data, filename, res) {
  console.log(data, filename, res);
  var fields = data.length > 0 ? _.keys(data[0]) : [];
  if(_.includes(filename, 'Program Metadata-')){
    var size = fields.length;
    if(size < 70){
      _.forEach(data, function(object, key){
        var tempObj = {
          date_submitted: object.date_submitted,
          fiscal_month: object.fiscal_month,
          program_number: object.program_number,
          program_name: object.program_name,
          program_manager: object.program_manager,
          pm_email: object.pm_email,
          pra: object.pra,
          Division: object.Division,
          'Division Manager': object['Division Manager'],
          Sector: object.Sector,
          'Sector Manager': object['Sector Manager'],
          business_area: object.business_area,
          'Business Area Manager': object['Business Area Manager'],
          'Contract Ceiling': object['Contract Ceiling'],
          'Contract Value': object['Contract Value'],
          'Contract Value Funded' : object['Contract Value Funded'],
          'Contract Value Margin %': object['Contract Value Margin %'],
          'FY Budget Revenue': object['FY Budget Revenue'],
          'FY Budget Cost': object['FY Budget Cost'],
          'FY Budget Margin': object['FY Budget Margin'],
          'FY Budget Margin %': object['FY Budget Margin %'],
          'Total Investment': object['Total Investment'],
          'CPI/SPI Indicator': object['CPI/SPI Indicator'],
          'Reserve': object['Reserve'],
          payment_terms: object.payment_terms,
          'GDIT Current': object['GDIT Current'],
          'Sub Current': object['Sub Current'],
          'GDIT Funded': object['GDIT Funded'],
          'Sub Funded': object['Sub Funded'],
          customer: object.customer,
          contract_type: object.contract_type,
          fee_structure: object.fee_structure,
          fee_value: object.fee_value,
          labor_cat_cert_required: object.labor_cat_cert_required,
          export_international: object.export_international,
          gfe_or_property: object.gfe_or_property,
          oci: object.oci,
          lead_for_export_international: object.lead_for_export_international,
          foreign_sourced_hw_sw: object.foreign_sourced_hw_sw,
          'Contractual Requirements and Credentials': object['Contractual Requirements and Credentials'],
          scope_of_services: object.scope_of_services,
          program_description: object.program_description,
          'Small Business %': object['Small Business Goal %'],
          'Small Business Actual %' : object['Small Business Actual %'],
          current_month: object.current_month,
          months_to_complete: object.months_to_complete
         };
         data[key] = tempObj;
       });
       fields = data.length > 0 ? _.filter(_.keys(data[0]), function(field){
         return !_.includes(field, '__');
       }) : [];
      };
    }



  // var datas = data.length > 0 ? _.filter()
  // if(data[0].contains)
  json2csv({ data: data, fields: fields }, function (err, csv) {
    if (err) {
      console.error(err);
      res.send(500, err);
    } else {
      res.attachment(filename);
      res.send(csv);
    }
  });
};

function handleError (res, err) {
  console.error(err);
  return res.status(500).send(err);
}

exports.handleError = handleError;

exports.processGeneralRequest = function (path, variables, req, res) {
  var hasPaging = !_.isUndefined(req.query.limit) && !_.isUndefined(req.query.offset);
  var isDownload = !_.isUndefined(req.query.downloadType);

  exports.readSQLFile(path, variables || {}).then(function (query) {
    sequelize.transaction(function (t) {
      return sequelize.query("SET LOCAL config.user_id = " + req.user.id + ";", {transaction: t}).then(function () {
        return Promise.all([
          new Promise(function (resolve, reject) {
            if (!hasPaging) return resolve({});

            variables.limit = "";
            variables.offset = "";

            exports.readSQLFile(path, variables).then(function (query) {
              query = "SELECT COUNT(*) FROM (" + query + ") c";
              sequelize.query(query, {type: sequelize.QueryTypes.SELECT, transaction: t}).then(function (results) {
                resolve(results[0]);
              }).catch(reject);
            }).catch(reject);
          }),
          sequelize.query(query, {type: sequelize.QueryTypes.SELECT, transaction: t})
        ]);
      });
    })
    .then(function (results) {
      var response = hasPaging ? {total: results[0].count, rows: results[1]} : results[1];

      if (isDownload) {
        exports.downloadCSV(results[1], req.query.filename, res);
      } else {
        res.json(response);
      }
    }).catch(function (err) {
      handleError(res, err)
    });
  }).catch(function (err) {
    handleError(res, err);
  });
};


/**
 * Iterates through the attributes in the model and sets them to null if they are undefined
 * so that validations picks up the changes
 *
 * @param Model
 * @param data
 */
exports.nullifyModel = function (Model, data) {
  return  _.reduce(Model.attributes, function (result, attr, key) {
    result[key] = _.isUndefined(_.get(data, key)) ? null : data[key];
    return result;
  }, {});
};

/**
 * Iterates through the attributes in the model and sets them to '' if they are undefined
 * so that validations picks up the changes
 *
 * @param Model
 * @param data
 */
exports.emptifyModel = function (Model, data) {
  return _.reduce(Model.attributes, function (result, attr, key) {
    result[key] = _.isUndefined(_.get(data, key)) || _.isNull(data[key]) ? '' : data[key];
    return result;
  }, {});
};

/**
 * Iterates through the model and sets any undefined entries to the defaultValue defined in the
 * schema and then invokes validation on the model returning validation errors if any were found.
 *
 * @param Model
 * @param data
 * @returns {*}
 */
exports.validateModel = function (Model, data) {
  var defaults = _.reduce(Model.attributes, function (result, attr, key) {
    result[key] = _.isUndefined(attr.defaultValue) ? null : attr.defaultValue;
    return result;
  }, {});
  var update = _.defaults({}, _.omit(data, _.isNull), defaults);
  return Model.build(update).validate();
};

exports.getGTEDate = function(month, monthsToSubtract) {
  return moment(month).subtract(monthsToSubtract, 'months').startOf('month').format("YYYY-MM-DD");
  // return Date.UTC(month.getFullYear(), month.getMonth() - (monthsToSubtract || 0), 1, 0, 0, 0, 0);
};

exports.getLTEDate = function(month, monthsToAdd) {
  return moment(month).add(monthsToAdd, 'months').endOf('month').format("YYYY-MM-DD");
  // return Date.UTC(month.getFullYear(), month.getMonth() + (monthsToAdd || 0), 0, 23, 59, 59, 999);
};
