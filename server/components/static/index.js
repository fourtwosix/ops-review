'use strict';

module.exports.bigip = function f5StatusPage (req, res) {
  res.status(200);
  res.render('bigip', function (err) {
    if (err) return res.status(500).send('server down');

    res.render('bigip');
  });
};

module.exports.staticLogout = function (req, res) {
  res.status(200);
  res.render('logout', {
    GlobalConfig: {
      environment: process.env.NODE_ENV,
      redirectUrl: process.env.REDIRECT_URL,
      google: {
        trackingId: process.env.GOOGLE_TRACKING_ID || 'UA-XXXXX-X'
      }
    }
  });
};

module.exports.redirect = function (req, res) {
  res.status(200);
  res.render('redirect', {
    GlobalConfig: {
      environment: process.env.NODE_ENV,
      redirectUrl: process.env.REDIRECT_URL,
      google: {
        trackingId: process.env.GOOGLE_TRACKING_ID || 'UA-XXXXX-X'
      }
    }
  });
};

module.exports.maintenance = function (req, res) {
  res.status(200);
  res.render('maintenance', {
    GlobalConfig: {
      environment: process.env.NODE_ENV,
      redirectUrl: process.env.REDIRECT_URL,
      google: {
        trackingId: process.env.GOOGLE_TRACKING_ID || 'UA-XXXXX-X'
      }
    }
  });
};