/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var staticPages = require('./components/static');
var auth = require('./auth/auth.service');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/emails', require('./api/emails'));
  app.use('/api/feedback', auth.hasRole('CSRA Validated'), require('./api/feedback'));
  app.use('/api/programs', auth.hasRole('CSRA Validated'), require('./api/program'));
  app.use('/api/contract', auth.hasRole('CSRA Validated'), require('./api/contract'));
  app.use('/api/pmrgroups', auth.hasRole('CSRA Validated'), require('./api/pmrgroup'));
  app.use('/api/staticDisplayData', auth.hasRole('CSRA Validated'), require('./api/staticDisplayData'));
  app.use('/api/users', require('./api/user'));
  app.use('/api/data', auth.hasRole('CSRA Validated'), require('./api/data'));
  app.use('/api/analytics', auth.hasRole('CSRA Validated'), require('./api/analytics'));
  app.use('/api/files', auth.hasRole('CSRA Validated'), require('./api/file'));
  app.use('/api/deckComments', auth.hasRole('CSRA Validated'), require('./api/deckComments'));

  app.use('/auth', require('./auth'));

  app.route('/status/bigip.html').get(staticPages.bigip);
  app.route('/logout.html').get(staticPages.staticLogout);

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);


  var getIndexRenderer = function () {
    if (process.env.MAINTENANCE_MODE === "true") {
      return staticPages.maintenance;
    } else if (process.env.REDIRECT_URL) {
      return staticPages.redirect;
    } else {
      return function(req, res) {
        res.render('index.ejs', {
          GlobalConfig: {
            environment: process.env.NODE_ENV,
            domain: process.env.DOMAIN_NAME || 'csra.com',
            google: {
              trackingId: process.env.GOOGLE_TRACKING_ID || 'UA-XXXXX-X'
            }
          }
        });
      }
    }
  };

  // All other routes should redirect to the index.html
  app.route('/*').get(getIndexRenderer());
};
