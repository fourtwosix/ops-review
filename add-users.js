var models = require('./server/models');
var User = models.User;
var async = require('async');

var users = [
];

async.each(users, function (user, done) {
  user.groups = [user.group];
  user.provider = 'local';
  delete user.group;

  User.count({where: {email: user.email.toLowerCase()}}).then(function (c) {
    if (c === 0) {
      User.create(user).then(function () {
        done();
      }).catch(function (err) {
        done(err);
      });
    } else {
      done(); // TODO update current user?
      console.log("RECORD ALREADY EXISTS", user.email);
    }

  });

}, function (err) {
  if (err) {
    console.log("Error creating users", err);
  } else {
    console.log("Done creating users");
  }
});

