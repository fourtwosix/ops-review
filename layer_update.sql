UPDATE candidate_data SET org_layer_2 = org_layer_2;
UPDATE candidate_data SET org_layer_2 = org_layer_3;
UPDATE candidate_data SET org_layer_3 = org_layer_4;
UPDATE candidate_data SET org_layer_4 = org_layer_6;
UPDATE candidate_data SET org_layer_6 = org_layer_7;
UPDATE candidate_data SET org_layer_7 = NULL;

UPDATE candidate_data SET layer_2_manager = layer_2_manager;
UPDATE candidate_data SET layer_2_manager = layer_3_manager;
UPDATE candidate_data SET layer_3_manager = layer_4_manager;
UPDATE candidate_data SET layer_4_manager = layer_6_manager;
UPDATE candidate_data SET layer_6_manager = layer_7_manager;
UPDATE candidate_data SET layer_7_manager = NULL;

UPDATE permast_data SET layer_2_org_name = layer_2_org_name;
UPDATE permast_data SET layer_2_org_name = layer_3_org_name;
UPDATE permast_data SET layer_3_org_name = layer_4_org_name;
UPDATE permast_data SET layer_4_org_name = layer_6_org_name;
UPDATE permast_data SET layer_6_org_name = layer_7_org_name;
UPDATE permast_data SET layer_7_org_name = NULL;

UPDATE permast_data SET layer_2_manager = layer_2_manager;
UPDATE permast_data SET layer_2_manager = layer_3_manager;
UPDATE permast_data SET layer_3_manager = layer_4_manager;
UPDATE permast_data SET layer_4_manager = layer_6_manager;
UPDATE permast_data SET layer_6_manager = layer_7_manager;
UPDATE permast_data SET layer_7_manager = NULL;

UPDATE resource_management_data SET org_layer_2 = org_layer_2;
UPDATE resource_management_data SET org_layer_2 = org_layer_3;
UPDATE resource_management_data SET org_layer_3 = org_layer_4;
UPDATE resource_management_data SET org_layer_4 = org_layer_6;
UPDATE resource_management_data SET org_layer_6 = org_layer_7;
UPDATE resource_management_data SET org_layer_7 = org_layer_8;
UPDATE resource_management_data SET org_layer_8 = NULL;

UPDATE resource_management_data SET layer_2_manager = layer_2_manager;
UPDATE resource_management_data SET layer_2_manager = layer_3_manager;
UPDATE resource_management_data SET layer_3_manager = layer_4_manager;
UPDATE resource_management_data SET layer_4_manager = layer_6_manager;
UPDATE resource_management_data SET layer_6_manager = layer_7_manager;
UPDATE resource_management_data SET layer_7_manager = layer_8_manager;
UPDATE resource_management_data SET layer_8_manager = NULL;
