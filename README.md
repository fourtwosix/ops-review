OPS Review Dashboard
====================


## Development

If you are interested in contributing the project here are some steps to help you get going

First follow the instructions bellow to install the development dependencies for your specific environment:

## Quick start for Unix / Linux systems

###Dependency Installation
1. Install sass `gem install sass` (might need to install [Ruby](https://www.ruby-lang.org/en/documentation/installation/))
2. Install [NodeJS](https://nodejs.org/en/download/) (any version is fine)
3. Install [Node Version Manager](https://github.com/creationix/nvm#installation)

### Initial project setup
1. Clone the project: `git clone --recursive git@bitbucket.org:fourtwosix/ops-review.git`. 

    *Notice the --recursive option.  This will checkout all required git submodules.*  

2. Enter the project's root directory: `cd ops-review`
3. Install the required NodeJS version using nvm: `nvm install` (looks at the .nvmrc file for the correct version)
4. Use installed NodeJS version: `nvm use` (you should always run this before working on the project)
5. Install global packages: `npm install -g bower grunt-cli sequelize-cli`
6. Install project node dependencies: `npm install`
7. Install project bower dependencies: `bower install`
8. Ask someone for the `database.json` and `mailer.json` files and put them in `server/config/`

### Running the project
1. When starting work on the project, you should always make sure nvm is using the correct NodeJS version: `nvm use`
2. Run the project `grunt serve`


### Using a local database (optional)
Having your own local database is nice if you want to experiment since you can roll back changes easily using VM snapshots. The project already has the necessary files to set up a VM with a database running. 

#### Initial setup
1. Install [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
2. Install [Vagrant](https://www.vagrantup.com/)
3. Install the vagrant guest additions plugin: `vagrant plugin install vagrant-vbguest`
3. Create a virtual machine: `vagrant up`

At this point you should have a database running and be able to connect to it as if it were running on your mac at `localhost:5432`. 

There isn't any data yet, so ask someone for a backup of production so you can seed your database:

1. Put the production backup SQL file in the project's root directory. Everything in this directory is synced to `/vagrant` on the VM.
2. Log into the VM: `vagrant ssh`
3. Become the postgres user: `sudo su - postgres`
4. Drop the existing database: `dropdb insight`
5. Create a new database: `createdb insight`
6. Seed your database: `bzcat /vagrant/<production_backup>.sql.bz2 | psql insight` (this will take a long time)
7. Clean your database: `vacuumdb --analyze insight`
8. Log out of the postgres user and then your vm
9. Create a snapshot of your newly seeded database: `vagrant snapshot save <snapshot name>`
10. Add the connection details to your `database.json` file:
```
  "development": {
    "username": "postgres",
    "password": "postgres",
    "database": "insight",
    "host": "127.0.0.1",
    "dialect": "postgres",
    "redis": "redis://localhost:6379"
  },
```
If you ever need to revert your VM back to the state it was in when you first seeded your database, run `vagrant snapshot restore <snapshot name>`. 

### Common problems

* 

  **Problem: During first time setup, your npm install gives you an error about a lock file already in use.**
  
  Solution: This is an issue with older versions of npm. Just run `npm install` again.
  
* 

  **Problem: vagrant gives you errors about not being able to find certain modules (ex. postgresql::server::plpython)**
  
  Soluton: Run `git submodule foreach git pull origin master` from the project's root directory

* 

  **Problem: After running `grunt serve`, you get a *mostly* blank web page and the error `Uncaught SyntaxError: Unexpected token e in JSON at position 0` in your web console.**
  
  Solution: This happens when you have another project on your system which also has a `token` cookie. Delete your `token` cookie and refresh. In Chrome, this can be done in the developer tools window by clicking the "Application" tab, right clicking "token", and hitting delete.
  

## Steps for Creating Cards
1. Create `.../server/cards/{cardName}.json` where {cardName} is lowerCamelCased  
    Example: `.../server/cards/humanCapitalSummary.json`  
    This file should contain all the barebones json configuration for the card  
2. Add {cardName} to `cards` array in `.../server/decks/{deckName}.json`  
    Example: `cards: ['wageTypeTotals', 'humanCapitalSummary']` in `.../servers/decks/humanCapital.json`  
1. Create `.../client/components/cards/{cardName}.js` where {cardName} is again lowerCamelCased  
    Example: `.../client/components/cards/humanCapitalSummary.js`  
    This file should contain all the custom functionality (overriding functions and/or use of AngularJS components) for the card and should use the following template:  
    NOTE: If the card has no custom functionality and uses the default `loadData` and `onResize` functions from `DataManager` you do **_NOT_** need to create this file on the front-end.  The framework will automatically add the default `loadData` and `onResize` functions to the card.

        'use strict';

        angular.module('opsReviewApp')
          .factory('{cardName}', function (Card, DataManager) {

          var {cardName} = function(cardData) {
              angular.extend(this, new Card(cardData));

              /*
               * START card specific custom functionality
               */
              this.title = DataManager.someConstant;
              this.afterFiltersApplied = function() { ... };
              this.summaryViewOptions.preDataTransform = function() { ... }
              /*
               * END card specific custom functionality
               */

              return this;
          };

          {cardName}.prototype = Card.prototype;

          return {cardName};
        });

1. The framework will handle merging the back-end json configuration with the front-end custom functionality based on the file names.
