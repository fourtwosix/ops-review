#!/bin/bash

APP_NAME="insight"
BUILD_ENV="$1"

if [[ -z "$BUILD_ENV" ]]; then
  echo "No build environment was specified"
  exit 1
else
  echo -e "Building $APP_NAME with env $BUILD_ENV\n"
fi

cd dist
git init
git remote add ${BUILD_ENV} ssh://build@insight.42six.com/var/www/repos/${APP_NAME}-${BUILD_ENV}.git
git fetch ${BUILD_ENV}
git reset --hard ${BUILD_ENV}/master
cd -

echo -e "Running grunt build for $APP_NAME with env $BUILD_ENV\n"
grunt build
built=$?

if [[ ${built} != 0 ]]; then
  echo -e "Failed to finish grunt build for $APP_NAME with env $BUILD_ENV\n"
  exit ${built}
else
  echo -e "Successfully finished grunt build for $APP_NAME with env $BUILD_ENV\n"
fi

echo -e "Building npm dependencies for $APP_NAME with env $BUILD_ENV\n"
#npm install --production --prefix ./dist
packages=$?

if [[ ${packages} != 0 ]]; then
  echo -e "Failed to install node depencencies for $APP_NAME with env $BUILD_ENV\n"
  exit ${packages}
else
  echo -e "Successfully built node dependencies for $APP_NAME with env $BUILD_ENV\n"
fi

echo -e "Deploying $APP_NAME with env $BUILD_ENV\n"

cd dist
git add .
git commit -m "Build $APP_NAME insight-$BUILD_ENV"
git push -u ${BUILD_ENV} master
echo -e "Making TAR file for current build"
tar czvf $APP_NAME-build.tar.gz *
