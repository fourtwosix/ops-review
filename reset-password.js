var promptly = require('promptly');
var models = require('./server/models');
var User = models.User;
var async = require('async');
var inquirer = require("inquirer");

async.waterfall([
  function (done) {
    promptly.prompt('Email: ', function (err, email) {
      User.findOne({where: {email: email}}).then(function (user) {
        done(user ? null : new Error('User not found!'), user);
      });
    });
  },
  function (user, done) {
    promptly.password('New Password: ', function (err, password) {
      user.password = password;
      user.save().then(function () {
        done();
      }).catch(function (err) {
        done(err);
      });
    })
  }
], function (err) {
  if (err) {
    console.error(err);
  } else {
    console.log("Successfully changed password!");
  }

});

