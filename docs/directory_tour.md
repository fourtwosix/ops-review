Project Directory Tour
======================

This project is a NodeJS/AngularJS-based project originally developed by a scaffolding process that is both opinionated and configuration driven.  This document is a tour of the higher level directories.

The *nix command line tool 'tree' is handy for exploring a directory's structure.  To install 'tree' on the Mac use the following command:

```bash
brew install tree
```

As a NodeJS project the more important directories to pay attention to are [client] and [server]. 

# Level One Directories

* [client]
* [docs]
* [e2e]
* [node_modules]
* [puppet]
* [server]

## client

The majority of source code touched by a typical front-end web developer is located in this directory.

Here are the next level subdirectories:

  * [client/app]
  * [client/assets]
  * [client/bower_components]
  * [client/components]

The most important folders from the front-end developer point-of-view (POV) are [client/app] and [client/components].

### client/app

This is one of the two more important directories for front-end developers.  The other directory to pay close attention to is [client/components].

### client/assets

This directory contains static assets (aka resources) such as fonts and images.

### client/bower_components

There are no developer source files located in this directory.

There are two package managers used on this project.  The Node Package Manager (npm) is used to manage primarily server-side (node) components.  On the client-side package management is provided by the 'bower' tool.  This directory contains the components that were install by bower.

### client/components

This is one of the two more important directories for front-end developers.  The other directory to pay close attention to is [client/app].

## docs

Has some project developer-oriented documentation.

## e2e

End-to-end testing.  The sad truth is that both unit-level testing a system-level testing efforts have been regulated to a low priority in the rush to get a functional product into the hands of the users.

## node_modules

This is basically a 'vendored' directory of third-party components.  No project-specific code files are located here.

## puppet

This project has a Vagrant-managed virtual machine implemented within Oracle's VirtualBox environment.  This VM is configured by puppet.  Project specific components of the VM are maintained in this directory.

## server

All server-side components of this project are maintained in the server directory.  This includes third-party components and project-specific components as well.

The JSON files that define decks and cards are also located within the server directory under their seperate sub-directories

Here are the next level subdirectories:

  * [server/api]
  * [server/auth]
  * [server/cards]
  * [server/components]
  * [server/config]
  * [server/decks]
  * [server/models]
  * [server/views]

### server/api

There are at least two subdirectories which are associated with the back-end database.  The 'user' subdirectory supports the user model management functions that should only be available to a user with the 'admin' role.

The other directory is 'data' which provides support for access to the other database tables.

### server/auth

Supports server-side user authentication.

### server/cards

This directory contains the JSON files that define the individual cards that are used in this project.  These JSON files are retrieved by the front-end from the server when the cards are initialized.

See also: [File Naming Conventioin for Cards and Decks]

### server/components

Nothing much.  Has a component to support the page not foun - 404 - error.

### server/config

The server-side config directory contains the folders and files that define the configuration of the back-end.  This includes the [server/config/seeds] directory where the comma-seperated-value (CSV) files are located.  These files define the initial configuration of the different database tables used by this project in the development environment.

The definition of the database tables is maintained in files located in the [server/models] directory.

### server/decks

This directory contains the JSON files that define the decks used in this project.  The deck definition files contain the names of the cards that compose the deck.

Sometimes a deck is referenced as a dashboard.  See the documentation file "what_is_a_deck_or_cards.md" for a discussion on the relationship betweek decks and cards.

The JSON files that define the decks are handled simular to the cards JSON files.

See also: [File Naming Conventioin for Cards and Decks]


### server/models

This directory contains the javascript files that define the configuration of the database tables (i.e., models within the MVC paradigm.)

### server/views

This directory contains common HTML files that are server (project) wide; for example, the error pages such as the 404 - not found - page.


# Some Important Files

## Client-side

TBD

## Server-side

### server/config/database.json

This file is used to define the connection to the back-end database.  It is similar in function to the Ruby/Rails database.yml file.


# File Naming Conventioin for Cards and Decks

The name of the JSON file that defines a card or a deck is used as a unique idenitifier in this project.  These filenames follow a naming convention that looksLikeThis.  

This naming convention is called lowerCamelCase.

