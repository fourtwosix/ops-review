How To Create a Card
====================

A card (in terms of actional functionality) in this project is componsed of several parts:

* [JSON Definition]
* [DataManagement Specializations]
* [Views]

## JSON Definition

The basic properties of a card (and the decks they are used in) are defined in a JSON object.  The JSON can be presisted in either a file or a database system.  Regardless of how the JSON content is presisted, the process of creating a card is the same.  Every new card starts as a JSON file.


1. Create `.../server/cards/{cardName}.json` where {cardName} is lowerCamelCased  
    Example: `.../server/cards/humanCapitalSummary.json`  
    This file should contain all the barebones json configuration for the card  
2. Add {cardName} to `cards` array in `.../server/decks/{deckName}.json`  
    Example: `cards: ['wageTypeTotals', 'humanCapitalSummary']` in `.../servers/decks/humanCapital.json`  

## DataManagement Specializations

1. Create `.../client/components/cards/{cardName}.js` where {cardName} is again lowerCamelCased  
    Example: `.../client/components/cards/humanCapitalSummary.js`  
    This file should contain all the custom functionality (overriding functions and/or use of AngularJS components) for the card and should use the following template:  
    NOTE: If the card has no custom functionality and uses the default `loadData` and `onResize` functions from `DataManager` you do **_NOT_** need to create this file on the front-end.  The framework will automatically add the default `loadData` and `onResize` functions to the card.

        'use strict';

        angular.module('opsReviewApp')
          .factory('{cardName}', function (Card, DataManager) {

          var {cardName} = function(cardData) {
              angular.extend(this, new Card(cardData));

              /*
               * START card specific custom functionality
               */
              this.title = DataManager.someConstant;
              this.afterFiltersApplied = function() { ... };
              this.summaryViewOptions.preDataTransform = function() { ... }
              /*
               * END card specific custom functionality
               */

              return this;
          };

          {cardName}.prototype = Card.prototype;

          return {cardName};
        });

1. The framework will handle merging the back-end json configuration with the front-end custom functionality based on the file names.


## Views

