What is a Deck of Cards
=======================

The foundational library for this project is decksterJS a jQuery plugin based upon gridsterJS.  The principle data structures (objects) for decksterJS is the Deck and the Card.

In short a deck is a collection of cards.  Cards make use of data queries (filtering) and views (HTML elements.)

There is an additional concept of the overall design that has not yet been fully realized - Card Templates.  All cards can be thought of as an instantion of a card template.

With this in mind then the definition of a deck becomes more like a collectiion of instances of card templates.

How one instance of a card template differs from another instance of the same template is TBD.

Decks are sometimes called dashboards.

* [The Deck Object]
* [The Card Object]
* [Card Templates]
* [The View Object]

# The Deck Object

The deck object is a collection of card names and overall configuration elements for the deck.

## Primary Structure

The Human Capital Deck is used as an example. It is defined in the file humanCapital.json located in the directory ../server/decks/.  Note the use of [lowerCamelCase](http://c2.com/cgi/wiki?LowerCamelCase) file naming convention.  This conventions is used for naming both deck JSON files as well as card JSON files.

```json
	{
	  "id": 0,
	  "title": "Human Capital",
	  "maxNumFilters": 1,
	  "gridsterOpts": {
	    "widget_base_dimensions": ["auto", 150]
	  },
	  "cards": ["humanCapitalSummary", "employeeLocations", "hiresAndAttrition", "headCount"]
	}
```

Each element is discussed in the following sections:

* id
* title
* maxNumFilters
* gridsterOpts
* cards

### id

Unique integer used to designate this deck from all others.

*(tdv: It really seems silly to have a deck designer atempt to come up with an unique numberic id for his deck.  The filename should be sufficient to uniquely identify this deck from all others.)*

### title

This is a string that is shown on the wweb-application as the name of the "dashboard" collection of cards.  The filename should have some close resimblance to the string used as the title.

### maxNumFilters

See also: ./data_filtering.md

TBD
*(TDV: this makes no sense outside of a list of filters and if we have a list of filters why does the deck designer have to count them?  Shut the Door!  Jacob says that this may OBE; that it refers to rhe maximum number of CONCURRENT filters and may be obsoleated or at least no longer used.)*

### gridsterOpts

TBD
Does gridster have its own documentation w/r/t options?

### cards

An array of card names.  The card name is a string that identifies the JSON file that defines the indicated card.  For example the first card name in the list "humanCapitalSummary" is the basename of the JSON file humanCapitalSummary.json located in the directory ../server/cards/.

## Deck-level Services

Some decks have specific services which support the functionality of the deck.  These services are JavaScript functions definded in the directory ../client/components/decks/ with a filename consistent with the deck's JSON filename.  For example the Human Capital Deck has its services defined in the file "humanCapital.js".

The deck's JavaScript file is where deck-level data filtering is implemented.

*(TDV: currently it looks like deck instantion is done in the deck's js file unlike card instantion which is done in the card json load process.  This means that every deck must have a deck.js file regardless of with it has a need for devk-level filtering or other deck-level services.)*


# The Card Object

A card object is a collection of configuration elements, a list of associated views, data filtering elements and a collection of functions for use as async callbacks.

## Primary Structure

A Card object is configured as a hash (JSON) data structure having configuration elements, data query (filtering) elements (generally expressed as SQL) and functional elements used as async callbacks for the prepration of data and the tweeking or selection of view components.

TBD
from ../server/cards/yumanCapitalSummary.json

```json
	{
	  "id": "basicBarChart",
	  "position": {
	    "size_x": 2,
	    "size_y": 3,
	    "row": 1,
	    "col": 1
	  },
	  "summaryViewType": "barChart",
	  "summaryViewOptions": {
	    "tooltipEnabled": true,
	    "dataTransform": {
	      "row": "category",
	      "titleFormats": {
	        "category": "name"
	      }
	    },
	    "queryTemplate": "SELECT ${subManagerType}, count( distinct(CASE WHEN employment_status LIKE '%active' AND report_date = (SELECT max(report_date) FROM permast_data) THEN personnel_number END) ) AS \"Head Count\", count( distinct(CASE WHEN hire_date BETWEEN '${fiscalYearStart}' AND '${fiscalYearEnd}' THEN personnel_number END) ) AS \"Hires\", count( distinct(CASE WHEN termination_type LIKE '% voluntary' AND termination_date BETWEEN '${fiscalYearStart}' AND '${fiscalYearEnd}' THEN personnel_number END) ) AS \"Voluntary Terminations\", count( distinct(CASE WHEN termination_type LIKE '%involuntary' AND termination_date BETWEEN '${fiscalYearStart}' AND '${fiscalYearEnd}' THEN personnel_number END) ) AS \"Involuntary Terminations\" FROM permast_data WHERE ${managerClause} AND ${subManagerType} IS NOT NULL AND date_trunc('day', report_date)::DATE BETWEEN '${fiscalYearStart}' AND '${fiscalYearEnd}' GROUP BY ${subManagerType}"
	  },
	  "detailsViewType": "table",
	  "detailsViewOptions": {
	    "tooltipEnabled": true,
	    "numColumns": 3,
	    "dataTransform": {},
	    "hiddenColumns": ["personnel_number", "report_date"],
	    "queryTemplate": "SELECT DISTINCT ON (personnel_number) personnel_number, report_date, first_name, last_name, ${subManagerType} FROM permast_data WHERE employment_status = 'active' AND ${managerClause} AND ${subManagerType} IS NOT NULL AND date_trunc('day', report_date)::DATE BETWEEN '${startDate}' AND '${endDate}' ORDER BY personnel_number ASC, report_date DESC"
	  }
	}
```



## Card-level Configuration Elements

TBD

* have a unique ID - name ?
* have an initial starting position on the screen within the definition of the screen's grid size.
* take up screen real estate based upon columns and rows
* have a normal size and an expanded size 



* id
* position
* summaryViewType / detailsViewType
* summaryViewOptions / detailsViewOptions
    * dataTransform
	* queryTemplate




### id

TBD
basename of JSON filename.

### position

TBD
location in deck space
normal size
expanded size


### summaryViewType / detailsViewType

TBD

### summaryViewOptions / detailsViewOptions

TBD

#### dataTransform

TBD

#### queryTemplate

TBD


## Card-level Services

TBD
some cards do not have services.

## Relationship of Associated Views

The overall concept with regard to the relationship between Cards and Views is that a Card has access to at least one view.  A card can have access to more than one view.  When more than one view is associated with a card the views take on a presoective with regard to the card's data as being 'summary', 'detailed' or 'drill down.'

# Card Templates

# The View Object

TBD

* views can be shared between cards - the same view object can be used (re-used) by one or more cards.  This means that the same view can appear on the display in more than one location.
* The content of a view depends on the data-filtering of the Deck and the Card that is associated with the instance of the view.
* views have an associated controller ???
