Hello New Guy
=============

Welcome to the Operations Review (ops-review) Dashboard project.

# Overview

The Operations Review Dashboard project (ops-review) is a web-based graphical depiction of operational data derived from existing reports obtained through an existing eMail-based report distribution process.

## High-level Process

1. Operational data in the form of reports in standardized formats (typically Microsoft Excel spreadsheets and comma-separated value files) is distributed via eMail attachments by subject matter experts to management. A special eMail address links the ops-review project into this report distribution service.
2. Operational reports are obtained as attachments to eMails sent to the ops-review data collection eMail address.  The data from the reports is extracted and stored in a standardized format within the ops-review PostgreSQL-based relational database management system (RDBMS). 
3. The ops-review user interacts with the application to see multiple views of the operational data from the distributed reports.

## Value Added

It is natural to ask "what is the value added?" by the ops-review application considering that it uses the same data already provided by the existing reports.  The value lies in the multiple views that ops-review provides.  These views provide the ability to see the data from various points of view including the ability to show multiple reports side-by-side.

This consolidation of multiple separate reports into a consistent web portal provides a convenience not obtained when dealing with separate spreadsheets.

The ops-review application is a labor saving device in that the typical use case for the existing reports is the generation of various graphics for inclusion within Microsoft Powerpoint slide presentations.  The ops-review application has the potential to replace many of the slide presentation formats that are currently produced by hand.

The existing reports are quantized by calendar week, calendar month and financial month.  Only the ops-review applications provides the ability to compare operational data over a time frame spanning multiple reporting periods.


## Associated Projects

TBD

These are other projects that are associated with this
project such as:

* data ingestion project
* PMR project


## Architecture/Stack/Libraries/Toolchain Components

* postgresql - [http://www.postgresql.org](http://www.postgresql.org)
* NodeJS - [https://nodejs.org/](https://nodejs.org/)
* AngularJS - [https://angularjs.org/](https://angularjs.org/)
* Deckster - [http://decksterteam.github.io/DecksterJS/](http://decksterteam.github.io/DecksterJS/)
* gridster - [http://gridster.net/](http://gridster.net/)
* jquery - [http://editorconfig.org/](http://editorconfig.org/)
* yeoman - [http://yeoman.io/](http://yeoman.io/)
* grunt - [http://gruntjs.com/](http://gruntjs.com/)
  * ?? gulp - [http://gulpjs.com/](http://gulpjs.com/)
* jshint - [http://jshint.com/](http://jshint.com/)
* EditorConfig - [http://editorconfig.org/](http://editorconfig.org/)

### Getting started hints

As a developer on the OPS-Review project you are expected to have some knowledge of the entire stack.  If you are new to full-stack development here are a few resources that will help spin you up in the key technologies used within the project.

As a new developer on the ops-review team you will need to have a good understanding of the following areas:

* [Scrum Process and Collaboration Tools]
    * [git]
    * [Google Tools]
    * [Atlassian Tools]
* [HTML & CSS]
* [Javascript]
* [jQuery]
* [AngularJS]
* [Yeoman (yo) generators]
* [NodeJS]
* [PostgreSQL]


#### Scrum Process amd Collaboration Tools

Scrum is a management and control process that focuses on incrementally and empirically building working software that meets business needs. 

Effective team collaboration is a key precept of the scrum process as clearly and succinctly described in The Scrum Guide. http://www.scrumguides.org/

This development team makes extensive use of collaboration tools provided by Google and Atlassian.


##### Git

The git tool is the chosen distributed version control system for this project.  It is the gold standard by which all other version control systems are measured.

If you are not familiar with the git tool chain the following resources are helpful:

* [https://git-scm.com/](https://git-scm.com/)
* [https://www.atlassian.com/git/tutorials/](https://www.atlassian.com/git/tutorials/)

###### Git Process

*Git Clone*

In the beginning there was the clone ...

There are at least two ways to grab an initial copy of the project's repository on bitbucket.org.  One way is to get your SSH keys configured on your workstation and at bitbucket.org that allows you to bypass the typical username/password protocol.  If you have the SSH keys setup you can clone the repository this way:

```bash
git clone --recursive git@bitbucket.org:fourtwosix/ops-review.git
```

If you prefer the username/password protocol clone the repo like this:

```bash
git clone --recursive https://YourUserName@bitbucket.org/fourtwosix/ops-review.git
```

The *--recursive* directive is important because within this project that are several sub-projects that are independantly managed by git in other repositories.  The use of *--recursive* ensures that the sub-directories are populated with the proper versions of those other directories.

*Git Branches*

This is a good tutorial on using branches with the git system:

[https://www.atlassian.com/git/tutorials/using-branches/](https://www.atlassian.com/git/tutorials/using-branches/)

Create your feature branch and make sure that its available through the central repository so other developers can see your stuff:

```bash
git checkout -b your-branch-name develop
git push -u your-branch-name origin

```

It is a good idea to use a branch name that reflects the JIRA ticket to which your software changes apply.



*Git Sync/Update from develop Branch*

To syncronize your branch with the develop branch just do this after you have retrived the latest develop branch:

```bash
git checkout your-branch-name
git merge develop
```

The merge may result is some conflicts which will require resolution before your branch is syncronized with develop.  Resolve those conflicts before you continue development.

##### Google Tools

Your 42six.com eMail address is provided through Google's gMail service.  You can use this eMail address to activate other Google services such as the following:

* Google gMail web-client
* Google Docs
* Google Calendar
* Google Plus
* Google Hangouts

Some documents critical to the development of this project are shared via Google Docs.

Team meetings are conducted via Google Hangouts.  The daily scrum meeting takes place at 10:30 AM CST.  The link for the meeting is:

[https://plus.google.com/hangouts/_/42six.com/ops-review](https://plus.google.com/hangouts/_/42six.com/ops-review)

BTW: Google tools are best supported by Google's Chrome browser.


##### Atlassian Tools

* Bitbucket
* JIRA

If you do not currently have a Bitbucket account, you may use your 42six.com Google gMail account to establish one.  All source code for this project is maintained in private git repositories maintained by Bitbucket.

* fourtwosix/ops-review


Features, issues and sprint definitions for this project are maintained in JIRA.


#### HTML & CSS

HTML & CSS - If you feel the need to

[http://www.codecademy.com/tracks/web](http://www.codecademy.com/tracks/web)

#### Javascript

Javascript - Take a few hours to brush up on javascript. Some key areas to take Objects I && Objects II of the Code Academy Course

[http://www.codecademy.com/tracks/javascript](http://www.codecademy.com/tracks/javascript)

#### jQuery

jQuery - You should be able to select and manipulate DOM Elements in a webpage. Also reacting to events.

[http://www.codecademy.com/tracks/jquery](http://www.codecademy.com/tracks/jquery)

#### AngularJS

AmgularJS has become a popular front-end framework within the model-view-controller (MVC) paradigm. AngularJS places its emphasis on the view component - not surprising since it is after all focused on the front-end. 

This project's source code layout was originally created using an AngularJS Full-Stack generator as a scaffolding tool. It makes use of the MEAN stack.  See http://mean.io for a description.  (Mongo has been replaced in this project by PostgreSQL.)

[https://github.com/DaftMonk/generator-angular-fullstack](https://github.com/DaftMonk/generator-angular-fullstack)

The following resources are available to help with overall understanding of the AngularJS capabilities:

* [https://docs.angularjs.org/api](https://docs.angularjs.org/api)
* [https://www.codeschool.com/courses/shaping-up-with-angular-js](https://www.codeschool.com/courses/shaping-up-with-angular-js)
* [https://thinkster.io/a-better-way-to-learn-angularjs/](https://thinkster.io/a-better-way-to-learn-angularjs/)


##### HighCharts

The highcharts library is one of the more comprehensive interactive plotting packages available for client-side presentation.  This library is used extensively in this project.  The following resources are available:

* [http://www.highcharts.com](http://www.highcharts.com)
* [http://code.tutsplus.com/tutorials/adding-charts-to-your-site-with-highcharts--cms-21692](http://code.tutsplus.com/tutorials/adding-charts-to-your-site-with-highcharts--cms-21692)
* [http://www.javascriptoo.com/highcharts](http://www.javascriptoo.com/highcharts)


#### Yeoman (yo) generators

(TDV: this is confusing because in a different section - AngularJS - we said that the project was generated with a different system that implemented the MEAN stack.)

Main thing to wrap your head around here is the use of the included Yeoman (yo) generators.

We will be using these to keep our project tidy and consistent. I will eventually come up with some coding conventions as well (Hopefully I have some time to do this)

[http://yeoman.io](http://yeoman.io)


#### NodeJS

NodeJS (aka node.js) has become a defacto standard for single page web applications.  It provides for the use of javascript functionality within the backend server-side as well as the frontend client-side of an application.

NodeJS provides the powerful Node Package Manager (npm) tool for access to 3rd party modules.

* [https://nodejs.org](https://nodejs.org)
* [https://www.airpair.com/javascript/node-js-tutorial](https://www.airpair.com/javascript/node-js-tutorial)
* [https://www.youtube.com/watch?v=FqMIyTH9wSg](https://www.youtube.com/watch?v=FqMIyTH9wSg)
* [http://code.tutsplus.com/series/nodejs-step-by-step--net-20500](http://code.tutsplus.com/series/nodejs-step-by-step--net-20500)
* [http://nodeguide.com/beginner.html](http://nodeguide.com/beginner.html)


#### PostgreSQL

Originally this project was to use the mongodb tool for data storage; however, during the early development sprints a change was made to support RDBMS.  Within the FOSS world PostgreSQL has become the defacto standard.

Within this project there are two principle ways in which the database is access.  The original way - now depricated - was through a query builder.  The current perfered way is throguh direct SQL statements.

A good understanding of SQL is needing order to understand how data is be transformed from its raw form within the database to its graphical view provided by the applkication.

The following resources provide information about PostgreSQL and SQL in general.

* [http://www.postgresql.org/docs](http://www.postgresql.org/docs)
* [http://www.w3schools.com/sql](http://www.w3schools.com/sql)
* [http://www.sqlcourse.com/intro.html](http://www.sqlcourse.com/intro.html)


