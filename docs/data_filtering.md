Data Filtering
==============

In other files the relationship between decks and cards is discussed.  This file goes a little more in depth regarding the way that database queries (filters) are generated.

# Deck-level Filtering

TBD
high-level
managers
dates

# Card-level Filtering

TBD
low-level
counts
events
statistics
