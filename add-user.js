var promptly = require('promptly');
var models = require('./server/models');
var User = models.User;
var async = require('async');
var inquirer = require("inquirer");

async.waterfall([
  function (done) {
    done(null, User.build({provider: 'local'}));
  },
  function (user, done) {
    promptly.prompt('Name: ', function (err, name) {
      user.name = name;
      done(err, user);
    })
  },
  function (user, done) {
    promptly.prompt('Email: ', function (err, email) {
      user.email = email;
      done(err, user);
    })
  },
  function (user, done) {
    promptly.prompt('Title: ', {default: 'user'}, function (err, title) {
      user.title = title;
      done(err, user);
    })
  },
  function (user, done) {
    inquirer.prompt([
      {
        type: "checkbox",
        message: "Select Your Groups",
        name: "groups",
        choices: [
          {
            name: "nes-reporting"
          },
          {
            name: "pmr-reporting"
          },
          {
            name: "ppmc-reporting"
          },
          {
            name: "mission-services"
          },
          {
            name: "staffing"
          }
        ]
      }
    ], function( answers ) {
      user.groups = answers.groups;
      done(null, user);
    });
  },
  function (user, done) {
    promptly.password('Password: ', function (err, password) {
      user.password = password;
      done(err, user);
    })
  },
  function (user, done) {
    user.save().then(function () {
      done();
    }).catch(function (err) {
      done(err);
    });
  }
], function (err) {
  if (err) {
    console.error(err);
  } else {
    console.log("Successfully created user!");
  }

});

