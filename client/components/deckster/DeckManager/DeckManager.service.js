'use strict';

angular.module('opsReviewApp')
  .factory('DeckManager', function ($http, $q, DataManager, Utils, $rootScope) {
    var DeckManager = {selectedDeck: null};

    DeckManager.saveDeck = function (deck) {
      // TODO save this using the deckstore service
    };

    DeckManager.getUserDecks = function (user) {
      // TODO fetch users decks from the deckstore
    };

    DeckManager.setCurrentDeck = function(deck) {
      var deferred = $q.defer();

      DeckManager.selectedDeck = deck;
      $rootScope.$broadcast('deck-selected', deck);
    };

    DeckManager.getDefaultDecks = function (group) {
      var deferred = $q.defer();

      $http.post('/api/data/decks/', {group: group}).then( function (response) {
        deferred.resolve(response.data);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    };

    /**
     * @deprecated since version 2.0
     */
    DeckManager.getFilterValues = function (table, filter, query, dependsOn, useWhitespace) {
      var deferred = $q.defer();

      $http.post('/api/data/filters', {table: table, filter: filter, query: query, dependsOn: dependsOn, useWhitespace: useWhitespace}).then(function (response) {
        var data = response.data || [];

        if (filter.displayNameFormatter && _.has(Utils.dataFormatter, filter.displayNameFormatter)) {
          _.each(data, function (item) {
            item.displayName = Utils.dataFormatter[filter.displayNameFormatter](item.displayName);
          })
        }

        deferred.resolve(data);
      }, function (err) {
        deferred.reject(err);
      });

      return deferred.promise;
    };

    DeckManager.getFilterAutocomplete = function (apiUrl, filter, query, dependsOn) {
      // console.log("dependsOn",dependsOn);

      var deferred = $q.defer();

      $http.get(apiUrl, {params: {query: query, dependsOn: dependsOn}}).then(function (response) {
        var data = response.data || [];

        if (filter.displayNameFormatter && _.has(Utils.dataFormatter, filter.displayNameFormatter)) {
          _.each(data, function (item) {
            item.displayName = Utils.dataFormatter[filter.displayNameFormatter](item[filter.nameColumn]);
          })
        }

        deferred.resolve(data);
      }, function (err) {
        deferred.reject(err);
      });

      return deferred.promise;
    };

    DeckManager.getDeckComments = function (deckId, programId, fiscalMonth) {
      var deferred = $q.defer();

      $http.get('/api/deckComments/' + deckId + '/' + programId + '/' + fiscalMonth).then( function (response) {
        deferred.resolve(response.data);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    };

    DeckManager.saveDeckComment = function(deckId, programId, fiscalMonth, comment) {
      var deferred = $q.defer();

      $http.post('/api/deckComments', {deckId: deckId, programId: programId, fiscalMonthEnd: fiscalMonth, comment: comment}).then( function (response) {
        deferred.resolve(response.data);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    };

    DeckManager.deleteDeckComment = function(commentId) {
      var deferred = $q.defer();

      $http.delete('/api/deckComments/' + commentId).then(function(response) {
        deferred.resolve(response.data);
      }, function(error) {
        deferred.reject(error);
      });

      return deferred.promise;
    };

    return DeckManager;
  });
