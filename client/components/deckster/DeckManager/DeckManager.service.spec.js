'use strict';

describe('Service: DeckManager', function () {

  // load the service's module
  beforeEach(module('opsReviewApp'));

  // instantiate service
  var DeckManager;
  beforeEach(inject(function (_DeckManager_) {
    DeckManager = _DeckManager_;
  }));

  it('should do something', function () {
    expect(!!DeckManager).toBe(true);
  });

});
