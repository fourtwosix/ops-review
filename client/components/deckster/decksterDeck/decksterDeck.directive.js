'use strict';

angular.module('opsReviewApp')
  .directive('decksterDeck', function (DeckFilterManager, $rootScope, $parse, $timeout, $window, SyncCharts) {
    var defaults = {
      gridsterOpts: {
        max_cols: 4,
        widget_margins: [10, 10],
        widget_base_dimensions: ['auto', 250],
        responsive_breakpoint: 850
      }
    };
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: 'components/deckster/decksterDeck/decksterDeck.html',
      scope: {
        deck: "=",
        initialized: "="
      },
      controller: function($scope, DeckManager, moment, $uibModal, Card) {
        $scope.deckInitialized = false;
        $scope.deck.summaryCollapsed = false;
        $scope.selectedFilters = {};
        if (DeckFilterManager.getFilter()) {
          $scope.deck.selectedFilters = DeckFilterManager.getFilter();
          _.forEach($scope.deck.filters, function(filter) {
            if (filter.checkboxes) {
              _.forEach(filter.checkboxes, function(option) {
                option.selected = $scope.deck.selectedFilters[filter.name];
              });
            }
          });
        }
        // if (DeckFilterManager.getDateFilter()) {
        //   $scope.deck.startDate = DeckFilterManager.getDateFilter().start;
        //   $scope.deck.endDate = DeckFilterManager.getDateFilter().end;
        // }
        // else {
        //   DeckFilterManager.setDateFilter({
        //     start: $scope.deck.startDate,
        //     end: $scope.deck.endDate,
        //     maxEndDate: $scope.deck.maxEndDate
        //   });
        // }

        var summaryCollapsedFromToggle = false;

        $scope.convertFilterValues = function () {
          var filterValues = angular.copy($scope.deck.selectedFilters);
          _.forEach(filterValues, function(value, key) {
            _.forEach($scope.deck.filters, function(filter) {
              if (key === filter.name && filter.exemptFromDeckStatus) {
                delete filterValues[filter.name];
                return;
              }
            });
          });
          return _.map(filterValues, function (value, key) {
            return _.isBoolean(value) ? _.startCase(key) : _.startCase(value);
          });
        }

        $scope.numSelectedFilters = function () {
          return $scope.convertFilterValues().length;
        };

        $scope.saveDeck = function () {
          DeckManager.saveDeck();
        };

        $scope.filterDeck = function () {
          var modal = $uibModal.open({
            templateUrl: 'components/deckFilters/deckFiltersModal.html',
            controller: 'DeckFiltersCtrl',
            backdrop: true,
            windowClass: 'minimal-modal',
            resolve: {
              deck: function () {
                return angular.copy($scope.deck);
              }
            }
          });
          modal.result.then($scope.broadcastFilters);
        };

        $scope.chooseDate = function () {
          var modal = $uibModal.open({
            templateUrl: 'components/deckDate/deckDateModal.html',
            controller: 'DeckDateCtrl',
            backdrop: true,
            windowClass: 'minimal-modal datepicker-modal ' + ($scope.deck.dateFilterType === 'month' ? 'single' : ''),
            resolve: {
              deck: function () {
                return angular.copy($scope.deck);
              }
            }
          });
          modal.result.then($scope.broadcastFilters);
        };

        $scope.broadcastFilters = function (filters) {
          if (filters.deckFilters) {
            $scope.deck.filters = filters.deckFilters;
            $scope.deck.selectedFilters = filters.filters;
          }
          if (filters.filters.date_range) {
            $scope.deckster.options.startDate = $scope.deck.startDate = filters.filters.date_range.start;
            $scope.deckster.options.endDate = $scope.deck.endDate = filters.filters.date_range.end;
            filters.filters = $scope.deck.selectedFilters;
          }

          // TODO for default deck filters

          //var getSelectedFiltersFromDeckFilters = function (decksterFilters, selFilters) {
          //  var filterObj = {l3_portfolio:null,layer_3_manager:null,program_manager:null,pra:null,project_id:null};
          //
          //  var arrayLength = decksterFilters.length;
          //  for (var i = 0; i < arrayLength; i++) {
          //    if (decksterFilters[i].columns) {
          //      var columnsarraylength = decksterFilters[i].columns.length;
          //      for (var j = 0; j < columnsarraylength; j++) {
          //        if (decksterFilters[i].columns[j].selectedFilters) {
          //          var selfilterarraylength = decksterFilters[i].columns[j].selectedFilters.length;
          //          for (var k = 0; k < selfilterarraylength; k++) {
          //            if (decksterFilters[i].columns[j].selectedFilters[k]) {
          //              var filterName = decksterFilters[i].columns[j].name;
          //              selFilters[filterName] = decksterFilters[i].columns[j].selectedFilters[k][filterName];
          //            }
          //          }
          //        }
          //      }
          //    }
          //  }
          //};

          $scope.selectedFilter = $scope.deck.selectedFilters = $scope.deckster.options.selectedFilters = filters.filters;

          $rootScope.$broadcast("deck:filtered", filters.filters);
        };

        $scope.toggleSummaryBar = function (sidebarDeck) {
          $scope.deck.summaryCollapsed = !$scope.deck.summaryCollapsed;
          summaryCollapsedFromToggle = !summaryCollapsedFromToggle;
          $timeout(function () {
            angular.element($window).trigger('resize');
          }, 400);

          // Close the summaryBarDetails card if it is opened.
          if($scope.deck.summaryCollapsed) {
            var cardId = "summaryBarDetails";
            var sumBarDetailsCardObj = _.find($scope.deckster.$cardHash, function (cardObj) {
              return cardObj.options.id == cardId;
            });
            if(!_.isUndefined(sumBarDetailsCardObj)) {
              sumBarDetailsCardObj.options.closeCard(sumBarDetailsCardObj, sidebarDeck);
            }
          }

        };


        this.checkWindowWidth = function () {
          var window = angular.element($window);
          if (window.width() < 480 && !$scope.deck.summaryCollapsed && !summaryCollapsedFromToggle) {
            $scope.deck.summaryCollapsed = true;
          } else if (window.width() >= 480 && $scope.deck.summaryCollapsed && !summaryCollapsedFromToggle) {
            $scope.deck.summaryCollapsed = false;
          }
        };


        this.addCard = function (card, callback) {
          $scope.deckster.addCard(card, function (card) {
            callback && callback(card);
          });
        };

        $scope.$on('deckster:resize', function () {
          if ($scope.deckster) {
            $timeout(function () {
              $scope.deckster.$gridster.recalculate_faux_grid();
            });
          }
        });

        this.init = function (element, opts) {
          $scope.deckster = $(element).deckster(opts).data('deckster');
          $scope.deckInitialized = true;
          this.checkWindowWidth();
        };
      },
      link: function (scope, element, attrs, ctrl) {
        if (DeckFilterManager.getFilter()) {
          scope.deck.selectedFilters = DeckFilterManager.getFilter();
          _.forEach(scope.deck.filters, function(filter) {
            if (filter.checkboxes) {
              _.forEach(filter.checkboxes, function(option) {
                option.selected = scope.deck.selectedFilters[filter.name];
              });
            }
          });
        }

        if (DeckFilterManager.getDateFilter()) {
          scope.deck.startDate = DeckFilterManager.getDateFilter().start;
          scope.deck.endDate = DeckFilterManager.getDateFilter().end;
        }
        else {
          DeckFilterManager.setDateFilter({
            start: scope.deck.startDate,
            end: scope.deck.endDate,
            maxEndDate: scope.deck.maxEndDate
          });
        }

        var deckOptions = $.extend(true, {}, defaults, scope.deck);
        var $deckEl = $(element).find('.deckster-deck');
        scope.$watch('initialized', function(init) {
          if (init && !scope.deckInitialized) {
            ctrl.init($deckEl, deckOptions);
          }
        });

        angular.element($window).resize(_.debounce(ctrl.checkWindowWidth, 200));

        scope.$on('$destroy', function() {
          scope.deckster.destroy();
          scope.deckInitialized = false;
        });



        SyncCharts.createCountdownLatchAwaits(scope, deckOptions);

      }
    };
  });
