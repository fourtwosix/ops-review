'use strict';

angular.module('opsReviewApp')
  .directive('decksterPopout', function ($injector, $compile, $http, Deckster) {
    return {
      restrict: 'E',
      link: function(scope, element) {
        var cardId, section;

        var $routeParams = $injector.get('$routeParams');
        cardId = $routeParams.id;
        section = $routeParams.section;


        var getSummaryTemplate = function(cardConfig, cb) {
          // Not using the cardConfig here but you could use it to make request
          $http.get('partials/testSummaryCard.html').success(function(html) {
            cb && cb($compile(html)(scope));
          });
        };

        var getDetailsTemplate = function(cardConfig, cb) {
          // Not using the cardConfig here but you could use it to make request
          $http.get('partials/testDetailsCard.html').success(function (html) {
            cb && cb($compile(html)(scope));
          });
        };

        Deckster.generatePopout(element, cardConfig, section);
      }
    };
  });
