'use strict';

angular.module('opsReviewApp')
  .factory('Deckster', function () {
    return window.Deckster;
  });
