'use strict';

angular.module('opsReviewApp')
  .directive('decksterSummaryBar', function ($timeout, $uibModal, CardBuilder, CardManager, DateUtils) {
    return {
      restrict: 'E',
      require: ['^decksterDeck', 'decksterSummaryBar'],
      replace: true,
      templateUrl: 'components/deckster/decksterSummaryBar/decksterSummaryBar.html',
      controller: function($scope, $timeout, DataManager) {
        var self = this;
        var sidebarData = {};
        var origFilters = _.cloneDeep($scope.deck.summaryBar.filters);
        $scope.loaded = false;

        $scope.getValue = function (column) {
          return sidebarData[column] || 'N/A'
        };

        $scope.$on("deck:filtered", function () {
          self.loadStats();
        });

        $scope.onSummaryCellClick = function(header, stat) {
          if(stat.details && sidebarData[stat.column]) {
            var cardId = "summaryBarDetails";
            var statTitle = stat.details.title ? stat.details.title : "KPI Summary Details (" + header + ": " + stat.name + ")";

            // Try to find the card in the deck
            var sumBarDetailsCardObj = _.find($scope.deckster.$cardHash, function(cardObj) {
              return cardObj.options.id == cardId;
            });
            // If the card wasn't found, fetch the card's JSON from the server, build the card, and add it to the deck.
            if(_.isUndefined(sumBarDetailsCardObj)) {
              CardManager.getCard(cardId).then(function(cardJSON) {
                cardJSON.title = statTitle;
                cardJSON.summaryViewType = stat.details.type || 'table';
                cardJSON.summaryViewOptions = angular.extend(cardJSON.summaryViewOptions, stat.details);
                var card = CardBuilder.build(cardJSON);
                $scope.deck.cards.splice(0, 0, card);
              });
            }
            // Otherwise, clear the card's query and load the proper queryTemplate, then reload the card.
            else {
              var sidebarCard = _.find($scope.deck.cards, function(deckCard) {
                return deckCard.id == cardId;
              });

              sumBarDetailsCardObj.options.title = statTitle;
              sidebarCard.title = statTitle;
              sumBarDetailsCardObj.options.summaryViewOptions = angular.extend(sumBarDetailsCardObj.options.summaryViewOptions, stat.details);
              sumBarDetailsCardObj.reloadContent();
            }
          }
        };

        function skipQuery() {
          return $scope.deck.id === "programs" &&
            ($scope.deck.summaryBar.filters.hasOwnProperty("l3_portfolio") ||
            $scope.deck.summaryBar.filters.hasOwnProperty("staffing_source") ||
            $scope.deck.summaryBar.filters.hasOwnProperty("pra"));
        }

        this.loadStats = function () {
          if (skipQuery()) {
            // we just want to show "N/A" for the data
            sidebarData = {};
            return;
          }

          DataManager.loadSummaryBarData($scope.deck, function (data) {
            if (data.length > 0) {
              sidebarData = data[0];
            } else {
              sidebarData = data;
            }

            $timeout(function () {
              $scope.loaded = true;
            });
          });
        };

        this.loadStats();
      },
      link: function(scope, element) {

        // Expose kebab case translation to the HTML
        scope.kebabCase = function(val) {
          return _.kebabCase(val);
        };

        // Dynamically sets the width of the main header table based on the number of tables that were defined
        scope.headers = _.keys(scope.deck.summaryBar.bars);
        var headerPercent = (Math.round((100 / scope.headers.length) * 100) / 100) + "%";
        $timeout(function() {
          element.find('table.summarybar-group').css('width', headerPercent);
        });

        // Dynamically sets the width of each stat cell based on the number of cells that were defined
        _.forEach(scope.headers, function(header) {
          scope.stats = scope.deck.summaryBar.bars[header];
          var statPercent = (Math.round((100 / scope.stats.length) * 100) / 100) + "%";
          _.forEach(scope.stats, function(stat) {
            $timeout(function() {
              element.find('table.summarybar-group td.summarybar-stat-' + _.kebabCase(header) + '-' + _.kebabCase(stat.name)).css('width', statPercent);
            });
          });
        });
      }
    };
  });

