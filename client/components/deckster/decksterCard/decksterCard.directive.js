'use strict';

angular.module('opsReviewApp')
  .directive('decksterCard', function ($parse, $q, $http, $timeout, $interval, $window, $cookieStore, FilterService, SyncCharts) {
    return {
      restrict: 'E',
      require: ['^decksterDeck', 'decksterCard'],
      controller: function ($scope, $compile, $uibModal) {
        this.getSummaryContent = function (card, cb) {
          $timeout(function() {
            cb($compile('<div></div>')($scope));
          });
        };

        this.getDetailsContent = function (card, cb) {
          $timeout(function() {
            cb($compile('<div></div>')($scope));
          });
        };

        this.getLeftControlsContent = function (card, cb) {
          $timeout(function() {
            cb($compile('<div ng-include="\'components/deckster/decksterCard/partials/left-controls.html\'" style="height: 100%; width: 100%;"></div>')($scope));
          });
        };

        this.getRightControlsContent = function (card, cb) {
          $timeout(function() {

            var controls = "";

            var cardOptions = card.options.getCurrentViewOptions(card.currentSection);
            if(!_.isUndefined(cardOptions.controls) && !_.isUndefined(cardOptions.controls.rightControlsContent)) {
              // This will ensure its an array
              var controlsContent = _.flatten([cardOptions.controls.rightControlsContent]);

              controls = _.map(controlsContent, function (controlContent) {
                return _.template('<span ng-include="\'<%= control %>\'"></span>')({control: controlContent});
              }).join(" ");
            }

            controls += " " + '<span ng-include="\'components/deckster/decksterCard/partials/right-controls.html\'"></span>';

            controls = _.template('<div style="height: 100%; width: 100%;"> <%= controls %> </div>')({controls: controls});
            cb($compile(controls)($scope));
          });
        };

        this.getCenterControlsContent = function (card, cb) {
          $timeout(function() {

            var controls = "";

            var cardOptions = card.options.getCurrentViewOptions(card.currentSection);
            if(!_.isUndefined(cardOptions.controls) && !_.isUndefined(cardOptions.controls.centerControlsContent)) {
              // This will ensure its an array
              var controlsContent = _.flatten([cardOptions.controls.centerControlsContent]);

              controls = _.map(controlsContent, function (controlContent) {
                return _.template('<span ng-include="\'<%= control %>\'"></span>')({control: controlContent});
              }).join(" ");
            }

            controls += " " + '<span ng-include="\'components/deckster/decksterCard/partials/center-controls.html\'" style="position: absolute;"></span>';

            controls = _.template('<div style="height: 100%; width: 100%;"> <%= controls %> </div>')({controls: controls});
            cb($compile(controls)($scope));
          });
        };

        this.onReload = function (card) {
          console.log('card reloaded', card)
        };

        this.onResize = function (card) {
          console.log('card resized', card);
        };

        this.onExpand = function (card)  {
          console.log("card expanded", card);
        };

        this.scrollToCard = function () {
          $scope.card.scrollToCard();
        };

        this.toggleCard = function () {
          $scope.card.hidden ? $scope.card.showCard() : $scope.card.hideCard();
        };

        this.downloadData = function (url, filename) {
          var body = angular.element('body');
          var iFrame = body.find("#downloadiframe");

          if (!(iFrame && iFrame.length > 0)) {
            iFrame = angular.element ("<iframe id='downloadiframe' style='position:fixed;display:none;top:-100px;left:-100px;'/>");
            body.append(iFrame);
          }

          var manageIframeProgress = function() {
            var defer = $q.defer();
            // notify that the download is in progress every half a second / do this for a maximum of 50 intervals
            var promise = $interval(function () {
              $http.get('/api/data/checkDownload', {params: {filename: filename}}).then(function (response) {
                if (!response.data) {
                  $interval.cancel(promise);
                }
              });
            }, 1000);

            promise.then(defer.reject, defer.resolve, defer.notify);

            promise.finally(function () {
              iFrame.remove();
            });

            return defer.promise;
          };

          iFrame.attr("src", url);

          return manageIframeProgress();
        };

        this.setUpCard = function (cardOpts) {
          if(!cardOpts.summaryViewType && !cardOpts.detailsViewType) {
            cardOpts.summaryContentHtml = this.getSummaryContent;
            cardOpts.detailsContentHtml = this.getDetailsContent;
            cardOpts.onResize = this.onResize;
            cardOpts.onReload = this.onReload;
          }

          cardOpts.showFooter = false;
          cardOpts.leftControlsHtml = this.getLeftControlsContent;
          cardOpts.rightControlsHtml = this.getRightControlsContent;
          cardOpts.centerControlsHtml = this.getCenterControlsContent;

          $scope.$on('deckster-card:scrollto-' + cardOpts.id, this.scrollToCard);
          $scope.$on('deckster-card:toggle-' + cardOpts.id, this.toggleCard);

          return cardOpts;
        };

        $scope.cardHelp = function () {
          var modal = $uibModal.open({
            templateUrl: 'components/cardHelp/cardHelpModal.html',
            controller: 'CardHelpCtrl',
            backdrop: true,
            windowClass: 'minimal-modal',
            size: 'help',
            resolve: {
              card: function () {
                return $scope.card; //angular.copy($scope.card);
              }
            }
          });
        };
      },
      link: function (scope, element, attrs, ctrls) {
        var deckCtrl = ctrls[0];
        var cardCtrl = ctrls[1];

        var cardOpts = $parse(attrs.cardOptions || {})(scope);

        scope.$watch('deckInitialized', function(initialized) {
          if(initialized) {
            deckCtrl.addCard(cardCtrl.setUpCard(cardOpts), function(card) {
              scope.card = card;
              scope.card.options.setLastUpdated(cardOpts);

              card.downloadData = cardCtrl.downloadData;

              // When the deck is resize resize this card as well
              scope.$on('deckster:resize', function () {
                $timeout(function () {
                  scope.card.resizeCardViews();
                });
              });

              scope.$on('deckster:redraw', function () {
                $timeout(function () {
                  scope.card.options.reloadView(scope.card);
                });
              });

              // handle apply filters to the query for each card
              scope.$on("deck:filtered", function (event, filters) {
                if (card.options.customFilterCard) {
                  card.options.customFilterCard(card, filters);
                } else {
                  FilterService.createFilteredQuery(card, filters, {deckFiltered: true});
                }
                SyncCharts.applyCountdownLatch(scope.card);
              });

              $($window).on('resize', _.debounce(function () {
                $timeout(function () {
                  scope.card.resizeCardViews();
                });
              }, 200));

              SyncCharts.applyCountdownLatch(scope.card);

            });
          }
        });
      }
    }
  });
