// quadChart.js
// "Chart" kinda implies a graph; not this time.  The quadChart view
// is a table-like resentation using HTML H2 and DIV elements.

;
(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function ($, Deckster) {
  
  Deckster.views = Deckster.views || {};

  var $quadChartElementContainer;
  var viewIdSuffix = '-quadchart'; // ID will be cardName-cardSection-quadchart

  // View configuration for quadChart
  Deckster.views.quadChart = {


    // ############################################################
    getContentHtml: function (card, cb) {

      var currentView = card.currentSection;
      var baseId      = card.options.id + '-' + currentView + viewIdSuffix;
      var viewId      = currentView + 'ViewId';

      // Create a unique id that we can use for the view container
      card[viewId] = baseId;

      var viewOptions      = card.options.getCurrentViewOptions(currentView);
      var quadChartMapping = viewOptions.quadChartMapping || {};

      var header      = quadChartMapping.header       || {};
      var topLeft     = quadChartMapping.topLeft      || {};
      var topRight    = quadChartMapping.topRight     || {};
      var bottomLeft  = quadChartMapping.bottomLeft   || {};
      var bottomRight = quadChartMapping.bottomRight  || {};
      var footer      = quadChartMapping.footer       || {};

      var placeHolder = '';

      // start quad-chart
      var quadChartContainerHtml =
        '<div id="' + 
           baseId +
        '" class="container quad-chart" style="display: none">';

      // define header
      quadChartContainerHtml +=
        '<div class="row quad-chart-header-row">' +
        '<div class="quad-chart-header-column">' +        
        '<h2 id="' +
          baseId + '-header-title' +
        '" style="' +
          (header.title.style || '') + 
        '">' + 
          (header.title.value || 'header Title') +
        '</h2>' +
        '<div id="' +
          baseId + '-header-data' + 
        '" class="col-md-12 quad-chart-header-data" style="' +
          (header.data.style || '') + 
        '">' +
          (header.data.value || 'header content') +
        '</div></div></div>';

      // start top-row and define top-left 
      quadChartContainerHtml +=
        '<div class="row quad-chart-top-row">' +
        '<div class="col-md-6 quad-chart-top-left">' +
        '<h2 id="' +
          baseId + '-top-left-title' +
        '" class="quad-chart-top-left-title" style="' +
          (topLeft.title.style || '') + 
        '">' +
          (topLeft.title.value || 'topLeft Title') +
        '</h2>' +
        '<div id="' + 
          baseId + '-top-left-data' +
        '" class="quad-chart-top-left-data" style="' +
          (topLeft.data.style || '') + 
        '">' +
          placeHolder + 
        '</div></div>';

      // top-right and finish top-row DIV
      quadChartContainerHtml +=
        '<div class="col-md-6 quad-chart-top-right">' +
          '<h2 id="' +
            baseId + '-top-right-title' + 
          '" class="quad-chart-top-right-title" style="' +
            (topRight.title.style || '') + 
          '">' +
            (topRight.title.value || 'topRight Title') +
          '</h2>' +
          '<div id="' + 
            baseId + '-top-right-data' +
          '" class="quad-chart-top-right-data" style="' +
            (topRight.data.style || '') + 
          '">' +
            placeHolder +
          '</div></div></div>'; 

      // start bottom-row and define bottom-left
      quadChartContainerHtml +=
        '<div class="row quad-chart-bottom-row">' +
        '<div class="col-md-6 quad-chart-bottom-left">' +
          '<h2 id="' +
            baseId + '-bottom-left-title' +
          '" class="quad-chart-bottom-left-title" style="' +
            (bottomLeft.title.style || '') + 
          '">' +
            (bottomLeft.title.value || 'bottomLeft Title') +
          '</h2>' +
          '<div id="' + 
            baseId + '-bottom-left-data' +
          '" class="quad-chart-bottom-left-data" style="' +
            (bottomLeft.data.style || '') + 
          '">' +
            placeHolder +
          '</div></div>';

      // define bottom-right and finish bottom-row
      quadChartContainerHtml +=
        '<div class="col-md-6 quad-chart-bottom-right">' +
        '<h2 id="' +
          baseId + '-bottom-right-title' +
        '" class="quad-chart-bottom-right-title" style="' +
          (bottomRight.title.style || '') + 
        '">' +
          (bottomRight.title.value || 'bottomRight Title') +
        '</h2>' +
        '<div id="' + 
          baseId + '-bottom-right-data' + 
        '" class="quad-chart-bottom-right-data" style="' +
          (bottomRight.data.style || '') + 
        '">' +
          placeHolder +
        '</div></div></div>';

      // define footer and finish quad-chart
      quadChartContainerHtml +=
        '<div class="row quad-chart-footer-row">' +
        '<div class="col-md-12 quad-chart-footer-column">' +
          '<h2 id="' + 
            baseId + '-footer-title' +
          '" class="quad-chart-footer-title" style="' +
            (footer.title.style || '') + 
          '">' +
            (footer.title.value || 'footer Title') +
          '</h2>' +
          '<div id="' + 
            baseId + '-footer-data' +
          '" class="quad-chart-footer-data" style="' +
            (footer.data.style || '') + 
          '">' +
            placeHolder +
          '</div></div></div></div>';

      cb(quadChartContainerHtml);
    }, // end of getContentHtml: function (card, cb)


    // ############################################################
    // SMELL: don't think this function is used
    init: function (card, section, options) {

      var baseId      = card.options.id + '-' + section + viewIdSuffix;

      var $quadChartElement = $('#' + baseId);
      var viewOptions = card.options.getCurrentViewOptions(section);

      if($quadChartElement.length != 0) {
        if (viewOptions.message && viewOptions.showMessage(card)) {
          $quadChartElement.hide();
          card.showMessage(viewOptions.message);
        } else {
          card.hideMessage();
          $quadChartElement.show();
        }
      }

    }, // end of init: function (card, section, options)


    // ############################################################
    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function (card, section) {

      var baseId            = card[section + 'ViewId'];
      var viewOptions       = card.options.getCurrentViewOptions(section);
      var quadChartMapping  = viewOptions.quadChartMapping;

      // overloading the variable name; in this function these variable
      // names contain the name of the column from results to insert into
      // the proper area of the table.

      var header      = quadChartMapping.header.data.column;
      var topLeft     = quadChartMapping.topLeft.data.column;
      var topRight    = quadChartMapping.topRight.data.column;
      var bottomLeft  = quadChartMapping.bottomLeft.data.column;
      var bottomRight = quadChartMapping.bottomRight.data.column;
      var footer      = quadChartMapping.footer.data.column;

      //Set card up for drilldown implementation
      if (card.options.getCurrentViewType(section) === "drilldownView") {
        var $drilldownEl = $('#' + card[section + "DrilldownViewId"]);
        this.getContentHtml(card, function (tableHtml) {
          $drilldownEl.append(tableHtml);
        });
      };

      // SMELL: don't think these are used
      var $card     = $('#' + card.options.id);
      var $quadChartElement  = $('#' + baseId);

      // setup some jQuery elements

      var $headerTitle        = $('#' + baseId  + '-header-title');
      var $headerData         = $('#' + baseId  + '-header-data');

      var $topRow             = $('#' + baseId  + '-top-row');

      var $topLeft            = $('#' + baseId  + '-top-left');
      var $topLeftTitle       = $('#' + baseId  + '-top-left-title');
      var $topLeftData        = $('#' + baseId  + '-top-left-data');

      var $topRight           = $('#' + baseId  + '-top-right');
      var $topRightTitle      = $('#' + baseId  + '-top-right-title');
      var $topRightData       = $('#' + baseId  + '-top-right-data');

      var $bottomRow          = $('#' + baseId  + '-bottom-row');

      var $bottomLeft         = $('#' + baseId  + '-bottom-left');
      var $bottomLeftTitle    = $('#' + baseId  + '-bottom-left-title');
      var $bottomLeftData     = $('#' + baseId  + '-bottom-left-data');

      var $bottomRight        = $('#' + baseId  + '-bottom-right');
      var $bottomRightTitle   = $('#' + baseId  + '-bottom-right-title');
      var $bottomRightData    = $('#' + baseId  + '-bottom-right-data');

      var $footerTitle        = $('#' + baseId  + '-footer-title');
      var $footerData         = $('#' + baseId  + '-footer-data');

      // SMELL: why do we need to know this?
      $quadChartElementContainer = $quadChartElement.parents().eq(1);

      if ($quadChartElement.length != 0) {

        card.options.loadData(card, function (dbResultTransformed) {

          if (viewOptions.message && viewOptions.showMessage(card)) {
            $quadChartElement.hide();
            card.showMessage(viewOptions.message);
          } else {
            card.hideMessage();
            $quadChartElement.show();
          }

          if (0 == dbResultTransformed.data.length) {
            $headerData.html(       'There is no data available');
            $topLeftData.html(      'There is no data available');
            $topRightData.html(     'There is no data available');
            $bottomLeftData.html(   'There is no data available');
            $bottomRightData.html(  'There is no data available');
            $footerData.html(       'There is no data available');
          } else {
            $headerData.html(dbResultTransformed.data[0][header]);
            $topLeftData.html(dbResultTransformed.data[0][topLeft]);
            $topRightData.html(dbResultTransformed.data[0][topRight]);
            $bottomLeftData.html(dbResultTransformed.data[0][bottomLeft]);
            $bottomRightData.html(dbResultTransformed.data[0][bottomRight]);
            $footerData.html(dbResultTransformed.data[0][footer]);
          }


          // Create a new chart
          Deckster.views.quadChart.init(card, section, viewOptions);


        }); // end card.options.loadData(card, function (dbResultTransformed)
      } // end if ($quadChartElement.length != 0)
    }, // end of onLoad: function (card, section)


    // ############################################################
    resize: function (card, section) {
      if (card[section + 'Table']) {
        card[section + 'Table'].bootstrapTable("resetView", {
          height: $quadChartElementContainer.height()
        });
      }
    } // end of resize: function (card, section)

  }; // end of Deckster.views.quadChart = {

})); // end of the main thing from line #11 (this, function ($, Deckster)

