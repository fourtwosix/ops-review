;
(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function ($, Deckster) {
  Deckster.views = Deckster.views || {};

  var viewIdSuffix = "-area-chart";
  var chartSuffix = "AreaChart";
  var chartIdSuffix = chartSuffix + "Id";

  // View configuration for areaChart
  Deckster.views.areaChart = {
    getContentHtml: function (card, cb) {
      var currentView = card.currentSection;
      // Create an unique id that we can use for the chart container
      card[currentView + chartIdSuffix] = card.options.id + card.currentSection + viewIdSuffix;

      // Send back the html (chart container) for this view
      cb('<div id="' + card[currentView + chartIdSuffix] + '" style="height: 100%;"></div>');
    },

    init: function (card, section, options) {
      var $chartEl = $('#' + card[section + chartIdSuffix]);

      // If the chart container exist initialize a chart object
      if ($chartEl.length != 0) {
        if (card[section + chartSuffix]) {
          card[section + chartSuffix].destroy();
          card[section + chartSuffix] = null;
        }

        // Create a new chart
        card[section + chartSuffix] = new Highcharts.Chart(options);

        // Handles the x-axis label click
        $('.highcharts-axis-labels text, .highcharts-axis-labels span').click(function () {
          var viewOptions = card.options.getCurrentViewOptions(section);
          if (viewOptions.onXAxisLabelClick) {
            viewOptions.onXAxisLabelClick(card, this);
          }
        });
      }
    },

    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function (card, section) {
      var viewOptions = card.options.getCurrentViewOptions(section);

      //Set card up for drilldown implementation
      if (card.options.getCurrentViewType(section) === "drilldownView") {
        var $drilldownEl = $('#' + card[section + "DrilldownViewId"]);
        this.getContentHtml(card, function (tableHtml) {
          $drilldownEl.append(tableHtml)
        })
      }

      var $chartEl = $('#' + card[section + chartIdSuffix]);


      // If the chart container exist initialize a chart object
      if ($chartEl.length != 0) {
        card.options.loadData(card, function (data) {

          // Destroy previously created chart
          if (card[section + chartSuffix]) {
            card[section + chartSuffix].destroy();
            card[section + chartSuffix] = null;
          }

          if (viewOptions.seriesOrdering) {

            var indexes = {};
            _.each(viewOptions.seriesOrdering, function (series, index) {
              indexes[series] = index;
            });

            data.series = _.sortBy(data.series, function (series) {
              return indexes[series.name];
            })
          }

          var chartOptions = {
            chart: {
              type: 'area',
              renderTo: card[section + chartIdSuffix],
              events: {
                click: function () {
                  if (viewOptions.onAreaClick) {
                    viewOptions.onAreaClick(card, this);
                  }
                }
              },
              className: 'deckster-chart'
            },
            title: {
              text: null
            },
            subtitle: {
              text: viewOptions.subtitle || null
            },
            xAxis: {
              categories: data.categories || [],
              title: {
                text: viewOptions.xTitle || null
              }
            },
            yAxis: {
              min: 0,
              title: {
                text: viewOptions.yTitle || null,
                align: 'middle'
              },
              labels: {
                overflow: 'justify'
              }
            },
            tooltip: {
              valueSuffix: null
            },
            plotOptions: viewOptions.plotOptions || {
              area: {
                marker: {
                  enabled: false,
                  symbol: 'circle',
                  radius: 2,
                  states: {
                    hover: {
                      enabled: true
                    }
                  }
                }
              },
              series: {
                cursor: 'pointer',
                point: {
                  events: {
                    click: function () {
                      if (viewOptions.onAreaClick) {
                        viewOptions.onAreaClick(card, this);
                      }
                    }
                  }
                }
              }
            },
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom',
              borderWidth: 0
            },
            credits: {
              enabled: false
            },
            series: data.series
          };

          if(!_.isUndefined(viewOptions.highchartOptions)) {
            _.merge(chartOptions, viewOptions.highchartOptions);
          }

          if(!_.isUndefined(viewOptions.syncChartsZoom)) {
            viewOptions.syncChartsZoom(card, chartOptions);
          }

          // Create a new chart
          Deckster.views.areaChart.init(card, section, chartOptions);
        });
      }
    },
    resize: function (card, section) {
      var chart = card[section + chartSuffix];
      if (chart) {
        chart.reflow();
      }
    },
    reload: function (card, section) {
      var chart = card[section + chartSuffix];

      if (chart) {
        var options = chart.userOptions;

        chart.destroy();
        card[section + chartSuffix] = null;
        Deckster.views.areaChart.init(card, section, options);
      }
    }
  };
}));
