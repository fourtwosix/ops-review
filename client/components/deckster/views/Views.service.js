'use strict';

angular.module('opsReviewApp')
  .factory('ViewsService', function (FilterService) {

    var ViewsService = {};

    // GeoMap services

    ViewsService.mapControlFilter = function (card) {
      var templateVariables = card.options.getTemplateVariables();
      var currentViewOptions = card.options.getCurrentViewOptions(card.currentSection);
      currentViewOptions.controls.selectedFilter = this.name;
      var filters = _.merge(templateVariables, this.clauses);
      FilterService.createFilteredQuery(card, filters);
    };

    ViewsService.displayMapLayer = function (card, mapLayer) {
      var mapSuffix = "GeoMap";
      var map = card[card.currentSection + mapSuffix];
      card.options.getCurrentViewOptions(card.currentSection).controls.selectedMapLayer = mapLayer;
      map.toggleLayer(mapLayer, map);
    };

    // Histogram services

    ViewsService.getHistogramControls = function () {
      return {
        rightControlsContent: ['components/deckster/views/histogramChart/histogramChart-controls.html'],
        selectedFilter: 10,
        changeBinNumber: function (card, binNumber) {
          var templateVariables = card.options.getTemplateVariables();
          var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
          viewOptions.controls.selectedFilter = binNumber;
          templateVariables.binClause = binNumber;
          FilterService.createFilteredQuery(card, templateVariables);
        }
      }
    };

    ViewsService.parseHistogramRanges = function(range) {
      var returnRange = {};
      if(range.indexOf('&lt;') > -1) {
        //If front range - has a < (<#)
        returnRange.lower = range.split('&lt; ')[1];
      }
      else if(range.indexOf('<') > -1) {
        //If front range - has a < (<#)
        returnRange.lower = range.split('< ')[1];
      }
      else if(range.indexOf('&gt;') > -1) {
        //If end range - has a > (>#)
        returnRange.upper = range.split('&gt; ')[1];
      }
      else if(range.indexOf('>') > -1) {
        //If end range - has a > (>#)
        returnRange.upper = range.split('> ')[1];
      }
      else if(range.indexOf('-') > -1) {
        var rangeSplit = range.split('-');
        switch (rangeSplit.length) {
          case 2:
            returnRange.lower = rangeSplit[0];
            returnRange.upper = rangeSplit[1];
            break;
          case 3:
            returnRange.lower = "-" + rangeSplit[1];
            returnRange.upper = rangeSplit[2];
            break;
          case 4:
            returnRange.lower = "-" + rangeSplit[1];
            returnRange.upper = "-" + rangeSplit[3];
            break;
        }
      }
      return returnRange;
    };

    ViewsService.getReqTypeFromClause = function(reqClause) {
      var reqType = '';
      if(reqClause != "true") {
        reqType = reqClause.match("'([^']*)'")[1];
      }
      return reqType;
    };

    ViewsService.getRangeValuesFromClause = function(rangeClause) {
      var rangeValues = '';
      rangeClause = rangeClause.toLowerCase();
      if(rangeClause != "true") {
        // If it is evaluating using a BETWEEN clause
        if(_.includes(rangeClause, "between")) {
          rangeValues = rangeClause.match(/(-*)(\d+)/g);
          rangeValues = rangeValues.join().match(/(-*)(\d+)/g).join('-');
        }
        // Otherwise it is evaluating with operators
        else {
          rangeValues = rangeClause.match(/((>=|<=|>|<)(\s*)(-*)(\d+))/g);
          if(rangeValues.length == 2) {
            rangeValues = rangeValues.join().match(/(-*)(\d+)/g).join('-');
          }
        }
      }
      return rangeValues;
    };

    return ViewsService;
  });
