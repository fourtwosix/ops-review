;(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function($, Deckster){
  Deckster.views = Deckster.views || {};

  var viewIdSuffix = "-line-chart";
  var chartSuffix = "LineChart";
  var chartIdSuffix = chartSuffix + "Id";

  // View configuration for lineChart
  Deckster.views.lineChart = {
    getContentHtml: function (card, cb) {
      var currentView = card.currentSection;
      // Create an unique id that we can use for the chart container
      card[currentView + chartIdSuffix] = card.options.id + card.currentSection + viewIdSuffix;

      // Send back the html (chart container) for this view
      cb('<div id="' + card[currentView + chartIdSuffix] + '" style="height: 100%;"></div>');
    },

    init: function (card, section, options) {
      var $chartEl = $('#' + card[section + chartIdSuffix]);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        if (card[section + chartSuffix]) {
          card[section + chartSuffix].destroy();
          card[section + chartSuffix] = null;
        }

        // Create a new chart
        card[section + chartSuffix] = new Highcharts.Chart(options);
      }
    },

    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function(card, section) {
      var viewOptions = card.options.getCurrentViewOptions(section);

      //Set card up for drilldown implementation
      if(card.options.getCurrentViewType(section) === "drilldownView") {
        var $drilldownEl = $('#' + card[section + "DrilldownViewId"]);
        this.getContentHtml(card, function(tableHtml) {
          $drilldownEl.append(tableHtml);
        });
      }

      var $chartEl = $('#' + card[section + chartIdSuffix]);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        card.options.loadData(card, function(series) {

          // Destroy previously created chart
          if (card[section + chartSuffix]) {
            card[section + chartSuffix].destroy();
            card[section + chartSuffix] = null;
          }

          // Set types for series
          if (viewOptions.seriesTypes) {
            _.each(series.series, function (obj) {
              if (viewOptions.seriesTypes[obj.name]) {
                obj.type = viewOptions.seriesTypes[obj.name];
              }
              else {
                obj.type = "line";
                obj.zIndex = 1;
              }
            });
          }

          var plotOptions = viewOptions.plotOptions || {};
          _.set(plotOptions, 'line.point.events.click', function () {
            if (viewOptions.onLineClick) {
              viewOptions.onLineClick(card, this);
            }
          });

          _.set(plotOptions, 'column.point.events.click', function () {
            if (viewOptions.onColumnClick) {
              viewOptions.onColumnClick(card, this);
            }
          });

          var chartOptions = {
            chart: {
              type: null,
              renderTo: card[section + chartIdSuffix],
              className: 'deckster-chart',
              zoomType: viewOptions.zoomType
            },
            title: {
              text: null
            },
            subtitle: {
              text: viewOptions.subtitle || null
            },
            xAxis: {
              categories: series.categories || [],
              title: {
                text: viewOptions.xTitle || null
              },
              labels: {
                formatter: function () {
                  if(viewOptions.xAxisFormatter) {
                    return viewOptions.xAxisFormatter.call(this, card);
                  } else {
                    return this.value;
                  }
                }
              },
              plotLines: viewOptions.xAxisPlotLines || []
            },
            yAxis: {
              title: {
                text: viewOptions.yTitle || null
              },
              labels: {
                formatter: function () {
                  if(viewOptions.yAxisFormatter) {
                    return viewOptions.yAxisFormatter.call(this, card);
                  } else {
                    return this.value;
                  }
                }
              },
              allowDecimals: _.get(viewOptions, 'yAxis.allowDecimals', true),
              plotLines: viewOptions.yAxisPlotLines || []
            },
            tooltip: {
              enabled: !_.isUndefined(viewOptions.tooltipEnabled) ? viewOptions.tooltipEnabled : true,
              formatter: viewOptions.tooltipFormatter
            },
            plotOptions: plotOptions,
            credits: {
              enabled: false
            },
            legend: {
              enabled: !_.isUndefined(viewOptions.legendEnabled) ? viewOptions.legendEnabled : true,
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            },
            series: series.series || [] // This is the data passed in by the loadData function
          };

          if(!_.isUndefined(viewOptions.highchartOptions)) {
            _.merge(chartOptions, viewOptions.highchartOptions);
          }

          if(!_.isUndefined(viewOptions.syncChartsZoom)) {
            viewOptions.syncChartsZoom(card, chartOptions);
          }

          // Create a new chart
          Deckster.views.lineChart.init(card, section, chartOptions);
        });
      }
    },
    resize: function(card, section) {
      var chart = card[section + chartSuffix];
      if(chart) {
        chart.reflow();
      }
    },
    reload: function (card, section) {
      var chart = card[section + chartSuffix];

      if (chart) {
        var options = chart.userOptions;

        chart.destroy();
        card[section + chartSuffix] = null;
        Deckster.views.lineChart.init(card, section, options);
      }
    }
  };
}));
