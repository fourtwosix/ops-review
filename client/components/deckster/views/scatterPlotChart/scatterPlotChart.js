;(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function($, Deckster){
  Deckster.views = Deckster.views || {};

  var viewIdSuffix = "-scatterPlot-chart";
  var chartSuffix = "scatterPlotChart";
  var chartIdSuffix = chartSuffix + "Id";

  // View configuration for lineChart
  Deckster.views.scatterPlotChart = {
    getContentHtml: function (card, cb) {
      var currentView = card.currentSection;
      // Create an unique id that we can use for the chart container
      card[currentView + chartIdSuffix] = card.options.id + card.currentSection + viewIdSuffix;

      // Send back the html (chart container) for this view
      cb('<div id="' + card[currentView + chartIdSuffix] + '" style="height: 100%;"></div>');
    },

    init: function (card, section, options) {
      var $chartEl = $('#' + card[section + chartIdSuffix]);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        if (card[section + chartSuffix]) {
          card[section + chartSuffix].destroy();
          card[section + chartSuffix] = null;
        }

        // Create a new chart
        card[section + chartSuffix] = new Highcharts.Chart(options, function (chart) {
            // Check option to see if we should add custom color legend
            if (card.options.summaryViewOptions.addColorLegend) {
              var options = chart.options.legend;

              // Create the legend box
              var $legend = $('<div>')
                .css({
                  maxHeight: 300,
                  padding: 10,
                  position: 'absolute',
                  overflow: 'auto',
                  right: 10,
                  top: 40
                })
                .appendTo(chart.container);


              _.each(card.options.summaryViewOptions.legendSymbols, function(symbol, key) {
                // Create the legend item
                var $legendItem = $('<div>')
                  .css({
                    position: 'relative',
                    marginLeft: 20,
                    fontFamily: "'Lucida Grande', 'Lucida Sans Unicode', Arial, Helvetica, sans-serif"
                  })
                  .css(options['itemStyle'])
                  .css({cursor: 'default'})
                  .html(key)
                  .appendTo($legend);

                // Create the circle with each series color
                var $symbol = $('<svg version="1.1" xmlns="http://www.w3.org/2000/svg">')
                  .css({
                    position: 'absolute',
                    left: -20,
                    top: 3,
                    height: 10,
                    width: 10
                  })
                  .append($(chart.renderer.symbol(symbol, 0, 0, 10, 10).element).css({
                    fill: "#000000"
                  }))
                  .appendTo($legendItem);
              });
            }
          });
      }
    },


    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function(card, section) {
      var viewOptions = card.options[section + 'ViewOptions'];

      var $chartEl = $('#' + card[section + chartIdSuffix]);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        card.options.loadData(card, function(series) {

          // Destroy previously created chart
          if (card[section + chartSuffix]) {
            card[section + chartSuffix].destroy();
            card[section + chartSuffix] = null;
          }

          // Set types for series
          if (viewOptions.seriesTypes) {
            _.each(series.series, function (obj) {
              if (viewOptions.seriesTypes[obj.name]) {
                obj.type = viewOptions.seriesTypes[obj.name];
              } else {
                obj.type = "scatter";
                obj.zIndex = 1;
              }
            });
          }

          // Create a new chart
            Deckster.views.scatterPlotChart.init(card, section, {
            chart: {
              type: 'scatter',
              renderTo: card[section + chartIdSuffix],
              className: 'deckster-chart',
              zoomType: viewOptions.zoomType
            },
            title: {
              text: null
            },
            subtitle: {
              text: viewOptions.subtitle || null
            },
            xAxis: {
              categories: series.categories,
              title: {
                text: viewOptions.xTitle || null
              },
              plotLines: viewOptions.xAxisPlotLines || []
            },
            yAxis: {
              title: {
                text: viewOptions.yTitle || null
              },
              plotLines: viewOptions.yAxisPlotLines || []
            },
            tooltip: viewOptions.tooltip || {},
            plotOptions: viewOptions.plotOptions || {},
            credits: {
              enabled: false
            },
            legend: {
              enabled: viewOptions.legendEnabled || true,
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            },
            series: series.series || [] // This is the data passed in by the loadData function
          });
        }


        );
      }
    },
    resize: function(card, section) {
      var chart = card[section + chartSuffix];
      if(chart) {
        chart.reflow();
      }
    },
    reload: function (card, section) {
      var chart = card[section + chartSuffix];

      if (chart) {
        var options = chart.userOptions;

        chart.destroy();
        card[section + chartSuffix] = null;
        Deckster.views.scatterPlotChart.init(card, section, options);
      }
    }
  };
}));
