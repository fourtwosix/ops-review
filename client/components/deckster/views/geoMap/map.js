;(function ($) {

  var themes = {
    "light": [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
    "dark": [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
  };

  var defaults = {
    mapOptions: {
      minZoom: 1,
      zoom: 2,
      styles: themes["dark"],
      tooltipFormatter: undefined
    },
    zoomTrigger: 10,
    heatmapOptions: {
      radius: 20,
      gradient: [
        'rgba(0, 255, 255, 0)',
        'rgba(0, 255, 255, 1)',
        'rgba(0, 191, 255, 1)',
        'rgba(0, 127, 255, 1)',
        'rgba(0, 63, 255, 1)',
        'rgba(0, 0, 255, 1)',
        'rgba(0, 0, 223, 1)',
        'rgba(0, 0, 191, 1)',
        'rgba(0, 0, 159, 1)',
        'rgba(0, 0, 127, 1)',
        'rgba(63, 0, 91, 1)',
        'rgba(127, 0, 63, 1)',
        'rgba(191, 0, 31, 1)',
        'rgba(255, 0, 0, 1)'
      ]
    },
    layerToggle: "auto",
    markerClustering: true
  };
  var map, heatmap, pointLayer, markerClusterer, streetView,
    infoWindow;

  //This is for knowing if the cluster click event is fired.  This is cleared on map click.
  var clusterClicked = false;

  /**
   * Toggles HeatMap layer
   * @param enable
   * @param cb
   * @private
   */
  var _toggleHeatLayer = function(enable, cb) {
    if(enable) {
      heatmap.setMap(map);
    } else {
      heatmap.setMap(null);
    }
    cb && cb();
  };

  /**
   * Toggles Pin/Marker layer
   * @param enable
   * @param cb
   * @private
   */
  var _togglePinLayer = function(self, enable, cb) {
    var toggleMarkers = function (enable) {
      var markers = markerClusterer.getMarkers();
      for (var i = 0; i < markers.length; i++) {
        markers[i].setVisible(enable);
      }
      markerClusterer.repaint();
    };

    self.options.markerClustering && toggleMarkers(enable);
    if(enable) {
      self.options.markerClustering ? markerClusterer.setMap(map) : pointLayer.setMap(map);
    } else {
      self.options.markerClustering ? markerClusterer.setMap(null) : pointLayer.setMap(null);
    }
    cb && cb();
  };

  /**
   * Decides what layer (Heat or Pins) to display based on the map's defined zoomTrigger
   * @param self
   * @private
   */
  var _autoToggleLayer = function(self) {
    if (self.options.layerToggle === "auto") {
      var zoom = map.getZoom();
      if (zoom >= self.options.zoomTrigger) {
        _toggleHeatLayer(false, function () {
          _togglePinLayer(self, true);
        });
      } else {
        _togglePinLayer(self, false, function () {
          _toggleHeatLayer(true);
        });
      }
    }
  };

  /**
   * Map constructor
   * @param el
   * @param config
   * @param features
   * @constructor
   */
  function Map(el, config, features) {
    this.$el = $(el);
    this.options = $.extend(true, {}, defaults, config);

    this.options.mapOptions.styles = themes[window.InsightConfig.theme];

    this.geoJson = {
      type: "FeatureCollection",
      features: features
    };
    var bounds = new google.maps.LatLngBounds();

    // If the user has specified a center, use it, otherwise, get the center from the bounding box.
    if(!_.isUndefined(this.options.mapOptions.center)) {
      this.options.mapOptions.center = new google.maps.LatLng(
        this.options.mapOptions.center[0],
        this.options.mapOptions.center[1]
      );
    } else {
      this.options.mapOptions.bounds = bounds;
      this.options.mapOptions.center = this.options.mapOptions.bounds.getCenter();
    }

    this.points = features.map(function(point) {
      var latLng = new google.maps.LatLng(point.geometry.coordinates[1], point.geometry.coordinates[0]);
      bounds.extend(latLng);
      return latLng;
    });
  }

  /**
   * Map initialization
   */
  Map.prototype.init = function() {
    map = new google.maps.Map(
      this.$el[0],
      this.options.mapOptions
    );

    //Fits the map zoom to a level that will make all points visible
    if(this.options.mapOptions.bounds) {
      map.fitBounds(this.options.mapOptions.bounds);
    }

    infoWindow = new google.maps.InfoWindow({maxWidth: 500});
    streetView = map.getStreetView();

    this.bindHandlers();
    this.initDataLayer();
    this.initHeatmap();
  };

  /**
   * Binds various event listeners to the map
   */
  Map.prototype.bindHandlers = function() {
    var self = this;

    google.maps.event.addListener(map, 'zoom_changed', function() {
      return _autoToggleLayer(self)
    });

    google.maps.event.addListener(map, 'click', function() {
      if(!clusterClicked) {
        infoWindow.close();
      }
      clusterClicked = false;
    });
  };

  /**
   * Initializes the map data
   */
  Map.prototype.initDataLayer = function() {

    var tooltipFormatter = this.options.mapOptions.tooltipFormatter;

    if(this.options.markerClustering){
      markerClusterer = new MarkerClusterer(map);
      markerClusterer.setMap(null);

      function multiChoice(clickedCluster) {
        if (clickedCluster.getMarkers().length > 1)
        {
          var markers = clickedCluster.getMarkers();
          var features = [];
          _.each(markers, function(marker){
            features.push(marker.feature);
          });

          if(!_.isUndefined(tooltipFormatter)) {
            infoWindow.setContent(tooltipFormatter(features));
            infoWindow.setPosition(features[0].getGeometry().get());
            infoWindow.open(map);
          }

          clusterClicked = true;

          return false;
        }
        return true;
      }

      markerClusterer.onClick = function(clickedClusterIcon) {
        return multiChoice(clickedClusterIcon.cluster_);
      };

      google.maps.event.addListener(map.data, 'addfeature', function (e) {
        if (e.feature.getGeometry().getType() === 'Point') {
          var marker = new google.maps.Marker({
            position: e.feature.getGeometry().get(),
            feature: e.feature
          });
          google.maps.event.addListener(marker, 'click', function (marker, e) {
            return function () {
              if(!_.isUndefined(tooltipFormatter)) {
                infoWindow.setContent(tooltipFormatter(e.feature));
                infoWindow.setPosition(e.feature.getGeometry().get());
                infoWindow.setOptions({pixelOffset: new google.maps.Size(0, -34)});
                infoWindow.open(map);
              }
            };
          }(marker, e));
          google.maps.event.addListener(marker, 'dblclick', function (marker, e) {
            return function () {
              streetView.setPosition(e.feature.getGeometry().get());
              streetView.setVisible(true);
            };
          }(marker, e));
          markerClusterer.addMarker(marker);
        }
      });
      map.data.addGeoJson(this.geoJson);
      map.data.setMap(null);
    }
    else {
      pointLayer = new google.maps.Data();
      pointLayer.addGeoJson(this.geoJson);
      pointLayer.setMap(null);

      pointLayer.addListener('dblclick', function (event) {
        streetView.setPosition(event.latLng);
        streetView.setVisible(true);
      });

      pointLayer.addListener('mousedown', function (event) {
        if(!_.isUndefined(tooltipFormatter)) {
          infoWindow.setContent(tooltipFormatter(event.feature));
          infoWindow.setPosition(event.latLng);
          infoWindow.setOptions({pixelOffset: new google.maps.Size(0, -34)});
          infoWindow.open(map);
        }
      });
    }
  };

  /**
   * Initializes the Heat Map layer
   */
  Map.prototype.initHeatmap = function() {
    heatmap = new google.maps.visualization.HeatmapLayer({
      data: this.points
    });

    heatmap.set('gradient', heatmap.get('gradient') ? null : this.options.heatmapOptions.gradient);
    heatmap.set('radius', heatmap.get('radius') ? null : this.options.heatmapOptions.radius);

    heatmap.setMap(map);
  };

  /**
   * Toggles the displayed map layer given the input layerType
   * @param layerType
   * @param card
   */
  Map.prototype.toggleLayer = function(layerType, card) {

    card.options.layerToggle = layerType;

    switch(layerType){
      case 'pins':
        _toggleHeatLayer(false, function() {
          _togglePinLayer(card, true);
        });
        break;
      case 'heat':
        _togglePinLayer(card, false, function() {
          _toggleHeatLayer(true);
        });
        break;
      default:
        card.options.layerToggle = "auto";
        _autoToggleLayer(card);
        break;
    }
  };

  /**
   * Called when map is resized; applies layer evaluation and centers map
   */
  Map.prototype.resizeMap = function() {
    var self = this;

    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    _autoToggleLayer(self);
    map.setCenter(center);
  };

  Map.prototype.setTheme = function (theme) {
    map.setOptions({styles: themes[theme]});
  };

  $.fn.opsMap = function(options, features) {
    return this.each(function() {
      // prevent multiple instantiation
      if (!$(this).data('opsMap'))
        $(this).data('opsMap', new Map(this, options, features));
    });
  };
})(jQuery);
