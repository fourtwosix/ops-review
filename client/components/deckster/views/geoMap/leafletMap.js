;(function ($) {

  var themes = {
    "light": 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png',
    "dark": 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png'
  };

  var defaults = {
    mapOptions: {
      minZoom: 1,
      zoom: 3,
      theme: themes["dark"],
      noWrap: true,
      attributionControl: false,
      tooltipFormatter: undefined,
      tooltipOptions: {
        maxHeight: 200
      }
    },
    zoomTrigger: 7,
    heatmapOptions: {
      "radius": 12,
      "blur": 1,
      latField: 'lat',
      lngField: 'lng',
      gradient: {
        0: 'rgba(0, 255, 255, 0)',
        0.8: 'rgba(0, 255, 255, 1)',
        0.98: 'rgba(0, 0, 255, 1)',
        0.99: 'rgba(0, 0, 127, 1)',
        1: 'rgba(255, 0, 0, 1)'
      }
    },
    heatLayerData: {
      max: 8
    },
    clusterOptions: {
      spiderfyOnMaxZoom: false
    },
    layerToggle: "auto"
  };

  var map;
  var mapLayer;
  var heatLayer;
  var clusterLayer;

  /**
   * Toggles HeatMap layer
   * @param enable
   * @param cb
   * @private
   */
  var _toggleHeatLayer = function(enable, cb) {
    if(enable) {
      map.addLayer(heatLayer);
    } else {
      map.removeLayer(heatLayer);
    }
    cb && cb();
  };

  /**
   * Toggles Pin/Marker layer
   * @param enable
   * @param cb
   * @private
   */
  var _togglePinLayer = function(enable, cb) {
    if(enable) {
      map.addLayer(clusterLayer);
    } else {
      map.removeLayer(clusterLayer);
    }
    cb && cb();
  };

  /**
   * Decides what layer (Heat or Pins) to display based on the map's defined zoomTrigger
   * @param self
   * @private
   */
  var _autoToggleLayer = function(self) {
    if (self.options.layerToggle === "auto") {
      var zoom = map.getZoom();
      if (zoom >= self.options.zoomTrigger) {
        _toggleHeatLayer(false, function () {
          _togglePinLayer(true);
        });
      } else {
        _togglePinLayer(false, function () {
          _toggleHeatLayer(true);
        });
      }
    }
  };

  /**
   * Map constructor
   * @param el
   * @param config
   * @param features
   * @constructor
   */
  function Map(el, options, data) {
    var self = this;

    this.$el = $(el);
    this.options = $.extend(true, {}, defaults, options);
    this.options.mapOptions.theme = themes[window.InsightConfig.theme];
    L.Icon.Default.imagePath = 'bower_components/leaflet/dist/images';

    // Setup the data
    this.markers = [];
    this.options.heatLayerData.data = [];
    _.forEach(data, function(dataPoint) {
      var lat = dataPoint.geometry.coordinates[1];
      var lng = dataPoint.geometry.coordinates[0];
      var latLng = new L.LatLng(lat, lng);

      self.markers.push(L.marker(latLng, dataPoint.properties));
      self.options.heatLayerData.data.push({"lat": lat, "lng": lng});

      // If a center is not defined, create bounds that will center the map
      if(_.isUndefined(self.options.mapOptions.center)) {
        if(_.isUndefined(self.options.mapOptions.bounds)){
          self.options.mapOptions.bounds = new L.LatLngBounds();
        }
        self.options.mapOptions.bounds.extend(latLng);
      }
    });
  }

  /**
   * Map initialization
   */
  Map.prototype.init = function() {
    map = new L.Map(this.$el[0], this.options.mapOptions);

    //Fits the map zoom to a level that will make all points visible
    if(!_.isUndefined(this.options.mapOptions.bounds)) {
      map.fitBounds(this.options.mapOptions.bounds);
    }

    this.initMapLayer();
    this.initClusterLayer();
    this.initHeatLayer();

    this.bindEventListeners();
  };

  /**
   * Binds various event listeners to the map
   */
  Map.prototype.bindEventListeners = function() {
    var self = this;
    map.on("zoomend", function(){
      _autoToggleLayer(self);
    });

    map.on("dragstart", function() {
      map.closePopup();
    });
  };

  /**
   * Initializes the map layer
   */
  Map.prototype.initMapLayer = function() {
    mapLayer = L.tileLayer(this.options.mapOptions.theme, this.options.mapOptions);
    map.addLayer(mapLayer);
  };

  /**
   * Initializes the cluster layer
   */
  Map.prototype.initClusterLayer = function() {
    var self = this;
    clusterLayer = L.markerClusterGroup(this.options.clusterOptions);
    clusterLayer.addLayers(this.markers);

    // Add tooltip listeners for clusters on marker and cluster click
    clusterLayer.on('click', function (e) {
      e.layer.bindPopup(self.options.mapOptions.tooltipFormatter(e.layer)).openPopup();
    });
    clusterLayer.on('clusterclick', function (e) {
      // Only show the tooltip if the cluster has no more unique lat/lngs under it
      if (e.layer._bounds._northEast.equals(e.layer._bounds._southWest) || map.getMaxZoom() === map.getZoom()) {
        e.layer.bindPopup(self.options.mapOptions.tooltipFormatter(e.layer.getAllChildMarkers()), self.options.mapOptions.tooltipOptions).openPopup();
      }
    });
  };

  /**
   * Initializes the heat layer
   */
  Map.prototype.initHeatLayer = function() {
    heatLayer = new HeatmapOverlay(this.options.heatmapOptions);
    // We have to add the heat layer prior to setting the data or else the heatLayer throws and error.
    map.addLayer(heatLayer);
    heatLayer.setData(this.options.heatLayerData);
    map.removeLayer(heatLayer);
  };

  /**
   * Toggles the displayed map layer given the input layerType
   * @param layerType
   * @param card
   */
  Map.prototype.toggleLayer = function(layerType, card) {

    card.options.layerToggle = layerType;

    switch(layerType){
      case 'pins':
        _toggleHeatLayer(false, function() {
          _togglePinLayer(true);
        });
        break;
      case 'heat':
        _togglePinLayer(false, function() {
          _toggleHeatLayer(true);
        });
        break;
      default:
        card.options.layerToggle = "auto";
        _autoToggleLayer(card);
        break;
    }
  };

  /**
   * Called when map is resized; applies layer evaluation and centers map
   */
  Map.prototype.resizeMap = function() {
    map.invalidateSize();
    _autoToggleLayer(this);
  };

  /**
   * Called when application theme is changed.
   * @param theme
   */
  Map.prototype.setTheme = function (theme) {
    this.options.mapOptions.theme = themes[theme];
    map.removeLayer(mapLayer);
    this.initMapLayer();
  };

  $.fn.leafletMap = function(options, data) {
    return this.each(function() {
      // prevent multiple instantiation
      if (!$(this).data('leafletMap'))
        $(this).data('leafletMap', new Map(this, options, data));
    });
  };
})(jQuery);
