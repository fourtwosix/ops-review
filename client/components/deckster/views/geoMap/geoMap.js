;(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function($, Deckster){
  Deckster.views = Deckster.views || {};

  var viewIdSuffix = "-geo-map";
  var mapSuffix = "GeoMap";
  var mapIdSuffix = mapSuffix + "Id";

  // View configuration for geoMap
  Deckster.views.geoMap = {
    getContentHtml: function (card, cb) {
      var currentView = card.currentSection;
      // Create an unique id that we can use for the map container
      card[currentView + mapIdSuffix] = card.options.id + card.currentSection + viewIdSuffix;
      var noDataDiv = '<div id="' + card.options.id + 'NoData" class="noData">No data to display</div>';
      cb('<div class="deckster-gmap" id="' + card[currentView + mapIdSuffix] + '" style="height: 100%;"></div>' + noDataDiv);
    },

    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function(card, section) {
      var $mapEl = $('#' + card[section + mapIdSuffix]);
      var viewOptions = card.options.getCurrentViewOptions(section);

      // If the map container exist initialize a map object
      if($mapEl.length != 0) {
        card.options.loadData(card, function(data) {

          var $noDataEl = $('#' + card.options.id + 'NoData');
          if(data.geoJSONData.length === 0) {
            $noDataEl.css('visibility', 'visible');
          } else {
            $noDataEl.css('visibility', 'hidden');
          }

          card[section + mapSuffix] = $mapEl.leafletMap(
            {
              mapOptions: {
                zoom: viewOptions.zoom,
                minZoom: viewOptions.minZoom,
                center: viewOptions.center,
                tooltipFormatter: viewOptions.tooltipFormatter || undefined,
                zoomTrigger: viewOptions.zoomTrigger
              }
            },
            data.geoJSONData).data('leafletMap');

          card[section + mapSuffix].init();

          //Display up the most recent, user selected, map layer
          card.options.getCurrentViewOptions(section).controls.displayMapLayer(card, card.options.getCurrentViewOptions(section).controls.selectedMapLayer);
        });
      }
    },
    resize: function(card, section) {
      var map = card[section + mapSuffix];
      if(map) {
        map.resizeMap();
      }
    },
    reload: function (card, section) {
      var map = card[section + mapSuffix];

      if (map) {
        map.setTheme(window.InsightConfig.theme);
      }
    }
  };
}));
