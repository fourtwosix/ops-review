;(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function($, Deckster){
  Deckster.views = Deckster.views || {};

  var viewIdSuffix = "-drilldown-view";
  var chartSuffix = "DrilldownView";
  var chartIdSuffix = chartSuffix + "Id";

  // View configuration for drilldownView
  Deckster.views.drilldownView = {
    getContentHtml: function (card, cb) {
      var currentView = card.currentSection;
      // Create an unique id that we can use for the chart container
      card[currentView + chartIdSuffix] = card.options.id + card.currentSection + viewIdSuffix;

      // Send back the html (chart container) for this view
      cb('<div id="' + card[currentView + chartIdSuffix] + '" style="height: 100%;"></div>');
    },
    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function(card, section) {
      var beforeLoad = card.options[section + 'ViewOptions'].beforeLoad;
      var load = function (card) {
        Deckster.views[card.options.getCurrentViewOptions(section).viewType].onLoad(card, section);
      };
      if (beforeLoad) {
        beforeLoad(card, function (card) {
          load(card);
        })
      } else {
        load(card);
      }
    },
    resize: function(card, section) {
      Deckster.views[card.options.getCurrentViewOptions(section).viewType].resize(card, section);
    },
    reload: function (card, section) {
      Deckster.views[card.options.getCurrentViewOptions(section).viewType].reload(card, section);
    }
  };
}));
