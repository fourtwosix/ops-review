/**
 * Created by omezu on 6/15/15.
 */
;(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function($, Deckster){
  Deckster.views = Deckster.views || {};

  var viewIdSuffix = "-bot-plot";

  // View configuration for boxPlot
  Deckster.views.boxPlot = {
    getContentHtml: function (card, cb) {
      var currentView = card.currentSection;
      // Create an unique id that we can use for the chart container
      card[currentView + 'BoxPlotId'] = card.options.id + card.currentSection + viewIdSuffix;

      // Send back the html (chart container) for this view
      cb('<div id="' + card[currentView + 'BoxPlotId'] + '" style="height: 100%;"></div>');
    },

    init: function (card, section, options) {
      var $chartEl = $('#' + card[section + 'BoxPlotId']);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        if (card[section + 'BoxPlot']) {
          card[section + 'BoxPlot'].destroy();
          card[section + 'BoxPlot'] = null;
        }

        // Create a new chart
        card[section + 'BoxPlot'] = new Highcharts.Chart(options);
      }
    },

    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function(card, section) {
      var viewOptions = card.options.getCurrentViewOptions(section);
      var $chartEl = $('#' + card[section + 'BoxPlotId']);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        card.options.loadData(card, function(series) {

          // Destroy previously created chart
          if (card[section + 'BoxPlot']) {
            card[section + 'BoxPlot'].destroy();
            card[section + 'BoxPlot'] = null;
          }

          // Create a new chart
          Deckster.views.boxPlot.init(card, section, {
            chart: {
              type: 'boxplot',
              renderTo: card[section + 'BoxPlotId'],
              className: 'deckster-chart'
            },
            title: {
              text: null
            },
            subtitle: {
              text: viewOptions.subtitle || null
            },
            xAxis: {
              categories: series.categories || [],
              title: {
                text: viewOptions.xTitle || null
              },
              plotLines: viewOptions.xAxisPlotLines || []
            },
            yAxis: {
              title: {
                text: viewOptions.yTitle || null
              },
              plotLines: viewOptions.yAxisPlotLines || []
            },
            tooltip: {
              enabled: viewOptions.tooltipEnabled || true
            },
            plotOptions: {
              series: {
                stacking: viewOptions.columnStacking || null //null, normal, or percent
              }
            },
            legend: {
              enabled: viewOptions.legendEnabled || true,
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            },
            credits: {
              enabled: false
            },
            series: [
              series,
              {
                name: 'Mean',
                type: 'scatter',
                data: series.mean,
                marker: {
                  fillColor: 'red'
                },
                tooltip: {
                  fillColor: 'red',
                  pointFormat: 'Mean Hours for Week: {point.y:.2f}'
                }
              }]// This is the data passed in by the loadData function
          });
        });
      }
    },
    resize: function(card, section) {
      var chart = card[section + 'BoxPlot'];
      if(chart) {
        chart.reflow();
      }
    },
    reload: function (card, section) {
      var chart = card[section + 'BoxPlot'];

      if (chart) {
        var options = chart.userOptions;

        chart.destroy();
        card[section + 'BoxPlot'] = null;
        Deckster.views.boxPlot.init(card, section, options);
      }
    }
  };
}));
