;(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function($, Deckster){
  Deckster.views = Deckster.views || {};

  var viewIdSuffix = "-column-chart";
  var chartSuffix = "ColumnChart";
  var chartIdSuffix = chartSuffix + "Id";

  // View configuration for columnChart
  Deckster.views.columnChart = {
    getContentHtml: function (card, cb) {
      var currentView = card.currentSection;
      // Create an unique id that we can use for the chart container
      card[currentView + chartIdSuffix] = card.options.id + card.currentSection + viewIdSuffix;

      // Send back the html (chart container) for this view
      cb('<div id="' + card[currentView + chartIdSuffix] + '" style="height: 100%;"></div>');
    },

    init: function (card, section, options) {
      var $chartEl = $('#' + card[section + chartIdSuffix]);
      var viewOptions = card.options.getCurrentViewOptions(section);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        if (card[section + chartSuffix]) {
          card[section + chartSuffix].destroy();
          card[section + chartSuffix] = null;
        }

        // Create a new chart
        card[section + chartSuffix] = new Highcharts.Chart(options);

        // Handels the x-axis label click
        $('.highcharts-axis-labels text, .highcharts-axis-labels span').click(function () {
          var viewOptions = card.options.getCurrentViewOptions(section);
          if(viewOptions.onXAxisLabelClick) {
            viewOptions.onXAxisLabelClick(card, this);
          }
        });

        if (viewOptions.message && viewOptions.showMessage(card)) {
          card.showMessage(viewOptions.message);
        } else {
          card.hideMessage();
        }
      }
    },

    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function(card, section) {
      var viewOptions = card.options.getCurrentViewOptions(section);

      //Set card up for drilldown implementation
      if(card.options.getCurrentViewType(section) === "drilldownView") {
        var $drilldownEl = $('#' + card[section + "DrilldownViewId"]);
        this.getContentHtml(card, function(tableHtml) {
          $drilldownEl.append(tableHtml)
        })
      }

      var $chartEl = $('#' + card[section + chartIdSuffix]);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        card.options.loadData(card, function(series) {

          // Destroy previously created chart
          if (card[section + chartSuffix]) {
            card[section + chartSuffix].destroy();
            card[section + chartSuffix] = null;
          }

          _.each(series.series, function(seriesItem){
            if(!seriesItem.type){
              seriesItem.type = "column";
            }
          });

          // Create a new chart
          var chartOptions = {
            chart: {
              type: 'column',
              renderTo: card[section + chartIdSuffix],
              className: 'deckster-chart'
            },
            title: {
              text: null
            },
            subtitle: {
              text: viewOptions.subtitle || null
            },
            xAxis: {
              categories: series.categories || [],
              title: {
                text: viewOptions.xTitle || null
              },
              plotLines: viewOptions.xAxisPlotLines || [],
              labels: {
                formatter: function () {
                  if(viewOptions.xAxisFormatter) {
                    return viewOptions.xAxisFormatter.call(this, card);
                  } else {
                    return this.value;
                  }
                }
              }
            },
            yAxis: {
              title: {
                text: viewOptions.yTitle || null
              },
              plotLines: viewOptions.yAxisPlotLines || [] ,
              labels: {
                formatter: function () {
                  if(viewOptions.yAxisFormatter) {
                    return viewOptions.yAxisFormatter.call(this, card);
                  } else {
                    return this.value;
                  }
                }
              },
              allowDecimals: _.get(viewOptions, 'yAxis.allowDecimals', true)
            },
            tooltip: {
              enabled: !_.isUndefined(viewOptions.tooltipEnabled) ? viewOptions.tooltipEnabled : true,
              formatter: viewOptions.tooltipFormatter
            },
            plotOptions: {
              column: viewOptions.plotOptionsColumn || {},
              series: {
                cursor: 'pointer',
                point: {
                  events: {
                    click: function () {
                      if(viewOptions.onColumnClick) {
                        viewOptions.onColumnClick(card, this);
                      }
                    }
                  }
                }
              }
            },
            legend: {
              enabled: !_.isUndefined(viewOptions.legendEnabled) ? viewOptions.legendEnabled : true,
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            },
            credits: {
              enabled: false
            },
            series: series.series || [] // This is the data passed in by the loadData function
          };

          Deckster.views.columnChart.init(card, section, chartOptions);
        });
      }
    },
    resize: function(card, section) {
      var chart = card[section + chartSuffix];
      if(chart) {
        chart.reflow();
      }
    },
    reload: function (card, section) {
      var chart = card[section + chartSuffix];

      if (chart) {
        var options = chart.userOptions;

        chart.destroy();
        card[section + chartSuffix] = null;
        Deckster.views.columnChart.init(card, section, options);
      }
    }
  };
}));
