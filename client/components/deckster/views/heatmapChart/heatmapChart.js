;(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function($, Deckster){
  Deckster.views = Deckster.views || {};

  var viewIdSuffix = "-heatmap-chart";
  var chartSuffix = "HeatmapChart";
  var chartIdSuffix = chartSuffix + "Id";

  // View configuration for heatmapChart
  Deckster.views.heatmapChart = {
    getContentHtml: function (card, cb) {
      var currentView = card.currentSection;
      // Create an unique id that we can use for the chart container
      card[currentView + chartIdSuffix] = card.options.id + card.currentSection + viewIdSuffix;

      // Send back the html (chart container) for this view
      cb('<div id="' + card[currentView + chartIdSuffix] + '" style="height: 100%;"></div>');
    },

    init: function (card, section, options) {
      var $chartEl = $('#' + card[section + chartIdSuffix]);
      var viewOptions = card.options.getCurrentViewOptions(section);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        if (card[section + chartSuffix]) {
          card[section + chartSuffix].destroy();
          card[section + chartSuffix] = null;
        }

        // Create a new chart
        card[section + chartSuffix] = new Highcharts.Chart(options);

        if (viewOptions.message && viewOptions.showMessage && viewOptions.showMessage(card)) {
          card.showMessage(viewOptions.message);
        } else {
          card.hideMessage();
        }
      }
    },

    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function(card, section) {
      var beforeLoad = card.options[section + 'ViewOptions'].beforeLoad;
      var load = function (card, section) {
        var viewOptions = card.options.getCurrentViewOptions(section);

        //Set card up for drilldown implementation
        if(card.options.getCurrentViewType(section) === "drilldownView") {
          var $drilldownEl = $('#' + card[section + "DrilldownViewId"]);
          Deckster.views.heatmapChart.getContentHtml(card, function(tableHtml) {
            $drilldownEl.append(tableHtml)
          })
        }

        var $chartEl = $('#' + card[section + chartIdSuffix]);

        // If the chart container exist initialize a chart object
        if($chartEl.length != 0) {
          card.options.loadData(card, function(series) {

            // Destroy previously created chart
            if (card[section + chartSuffix]) {
              card[section + chartSuffix].destroy();
              card[section + chartSuffix] = null;
            }

            // Create a new chart
            Deckster.views.heatmapChart.init(card, section, {
              chart: {
                type: 'heatmap',
                renderTo: card[section + chartIdSuffix],
                marginTop: viewOptions.marginTop || 50,
                className: 'deckster-chart'
              },
              title: {
                text: null
              },
              xAxis: {
                tickColor: _.get(viewOptions, 'xAxis.tickColor') || "#D8D8D8",
                categories: series.categories.x || [],
                title: {
                  text: viewOptions.xTitle || null
                },
                opposite: true,
                labels: {
                  rotation: viewOptions.rotateXLabel ? -45 : 0
                }
              },
              yAxis: {
                categories: series.categories.y || [],
                title: {
                  text: viewOptions.yTitle || null
                },
                labels: {
                  formatter: function() {
                    return viewOptions.yAxisFormatter ?
                    viewOptions.yAxisFormatter(card, this.value) :
                    this.value;
                  }
                },
                reversed: true
              },
              colorAxis: viewOptions.colorAxis || {},
              legend: {
                enabled: !_.isUndefined(viewOptions.legendEnabled) ? viewOptions.legendEnabled : true,
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'middle'
              },
              tooltip: {
                enabled: !_.isUndefined(viewOptions.tooltipEnabled) ? viewOptions.tooltipEnabled : true,
                formatter: viewOptions.tooltipFormatter || undefined
              },
              credits: {
                enabled: false
              },
              plotOptions: {
                series: {
                  point: {
                    events: {
                      click: function(){
                        if(viewOptions.onSeriesPointClick) {
                          viewOptions.onSeriesPointClick(card, this);
                        }
                      }
                    }
                  },
                  states: {
                    hover: {
                      enabled: !_.isUndefined(viewOptions.hoverEnabled) ? viewOptions.hoverEnabled : true
                    }
                  }
                }
              },
              series: [{
                borderWidth: viewOptions.borderWidth || 1,
                color: viewOptions.seriesBackgroundColor,
                data: series.data || [],
                dataLabels: {
                  enabled: true,
                  color: viewOptions.dataLabelsColor || '#000000',
                  style: {textShadow: 'none', fontSize: _.get(viewOptions, 'dataLabels.style.fontSize') || 11},
                  formatter: viewOptions.seriesFormatter || function(){ return this.point.value; }
                }
              }]
            });
          });
        }
      };

      if (beforeLoad) {
        beforeLoad(card, function (card, section) {
          load(card, section);
        })
      } else {
        load(card, section);
      }
    },
    resize: function(card, section) {
      var chart = card[section + chartSuffix];
      if(chart) {
        chart.reflow();
      }
    },
    reload: function (card, section) {
      var chart = card[section + chartSuffix];

      if (chart) {
        var options = chart.userOptions;

        chart.destroy();
        card[section + chartSuffix] = null;
        Deckster.views.heatmapChart.init(card, section, options);
      }
    }
  };
}));
