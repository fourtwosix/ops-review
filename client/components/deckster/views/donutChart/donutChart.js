;(function (root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'deckster'], factory);
  } else {
    root.DecksterMapCard = factory(root.$ || root.jQuery, root.Deckster);
  }

}(this, function($, Deckster){
  Deckster.views = Deckster.views || {};

  var viewIdSuffix = "-donut-chart";
  var chartSuffix = "DonutChart";
  var chartIdSuffix = chartSuffix + "Id";

  // View configuration for donutChart
  Deckster.views.donutChart = {
    getContentHtml: function (card, cb) {
      var currentView = card.currentSection;
      // Create an unique id that we can use for the chart container
      card[currentView + chartIdSuffix] = card.options.id + card.currentSection + viewIdSuffix;

      // Send back the html (chart container) for this view
      cb('<div id="' + card[currentView + chartIdSuffix] + '" style="height: 100%;"></div>');
    },

    init: function (card, section, options) {
      var $chartEl = $('#' + card[section + chartIdSuffix]);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        if (card[section + chartSuffix]) {
          card[section + chartSuffix].destroy();
          card[section + chartSuffix] = null;
        }

        // Create a new chart
        card[section + chartSuffix] = new Highcharts.Chart(options);
      }
    },

    // This function gets bound to the correct load callback
    // will either be summaryContentHtml or detailsContentHtml
    onLoad: function(card, section) {
      var viewOptions = card.options.getCurrentViewOptions(section);

      //Set card up for drilldown implementation
      if(card.options.getCurrentViewType(section) === "drilldownView") {
        var $drilldownEl = $('#' + card[section + "DrilldownViewId"]);
        this.getContentHtml(card, function(tableHtml) {
          $drilldownEl.append(tableHtml)
        })
      }

      var $chartEl = $('#' + card[section + chartIdSuffix]);

      // If the chart container exist initialize a chart object
      if($chartEl.length != 0) {
        card.options.loadData(card, function(series) {

          // TODO show a dropdown when series.data is a object (this means there are numerous sets of data)

          // Destroy previously created chart
          if (card[section + chartSuffix]) {
            card[section + chartSuffix].destroy();
            card[section + chartSuffix] = null;
          }

          // Create a new chart
          Deckster.views.donutChart.init(card, section, {
            chart: {
              type: 'donut',
              renderTo: card[section + chartIdSuffix],
              className: 'deckster-chart'
            },
            title: {
              text: null
            },
            subtitle: {
              text: viewOptions.subtitle || null
            },
            tooltip: {
              enabled: !_.isUndefined(viewOptions.tooltipEnabled) ? viewOptions.tooltipEnabled : true,
              formatter: viewOptions.tooltipFormatter,
              pointFormat: _.isUndefined(viewOptions.tooltipFormatter) ? '{series.name}: <b>{point.percentage:.1f}%</b>' : null
            },
            xAxis: {
              labels: {
                formatter: function () {
                  if(viewOptions.xAxisFormatter) {
                    return viewOptions.xAxisFormatter.call(this, card);
                  } else {
                    return this.value;
                  }
                }
              }
            },
            plotOptions: {
              pie: {
                borderWidth: 0,
                innerSize: '95%', // TODO compute this number based on the number of data sources? size of card?
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: false
                },
                showInLegend: true
              },
              series: {
                states: {
                  hover: {
                    enabled: false
                  }
                }
              }
            },
            legend: {
              symbolRadius: 50,
              symbolWidth: 10,
              symbolHeight: 10,
              useHTML: true,
              labelFormatter: function() {
                if(viewOptions.legendFormatter) {
                  return viewOptions.legendFormatter.call(this, card);
                } else {
                  return this.name + " (" + this.percentage.toFixed(1) + "%)";
                }
              }
            },
            credits: {
              enabled: false
            },
            series: [{
              type: 'pie',
              name: viewOptions.seriesName,
              data: series.data
            }]
          });
        });
      }
    },
    resize: function(card, section) {
      var chart = card[section + chartSuffix];
      if(chart) {
        chart.reflow();
      }
    },
    reload: function (card, section) {
      var chart = card[section + chartSuffix];

      if (chart) {
        var options = chart.userOptions;

        chart.destroy();
        card[section + chartSuffix] = null;
        Deckster.views.donutChart.init(card, section, options);
      }
    }
  };
}));
