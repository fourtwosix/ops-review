'use strict';

angular.module('opsReviewApp')
  .factory('CardManager', function ($http, $q, DataManager) {
    var CardManager = {};

    CardManager.getCard = function (cardId) {
      var deferred = $q.defer();

      $http.post('/api/data/card/', {cardId: cardId}).then( function (response) {
        deferred.resolve(response.data);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    };

    return CardManager;
  });
