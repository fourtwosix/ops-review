'use strict';

angular.module('opsReviewApp')
  .factory('billableProductiveUtilization', function (Card) {

    var billableProductiveUtilization = function(cardData) {
      angular.extend(this, new Card(cardData));

      this.summaryViewOptions.preDataTransform = function(card, data, callback) {
        var templateVariables = card.options.getTemplateVariables();

        var managerLevel = templateVariables.subManagerType,
          parentManager = card.options.getCurrentViewOptions(card.currentSection).parentManager,
          target;

        data = _.reject(data, function (dataItem) {
          return dataItem.layer_3_manager === "fitzpatrick, brian";
        });

        data = _.map(data, function(dataItem) {
          if (dataItem[managerLevel]) {
            if (parentManager.name) {
              target = card.options.utilizationTargets.targets[parentManager.name];
            } else {
              target = card.options.utilizationTargets.targets[dataItem[managerLevel]];
            }

            dataItem.wtd_billable -= _.get(target, 'billable', 0);
            dataItem.wtd_productive -= _.get(target, 'productive', 0);
            dataItem.mtd_billable -= _.get(target, 'billable', 0);
            dataItem.mtd_productive -= _.get(target, 'productive', 0);
            dataItem.ytd_billable -= _.get(target, 'billable', 0);
            dataItem.ytd_productive -= _.get(target, 'productive', 0);

            return dataItem;
          }
        });

        callback && callback(data);
      };

      this.afterFiltersApplied = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var currentViewOptions = card.options.getCurrentViewOptions(card.currentSection);
        currentViewOptions.parentManager = {};

        var managerClause = templateVariables.managerClause.split("~");
        var layerColumn = managerClause[0].trim();
        var managerName = managerClause[1].replace(/'/g, "").trim();
        var layerNumb = parseInt(layerColumn.match(/layer_(\d)_manager/)[1]);
        if (layerNumb === card.options.utilizationTargets.layer &&
          _.has(card.options.utilizationTargets.targets, managerName)) {
          currentViewOptions.parentManager.name = managerName;
          currentViewOptions.parentManager.layer = layerColumn;
        }

        currentViewOptions.dataTransform.nameColumn = templateVariables.subManagerType;
      };

      this.summaryViewOptions.tooltipFormatter = function () {
        return this.x + '<br/>' + this.series.name + ' Utilization: <b>' +
          Highcharts.numberFormat(this.y * 100, 2,'.') + '%</b>';
      };

      this.summaryViewOptions.yAxisFormatter = function () {
        return Highcharts.numberFormat(this.value * 100, 0,'.') + '%'
      };

      return this;
    };

    billableProductiveUtilization.prototype = Card.prototype;

    return billableProductiveUtilization;
  });
