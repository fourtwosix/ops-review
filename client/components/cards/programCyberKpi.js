'use strict';

angular.module('opsReviewApp')
  .factory('programCyberKpi', function (Card, DateUtils, FilterService, ProgramHeatmapColorChangeUtils, Utils, Auth) {

    var programCyberKpi = function (cardData) {
      angular.extend(this, new Card(cardData));

      this.drilldownFilters = {};
      this.canExportToCSV = Auth.isAdmin() || Auth.isExecutive() || Auth.isProgramManager();

      var beforeLoad = function (card, cb) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        var collapse = !filters.project_id && card.currentSection === 'details';

        function before() {
          var viewOptions = card.options[card.currentSection + 'ViewOptions'];
          if (filters.project_id) {
            viewOptions.activeView = "programCyberKpiHeatmap";
            card.options.expandable = true;
          } else if (_.isUndefined(card.options.drilldownFilters.program_cyber_kpi_color)) {
            viewOptions.activeView = "programCyberKpiColumnChart";
            card.options.expandable = false;
          } else {
            viewOptions.activeView = "programCyberKpiTable";
            card.options.expandable = false;
          }
          cb(card, card.currentSection);
        }
        if (collapse) {
          card.toggleSection('summary');
        }
        before();
      };

      this.summaryViewOptions.beforeLoad = this.detailsViewOptions.beforeLoad = beforeLoad;

      function showMessage(card) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        return !filters.project_id  && !filters.pra && !filters.program_manager && !filters.l4_manager && !filters.l3_portfolio && !filters.pra && !filters.l3_manager && !filters.group_name && !filters.division_name && !filters.l2_manager && !filters.l1;
      }

      this.summaryViewOptions.views.programCyberKpiHeatmap.showMessage = showMessage;
      this.summaryViewOptions.views.programCyberKpiColumnChart.showMessage = showMessage;
      this.summaryViewOptions.views.programCyberKpiTable.showMessage = showMessage;

      function isDisplayColumn(column) {
        return !_.contains(['project_id', 'id', 'fiscal_month_end', 'updated_at', 'created_at', 'program_id', 'type'], column);
      }

      var pivotKPIDataArrayForPast5Months = function (data, card) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        var endDate = DateUtils.getMomentFromDateString(filters.endDate);
        var newData = {};
        var dateColumn = 'fiscal_month_end';
        var monthsRange = DateUtils.getMonthsAgoRange(data, card, 5);

        DateUtils.prependInitializedMonths(data, monthsRange, isDisplayColumn, dateColumn);

        function monthIsSameAsEndDate(monthDate) {
          return moment.utc(new Date(monthDate)).isSame(endDate, 'month');
        }

        var currentFiscalMonthRowIndex = _.findIndex(data, function (row) {
          return monthIsSameAsEndDate(row.fiscal_month_end) && row.type === null;
        });

        if (currentFiscalMonthRowIndex === -1) {
          currentFiscalMonthRowIndex = data.length;
        }

        function setObjProperties(obj, newValue) {
          _.forOwn(obj, function (value, key) {
            obj[key] = newValue;
          });
        }

        // append an empty blank row after the current month row, this will be the empty column in the heatmap table

        // find the PRA Assessment and Forecast rows to put them in the right order

        // add in the praAssessment & forecast rows in right order

        _.each(data, function (point) {
          _.each(point, function (value, column) {
            var columnHeaderTitle = null;
            var date = moment.utc(new Date(point[dateColumn]));
            var dateFormat = "MMM YYYY";
              // Put an empty title for the blank column in between the 6 months columns and PRA assessment & Forecast columns
              columnHeaderTitle = point[dateColumn] === ' ' ? ' ' : date.format(dateFormat);


            if (isDisplayColumn(column)) {
              newData[column] = newData[column] || {};
              newData[column].name = column;
              newData[column][columnHeaderTitle] = value ? value : "N/A";
            }
          })
        });
        return _.values(newData);
      };

      var pivotKPIDataArrayForPrograms = function (data, card) {
        var newData = {};
        var dateColumn = 'fiscal_month_end';
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        var lastYearRange = DateUtils.getMonthsAgoRange(data, card, 11);

        DateUtils.prependInitializedMonths(data, lastYearRange, isDisplayColumn, dateColumn);

        _.each(data.reverse(), function (point) {
          var programId = null;
          var date = moment.utc(point[dateColumn]);

          if (date.isSame(moment.utc(new Date(filters.endDate)), 'month')) {
            programId = point['project_id'];

            if (angular.isUndefined(newData[programId])) {
              newData[programId] = {name: programId};

              _.each(point, function (value, column) {
                if (isDisplayColumn(column)) {
                  newData[programId][column] = value ? value : "N/A";
                }
              })
            }
          }
        });

        return _.values(newData);
      };

      var pivotKPIDataArrayForYear = function(data, card) {
        var newData = {};
        var dateColumn = 'fiscal_month_end';
        var lastYearRange = DateUtils.getMonthsAgoRange(data, card, 11);
        // Initialize all months' KPI in the year range as "N/A"
        DateUtils.prependInitializedMonths(data, lastYearRange, isDisplayColumn, dateColumn);

        _.each(data, function (point) {
          _.each(point, function(value, column) {
            var date = moment.utc(point[dateColumn]).format("MMM YYYY");

            if (isDisplayColumn(column)) {
              newData[column] = newData[column] || {};
              newData[column].name = column;
              newData[column][date] = value ? value : "N/A";
            }
          })
        });
        return _.values(newData);
      };

      var initEmptyProgram = function (filters) {
        return [{
          "security_authorization": "N/S",
          "scanning": "N/S",
          "flaw_remediation": "N/S",
          "inventory_status": "N/S",
          "asset_status": "N/S",
          "external_access": "N/S",
          "device_hardening": "N/S",
          "security_event_management": "N/S",
          "identified_security_roles_responsibilities": "N/S",
          "account_management": "N/S",
          "contract_security_requirements": "N/S",
          "overall": "N/S",
          "project_id": filters.project_id,
          "fiscal_month_end": new Date(filters.endDate)
        }];
      };

      this.summaryViewOptions.views.programCyberKpiHeatmap.preDataTransform = function (card, data, callback) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        card.options.title = "Program Cyber Summary KPI";

        if (showMessage(card)) {
          data = [];
        } else {
          //currentCard = card;

          if (filters.project_id) { // if only showing for a single program
            if (_.isEmpty(data)) {
              data = initEmptyProgram(filters);
            }
            data = pivotKPIDataArrayForPast5Months(data, card);
          } else if (!_.isEmpty(data)) {
            data = pivotKPIDataArrayForPrograms(data, card);
          }
        }
        callback && callback(data);
      };

      this.summaryViewOptions.views.programCyberKpiColumnChart.onColumnClick = function (card, colData) {
        var kpi = Utils.dataFormatter.default(colData.category.toLowerCase(), true);
        var color = colData.series.name.toUpperCase();

        card.options.title += ": " + colData.category + " — " + color;
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];

        card.options.drilldownFilters = {
          program_cyber_kpi: kpi,
          program_cyber_kpi_color: color
        };

        viewOptions.activeView = "programCyberKpiTable";
        FilterService.createFilteredQuery(card);
      };

      this.summaryViewOptions.views.programCyberKpiTable.drillUp = function (card) {
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "programCyberKpiColumnChart";
        card.options.drilldownFilters = {};

        card.options.title = "Program Cyber Summary KPI";

        FilterService.createFilteredQuery(card);
      };

      this.summaryViewOptions.views.programCyberKpiTable.preDataTransform = function (card, data, callback) {
        if (showMessage(card)) {
          data = [];
        }
        callback && callback(data);
      };

      this.summaryViewOptions.views.programCyberKpiColumnChart.preDataTransform = function (card, data, callback) {
        if (showMessage(card)) {
          data = [];
          callback && callback(data);
        } else {
          var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
          var nameColumn = viewOptions.dataTransform.nameColumn;
          var ret = {};

          _.each(_.groupBy(data, nameColumn), function (entries) {
            _.each(entries, function (entry) {
              _.each(entry, function (value, kpi) {
                if (value === 'G') {
                  value = "Green";
                } else if (value === 'Y') {
                  value = "Yellow";
                } else if (value === 'R') {
                  value = "Red";
                } else if (value === 'Null' || value === null) {
                  value = "Not Submitted";
                } else {
                  value = "N/A";
                }

                if (!ret[kpi]) {
                  ret[kpi] = {kpi: kpi};

                  _.each(['Green', 'Yellow', 'Red', 'N/A', 'Not Submitted'], function (color) {
                    ret[kpi][color] = 0;
                  });
                }

                ret[kpi][value] ? ret[kpi][value]++ : ret[kpi][value] = 1;
              });
            })
          });
          callback && callback(_.flatten(_.values(ret)));
        }
      };

      this.summaryViewOptions.views.programCyberKpiTable.htmlTransform = function (card) {
        //These are the columns we might want to modify (depending on value)
        var $headers = $("#programCyberKpisummary-table > thead > tr > th");
        var $values = $("#programCyberKpisummary-table > tbody > tr").find('td');
        ProgramHeatmapColorChangeUtils.populateIndex($headers, $values);
      };

      this.detailsViewOptions.preDataTransform = function(card, data, callback) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();

        if (showMessage(card)) {
          data = [];
        } else {
          if (filters.project_id) { // if only showing for a single program
            if (_.isEmpty(data)) {
              data = initEmptyProgram(filters);
            }

            data = pivotKPIDataArrayForYear(data, card);
          } else if (!_.isEmpty(data)) {
            data = pivotKPIDataArrayForPrograms(data, card);
          }
        }
        callback && callback(data);
      };

      return this;
    };

    programCyberKpi.prototype = Card.prototype;

    return programCyberKpi;
  });
