/**
 * Created by omezu on 9/25/15.
 */
'use strict';

angular.module('opsReviewApp')
  .factory('reqTimeToFillAcceptAndAvgDaysOpen', function (Card, DataManager, FilterService) {

    var reqTimeToFillAcceptAndAvgDaysOpen = function(cardData) {
      angular.extend(this, new Card(cardData));

      var reqControlFilter = function (card) {
        var templateVariables = card.options.getTemplateVariables();
        var currentViewOptions = card.options.getCurrentViewOptions(card.currentSection);
        currentViewOptions.controls.selectedFilter = this.name;
        var filters = _.merge(templateVariables, this.clauses);
        FilterService.createFilteredQuery(card, filters);
      };

      this.summaryViewOptions.controls = {
        rightControlsContent: ['components/cards/controls/req-type-geomap-controls.html'],
        selectedFilter: 'Funded',
        filterMenu:
        {
          populated: false,
          categories: [
            {
              name: 'Requisition Type',
              filters: [
                {
                  name: 'Funded',
                  clauses: {
                    reqTypeClause: "req_type = 'funded'"
                  },
                  onFilter: reqControlFilter
                },
                {
                  name: 'Conditional',
                  clauses: {
                    reqTypeClause: "req_type = 'conditional'"
                  },
                  onFilter: reqControlFilter
                }
              ]
            }
          ]
        }
      };

      this.afterFiltersApplied = function (card) {
        var templateVariables = card.options.getTemplateVariables();
        //Sets the existing card query
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        var categories = viewOptions.controls.filterMenu.categories;
        _.forEach(categories, function(category) {
          _.forEach(category.filters, function(filter){
            if(filter.name === viewOptions.controls.selectedFilter) {
              if(filter.clauses.reqTypeClause != templateVariables.reqTypeClause) {
                filter.onFilter(card);
              }
            }
          });
        });
      };

      return this;
    };
    reqTimeToFillAcceptAndAvgDaysOpen.prototype = Card.prototype;

    return reqTimeToFillAcceptAndAvgDaysOpen;
  });
