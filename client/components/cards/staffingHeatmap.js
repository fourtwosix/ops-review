'use strict';

angular.module('opsReviewApp')
  .factory('staffingHeatmap', function (Card, DateUtils) {

    /**
     * Calculates the cumulative status given the number of greens, yellows, and reds
     * @param kpi an object with key/value pairs of colors ('G', 'Y', and 'R') to counts for each.  For example ... {G: 5, Y: 3, R: 9}.  Any key (color) missing in the object will be defaulted to 0
     * Returns 'G' (for green), 'Y' (for yellow), or 'R' (for red) for the cumulative status based on the following rules
     * The rules for cumulative status are as follows:
     *    Cumulative Status     Individual Statuses
     *    -------------------   ---------------------
     *    Green                 All Green
     *    Yellow                Any # of Yellows and 0 Reds
     *    Yellow                Any # of Yellows and 1 Red
     *    Yellow                0 Yellows and 1 Red
     *    Red                   More than one red
     */
    var calcCumStatus = function (kpi) {
      kpi = _.defaults(kpi, {G: 0, Y: 0, R: 0});
      if (kpi['R'] === 0 && kpi['Y'] === 0 && kpi['G'] === 0) {
        return 'N/A';
      } else if (kpi['R'] === 0 && kpi['Y'] === 0) {
        return 'G'
      } else if (kpi['R'] <= 1) {
        return 'Y';
      } else {
        return 'R';
      }
    };

    /**
     * Calculates the percentage of each of the given greens, yellows, and reds
     * @param kpi an object with key/value pairs of colors ('G', 'Y', and 'R') to counts for each.  For example ... {G: 5, Y: 3, R: 9}.  Any key (color) missing in the object will be defaulted to 0
     * Returns the same kpi object (parameter passed in) with the percentages for each of the colors (instead of counts)
     */
    var calcPercentages = function (kpi) {
      kpi = _.defaults(kpi, {G: 0, Y: 0, R: 0});
      var total = _.reduce(kpi, function (total, n) {
        return total + n;
      });
      return _.mapValues(kpi, function (val) {
        return val / total;
      });
    };

    /**
     * Counts to the total number of greens, reds, and yellows for each kpi for all resources, internal (CSC) resources, and subcontractor resources
     * Returns an object with the following structure
     * {
     *    TOTAL: {
     *      kpi-1: {
     *        G: # of greens,
     *        Y: # of yellows,
     *        R: # of reds
     *      },
     *      kpi-2: {...},
     *      kpi-3: {...}
     *    },
     *    TOTAL - CSC: {...},
     *    TOTAL - SUB: {...}
     * }
     */
    var getKpiCounts = function (data, getSubTotals) {
      var returnObj = {};
      returnObj["TOTAL"] = {};
      if (getSubTotals) {
        returnObj["TOTAL - CSC"] = {};
        returnObj["TOTAL - SUB"] = {};
      }

      var keys = _.without(_.keys(data[0]), 'name', 'type');
      _.forEach(keys, function (kpi) {
        var allKpiValues = _.pluck(data, kpi);  // get array of greens, yellows, and reds for this kpi
        returnObj["TOTAL"][kpi] = _.countBy(allKpiValues);  // count greens, yellows, and reds and default each to 0

        if (getSubTotals) {
          var internalKpiValues = _.pluck(_.where(data, {'type': 'CSC'}), kpi);  // get array of greens, yellows, and reds, for this kpi for all internal resources
          returnObj["TOTAL - CSC"][kpi] = _.countBy(internalKpiValues);

          var subKpiValues = _.pluck(_.where(data, {'type': 'SUB'}), kpi);  // get array of greens, yellows, and reds, for this kpi for all subs
          returnObj["TOTAL - SUB"][kpi] = _.countBy(subKpiValues);
        }
      });
      return returnObj;
    };

    var notFiltered = function (card) {
      var templateVariables = card.options.getTemplateVariables();
      return templateVariables.programClause === 'true' && templateVariables.staffingSourceClause === 'true';
    };

    var COLOR_NAMES = {
      G: "Green",
      Y: "Yellow",
      R: "Red"
    };

    var isDisplayColumn = function (column) {
      return !_.contains(['id', 'date', 'updated_at', 'created_at', 'resource_id', 'project_id', 'type', 'program', 'rid'], column);
    };

    var staffingHeatmap = function (cardData) {
      angular.extend(this, new Card(cardData));

      function showMessage(card) {
        var templateVariables = card.options.getTemplateVariables();
        return templateVariables.programClause === 'true' && templateVariables.staffingSourceClause === 'true';
      }

      this.detailsViewOptions.showMessage = showMessage;

      this.summaryViewOptions.preDataTransform = function (card, data, callback) {
        if (data.length > 0) {
          var newData = {};
          var dateColumn = 'date';
          var lastYearRange = DateUtils.getMonthsAgoRange(card, 11);
          // Initialize all months' KPI in the year range as "N/A"
          DateUtils.prependInitializedMonths(data, lastYearRange, isDisplayColumn, dateColumn);

          _.each(data, function (point) {
            _.each(point, function (value, column) {
              var date = moment.utc(point.date).format("MMM YYYY");

              if (isDisplayColumn(column)) {
                newData[column] = newData[column] || {};
                newData[column].name = column;
                value = value ? value : 'N/A';
                newData[column][date] = newData[column][date] || {G: 0, Y: 0, R: 0, 'N/A': 0};
                newData[column][date][value]++; //Count the number of each color for a given month + kpi
              }
            })
          });

          //get the total kpi value for each kpi
          _.each(newData, function (point, date) {
            _.each(point, function (value, kpi) {
              if (kpi != "name") {
                newData[date][kpi] = calcCumStatus(value);
              }
            });
          });

          data = _.values(newData);

          ////TODO May want to enforce a limit later
          //// if heatmap is not being filtered by program or resource, only show the last 3 items for totals
          //// otherwise the chart would be unusable since there are just too many programs and resources to show
          ////if (showMessage(card)) {
          ////  data = data.splice(-3, 3);
          ////}
        }
        callback && callback(data);
      };

      this.detailsViewOptions.preDataTransform = function (card, data, callback) {

        var templateVariables = card.options.getTemplateVariables();

        if (showMessage(card)) {
          data = [];
        }

        data.push({name: ''}); // add empty line as separator between actual data and calculated totals
        var getSubTotals = templateVariables.programOrResourceColumn === 'staffing_source';
        var kpiCounts = getKpiCounts(data, getSubTotals);
        _.forEach(kpiCounts, function (kpiCount, type) {
          var total = {name: type};
          _.forEach(kpiCount, function (kpi, kpiType) {
            total[kpiType] = calcCumStatus(kpi);
          });
          data.push(total);
        });

        _.forEach(data, function (row, index) {
          if (templateVariables.programOrResourceColumn === 'staffing_source') { // if the name column is the name of a resource, add subcontractor designation if applicable
            row['name'] += row['sub'] ? ' (Sub)' : '';
          }
          _.forEach(row, function (value, kpi) {
            if (value == null) {
              data[index][kpi] = "N/A";
            }
          });
          delete row['sub'];
          delete row['type'];
        });

        //TODO May want to enforce a limit later
        // if heatmap is not being filtered by program or resource, only show the last 3 items for totals
        // otherwise the chart would be unusable since there are just too many programs and resources to show
        //if (showMessage(card)) {
        //  data = data.splice(-3, 3);
        //}
        callback && callback(data);
      };

      return this;
    };

    staffingHeatmap.prototype = Card.prototype;

    return staffingHeatmap;
  });
