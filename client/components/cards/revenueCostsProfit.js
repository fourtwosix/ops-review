'use strict';

angular.module('opsReviewApp')
  .factory('revenueCostsProfit', function (Card) {

    var revenueCostsProfit = function (cardData) {
      angular.extend(this, new Card(cardData));

      this.summaryViewOptions.tooltipFormatter = function () {
        return this.x + '<br/>' + this.series.name + '<br/> <b>' +
        this.y.toLocaleString("en", {style: "currency", currency: "USD", maximumFractionDigits: 0}) + '</b>';
      };

      this.summaryViewOptions.yAxisFormatter = function (card) {
        return this.value.toLocaleString("en", {style: "currency", currency: "USD", maximumFractionDigits: 0})
      };

      return this;
    };

    revenueCostsProfit.prototype = Card.prototype;

    return revenueCostsProfit;
  });
