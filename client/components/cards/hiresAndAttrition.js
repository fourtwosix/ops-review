'use strict';

angular.module('opsReviewApp')
  .factory('hiresAndAttrition', function (Card, DataManager, FilterService) {

	var hiresAndAttrition = function(cardData) {
      angular.extend(this, new Card(cardData));

      // hiresAndAttritionLineChart
      // Hires

      this.summaryViewOptions.views.hiresAndAttritionLineChart.onLineClick = function (card, lineData) {
        var templateVariables = card.options.getTemplateVariables();

        var category_date = moment(lineData.category, "MMMM YYYY");
        var start_date    = category_date.startOf('month').format("MM-DD-YYYY");
        var end_date      = category_date.endOf('month').format("MM-DD-YYYY");

        templateVariables.ddStartDate = start_date;
        templateVariables.ddEndDate   = end_date;

	      card.options.summaryViewOptions.activeView = "hiresDrillDownTable";
        FilterService.createFilteredQuery(card, templateVariables)

      };


      // Attrition

      this.summaryViewOptions.views.hiresAndAttritionLineChart.onColumnClick = function (card, colData) {
        var templateVariables = card.options.getTemplateVariables();
        var category_date = moment(colData.category, "MMMM YYYY");
        var start_date    = category_date.startOf('month').format("MM-DD-YYYY");
        var end_date      = category_date.endOf('month').format("MM-DD-YYYY");

        templateVariables.ddStartDate = start_date;
        templateVariables.ddEndDate   = end_date;

	      card.options.summaryViewOptions.activeView = "attritionsDrillDownTable";
        FilterService.createFilteredQuery(card, templateVariables)

      };


      // hiresDrillDownTable

      this.summaryViewOptions.views.hiresDrillDownTable.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
	      card.options.summaryViewOptions.activeView = "hiresAndAttritionLineChart";
        FilterService.createFilteredQuery(card, templateVariables);
      };



      // attritionsDrillDownTable

      this.summaryViewOptions.views.attritionsDrillDownTable.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
	      card.options.summaryViewOptions.activeView = "hiresAndAttritionLineChart";
        FilterService.createFilteredQuery(card, templateVariables);
      };

      return this;
	};

    hiresAndAttrition.prototype = Card.prototype;

    return hiresAndAttrition;
  });
