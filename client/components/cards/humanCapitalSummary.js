'use strict';

angular.module('opsReviewApp')
  .factory('humanCapitalSummary', function (Card, DataManager, FilterService, Utils) {

    var humanCapitalSummary = function(cardData) {
      angular.extend(this, new Card(cardData));

      var getFYTitle = function() {
        var fiscalYear = DataManager.fiscalYearStart.getFullYear() + 1;
        var trimmedYear = fiscalYear.toString().substr(2,2);
        return 'FY' + trimmedYear + ' Human Capital Summary';
      };

      this.title = getFYTitle();

      this.summaryViewOptions.views.hcSummaryBarChart.preDataTransform = function(card, data, callback) {
        var columns = ['Head Count', 'Hires', 'Involuntary Terminations', 'Voluntary Terminations', 'Head Count Of Non-Casual Employees', 'Hires of Non-Casual Employees', 'Voluntary Terminations of Non-Casual Employees', 'Involuntary Terminations of Non-Casual Employees'];

        var hasData = function (d) {
          return _.some(_.values(_.pick(d, columns)), function (value) {
            return parseFloat(value) != 0;
          });
        };

        data = _.reject(data, function(d) {
          return !hasData(d);
        });

        callback && callback(data);
      };

      this.summaryViewOptions.views.hcSummaryBarChart.tooltipFormatter = function() {
        var tooltipText = '<b>' + this.x + '</b><br/>' + this.series.name + ': ' + this.y;
        if(this.series.name.toLowerCase().indexOf("voluntary") >= 0) {
          tooltipText += '<br/>' + 'Total: ' + this.point.stackTotal;
        }
        return tooltipText;
      };

      this.summaryViewOptions.views.hcSummaryBarChart.onBarClick = function(card, barData) {
        var templateVariables = card.options.getTemplateVariables();
        var managerName = Utils.dataFormatter.name(barData.category.toLowerCase(), true);
        var series = barData.series.name.toLowerCase();

        //Set the parentManagerClause, so we can retrieve it when returning to this view
        templateVariables.parentManagerClause = templateVariables.managerClause;
        templateVariables.managerClause = templateVariables.subManagerType + " ~ '" + managerName + "'";

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        var tableView = viewOptions.views.hcDetailsTable;

        if(series === "head count"){
          templateVariables.activeClause = "employment_status LIKE '%active' AND report_date = (SELECT MAX(report_date) FROM permast_data)";
          tableView.numColumns = 4;
          tableView.hiddenColumns = ["personnel_number", "report_date", "hire_date", "termination_date", "termination_type", "tenure"];
        }
        else if(series === "hires") {
          templateVariables.activeClause = "date_trunc('day', hire_date)::DATE BETWEEN '" + DataManager.getPostgresDate(DataManager.fiscalYearStart) + "' AND '" + DataManager.getPostgresDate(DataManager.fiscalYearEnd) + "'";
          tableView.numColumns = 5;
          tableView.hiddenColumns = ["personnel_number", "report_date", "termination_date", "termination_type", "tenure"];
        }
        else if(_.includes(series, "terminations")){
          templateVariables.activeClause = "termination_type LIKE '%voluntary' AND date_trunc('day', termination_date)::DATE BETWEEN '" + DataManager.getPostgresDate(DataManager.fiscalYearStart) + "' AND '" + DataManager.getPostgresDate(DataManager.fiscalYearEnd) + "'";
          tableView.numColumns = 7;
          tableView.hiddenColumns = ["personnel_number", "report_date", "hire_date"];
        }

        viewOptions.activeView = "hcDetailsTable";
        FilterService.createFilteredQuery(card, templateVariables)
      };

      this.summaryViewOptions.views.hcDetailsTable.drillUp = function(card){
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "hcSummaryBarChart";

        //A parentManagerClause will only be set if the filters have not been reset by the user,
        // in this case, we will need to retrieve the parent clause to know how to query the parent view.
        if(templateVariables.parentManagerClause) {
          templateVariables.managerClause = templateVariables.parentManagerClause;
        }

        FilterService.createFilteredQuery(card, templateVariables);
      };

      this.afterFiltersApplied = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.summaryViewOptions;
        if (viewOptions.views[viewOptions.activeView] != viewOptions.views.hcDetailsTable) {
          viewOptions.views[viewOptions.activeView].dataTransform.nameColumn = templateVariables.subManagerType;
        }
      };

      this.onReload = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        card.options.title = getFYTitle();
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        if(card.currentSection === "summary" && viewOptions === card.options[card.currentSection + "ViewOptions"].views.hcDetailsTable) {
          var managerName = Utils.dataFormatter.name(templateVariables.managerClause.split("~")[1].replace(/'/g, '').trim());
          var seriesType = "";
          if(_.includes(templateVariables.activeClause, "active")) {
            seriesType = "head count";
          }
          else if (_.includes(templateVariables.activeClause, "hire_date")) {
            seriesType = "hires";
          }
          else if (_.includes(templateVariables.activeClause, "termination_date")) {
            seriesType = "terminations";
          }
          card.options.title = managerName + ": " + seriesType;
        }
      };

      return this;
    };

    humanCapitalSummary.prototype = Card.prototype;

    return humanCapitalSummary;
  });
