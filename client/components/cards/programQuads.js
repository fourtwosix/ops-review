'use strict';

angular.module('opsReviewApp')
  .factory('programQuads', function (Card, DataManager, FilterService, DeckManager, Auth) {

    var programQuads = function (cardData) {
      angular.extend(this, new Card(cardData));

      function showMessage(card) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        return !filters.project_id && !filters.program_manager && !filters.l3_portfolio && !filters.pra &&
        !filters.l3_manager && !filters.group_name && !filters.division_name && !filters.l2_manager &&
        !filters.l1;

      } // end of function showMessage(card)

      this.summaryViewOptions.showMessage = showMessage;

      this.summaryViewOptions.pnFilter = {
        selectedFilter: '',
        filterValues:   [
          {program_number:'one'},
          {program_number:'two'},
          {program_number:'three'},
          {program_number:'four'},
          {program_number:'five'}
        ]
      };


      this.canExportToCSV = Auth.isAdmin() || Auth.isExecutive() || Auth.isProgramManager();

      // #######################################################
      this.summaryViewOptions.filterByProgramNumber = function (card, pn) {

        var baseId            = card[card.currentSection + 'ViewId'];
        var quadChartMapping  = card.options.getCurrentViewOptions(card.currentSection).quadChartMapping;

        var header      = quadChartMapping.header.data.column;
        var topLeft     = quadChartMapping.topLeft.data.column;
        var topRight    = quadChartMapping.topRight.data.column;
        var bottomLeft  = quadChartMapping.bottomLeft.data.column;
        var bottomRight = quadChartMapping.bottomRight.data.column;
        var footer      = quadChartMapping.footer.data.column;

        // setup some jQuery elements
        var $headerData         = $('#' + baseId  + '-header-data');
        var $topLeftData        = $('#' + baseId  + '-top-left-data');
        var $topRightData       = $('#' + baseId  + '-top-right-data');
        var $bottomLeftData     = $('#' + baseId  + '-bottom-left-data');
        var $bottomRightData    = $('#' + baseId  + '-bottom-right-data');
        var $footerData         = $('#' + baseId  + '-footer-data');

        $headerData.html(         pn[header]);
        $topLeftData.html(        pn[topLeft]);
        $topRightData.html(       pn[topRight]);
        $bottomLeftData.html(     pn[bottomLeft]);
        $bottomRightData.html(    pn[bottomRight]);
        $footerData.html(         pn[footer]);

      }; // end of this.summaryViewOptions.filterByProgramNumber = function (card, pn) {


      // #######################################################
      this.summaryViewOptions.getFilterValues = function (table, filter, query) {
        DeckManager.getFilterValues(table, filter, query).then($.proxy(function (data) {
          this.pnFilter.filterValues = data || [];
        }, this));
      }; // end of this.summaryViewOptions.getFilterValues = function (table, filter, query)


      // #######################################################
      this.summaryViewOptions.preDataTransform = function (card, data, callback) {
        var viewOptions       = card.options.getCurrentViewOptions(card.currentSection);

        if ( data.length > 0 && !card.options.summaryViewOptions.showMessage(card) ) {
          viewOptions.pnFilter.filterValues   = data;
          data = [ data[0] ];
        } else {
          data = [];
          viewOptions.pnFilter.filterValues   = [];
        }

        viewOptions.pnFilter.selectedFilter = data[0] || {program_number: 'No Data'};

        callback && callback(data);
      }; // end of this.summaryViewOptions.preDataTransform = function (card, data, callback)

      return this;
    }; // end of var programQuads = function (cardData)

    programQuads.prototype = Card.prototype;

    return programQuads;
  }); // end of .factory('programQuads', function (Card, DataManager, FilterService, DeckManager, Auth)
// end of file programQuads.js
