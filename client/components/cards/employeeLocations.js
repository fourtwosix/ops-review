'use strict';

angular.module('opsReviewApp')
  .factory('employeeLocations', function (Card, DataManager, ViewsService, FilterService, Utils) {

    var employeeLocations = function(cardData) {
      angular.extend(this, new Card(cardData));

      this.summaryViewOptions.controls = {
        rightControlsContent: ['components/cards/controls/employeeLocations-headcount-controls.html', 'components/deckster/views/geoMap/geoMap-controls.html'],
        selectedFilter: 'Headcount',
        filterMenu:
        {
          populated: false,
          categories: [
            {
              name: 'Headcount',
              filters: [{
                name: 'Headcount',
                clauses: {
                  activeClause: "employment_status = 'active'"
                },
                onFilter: ViewsService.mapControlFilter
              }]
            },
            {
              name: 'Fiscal Year Hires',
              filters: [
                {
                  name: 'Current Hires',
                  clauses: {
                    activeClause: "hire_date >= '" + DataManager.getPostgresDate(DataManager.fiscalYearStart) + "'"
                  },
                  onFilter: ViewsService.mapControlFilter
                },
                {
                  name: 'Terminated Hires',
                  clauses: {
                    activeClause: "termination_date >= '" + DataManager.getPostgresDate(DataManager.fiscalYearStart) + "'"
                  },
                  onFilter: ViewsService.mapControlFilter
                }
              ]
            },
            {
              name: 'Fiscal Year Terms',
              filters: [
                {
                  name: 'Involuntary Terms',
                  clauses: {
                    activeClause: "termination_date >= '" + DataManager.getPostgresDate(DataManager.fiscalYearStart) + "'" +
                    "AND termination_type LIKE '% involuntary'"
                  },
                  onFilter: ViewsService.mapControlFilter
                },
                {
                  name: 'Voluntary Terms',
                  clauses: {
                    activeClause: "termination_date >= '" + DataManager.getPostgresDate(DataManager.fiscalYearStart) + "' " +
                    "AND termination_type LIKE '% voluntary'"
                  },
                  onFilter: ViewsService.mapControlFilter
                }
              ]
            }
          ]
        },
        selectedMapLayer: "auto",
        displayMapLayer: ViewsService.displayMapLayer
      };

      this.summaryViewOptions.tooltipFormatter =  function(markers) {
        var $wrapper = $("<div class='map-info-window-content' />");

        if(_.isArray(markers)) {
          var $table = $("<table class='footable table'/>");
          var $headers = $("<thead tr/>");
          $headers.append('<th>Name</th>');
          $headers.append('<th>Status</th>');
          $headers.append('<th>L3 Manager</th>');
          $headers.append('<th>Work Location</th>');
          $table.append($headers);

          _.each(markers, function(marker) {
            var markerOptions = marker.options;
            var $row = $("<tr/>");

            var name    = Utils.dataFormatter.name(markerOptions['last_name'] + ". " + markerOptions['first_name']);
            var status  = Utils.dataFormatter.default(markerOptions['employment_status']);
            var l3man   = Utils.dataFormatter.name(markerOptions['layer_3_manager']);
            var workLoc = Utils.dataFormatter.default(markerOptions['location']);

            $row.append('<th>' + name + '</th>');
            $row.append('<th>' + status + '</th>');
            $row.append('<th>' + l3man + '</th>');
            $row.append('<th>' + workLoc + '</th>');

            $table.append($row);
          });

          $table.attr('data-page-size', markers.length);
          $wrapper.append($table.footable());
        } else {
          var markerOptions = markers.options;

          var name    = Utils.dataFormatter.name(markerOptions['last_name'] + ". " + markerOptions['first_name']);
          var status  = Utils.dataFormatter.default(markerOptions['employment_status']);
          var l3man   = Utils.dataFormatter.name(markerOptions['layer_3_manager']);
          var workLoc = Utils.dataFormatter.name(markerOptions['location']);

          var $name = $("<div/>");
          $name.append('<label style="display: inline; font-weight: bold;">Name: &nbsp;</label>');
          $name.append('<span>' + name + '</span>');

          var $status = $("<div/>");
          $status.append('<label style="display: inline; font-weight: bold;">Status: &nbsp;</label>');
          $status.append('<span>' + status + '</span>');

          var $l3man = $("<div/>");
          $l3man.append('<label style="display: inline; font-weight: bold;">L3 Manager: &nbsp;</label>');
          $l3man.append('<span>' + l3man + '</span>');

          var $workLoc = $("<div/>");
          $workLoc.append('<label style="display: inline; font-weight: bold;">Work Location: &nbsp;</label>');
          $workLoc.append('<span>' + workLoc + '</span>');


          $wrapper.append($name);
          $wrapper.append($status);
          $wrapper.append($l3man);
          $wrapper.append($workLoc);
        }
        return $wrapper.html();
      };

      this.afterFiltersApplied = function (card) {
        FilterService.setSelectedFilter(card, 'activeClause');
      };

      return this;
    };

    employeeLocations.prototype = Card.prototype;

    return employeeLocations;
  });
