'use strict';

angular.module('opsReviewApp')
  .factory('summaryBarDetails', function (Card) {

    var summaryBarDetails = function(cardData) {
      angular.extend(this, new Card(cardData));

      this.closeCard = function(card, sidebarDeck) {
        // Remove the card from the deck
        card.hidden = true;
        card.hideCard();
        var cardToRemove = _.find(card.$deckster.$cardHash, function(deckCard) {
          return deckCard.options.id == card.options.id;
        });
        delete card.$deckster.$cardHash[cardToRemove.$cardHashKey];

        // Remove the card from the sidebar
        _.remove(sidebarDeck.cards, function(deckCard) {
          return deckCard.id == card.options.id;
        });
      };

      return this;
    };

    summaryBarDetails.prototype = Card.prototype;

    return summaryBarDetails;
  });
