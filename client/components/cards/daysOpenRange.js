'use strict';

angular.module('opsReviewApp')
  .factory('daysOpenRange', function (Card, DataManager, ViewsService, FilterService) {

    var daysOpenRange = function(cardData) {
      angular.extend(this, new Card(cardData));
      var self = this;

      // ColumnRange Chart

      this.summaryViewOptions.views.daysOpenRangeColumnRange.tooltipFormatter = function() {
        var returnFormat = "<b>" + this.x + "</b>";
        var seriesName = this.series.name;
        if(seriesName === "Range") {
          returnFormat += "<br/>Range: <b>" + this.point.low + "</b> - <b>" + this.point.high + "</b> day(s)";
        } else if (seriesName === "Average") {
          returnFormat += "<br/>Average: <b>" + this.y + "</b> day(s)";
        }
        return returnFormat;
      };

      var reqTypeFilter = function (card) {
        var templateVariables = card.options.getTemplateVariables();
        var currentViewOptions = card.options.getCurrentViewOptions(card.currentSection);
        currentViewOptions.controls.selectedFilter = this.name;
        var filters = _.merge(templateVariables, this.clauses);
        FilterService.createFilteredQuery(card, filters);
      };

      this.summaryViewOptions.views.daysOpenRangeColumnRange.controls = {
        rightControlsContent: ['components/cards/controls/paging-controls.html', 'components/cards/controls/req-count-variable-control.html', 'components/cards/controls/req-type-geomap-controls.html'],
        selectedFilter: 'Funded',
        filterMenu:
        {
          populated: false,
          categories: [
            {
              name: 'Requisition Type',
              filters: [
                {
                  name: 'All Reqs',
                  clauses: {
                    reqTypeClause: "true"
                  },
                  onFilter: reqTypeFilter
                },
                {
                  name: 'Funded',
                  clauses: {
                    reqTypeClause: "req_type = 'funded'"
                  },
                  onFilter: reqTypeFilter
                },
                {
                  name: 'Conditional',
                  clauses: {
                    reqTypeClause: "req_type = 'conditional'"
                  },
                  onFilter: reqTypeFilter
                }
              ]
            }
          ]
        },
        selectedMapLayer: "auto",
        displayMapLayer: ViewsService.displayMapLayer
      };

      // View Functions
      this.summaryViewOptions.reqVariable = {
        filterValues: ["Role", "Level", "Contract", "Security Clearance"],
        selectedFilter: "Role"
      };

      this.summaryViewOptions.filterByReqVar = function(card, reqVar) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        if (reqVar === "Level") {
          templateVariables.requisitionVariableClause = "CASE WHEN csc_title ~ '.*:' THEN trim(split_part(csc_title, ':', 1)) ELSE csc_title END AS level, ";
          templateVariables.requisitionVariableGroupByClause = "level ";
          viewOptions.dataTransform.nameColumn = "level";
          templateVariables.cardFilter_variable = "csc_title";
          templateVariables.drillDownReqVariableClause = "trim(split_part(csc_title, ':', 1))";
        } else if (reqVar === "Security Clearance") {
          templateVariables.requisitionVariableClause = " security_clearance, ";
          templateVariables.requisitionVariableGroupByClause = "security_clearance ";
          viewOptions.dataTransform.nameColumn = "security_clearance";
          templateVariables.cardFilter_variable = "security_clearance";
          templateVariables.drillDownReqVariableClause = "security_clearance";
        } else if (reqVar === "Contract") {
          templateVariables.requisitionVariableClause = " client, ";
          templateVariables.requisitionVariableGroupByClause = "client ";
          viewOptions.dataTransform.nameColumn = "client";
          templateVariables.cardFilter_variable = "client";
          templateVariables.drillDownReqVariableClause = "client";
        } else {
          templateVariables.requisitionVariableClause = "CASE WHEN csc_title ~ '.*:' THEN trim(split_part(csc_title, ':', 2)) ELSE csc_title END AS role, ";
          templateVariables.requisitionVariableGroupByClause = "role ";
          viewOptions.dataTransform.nameColumn = "role";
          templateVariables.cardFilter_variable = "csc_title";
          templateVariables.drillDownReqVariableClause = "trim(split_part(csc_title, ':', 2))";
        }
        FilterService.createFilteredQuery(card, templateVariables);
      };

      this.summaryViewOptions.views.daysOpenRangeColumnRange.onColumnRangeClick = function(card, colRngData) {
        var templateVariables = card.options.getTemplateVariables();
        templateVariables.cardFilter_role = colRngData.category.toLowerCase();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "daysOpenRangeHistogram";
        FilterService.createFilteredQuery(card, templateVariables);
      };

      // Expose the chartControls to the card object for paging purposes
      this.chartPaging = DataManager.chartPaging;


      // Histogram

      this.summaryViewOptions.views.daysOpenRangeHistogram.controls = ViewsService.getHistogramControls();
      this.summaryViewOptions.views.daysOpenRangeHistogram.controls.rightControlsContent.splice(0, 0, "components/deckster/views/drilldownView/drilldownView-controls.html");

      this.summaryViewOptions.views.daysOpenRangeHistogram.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "daysOpenRangeColumnRange";
        FilterService.createFilteredQuery(card, templateVariables);
      };

      this.summaryViewOptions.views.daysOpenRangeHistogram.onColumnClick = function(card, columnData) {
        var templateVariables = card.options.getTemplateVariables();

        var ranges = ViewsService.parseHistogramRanges(columnData.category);
        if(ranges.lower && ranges.upper) {
          templateVariables.rangeClause = "req_days_open >= " + ranges.lower + " AND req_days_open <= " + ranges.upper;
        } else if (ranges.lower) {
          templateVariables.rangeClause = "req_days_open < " + ranges.lower;
        } else if (ranges.upper) {
          templateVariables.rangeClause = "req_days_open > " + ranges.upper;
        }

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "daysOpenRangeTable";
        FilterService.createFilteredQuery(card, templateVariables)
      };



      // Table

      this.summaryViewOptions.views.daysOpenRangeTable.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "daysOpenRangeHistogram";
        FilterService.createFilteredQuery(card, templateVariables);
      };



      // Card Functions

      this.loadData = function(card, callback) {
        if(_.isUndefined(card.options.getTemplateVariables().cardFilter_pagingClause)) {
          card.options.getTemplateVariables().cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
        }
        DataManager.defaultLoadData(card, callback);
      };

      this.onReload = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        // Set title for appropriate view
        card.options.title = "Days Open Range";
        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenRangeHistogram) {
          card.options.title += ": " + templateVariables.cardFilter_role + " (" + ViewsService.getReqTypeFromClause(templateVariables.reqTypeClause) + ")";
        }
        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenRangeTable) {
          card.options.title += ": " + templateVariables.cardFilter_role + " (" + ViewsService.getReqTypeFromClause(templateVariables.reqTypeClause) +
            ", " + ViewsService.getRangeValuesFromClause(templateVariables.rangeClause) + ")";
        }
      };

      this.afterFiltersApplied = function (card, filters, deckFiltered) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        // If we are not in the top view, and the deck was filtered, go back to the top view
        if(viewOptions != card.options[card.currentSection + "ViewOptions"].views.daysOpenRangeColumnRange && deckFiltered) {
          var viewOptions = card.options[card.currentSection + 'ViewOptions'];
          viewOptions.activeView = "daysOpenRangeColumnRange";
          FilterService.createFilteredQuery(card, templateVariables);
        }

        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenRangeColumnRange) {
          DataManager.chartPaging.getRecTotal(card, function(recTotal) {
            templateVariables.cardFilter_pagingRecTotal = recTotal;
            // If the deck was filtered, take the user back to the first page
            if(deckFiltered) {
              templateVariables.cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
              FilterService.createFilteredQuery(card, templateVariables);
            }
          });
        }

        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenRangeColumnRange) {
          FilterService.setSelectedFilter(card, 'reqTypeClause');
        }

        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenRangeHistogram) {
          FilterService.setSelectedFilter(card, 'binClause');
        }
      };

      return this;
    };

    daysOpenRange.prototype = Card.prototype;

    return daysOpenRange;
  });
