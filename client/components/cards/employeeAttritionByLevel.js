'use strict';

angular.module('opsReviewApp')
  .factory('employeeAttritionByLevel', function (Card, DataManager, ViewsService, FilterService) {

    var employeeAttritionByLevel = function(cardData) {
      angular.extend(this, new Card(cardData));
      var self = this;

      var termTypeFilter = function (card) {
        var templateVariables = card.options.getTemplateVariables();
        var currentViewOptions = card.options.getCurrentViewOptions(card.currentSection);
        currentViewOptions.controls.selectedFilter = this.name;
        var filters = _.merge(templateVariables, this.clauses);
        FilterService.createFilteredQuery(card, filters);
      };

      this.summaryViewOptions.controls = {
        rightControlsContent: ['components/cards/controls/req-type-geomap-controls.html'],
        selectedFilter: 'All Terminations',
        filterMenu:
        {
          categories: [
            {
              name: 'Termination Type',
              filters: [
                {
                  name: 'All Terminations',
                  clauses: {
                    termTypeClause: "all"
                  },
                  onFilter: termTypeFilter
                },
                {
                  name: 'Voluntary Terminations',
                  clauses: {
                    termTypeClause: "vol"
                  },
                  onFilter: termTypeFilter
                },
                {
                  name: 'Involuntary Terminations',
                  clauses: {
                    termTypeClause: "invol"
                  },
                  onFilter: termTypeFilter
                }
              ]
            }
          ]
        }
      };

      this.summaryViewOptions.preDataTransform = function(card, data, cb) {
        var returnValue = {};
        var possibleLevels = _.uniq(_.pluck(data, 'job_level'));

        var defaults = {};
        _.each(possibleLevels, function (level) {
          defaults[level] = 0;
        });

        _.each(data, function(row){
          returnValue[row.month] = returnValue[row.month] || {month: row.month};
          _.defaults(returnValue[row.month], defaults);
          returnValue[row.month][row.job_level] = row.termination_percentage;
        });
        cb(_.values(returnValue));
      };

      this.summaryViewOptions.tooltipFormatter = function () {
        return this.x + '<br/>' + this.series.name +
          Highcharts.numberFormat(this.y, 2,'.') + '%</b>';
      };


      // Card Functions

      this.onReload = function(card) {
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        // Reset Hidden Categories any time the card is reloaded
        delete viewOptions.hiddenCategories;

        // Set title for appropriate view
        card.options.title = "Employee Attrition By Level";
      };

      return this;
    };

    employeeAttritionByLevel.prototype = Card.prototype;

    return employeeAttritionByLevel;
  });
