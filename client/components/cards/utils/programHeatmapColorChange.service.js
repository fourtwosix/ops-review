'use strict';

angular.module('opsReviewApp')
  .factory('ProgramHeatmapColorChangeUtils', function () {
    var dlColor = "na";

    var changeColor = function ($key, $cell, type, budgetOI, bmp, eac, fyoi, ct, fiscalMonth) {
      var mayDate = new Date('2018-05-01');
      var fyDate = fiscalMonth.split("-");
      var fiscalDate = new Date(fyDate[1]+"-"+fyDate[0]+"-01");
      var value = $cell.text().toLowerCase();
      var green = "green";
      var yellow = "yellow";
      var red = "red";
      var notAvailable = "na";
      var color = null;
      if($key === "pra_fy_margin" || $key === "fy_margin_dollar" || $key === "fy_revenue_dollar" || $key === "pra_eac_margin" || $key === "fy_margin_color" || $key === "eac_margin_color")  {
        type = "color";
      }

      if (type === "percent") {
        if (value === 'N/A' || value === 'null' || value === '') {
          $cell.text("N/A");
          color = notAvailable;
        } else {
          if (_.any(["eac_margin_variance", "eac_margin", "fy_margin", "fy_margin_variance"], function(val) {return $key.indexOf(val) > -1; })) {
            var percentVal = (eac / bmp) * 100;

            var compareMargins = function(operator, marginRulePercent) {
              var compare = {
                '<': function(x, y) {return x < y},
                '>': function(x, y) {return x > y},
                '<=': function(x, y) {return x <= y},
                '>=': function(x, y) {return x >= y}
              };

              var negMappings = {
                '<': '>',
                '>': '<',
                '<=': '>=',
                '>=': '<='
              };

              return bmp < 0 ? compare[negMappings[operator]](percentVal, marginRulePercent) : compare[operator](percentVal, marginRulePercent);
            };
          if($key === "eac_margin" || $key === "eac_margin_variance"){
            if(fiscalDate >= mayDate){
              if (compareMargins('>=', 100)) {
                color = green;
              } else if ((compareMargins('>=', 90) && compareMargins('<', 100))) {
                color = yellow;
              } else if (compareMargins('<', 90) || eac == 0) {
                color = red;
              }
            }
            else{
              //General margin rules
              if (compareMargins('>', 90)) {
                color = green;
              } else if ((compareMargins('>=', 80) && compareMargins('<=', 90))) {
                color = yellow;
              } else if (compareMargins('<', 80)) {
                color = red;
              }
            }
          }
            if($key === "fy_margin") {
              value = value.substring(0, value.length-1);
              budgetOI = budgetOI.substring(0, budgetOI.length-1);
              if(fiscalDate >= mayDate){
                if (value < budgetOI * .9 || value < 0) {
                  color = red;
                }
                else if (value >= budgetOI * .9 && value < budgetOI * 1.0) {
                  color = yellow;
                }
                else if (value >= budgetOI * 1.0) {
                  color = green;
                }
              }

              else{
                if (value <= 3) {
                  color = red;
                }
                else if ((value > 3) && (value <= 5)) {
                  color = yellow;
                  if(value < budgetOI * .8) {
                    color = red;
                  }
                } else if (value >= budgetOI * .8 && value < budgetOI * .9) {
                  color = yellow;

                } else if (value < budgetOI * .8) {
                  color = red;
                }
                else if (value >= budgetOI * .9) {
                  color = green;
              }
            }
          }
            if($key === "fy_margin_variance"){
              value = fyoi.substring(0, fyoi.length-1);
              budgetOI = budgetOI.substring(0, budgetOI.length-1);
              if(fiscalDate >= mayDate){
                if (value < budgetOI * .9 || value < 0) {
                  color = red;
                }
                else if (value >= budgetOI * .9 && value < budgetOI * 1.0) {
                  color = yellow;
                }
                else if (value >= budgetOI * 1.0) {
                color = green;
                }
            }
            else{
              if (value <= 3) {
                color = red;
              }
              else if ((value > 3) && (value <= 5)) {
                color = yellow;
                if(value < budgetOI * .8) {
                  color = red;
                }
              } else if (value >= budgetOI * .8 && value < budgetOI * .9) {
                color = yellow;

              } else if (value < budgetOI * .8) {
                color = red;
              }
              else if (value >= budgetOI * .9) {
              color = green;
            }
            }
          }

          }
          //  if ($key.indexOf("staffing") > -1) {
          //   if (_.includes(ct.toLowerCase(), 'ffp') || _.includes(ct.toLowerCase(), 'fup')) {
          //     if (value >= 90 && value <= 105) {
          //       color = green;
          //     } else if (value >= 80 && value < 90) {
          //       color = yellow;
          //     } else if (value > 105 || value < 80) {
          //       color = red;
          //     }
          //   }
          //   else {
          //     if (value > 95) {
          //       color = green;
          //     } else if (value >= 85 && value <= 95) {
          //       color = yellow;
          //     } else if (value < 85) {
          //       color = red;
          //     }
          //   }
          // }
        }
      } else if (type === "color") {
        if (value === 'g') {
          color = green;
        } else if (value === 'y') {
          color = yellow;
        } else if (value === 'r') {
          color = red;
        } else if (value === 'n/a' || value === 'null' || value === '') {
          $cell.text("N/A");
          color = notAvailable;
        }
      }
      $cell.addClass("colored " + color);
    };

    var populateIndex = function ($headers, $values) {
      var index = {
        contract: null,
        technical: null,
        investment: null,
        schedule: null,
        customer_satisfaction: null,
        staffing: null,
        internal_support_suppliers: null,
        cyber_security: null,
        overall_project: null,
        security_authorization: null,
        scanning: null,
        flaw_remediation: null,
        inventory_status: null,
        asset_status: null,
        external_access: null,
        device_hardening: null,
        security_event_management: null,
        identified_security_roles_responsibilities: null,
        account_management: null,
        contract_security_requirements: null,
        overall: null,
        hardware_software: null,
        performance_services: null,
        us_subcontractor: null,
        foreign_subcontractor: null,
        foreign_hardware_software: null,
        personal_protective_equipment: null,
        foreign_military_sale: null,
        export_license: null,
        baseline_eac_margin: null,
        eac_margin_variance: null,
        eac_margin: null,
        eac_margin_color: null,
        baseline_fy_margin: null,
        fy_margin: null,
        fy_margin_dollar: null,
        fy_margin_color: null,
        fy_margin_variance: null,
        fy_revenue_dollar: null,
        contract_type: null,
        pra_contract: null,
        pra_technical: null,
        pra_margin: null,
        forecast_next_overall: null,
        pra_schedule: null,
        pra_customer_satisfaction: null,
        pra_staffing: null,
        pra_internal_support_suppliers: null,
        pra_cyber_security: null,
        pra_fy_margin: null,
        pra_margin_dollar: null,
        pra_fy_margin_dollar: null,
        pra_investment: null,
        pra_revenue: null,
        pra_eac_margin : null,
        pra_overall_project: null,
        pra_contract_type: null
      };
      //populate the header index
      $headers.each(function (loc, h) {
        var header = _.snakeCase($(h).text());
        if($(h).text().includes('$')){
          header = header+'_dollar';
        }
        console.log(header) //remove later if not needed
        if (header in index) {
          index[header] = loc;
        }
      });

      //change the color of the kpi cells for coloring

      _.each(index, function (i, key) {
        // No color should be set for baseline_ros
        if (key.indexOf("baseline_eac_margin") > -1 || key.indexOf("contract_type") > -1 || key.indexOf("baseline_fy_margin") > -1  ||  key.indexOf("date_submitted") > -1 ){
          return;
        }
        var type;

        if (_.any(["fy_margin", "fy_margin_variance","eac_margin","eac_margin_variance"], function(val) { return key.indexOf(val) > -1; })) {
          type = "percent";
        } else {
          type = "color";
        }
        var baselineMarginColIndex = index.baseline_eac_margin;
        var eacMarginPercentageColIndex = index.eac_margin;
        var fyoiPercentageColIndex = index.fy_margin;
        var contractTypeColIndex = index.contract_type;
        var budgetOiPercentageColIndex = index.baseline_fy_margin;

        while (i < $values.size()) {
	          changeColor(key,$($values[i]), type, $($values[budgetOiPercentageColIndex]).text(),   $($values[baselineMarginColIndex]).text(), $($values[eacMarginPercentageColIndex]).text(), $($values[fyoiPercentageColIndex]).text(), $($values[contractTypeColIndex]).text(), $($values[1]).text());
          i += $headers.size();
          baselineMarginColIndex += $headers.size();
          eacMarginPercentageColIndex += $headers.size();
          fyoiPercentageColIndex += $headers.size();
          contractTypeColIndex += $headers.size();
          budgetOiPercentageColIndex += $headers.size();
        }
      });
    };
    return {
      populateIndex: populateIndex
    }
  });
