'use strict';

angular.module('opsReviewApp')
  .factory('glassdoorRating', function (Card, SyncCharts) {

    var glassdoorRating = function(cardData) {
      angular.extend(this, new Card(cardData));

      this.summaryViewOptions.highchartOptions = {
        chart: {
          marginLeft: 50
        },
        xAxis: {
          crosshair: true
        },
        tooltip: {
          positioner: function () {
            return {
              x: this.chart.chartWidth - this.label.width - 70, // right aligned
              y: 3
            };
          },
          borderWidth: 0,
          backgroundColor: 'none',
          pointFormat: '{point.y}',
          headerFormat: '',
          shadow: false,
          style: {
            fontSize: '18px'
          },
          delayForDisplay: 1
        }
      };

      this.summaryViewOptions.syncChartsZoom = SyncCharts.syncChartsZoom;

      this.onReload = function(card) {
        SyncCharts.onCardReload(card);
      };

      return this;
    };

    glassdoorRating.prototype = Card.prototype;

    return glassdoorRating;
  });
