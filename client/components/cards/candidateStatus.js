'use strict';

angular.module('opsReviewApp')
  .factory('candidateStatus', function (Card, DataManager, FilterService, DeckManager, Utils) {

    var candidateStatus = function(cardData) {
      angular.extend(this, new Card(cardData));

      // candidatesDonutChart

      this.summaryViewOptions.views.candidatesDonutChart.columnChartView = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "candidatesColumnChart";
        FilterService.createFilteredQuery(card, templateVariables);
      };


      this.summaryViewOptions.views.candidatesDonutChart.tooltipFormatter = function () {
        return this.key + ' ' + this.series.name + ': <b>' + this.y + '</b>'
      };

      this.summaryViewOptions.views.candidatesDonutChart.legendFormatter = function () {
        return this.name + ' ' + '<br/><div style="font-weight: bold; font-size: 20px; text-align: center; width: 100%; margin-left: -5px;">' + this.y + '</div>'
      };


      // candidatesColumnChart

      this.summaryViewOptions.views.candidatesColumnChart.preDataTransform = function(card, data, callback) {
        //This will proper case the category names without removing special characters.
        _.forEach(data, function(d) {
          d.step = Utils.dataFormatter.titleKeepSymbols(d.step);
        });
        callback && callback(data);
      };

      this.summaryViewOptions.views.candidatesColumnChart.onColumnClick = function(card, columnData) {
        var viewOptions       = card.options[card.currentSection + 'ViewOptions'];
        var templateVariables = card.options.getTemplateVariables();
        var series_name       = columnData.series.name.toLowerCase();

/*
  To get a stacked/grouped column chart the group name is prepended to the series name.
  In this case there are only two groups 'conditional' and 'funded'.  Only 'funded' is
  prepended; ergo the default group is 'conditional'
*/
        var reqType           = 'conditional';

        if( 0 == series_name.indexOf("funded") ) {
          reqType     = 'funded';
          series_name = series_name.slice(7); // MAGIC: 7 == 'funded'.length+1; remove the group name
        }

        templateVariables.candidateStep         = columnData.category.toLowerCase();
        templateVariables.candidateRollupStatus = series_name;
        templateVariables.reqType               = reqType;

        viewOptions.activeView = "candidatesTable";
        FilterService.createFilteredQuery(card, templateVariables)
      };

      this.summaryViewOptions.views.candidatesColumnChart.drillUp = function(card){
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "candidatesDonutChart";

        FilterService.createFilteredQuery(card, templateVariables);
      };

      this.summaryViewOptions.views.candidatesColumnChart.onXAxisLabelClick = function(card, labelData) {
        // If the label is not wrapped by Highcharts, the category name is simply the textContent.
        // However, if Highcharts wrapped the label, then we have to investigate the HTML to get the value of the title tag.
        var categoryName = labelData.textContent;
        var categoryHTML = $.parseHTML(labelData.innerHTML);
        var categoryTitleTag = _.find(categoryHTML, function(tag) {
          return tag.nodeName === 'TITLE'
        });
        if(!_.isUndefined(categoryTitleTag)) {
          categoryName = categoryTitleTag.textContent;
        }

        var chart = card["summaryColumnChart"];
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        //Save off the hidden category or restore it if hidden.
        if(_.isUndefined(viewOptions.hiddenCategories)) {
          viewOptions.hiddenCategories = {
            seriesData: _.cloneDeep(chart.series),
            names: [],
            indexes: []
          }
        }

        var categoryIndex = _.indexOf(chart.xAxis[0].categories, categoryName);

        // If the category is already hidden, then we need to restore the data to the chart's series
        // Remove the hidden category from the JSON, or if the category isn't hidden, add it to the JSON
        // Iterate through each series in the chart
        // Map the "y" data to an array for manipulation
        // Manipulate the "y" data, setting it to the value in the seriesData that was stored in the JSON, or setting it to 0 for the category being hidden
        // Set the data back to the chart's series - do not redraw the chart yet
        // Redraw the chart when done
        var isHidden = _.includes(viewOptions.hiddenCategories.indexes, categoryIndex);
        if(isHidden) {
          _.pull(viewOptions.hiddenCategories.indexes, categoryIndex);
          _.pull(viewOptions.hiddenCategories.names, categoryName);
        } else {
          viewOptions.hiddenCategories.indexes.push(categoryIndex);
          viewOptions.hiddenCategories.names.push(categoryName);
        }
        _.forEach(chart.series, function(series, i){
          var data = _.cloneDeep(series.yData);
          if(isHidden){
            data[categoryIndex] = viewOptions.hiddenCategories.seriesData[i].yData[categoryIndex];
          } else {
            data[categoryIndex] = 0;
          }
          series.setData(data, false);
        });
        chart.redraw();
      };

      this.summaryViewOptions.views.candidatesColumnChart.xAxisFormatter = function(card) {
        var categoryName = '<span>(Cond, Funded)</span> <br>'+ this.value;
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        if(!_.isUndefined(viewOptions.hiddenCategories) && _.includes(viewOptions.hiddenCategories.names, categoryName)) {
          categoryName = '<span style="fill: grey;">' + categoryName + '</span>';
        }
        return  categoryName;
      };

      this.summaryViewOptions.views.candidatesColumnChart.tooltipFormatter = function () {
        var name = _.contains(this.series.name, 'Funded') ? this.series.name : 'Conditional ' + this.series.name;
        return this.x + '<br/>' + name + ': <b>' + this.y + '</b>'
      };


      // candidatesTable

      this.summaryViewOptions.views.candidatesTable.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "candidatesColumnChart";

        FilterService.createFilteredQuery(card, templateVariables);
      };



      // View Functions

      this.summaryViewOptions.getFilterValues = function (table, filter, query) {
        DeckManager.getFilterValues(table, filter, query).then($.proxy(function (data) {
          this.reqFilter.filterValues = data || [];
        }, this));
      };



      // Card Functions

      this.onReload = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        // Reset Hidden Categories any time the card is reloaded
        delete viewOptions.hiddenCategories;

        // Set title for appropriate view
        card.options.title = "Candidate Status";
        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.candidatesTable) {
          card.options.title += ": " + templateVariables.candidateStep;
        }
      };

      return this;
    };

    candidateStatus.prototype = Card.prototype;

    return candidateStatus;
  });
