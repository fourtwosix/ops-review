'use strict';

angular.module('opsReviewApp')
  .factory('requisitionCountByVariable', function (Card, DataManager, ViewsService, FilterService) {

    var requisitionCountByVariable = function(cardData) {
      angular.extend(this, new Card(cardData));
      var self = this;

      var reqTypeFilter = function (card) {
        var templateVariables = card.options.getTemplateVariables();
        var currentViewOptions = card.options.getCurrentViewOptions(card.currentSection);
        currentViewOptions.controls.selectedFilter = this.name;
        var filters = _.merge(templateVariables, this.clauses);
        FilterService.createFilteredQuery(card, filters);
      };

      this.summaryViewOptions.controls = {
        rightControlsContent: ['components/cards/controls/paging-controls.html', 'components/cards/controls/req-count-variable-control.html', 'components/cards/controls/req-type-geomap-controls.html'],
        selectedFilter: 'Funded',
        filterMenu:
        {
          populated: false,
          categories: [
            {
              name: 'Requisition Type',
              filters: [
                {
                  name: 'All Reqs',
                  clauses: {
                    reqTypeClause: "true"
                  },
                  onFilter: reqTypeFilter
                },
                {
                  name: 'Funded',
                  clauses: {
                    reqTypeClause: "req_type = 'funded'"
                  },
                  onFilter: reqTypeFilter
                },
                {
                  name: 'Conditional',
                  clauses: {
                    reqTypeClause: "req_type = 'conditional'"
                  },
                  onFilter: reqTypeFilter
                }
              ]
            }
          ]
        },
        selectedMapLayer: "auto",
        displayMapLayer: ViewsService.displayMapLayer
      };

      // View Functions
      this.summaryViewOptions.reqVariable = {
        filterValues: ["Role", "Level", "Contract", "Security Clearance"],
        selectedFilter: "Role"
      };

      this.summaryViewOptions.filterByReqVar = function(card, reqVar) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        if (reqVar === "Level") {
          templateVariables.requisitionVariableClause = "CASE WHEN csc_title ~ '.*:' THEN trim(split_part(csc_title, ':', 1)) ELSE csc_title END AS level, ";
          templateVariables.requisitionVariableGroupByClause = "level ";
          viewOptions.dataTransform.nameColumn = "level";
        } else if (reqVar === "Security Clearance") {
          templateVariables.requisitionVariableClause = " security_clearance, ";
          templateVariables.requisitionVariableGroupByClause = "security_clearance ";
          viewOptions.dataTransform.nameColumn = "security_clearance";
        } else if (reqVar === "Contract") {
          templateVariables.requisitionVariableClause = " client, ";
          templateVariables.requisitionVariableGroupByClause = "client ";
          viewOptions.dataTransform.nameColumn = "client";
        } else {
          templateVariables.requisitionVariableClause = "CASE WHEN csc_title ~ '.*:' THEN trim(split_part(csc_title, ':', 2)) ELSE csc_title END AS role, ";
          templateVariables.requisitionVariableGroupByClause = "role ";
          viewOptions.dataTransform.nameColumn = "role";
        }
        FilterService.createFilteredQuery(card, templateVariables);
      };


      // Expose the chartControls to the card object for paging purposes
      this.chartPaging = DataManager.chartPaging;

      // Card Functions

      this.loadData = function(card, callback) {
        if(_.isUndefined(card.options.getTemplateVariables().cardFilter_pagingClause)) {
          card.options.getTemplateVariables().cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
        }
        DataManager.defaultLoadData(card, callback);
      };


      this.afterFiltersApplied = function (card, filters, deckFiltered) {
        var templateVariables = card.options.getTemplateVariables();
        DataManager.chartPaging.getRecTotal(card, function(recTotal) {
          templateVariables.cardFilter_pagingRecTotal = recTotal;
          // If the deck was filtered, take the user back to the first page
          if(deckFiltered) {
            templateVariables.cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
            FilterService.createFilteredQuery(card, templateVariables);
          }
        });
      };

      return this;
    };

    requisitionCountByVariable.prototype = Card.prototype;

    return requisitionCountByVariable;
  });
