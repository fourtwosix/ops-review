'use strict';

angular.module('opsReviewApp')
  .factory('revenueCostsTimeline', function (Card, DataManager, DateUtils, $filter) {

    var revenueCostsTimeline = function (cardData) {
      angular.extend(this, new Card(cardData));

      var getFYTitle = function(card) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        return moment(filters.fiscalGDITEnd, "MM/DD/YYYY").year() + ' FY Revenue & Costs';
      };

      this.summaryViewOptions.tooltipFormatter = function () {
        return this.x + '<br/>' + this.series.name + '<br/> <b>' + $filter('currency')(this.y, '$', 0) + '</b>';
      };

      this.summaryViewOptions.yAxisFormatter = function (card) {
        return $filter('currency')(this.value, '$', 0);
      };

      this.summaryViewOptions.preDataTransform = function(card, data, callback) {
        card.options.title = getFYTitle(card);
        var filters = card.$deckster.options.getSelectedFiltersJSON();

        var monthIndex = 0;
        _.each(data, function(p, i) {
          if(moment(filters.endDate).isSame(p.month, 'month')) {
            monthIndex = i;
            return;
          }
        });

        _.set(card, 'options.summaryViewOptions.xAxisPlotLines', [{
          color: '#e67447', // Color value
          dashStyle: 'DashDot', // Style of the plot line. Default to solid
          value: monthIndex, // Value of where the line will appear
          width: 2 // Width of the line
        }]);

        data = _.map(data, function(point) {
          if(_.isNull(point.target_revenue)) {
            delete point.target_revenue;
          }
          return point;
        });

        callback && callback(data);
      };

      return this;
    };

    revenueCostsTimeline.prototype = Card.prototype;

    return revenueCostsTimeline;
  });
