'use strict';

angular.module('opsReviewApp')
  .factory('requisitionsByProgram', function (Card, DataManager, ViewsService, FilterService) {

    var requisitionsByProgram = function(cardData) {
      angular.extend(this, new Card(cardData));

      // Expose the chartControls to the card object for paging purposes
      this.chartPaging = DataManager.chartPaging;

      // Card Functions

      this.loadData = function(card, callback) {
        if(_.isUndefined(card.options.getTemplateVariables().cardFilter_pagingClause)) {
          card.options.getTemplateVariables().cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
        }
        DataManager.defaultLoadData(card, callback);
      };


      this.afterFiltersApplied = function (card, filters, deckFiltered) {
        var templateVariables = card.options.getTemplateVariables();
        DataManager.chartPaging.getRecTotal(card, function(recTotal) {
          templateVariables.cardFilter_pagingRecTotal = recTotal;
          // If the deck was filtered, take the user back to the first page
          if(deckFiltered) {
            templateVariables.cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
            FilterService.createFilteredQuery(card, templateVariables);
          }
        });
      };

      return this;
    };

    requisitionsByProgram.prototype = Card.prototype;

    return requisitionsByProgram;
  });
