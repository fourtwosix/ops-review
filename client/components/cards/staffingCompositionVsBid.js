'use strict';

angular.module('opsReviewApp')
  .factory('staffingCompositionVsBid', function (Card) {

    var staffingComposition = function(cardData) {
      angular.extend(this, new Card(cardData));

      this.summaryViewOptions.tooltipFormatter = function() {
        return this.x + '<br/>' + this.series.name + ': <b>' +
          Highcharts.numberFormat(this.y, 1,'.') + '%</b>';
      };

      this.summaryViewOptions.yAxisFormatter = function () {
        return Highcharts.numberFormat(this.value, 0,'.') + '%'
      };

      return this;
    };

    staffingComposition.prototype = Card.prototype;

    return staffingComposition;
  });

