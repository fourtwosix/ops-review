'use strict';

angular.module('opsReviewApp')
  .factory('revenueCostsSummary', function (Card) {

    var revenueCostsSummary = function (cardData) {
      angular.extend(this, new Card(cardData));

      function showMessage(card) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        return !filters.project_id && !filters.program_manager && !filters.l3_portfolio && !filters.pra &&
          !filters.l3_manager && !filters.group_name && !filters.division_name && !filters.l2_manager &&
          !filters.l1;
      }

      this.summaryViewOptions.showMessage = showMessage;

      this.summaryViewOptions.preDataTransform = function (card, data, callback) {

        if (card.options.summaryViewOptions.showMessage(card)) {
          data = {};
        }

        callback && callback(data);
      };

      return this;
    };

    revenueCostsSummary.prototype = Card.prototype;

    return revenueCostsSummary;
  });
