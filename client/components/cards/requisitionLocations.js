'use strict';

angular.module('opsReviewApp')
  .factory('requisitionLocations', function (Card, DataManager, ViewsService, FilterService) {

    var requisitionLocations = function(cardData) {
      angular.extend(this, new Card(cardData));

      this.summaryViewOptions.controls = {
          rightControlsContent: ['components/cards/controls/req-type-geomap-controls.html', 'components/deckster/views/geoMap/geoMap-controls.html'],
          selectedFilter: 'Funded',
          filterMenu:
          {
            populated: false,
            categories: [
              {
                name: 'Requisition Type',
                filters: [
                  {
                    name: 'Funded',
                    clauses: {
                      reqTypeClause: "req_type = 'funded'"
                    },
                    onFilter: ViewsService.mapControlFilter
                  },
                  {
                    name: 'Conditional',
                    clauses: {
                      reqTypeClause: "req_type = 'conditional'"
                    },
                    onFilter: ViewsService.mapControlFilter
                  }
                ]
              }
            ]
          },
          selectedMapLayer: "auto",
          displayMapLayer: ViewsService.displayMapLayer
      };

      this.summaryViewOptions.tooltipFormatter =  function(markers) {
        var $wrapper = $("<div class='map-info-window-content' />");

        if(_.isArray(markers)) {
          var $table = $("<table class='footable table'/>");
          var $headers = $("<thead tr/>");
          $headers.append('<th>Average Requisition Days Open</th>');
          $table.append($headers);

          _.each(markers, function(marker) {
            var $row = $("<tr/>");
            var avg = marker.options['avg'];
            var avgRounded = Math.round(avg * 100) / 100;
            $row.append('<th>' + avgRounded + '</th>');
            $table.append($row);
          });

          $table.attr('data-page-size', markers.length);
          $wrapper.append($table.footable());
        } else {
          var avg = markers.options['avg'];
          var avgRounded = Math.round(avg * 100) / 100;

          var $avg = $("<div/>");
          $avg.append('<label style="display: inline; font-weight: bold;">Average Requisition Days Open: &nbsp;</label>');
          $avg.append('<span>' + avgRounded + '</span>');

          $wrapper.append($avg);
        }
        return $wrapper.html();
      };

      this.afterFiltersApplied = function (card) {
        FilterService.setSelectedFilter(card, 'reqTypeClause');
      };

      return this;
    };

    requisitionLocations.prototype = Card.prototype;

    return requisitionLocations;
  });
