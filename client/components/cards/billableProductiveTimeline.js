'use strict';

angular.module('opsReviewApp')
  .factory('billableProductiveTimeline', function (Card) {

    var billableProductiveTimeline = function(cardData) {
      angular.extend(this, new Card(cardData));

      this.summaryViewOptions.preDataTransform = function(card, data, callback) {
        data = _.map(_.groupBy(data, 'week_ending_date'), function (week, date) {
          var ret = {
            week_ending_date: date,
            total_productive: 0,
            total_billable: 0,
            manager_total_productive: 0,
            manager_total_billable: 0,
            non_manager_total_productive: 0,
            non_manager_total_billable: 0
          };

          _.each(week, function (utilization) {
            if (JSON.parse(utilization.is_manager)) {
              ret.manager_total_productive = utilization.total_productive;
              ret.manager_total_billable = utilization.total_billable
            } else if(utilization.is_manager === null) {
              ret.total_productive = utilization.total_productive;
              ret.total_billable = utilization.total_billable;
            } else {
              ret.non_manager_total_productive = utilization.total_productive;
              ret.non_manager_total_billable = utilization.total_billable;
            }
          });
          return ret;
        });

        callback && callback(data);
      };

      this.summaryViewOptions.tooltipFormatter = function () {
        return this.x + '<br/>' + this.series.name + '<br/>Utilization: <b>' +
          Highcharts.numberFormat(this.y * 100, 2,'.') + '%</b>';
      };

      this.summaryViewOptions.yAxisFormatter = function () {
        return Highcharts.numberFormat(this.value * 100, 0,'.') + '%'
      };

      return this;
    };

    billableProductiveTimeline.prototype = Card.prototype;

    return billableProductiveTimeline;
  });
