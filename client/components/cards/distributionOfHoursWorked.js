'use strict';

angular.module('opsReviewApp')
  .factory('distributionOfHoursWorked', function (Card, DataManager, ViewsService, FilterService) {

    var distributionOfHoursWorked = function(cardData) {
      angular.extend(this, new Card(cardData));

      this.detailsViewOptions.views.distHoursHistogram.controls = ViewsService.getHistogramControls();

      this.detailsViewOptions.views.distHoursHistogram.onColumnClick = function(card, columnData) {
        var templateVariables = card.options.getTemplateVariables();

        var ranges = ViewsService.parseHistogramRanges(columnData.category);
        if(ranges.lower && ranges.upper) {
          templateVariables.rangeClause = "wtd_total_hours >= " + ranges.lower + " AND wtd_total_hours <= " + ranges.upper;
        } else if (ranges.lower) {
          templateVariables.rangeClause = "wtd_total_hours < " + ranges.lower;
        } else if (ranges.upper) {
          templateVariables.rangeClause = "wtd_total_hours > " + ranges.upper;
        }

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "distHourTable";
        FilterService.createFilteredQuery(card, templateVariables)
      };

      //Labor Type Histogram View
      this.detailsViewOptions.views.distHourTable.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        card.options.title = "Distribution of Hours Worked";

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "distHoursHistogram";

        //A parentManagerClause will only be set if the filters have not been reset by the user,
        // in this case, we will need to retrieve the parent clause to know how to query the parent view.
        if(templateVariables.parentManagerClause) {
          templateVariables.managerClause = templateVariables.parentManagerClause;
        }

        FilterService.createFilteredQuery(card, templateVariables);
      };

      return this;
    };

    distributionOfHoursWorked.prototype = Card.prototype;

    return distributionOfHoursWorked;
  });
