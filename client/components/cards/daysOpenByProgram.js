'use strict';

angular.module('opsReviewApp')
  .factory('daysOpenByProgram', function (Card, DataManager, ViewsService, FilterService) {

    var daysOpenByProgram = function(cardData) {
      angular.extend(this, new Card(cardData));

      // ColumnRange Chart

      this.summaryViewOptions.views.daysOpenByProgramColumnRange.tooltipFormatter = function() {
        var returnFormat = "<b>" + this.x + "</b>";
        var seriesName = this.series.name;
        if(seriesName === "Range") {
          returnFormat += "<br/>Range: <b>" + this.point.low + "</b> - <b>" + this.point.high + "</b> day(s)";
        } else if (seriesName === "Average") {
          returnFormat += "<br/>Average: <b>" + this.y + "</b> day(s)";
        }
        return returnFormat;
      };

      var reqTypeFilter = function (card) {
        var templateVariables = card.options.getTemplateVariables();
        var currentViewOptions = card.options.getCurrentViewOptions(card.currentSection);
        currentViewOptions.controls.selectedFilter = this.name;
        var filters = _.merge(templateVariables, this.clauses);
        FilterService.createFilteredQuery(card, filters);
      };

      this.summaryViewOptions.views.daysOpenByProgramColumnRange.controls = {
        rightControlsContent: ['components/cards/controls/paging-controls.html', 'components/cards/controls/req-type-geomap-controls.html'],
        selectedFilter: 'Funded',
        filterMenu:
        {
          categories: [
            {
              name: 'Requisition Type',
              filters: [
                {
                  name: 'Funded',
                  clauses: {
                    reqTypeClause: "req_type = 'funded'"
                  },
                  onFilter: reqTypeFilter
                },
                {
                  name: 'Conditional',
                  clauses: {
                    reqTypeClause: "req_type = 'conditional'"
                  },
                  onFilter: reqTypeFilter
                }
              ]
            }
          ]
        }
      };

      this.summaryViewOptions.views.daysOpenByProgramColumnRange.onColumnRangeClick = function(card, colRngData) {
        var templateVariables = card.options.getTemplateVariables();
        templateVariables.cardFilter_program = colRngData.category.toLowerCase();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "daysOpenByProgramHistogram";
        FilterService.createFilteredQuery(card, templateVariables);
      };

      // Expose the chartControls to the card object for paging purposes
      this.chartPaging = DataManager.chartPaging;


      // Histogram

      this.summaryViewOptions.views.daysOpenByProgramHistogram.controls = ViewsService.getHistogramControls();
      this.summaryViewOptions.views.daysOpenByProgramHistogram.controls.rightControlsContent.splice(0, 0, "components/deckster/views/drilldownView/drilldownView-controls.html");

      this.summaryViewOptions.views.daysOpenByProgramHistogram.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "daysOpenByProgramColumnRange";
        FilterService.createFilteredQuery(card, templateVariables);
      };

      this.summaryViewOptions.views.daysOpenByProgramHistogram.onColumnClick = function(card, columnData) {
        var templateVariables = card.options.getTemplateVariables();

        var ranges = ViewsService.parseHistogramRanges(columnData.category);
        if(ranges.lower && ranges.upper) {
          templateVariables.rangeClause = "req_days_open >= " + ranges.lower + " AND req_days_open <= " + ranges.upper;
        } else if (ranges.lower) {
          templateVariables.rangeClause = "req_days_open < " + ranges.lower;
        } else if (ranges.upper) {
          templateVariables.rangeClause = "req_days_open > " + ranges.upper;
        }

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "daysOpenByProgramTable";
        FilterService.createFilteredQuery(card, templateVariables)
      };



      // Table

      this.summaryViewOptions.views.daysOpenByProgramTable.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "daysOpenByProgramHistogram";
        FilterService.createFilteredQuery(card, templateVariables);
      };



      // Card Functions

      this.loadData = function(card, callback) {
        if(_.isUndefined(card.options.getTemplateVariables().cardFilter_pagingClause)) {
          card.options.getTemplateVariables().cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
        }
        DataManager.defaultLoadData(card, callback);
      };

      this.onReload = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        // Set title for appropriate view
        card.options.title = "Days Open by Program";
        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenByProgramHistogram) {
          card.options.title += ": " + templateVariables.cardFilter_program + " (" + ViewsService.getReqTypeFromClause(templateVariables.reqTypeClause) + ")";
        }
        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenByProgramTable) {
          card.options.title += ": " + templateVariables.cardFilter_program + " (" + ViewsService.getReqTypeFromClause(templateVariables.reqTypeClause) +
            ", " + ViewsService.getRangeValuesFromClause(templateVariables.rangeClause) + ")";
        }
      };

      this.afterFiltersApplied = function (card, filters, deckFiltered) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        // If we are not in the top view, and the deck was filtered, go back to the top view
        if(viewOptions != card.options[card.currentSection + "ViewOptions"].views.daysOpenByProgramColumnRange && deckFiltered) {
          var viewOptions = card.options[card.currentSection + 'ViewOptions'];
          viewOptions.activeView = "daysOpenByProgramColumnRange";
          FilterService.createFilteredQuery(card, templateVariables);
        }

        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenByProgramColumnRange) {
          DataManager.chartPaging.getRecTotal(card, function(recTotal) {
            templateVariables.cardFilter_pagingRecTotal = recTotal;
            // If the deck was filtered, take the user back to the first page
            if(deckFiltered) {
              templateVariables.cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
              FilterService.createFilteredQuery(card, templateVariables);
            }
          });
        }

        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenByProgramColumnRange) {
          FilterService.setSelectedFilter(card, 'reqTypeClause');
        }

        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenByProgramHistogram) {
          FilterService.setSelectedFilter(card, 'binClause');
        }
      };

      return this;
    };

    daysOpenByProgram.prototype = Card.prototype;

    return daysOpenByProgram;
  });
