'use strict';

angular.module('opsReviewApp')
  .factory('programMetadata', function (Card, DataManager, FilterService, DeckManager, Auth, ProgramHeatmapColorChangeUtils) {
    var programMetadata = function (cardData) {
      angular.extend(this, new Card(cardData));

      function showMessage(card) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        return !filters.project_id  && !filters.pra && !filters.program_manager && !filters.l4_manager && !filters.l3_portfolio && !filters.pra &&
          !filters.l3_manager && !filters.group_name && !filters.division_name && !filters.l2_manager &&
          !filters.l1;
      }

      this.summaryViewOptions.showMessage = showMessage;
      this.detailsViewOptions.showMessage = showMessage;
      this.canExportToCSV = Auth.isAdmin() || Auth.isExecutive() || Auth.isProgramManager();

      this.summaryViewOptions.preDataTransform = function (card, data, callback) {

        if (card.options.summaryViewOptions.showMessage(card)) {
          data = {};
        }

        callback && callback(data);
      };


      // TODO: this does not work when transpose is true
      this.detailsViewOptions.htmlTransform = function (card) {
        //These are the columns we might want to modify (depending on value)
        var $headers = $("#programMetadatadetails-table > thead > tr > th");
        var $values = $("#programMetadatadetails-table > tbody > tr").find('td');
        ProgramHeatmapColorChangeUtils.populateIndex($headers, $values);
      };

      return this;
    };

    programMetadata.prototype = Card.prototype;

    return programMetadata;
  });
