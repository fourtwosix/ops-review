'use strict';

angular.module('opsReviewApp')
  .factory('staffingLevels', function (Card, DataManager, FilterService) {
    var staffingLevels = function (cardData) {
      angular.extend(this, new Card(cardData));

      var staffingLevelsFilter = function (card) {
        var templateVariables = card.options.getTemplateVariables();
        var currentViewOptions = card.options.getCurrentViewOptions(card.currentSection);
        currentViewOptions.controls.selectedFilter = this.name;
        var filters = _.merge(templateVariables, this.clauses);
        FilterService.createFilteredQuery(card, filters);
      };

      this.summaryViewOptions.controls = {
        rightControlsContent: ['components/cards/controls/staffingLevel-controls.html'],
        selectedFilter: 'Total',
        filterMenu: {
          populated: false,
          categories: [
            {
              filters: [
                {
                  name: 'Total',
                  clauses: {
                    staffingLevelClause: "type = 'total'"
                  },
                  onFilter: staffingLevelsFilter
                },
                {
                  name: 'CSC',
                  clauses: {
                    staffingLevelClause: "type = 'csc'"
                  },
                  onFilter: staffingLevelsFilter
                },
                {
                  name: 'Subcontractors',
                  clauses: {
                    staffingLevelClause: "type = 'sub'"
                  },
                  onFilter: staffingLevelsFilter
                }
              ]
            }
          ]
        }
      };

      this.afterFiltersApplied = function (card) {
        var templateVariables = card.options.getTemplateVariables();

        //hides the dropdown filter menu if the staffing source deck filter is set
        if(templateVariables.staffingSourceClause != "true") {
          card.options.hideFilter = true;
          card.options.summaryViewOptions.controls.selectedFilter = 'Total';
        } else {
          card.options.hideFilter = false;
        }

        //Sets the existing card query
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        var categories = viewOptions.controls.filterMenu.categories;
        _.forEach(categories, function(category) {
          _.forEach(category.filters, function(filter){
            if(filter.name === viewOptions.controls.selectedFilter) {
              if(filter.clauses.staffingLevelClause !== templateVariables.staffingLevelClause) {
                filter.onFilter(card);
              }
            }
          });
        });
      };

      return this;
    };

    staffingLevels.prototype = Card.prototype;

    return staffingLevels;
  });
