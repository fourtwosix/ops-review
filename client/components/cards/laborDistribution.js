'use strict';

angular.module('opsReviewApp')
  .factory('laborDistribution', function (Card, DataManager, FilterService, ViewsService, Utils) {

    var laborDistribution = function(cardData) {
      angular.extend(this, new Card(cardData));

      //Labor Distribution Heatmap View
      this.summaryViewOptions.views.laborDistHeatmap.colorAxis.stops = [
        [0, DataManager.colorMap.white],
        [0.5, DataManager.colorMap.lightorange],
        [1, DataManager.colorMap.darkpink]
      ];


      this.summaryViewOptions.views.laborDistHeatmap.preDataTransform = function(card, data, callback) {
        var drillViewOptions = card.options.getCurrentViewOptions(card.currentSection);

        //Calculates and stored the totals for each key
        var totals = {};
        var keys = _.omit(_.keys(data[0]), drillViewOptions.dataTransform.nameColumn);
        _.forEach(keys, function (key) {
          totals[key] = _.sum(_.pluck(data, key));
        });

        //Calculates the relative average percentage of each value
        data = _.map(data, function (row) {
          _.forEach(_.keys(totals), function (key) {
            if (totals[key] != 0) {
              var rawRelAvg = row[key] / totals[key];
              row[key] = Math.round(rawRelAvg * 10000) / 100;
            }
          });
          return row;
        });

        callback && callback(data);
      };


      this.summaryViewOptions.views.laborDistHeatmap.tooltipFormatter = function () {
        //Keeping this for reference, this is how you get the point value.
        //'<b>' + Highcharts.numberFormat(this.point.value,2,'.') + '%</b>';
        return this.series.yAxis.categories[this.point.y] + '<br>' +
          this.series.xAxis.categories[this.point.x];
      };


      this.summaryViewOptions.views.laborDistHeatmap.seriesFormatter = function() {
        return Highcharts.numberFormat(this.point.value,2,'.') + '%';
      };


      this.summaryViewOptions.views.laborDistHeatmap.onSeriesPointClick = function (card, point) {
        var templateVariables = card.options.getTemplateVariables();
        var managerName = Utils.dataFormatter.name(point.series.yAxis.categories[point.y], true);
        templateVariables.laborType = Utils.dataFormatter.default('wtd_' + point.series.xAxis.categories[point.x], true);

        //Set the parentManagerClause, so we can retrieve it when returning to this view
        templateVariables.parentManagerClause = templateVariables.managerClause;
        templateVariables.managerClause = templateVariables.subManagerType + " ~ '" + managerName + "'";

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = 'laborTypeHistogram';

        FilterService.createFilteredQuery(card, templateVariables);
      };


      //Labor Type Histogram View
      this.summaryViewOptions.views.laborTypeHistogram.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = 'laborDistHeatmap';

        //A parentManagerClause will only be set if the filters have not been reset by the user,
        // in this case, we will need to retrieve the parent clause to know how to query the parent view.
        if(templateVariables.parentManagerClause) {
          templateVariables.managerClause = templateVariables.parentManagerClause;
        }

        FilterService.createFilteredQuery(card, templateVariables);
      };


      this.summaryViewOptions.views.laborTypeHistogram.onColumnClick = function (card, point) {
        var templateVariables = card.options.getTemplateVariables();
        var hoursRange        = ViewsService.parseHistogramRanges(point.series.xAxis.categories[point.x]);

        templateVariables.rangeClause = templateVariables.laborType + ' BETWEEN ' +
          hoursRange.lower + ' AND ' + hoursRange.upper;

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = 'laborDrillDownTable';

        FilterService.createFilteredQuery(card, templateVariables);
      };


      // Labor drill-down table
      this.summaryViewOptions.views.laborDrillDownTable.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];

        viewOptions.activeView = 'laborTypeHistogram';

        FilterService.createFilteredQuery(card, templateVariables);
      };


      //Card Functions
      // TODO: function now supports three parameters.  The last parameter
      //       is a boolean that shows when a deck filter has been applied.
      this.afterFiltersApplied = function (card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.summaryViewOptions;
        if (viewOptions.views[viewOptions.activeView] != viewOptions.views.laborTypeHistogram) {
          viewOptions.views[viewOptions.activeView].dataTransform.nameColumn = templateVariables.subManagerType;
        }
      };


      this.onReload = function(card) {
        var managerName, laborType;
        var templateVariables = card.options.getTemplateVariables();
        card.options.title = 'Labor Distribution';
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        if(card.currentSection === 'summary' && viewOptions === card.options[card.currentSection + 'ViewOptions'].views.laborTypeHistogram) {
          managerName = Utils.dataFormatter.name(templateVariables.managerClause.split('~')[1].replace(/'/g, '').trim());
          laborType = templateVariables.laborType.replace("wtd_", "").replace(/_/g, " ").trim();
          card.options.title = managerName + ': ' + laborType;
        }
        if(card.currentSection === 'summary' && viewOptions === card.options[card.currentSection + 'ViewOptions'].views.laborDrillDownTable) {
          managerName = Utils.dataFormatter.name(templateVariables.managerClause.split('~')[1].replace(/'/g, '').trim());
          laborType = templateVariables.laborType.replace("wtd_", "").replace(/_/g, " ").trim();
          var range = ViewsService.getRangeValuesFromClause(templateVariables.rangeClause);
          card.options.title = managerName + ': ' + laborType + " (" + range + ")";
        }
      };

      return this;
    };

    laborDistribution.prototype = Card.prototype;

    return laborDistribution;
  });
