'use strict';

angular.module('opsReviewApp')
  .factory('utilizationByLOS', function (Card, DataManager) {

    var utilizationByLOS = function(cardData) {
      angular.extend(this, new Card(cardData));

      this.title = (function(){
        var fiscalYear = DataManager.fiscalYearStart.getFullYear() + 1;
        var trimmedYear = fiscalYear.toString().substr(2,2);
        return 'FY' + trimmedYear + ' Utilization';
      }());

      this.summaryViewOptions.tooltipFormatter = function () {
        return this.x + '<br/>' + this.series.name + ' Utilization: <b>' +
          Highcharts.numberFormat(this.y * 100, 2,'.') + '%</b>';
      };

      this.summaryViewOptions.yAxisFormatter = function () {
        return Highcharts.numberFormat(this.value * 100, 0,'.') + '%'
      };

      this.afterFiltersApplied = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        card.options.getCurrentViewOptions(card.currentSection).dataTransform.nameColumn = templateVariables.subManagerType;
      };

      return this;
    };

    utilizationByLOS.prototype = Card.prototype;

    return utilizationByLOS;
  });
