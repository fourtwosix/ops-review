'use strict';

angular.module('opsReviewApp')
  .factory('staffingChangeByMonth', function (Card) {

    var staffingChange = function (cardData) {
      angular.extend(this, new Card(cardData));

      return this;
    };

    staffingChange.prototype = Card.prototype;

    return staffingChange;
  });
