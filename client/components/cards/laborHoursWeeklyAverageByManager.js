'use strict';

angular.module('opsReviewApp')
  .factory('laborHoursWeeklyAverageByManager', function (Card, DataManager, FilterService, Utils) {

    var laborHoursWeeklyAverageByManager = function(cardData) {
      angular.extend(this, new Card(cardData));


      // SMELL: these getXxxxxTitle functions seem like something that some be
      //        part of the standard JSON card difinition with templateFiltered
      //        strings for each view in the views array.
      var getSummaryTitle = function() {
        return 'Avg. Wk. Total Hours by Mgr.';
      };

      var getDetailsTitle = function() {
        return getSummaryTitle();
      };

      var getDrillDownTitle = function(card, barData) {
        var templateVariables = card.options.getTemplateVariables();
        var managerName = Utils.dataFormatter.name(barData.category.toLowerCase(), true);
        var startDate   = templateVariables.startDate;
        var endDate     = templateVariables.endDate;

        return "Total Weekly Hours for " + managerName + " From " + startDate + ' To ' + endDate;
      };



      this.summaryViewOptions.views.lhwabmSummaryBarChart.tooltipFormatter = function() {
        var tooltipText = '<b>' + this.x + '</b><br/>' + this.series.name + ': ' + Highcharts.numberFormat(this.y, 2);
        if(this.series.name.toLowerCase().indexOf("voluntary") >= 0) {
          tooltipText += '<br/>' + 'Total: ' + this.point.stackTotal;
        }
        return tooltipText;
      };



      this.summaryViewOptions.views.lhwabmSummaryBarChart.onBarClick = function(card, barData) {
        var templateVariables = card.options.getTemplateVariables();
        var managerName = Utils.dataFormatter.name(barData.category.toLowerCase(), true);

        templateVariables.lhwabmAverage = barData.y;

        //Set the parentManagerClause, so we can retrieve it when returning to this view
        templateVariables.parentManagerClause = templateVariables.managerClause;
        templateVariables.managerClause = templateVariables.subManagerType + "='" + managerName + "'";

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];

        card.options.title = getDrillDownTitle(card, barData);

        viewOptions.activeView = "lhwabmDrillDownLineChart";
        FilterService.createFilteredQuery(card, templateVariables)
      };



      this.summaryViewOptions.views.lhwabmDrillDownLineChart.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "lhwabmSummaryBarChart";

        //A parentManagerClause will only be set if the filters have not been reset by the user,
        // in this case, we will need to retrieve the parent clause to know how to query the parent view.
        if(templateVariables.parentManagerClause) {
          templateVariables.managerClause = templateVariables.parentManagerClause;
        }

        FilterService.createFilteredQuery(card, templateVariables);
      };



      this.afterFiltersApplied = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.summaryViewOptions;
        if (viewOptions.views[viewOptions.activeView] != viewOptions.views.lhwabmDrillDownLineChart) {
          viewOptions.views[viewOptions.activeView].dataTransform.nameColumn = templateVariables.subManagerType;
        }
      };



      this.onReload = function(card) {
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        if (card.currentSection === "summary" && viewOptions === card.options[card.currentSection + "ViewOptions"].views.lhwabmDrillDownLineChart) {
          // Do Nothing
        } else if (card.currentSection === "summary") {
          card.options.title = getSummaryTitle();
        } else if (card.currentSection === "details") {
          card.options.title = getDetailsTitle();
        }
      };

      return this;
    };

    laborHoursWeeklyAverageByManager.prototype = Card.prototype;

    return laborHoursWeeklyAverageByManager;
  });
