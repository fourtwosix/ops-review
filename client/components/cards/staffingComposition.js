'use strict';

angular.module('opsReviewApp')
  .factory('staffingComposition', function (Card, DataManager, FilterService, DeckManager) {

    var staffingComposition = function (cardData) {
      angular.extend(this, new Card(cardData));
      this.summaryViewOptions.views = {};

      this.summaryViewOptions.drillUp = function (card) {
        var templateVariables = card.options.getTemplateVariables();

        if (templateVariables.staffingCompositionNameCol === "resource" && card.options.summaryViewOptions.deckFilter !== "program") {
          templateVariables.staffingCompositionNameCol = "los";
        } else if (card.options.summaryViewOptions.deckFilter !== "los" && card.options.summaryViewOptions.deckFilter !== "program") {
          templateVariables.staffingCompositionNameCol = "top";
        } else {
          return;
        }

        card.options.summaryViewOptions.filterByCategory(card, card.options.summaryViewOptions.views);
      };

      this.summaryViewOptions.filterByCategory = function (card, cat) {
        var templateVariables = card.options.getTemplateVariables();

        if (cat) {
          switch (templateVariables.staffingCompositionNameCol) {
            case "top":
              templateVariables.staffingCompositionClause = "los NOTNULL";
              templateVariables.staffingCompositionNameCol = "los";
              break;
            case "los": //los view -> project_id view
              card.options.summaryViewOptions.views["los"] = cat.los;
              card.options.summaryViewOptions.table = 'staffing_composition';
              templateVariables.staffingCompositionClause = "los = '" + cat.los + "'";
              templateVariables.staffingCompositionNameCol = "project_id";
              card.options.summaryViewOptions.catFilter.selectedFilter = "";
              break;
            case "project_id": // project_id view -> staffing source view
              card.options.summaryViewOptions.views["project_id"] = cat.project_id;
              templateVariables.staffingCompositionClause = "project_id = '" + cat.project_id + "'";
              templateVariables.staffingCompositionNameCol = "resource";
              break;
            case "resource":
              card.options.summaryViewOptions.views["project_id"] = cat.project_id;
              templateVariables.staffingCompositionClause = "project_id = '" + cat.project_id + "'";
              break;
          }
        }
        card.options.summaryViewOptions.filterApplied = true;
        FilterService.createFilteredQuery(card, templateVariables);
      };

      this.summaryViewOptions.getFilterValues = function (table, filter, query) {
        DeckManager.getFilterValues(table, filter, query).then($.proxy(function (data) {
          this.catFilter.filterValues = data || [];
        }, this));
      };

      this.summaryViewOptions.preDataTransform = function (card, data, callback) {
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        var nameColumn = viewOptions.dataTransform.nameColumn;
        var ret = {};

        // For each name column you need to make an object that contains key for each contractor with the value as the hours
        _.each(_.groupBy(data, nameColumn), function (month, date) {
          ret[date] = ret[date] || {month: date};
          _.each(month, function (point) {
            ret[date][point["categories"]] = point.hours;
          })
        });
        callback && callback(_.flatten(_.values(ret)));
      };

      this.afterFiltersApplied = function (card) {
        var templateVariables = card.options.getTemplateVariables();

        var cat = {};
        if (templateVariables.losClause !== "true" && !card.options.summaryViewOptions.filterApplied) {
          card.options.summaryViewOptions.deckFilter = "los";
          cat.los = templateVariables.staffingCompositionClause;
          templateVariables.staffingCompositionNameCol = "los";
          card.options.summaryViewOptions.filterByCategory(card, cat);
        } else if (templateVariables.programClause !== "true" && !card.options.summaryViewOptions.filterApplied) {
          card.options.summaryViewOptions.deckFilter = "program";
          cat.project_id = templateVariables.staffingCompositionClause;
          templateVariables.staffingCompositionNameCol = "project_id";
          card.options.summaryViewOptions.filterByCategory(card, cat);
        } else {
          card.options.summaryViewOptions.table = 'staffing_composition';
          var wheres = {};
          var deckVar = templateVariables.staffingCompositionClause;
          var splitVar = {};

          if (deckVar !== "true" && deckVar !== "los NOTNULL") {
            splitVar = deckVar.split("=");
            wheres[splitVar[0].trim()] = splitVar[1].replace(/'/g, '').trim();
          } else {
            splitVar[0] = "null";
          }

          if (deckVar === "los NOTNULL") {
            card.options.summaryViewOptions.catFilter = DataManager.generateAutocompleteConfig("los", null, "caps");
          } else if (splitVar[0].trim() === "los") {
            card.options.summaryViewOptions.catFilter = DataManager.generateAutocompleteConfig("project_id", null, "caps",
              {
                los: splitVar[1].replace(/'/g, '').trim(),
                project_id: {$notLike: '%.%'}
              });
          }
          //staffing source view
          //else if (splitVar[0].trim() === "project_id") {
          //  card.options.summaryViewOptions.catFilter = DataManager.generateAutocompleteConfig("resource", null, "caps",
          //    {
          //      project_id: splitVar[1].replace(/'/g, '').trim()
          //    });
          //}

          if (templateVariables.losClause === "true" && templateVariables.programClause === "true") {
            card.options.summaryViewOptions.deckFilter = false;

          }

          card.options.summaryViewOptions.filterApplied = false;
        }
      };

      return this;
    };

    staffingComposition.prototype = Card.prototype;

    return staffingComposition;
  });
