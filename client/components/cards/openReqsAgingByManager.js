'use strict';

angular.module('opsReviewApp')
  .factory('openReqsAgingByManager', function (Card, DataManager, FilterService) {

    var openReqsAgingByManager = function(cardData) {
      angular.extend(this, new Card(cardData));
      var self = this;

      this.summaryViewOptions.xAxisFormatter = function () {
        return '<span>(Cond, Funded)</span> <br>' + this.value;
      };

      this.summaryViewOptions.tooltipFormatter = function () {
        var name = _.contains(this.series.name, 'Funded') ? this.series.name : 'Conditional ' + this.series.name;
        return this.x + '<br/>' + name + ': <b>' + this.y + '</b>'
      };

      // Expose the chartControls to the card object for paging purposes
      this.chartPaging = DataManager.chartPaging;

      this.loadData = function(card, callback) {
        if(_.isUndefined(card.options.getTemplateVariables().cardFilter_pagingClause)) {
          card.options.getTemplateVariables().cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
        }
        DataManager.defaultLoadData(card, callback);
      };

      this.afterFiltersApplied = function(card, filters, deckFiltered) {

        var templateVariables = card.options.getTemplateVariables();
        card.options.getCurrentViewOptions(card.currentSection).dataTransform.nameColumn = templateVariables.subManagerType;

        DataManager.chartPaging.getRecTotal(card, function(recTotal) {
          templateVariables.cardFilter_pagingRecTotal = recTotal;
          // If the deck was filtered, take the user back to the first page
          if(deckFiltered) {
            templateVariables.cardFilter_pagingClause = "LIMIT " + card.options.getCurrentViewOptions(card.currentSection).chartPagingSize + " OFFSET 0";
            FilterService.createFilteredQuery(card, templateVariables);
          }
        });

      };

      return this;
    };

    openReqsAgingByManager.prototype = Card.prototype;
    return openReqsAgingByManager;
  }

);
