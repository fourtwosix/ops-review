/**
 * Created by mcurtis on 7/20/15.
 */
'use strict';

angular.module('opsReviewApp')
  .factory('requisitionsAverageDaysOpen', function (Card) {

    var requisitionsAverageDaysOpen = function(cardData) {
      angular.extend(this, new Card(cardData));
      this.summaryViewOptions.nameColumn = "layer_3_manager";

      this.afterFiltersApplied = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        viewOptions.dataTransform.nameColumn = templateVariables.subManagerType;
      };

      return this;
    };

    requisitionsAverageDaysOpen.prototype = Card.prototype;
    return requisitionsAverageDaysOpen;
  }

);
