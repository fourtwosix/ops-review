/**
 * Created by omezu on 9/8/15.
 */
'use strict';

angular.module('opsReviewApp')
  .factory('averageSitTimeInCWS', function (Card, DataManager, ViewsService, FilterService, DeckManager, Utils) {

    var averageSitTimeInCWS = function(cardData) {
      angular.extend(this, new Card(cardData));
      var self = this;
      // sitTimeColumnChart

      this.summaryViewOptions.views.sitTimeColumnChart.columnChartView = function(card) {
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "sitTimeColumnChart";
        FilterService.createFilteredQuery(card, card.options.getTemplateVariables());
      };

      this.summaryViewOptions.views.sitTimeColumnChart.preDataTransform = function(card, data, callback) {
        //This will proper case the category names without removing special characters.
        var steps = ["New", "Phone Screen", "Hm Review", "1st Interview",
          "Additional Interview(s)", "Optional Interviews", "Pre-offer Actions",
          "Offer", "Post-offer Actions", "Hire"];

        var newData = {};
        _.forEach(data, function(d) {
          var step = Utils.dataFormatter.titleKeepSymbols(d.step);
          newData[step] = newData[step] || {step: step, conditional_avg_time_in_step: 0, funded_avg_time_in_step: 0};
          newData[step][d.req_type + '_avg_time_in_step'] = d.avg_time;
        });
        var sortedValues = _.sortBy(_.values(newData), function (d) {
          return _.indexOf(steps, d.step);
        });

        var reqData = _.map(data, function (row) {
          return {
            step: row.step,
            reqType: row.req_type,
            reqCount: parseInt(row.number_of_reqs)
          };
        });
        self.reqData = reqData;
        callback && callback(sortedValues);
        return reqData;
      };

      this.summaryViewOptions.views.sitTimeColumnChart.tooltipFormatter = function () {
        var reqItem = _.find(self.reqData, {step: this.key.toLowerCase(), reqType: this.series.name.substr(0, this.series.name.indexOf(" ")).toLowerCase()});
          return this.x + '<br/>' + this.series.name + ': ' + Highcharts.numberFormat(this.y, 1, '.') + '</b>' +
            '<br/>Number Of Reqs: <b>' + ' ' + reqItem.reqCount;
      };

      this.summaryViewOptions.views.sitTimeColumnChart.onColumnClick = function(card, columnData) {
        card.options.getTemplateVariables().candidateStep = columnData.category.toLowerCase();
        card.options.getTemplateVariables().candidateReqType = _.contains(columnData.series.name, "Conditional") ? "conditional" : "funded";

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "sitTimeHistogram";
        FilterService.createFilteredQuery(card, card.options.getTemplateVariables())
      };

      this.summaryViewOptions.views.sitTimeColumnChart.onXAxisLabelClick = function(card, labelData) {
        // If the label is not wrapped by Highcharts, the category name is simply the textContent.
        // However, if Highcharts wrapped the label, then we have to investigate the HTML to get the value of the title tag.
        var categoryName = labelData.textContent;
        var categoryHTML = $.parseHTML(labelData.innerHTML);
        var categoryTitleTag = _.find(categoryHTML, function(tag) {
          return tag.nodeName === 'TITLE'
        });
        if(!_.isUndefined(categoryTitleTag)) {
          categoryName = categoryTitleTag.textContent;
        }

        var chart = card["summaryColumnChart"];
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        //Save off the hidden category or restore it if hidden.
        if(_.isUndefined(viewOptions.hiddenCategories)) {
          viewOptions.hiddenCategories = {
            seriesData: _.cloneDeep(chart.series),
            names: [],
            indexes: []
          }
        }

        var categoryIndex = _.indexOf(chart.xAxis[0].categories, categoryName);

        // If the category is already hidden, then we need to restore the data to the chart's series
        // Remove the hidden category from the JSON, or if the category isn't hidden, add it to the JSON
        // Iterate through each series in the chart
        // Map the "y" data to an array for manipulation
        // Manipulate the "y" data, setting it to the value in the seriesData that was stored in the JSON, or setting it to 0 for the category being hidden
        // Set the data back to the chart's series - do not redraw the chart yet
        // Redraw the chart when done
        var isHidden = _.includes(viewOptions.hiddenCategories.indexes, categoryIndex);
        if(isHidden) {
          _.pull(viewOptions.hiddenCategories.indexes, categoryIndex);
          _.pull(viewOptions.hiddenCategories.names, categoryName);
        } else {
          viewOptions.hiddenCategories.indexes.push(categoryIndex);
          viewOptions.hiddenCategories.names.push(categoryName);
        }
        _.forEach(chart.series, function(series, i){
          var data = _.cloneDeep(series.yData);
          if(isHidden){
            data[categoryIndex] = viewOptions.hiddenCategories.seriesData[i].yData[categoryIndex];
          } else {
            data[categoryIndex] = 0;
          }
          series.setData(data, false);
        });
        chart.redraw();
      };

      this.summaryViewOptions.views.sitTimeColumnChart.xAxisFormatter = function(card) {
        var categoryName = this.value;
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
        if(!_.isUndefined(viewOptions.hiddenCategories) && _.includes(viewOptions.hiddenCategories.names, categoryName)) {
          categoryName = '<span style="fill: grey;">' + categoryName + '</span>';
        }
        return  categoryName;
      };

      //Sit Time Histogram View

      this.summaryViewOptions.views.sitTimeHistogram.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = 'sitTimeColumnChart';

        //A parentManagerClause will only be set if the filters have not been reset by the user,
        // in this case, we will need to retrieve the parent clause to know how to query the parent view.
        if(templateVariables.parentManagerClause) {
          templateVariables.managerClause = templateVariables.parentManagerClause;
        }

        FilterService.createFilteredQuery(card, templateVariables);
      };

      this.summaryViewOptions.views.sitTimeHistogram.controls = {
          rightControlsContent: ['components/deckster/views/drilldownView/drilldownView-controls.html', 'components/deckster/views/histogramChart/histogramChart-controls.html'],
          selectedFilter: 10,
          changeBinNumber: function (card, binNumber) {
            var templateVariables = card.options.getTemplateVariables();
            var viewOptions = card.options.getCurrentViewOptions(card.currentSection);
            viewOptions.controls.selectedFilter = binNumber;
            templateVariables.binClause = binNumber;
            FilterService.createFilteredQuery(card, templateVariables);
          }
      };

      this.summaryViewOptions.views.sitTimeHistogram.onColumnClick = function (card, point) {
        var templateVariables = card.options.getTemplateVariables();
        var dayRange = ViewsService.parseHistogramRanges(point.series.xAxis.categories[point.x]);

        if(dayRange.lower && dayRange.upper) {
          templateVariables.rangeClause = ' BETWEEN ' + dayRange.lower + ' AND ' + dayRange.upper;
        } else if (dayRange.lower) {
          templateVariables.rangeClause = "< " + dayRange.lower;
        } else if (dayRange.upper) {
          templateVariables.rangeClause = "> " + dayRange.upper;
        }

        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = 'sitTimeTable';

        FilterService.createFilteredQuery(card, templateVariables);
      };

      //Sit Time drill-down Table

      this.summaryViewOptions.views.sitTimeTable.drillUp = function(card) {
        var templateVariables = card.options.getTemplateVariables();
        var viewOptions = card.options[card.currentSection + 'ViewOptions'];
        viewOptions.activeView = "sitTimeHistogram";

        FilterService.createFilteredQuery(card, templateVariables);
      };

      // View Functions

      this.summaryViewOptions.getFilterValues = function (table, filter, query) {
        DeckManager.getFilterValues(table, filter, query).then($.proxy(function (data) {
          this.reqFilter.filterValues = data || [];
        }, this));
      };

      // Card Functions

      this.onReload = function(card) {
        var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

        // Reset Hidden Categories any time the card is reloaded
        delete viewOptions.hiddenCategories;

        // Set title for appropriate view
        card.options.title = "Average Sit Time in Candidate Workflow Selection";
        if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.sitTimeHistogram) {
          card.options.title += ": " + card.options.getTemplateVariables().candidateStep;
          card.options.title += " (" + card.options.getTemplateVariables().candidateReqType + ")";
        }
        else if(viewOptions === card.options[card.currentSection + "ViewOptions"].views.sitTimeTable) {
          card.options.title += ": " + card.options.getTemplateVariables().candidateStep;
          card.options.title += " (" + card.options.getTemplateVariables().candidateReqType + ")";
          card.options.title += " [" + card.options.getTemplateVariables().rangeClause + " Days In Step]";
        }
      };

      return this;
    };

    averageSitTimeInCWS.prototype = Card.prototype;

    return averageSitTimeInCWS;
  });
