'use strict';

angular.module('opsReviewApp')
  .factory('Modal', function ($rootScope, $uibModal) {
    /**
     * Opens a modal
     * @param  {Object} scope      - an object to be merged with modal's scope
     * @param  {String} modalClass - (optional) class(es) to be applied to the modal
     * @return {Object}            - the instance $uibModal.open() returns
     * @param size
     * @param template
     */
    function openModal(scope, modalClass, size, template) {
      var modalScope = $rootScope.$new();
      scope = scope || {};
      modalClass = modalClass || 'modal-default';
      template = template || 'components/modal/modal.html';

      angular.extend(modalScope, scope);

      return $uibModal.open({
        templateUrl: template,
        windowClass: modalClass,
        scope: modalScope,
        size: size,
        backdrop: 'static'
      });
    }

    // Public API here
    return {

      /* Confirmation modals */
      confirm: {

        /**
         * Create a function to open a delete confirmation modal (ex. ng-click='myModalFn(name, arg1, arg2...)')
         * @param  {Function} del - callback, ran when delete is confirmed
         * @return {Function}     - the function to open the modal (ex. myModalFn)
         */
        delete: function(del) {
          del = del || angular.noop;

          /**
           * Open a delete confirmation modal
           * @param  {String} name   - name or info to show on modal
           * @param  {All}           - any additional args are passed staight to del callback
           */
          return function() {
            var args = Array.prototype.slice.call(arguments),
                name = args.shift(),
                deleteModal;

            deleteModal = openModal({
              modal: {
                dismissable: true,
                title: 'Confirm Delete',
                html: '<p>Are you sure you want to delete <strong>' + name + '</strong> ?</p>',
                buttons: [{
                  classes: 'btn-danger',
                  text: 'Delete',
                  click: function(e) {
                    deleteModal.close(e);
                  }
                }, {
                  classes: 'btn-default',
                  text: 'Cancel',
                  click: function(e) {
                    deleteModal.dismiss(e);
                  }
                }]
              }
            }, 'modal-danger');

            deleteModal.result.then(function(event) {
              del.apply(event, args);
            });
          };
        },
        general: function(done) {
          done = done || angular.noop;

          return function() {
            var args = Array.prototype.slice.call(arguments),
              title = args.shift(),
              text = args.shift(),
              confirmModal;

            confirmModal = openModal({
              modal: {
                dismissable: false,
                title: title || 'confirm',
                html: text || '',
                buttons: [{
                  classes: 'white',
                  text: 'No',
                  click: function(e) {
                    confirmModal.dismiss(e);
                  }
                }, {
                  classes: '',
                  text: 'Yes',
                  click: function(e) {
                    confirmModal.close(e);
                  }
                }]
              }
            }, 'minimal-modal');

            confirmModal.result.then(function(event) {
              done.apply(event, [true]);
            }).catch(function(event) {
              done.apply(event, [false]);
            });
          };
        }
      },
      form: function (callback) {
        callback = callback || angular.noop;

        return function () {
          var args = Array.prototype.slice.call(arguments),
            formConfig = args.shift();

          formConfig.modal = openModal({
            form: formConfig
          }, 'form-modal minimal-modal', 'lg', 'components/modal/form-modal.html');

          formConfig.modal.result.then(function(event) {
            callback.apply(event, args);
          });
        }
      }
    };
  });
