'use strict';

angular.module('opsReviewApp')
  .factory('Card', function (DataManager, FilterService) {

    var Card = function (cardData) {
      return this.setData(cardData);
    };

    Card.prototype.setData = function (cardData) {
      angular.extend(this, cardData);
      this.onResize = DataManager.defaultOnResize;
      this.loadData = DataManager.defaultLoadData;
      this.reloadView = DataManager.defaultOnReload;
      return this;
    };

    Card.prototype.setLastUpdated = function (card) {
      DataManager.getLastUpdated(this, function (lastUpdated){
        card.lastUpdatedDate = lastUpdated;
      });
    };

    Card.prototype.isNew = function () {
      return !angular.isDefined(this.id);
    };

    Card.prototype.getCurrentViewType = function (section) {
      return this[section + 'ViewType'];
    };

    /**
     * Get the view options of associated with the currentSection.
     * If the view is a drilldownView it gets the view options associated with the
     * active view.
     *
     * @param section
     * @returns {*}
     */
    Card.prototype.getCurrentViewOptions = function (section) {
      var viewOptions = this[section + 'ViewOptions'];
      if (this.getCurrentViewType(section) === "drilldownView") {
        return viewOptions.views[viewOptions.activeView];
      } else {
        return viewOptions;
      }
    };

    Card.prototype.getTemplateVariables = function () {
      return FilterService.getTemplateVariables(this.id);
    };

    Card.prototype.getDataFormatter = function (format) {
      return DataManager.getDataFormatter(format);
    };

    return Card;
  });
