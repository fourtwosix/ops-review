angular.module('opsReviewApp')
  .factory('CardBuilder', function ($injector, Card) {
    var CardBuilder = {};

    CardBuilder.build = function(card) {
      if ($injector.has(card.cardName)) {
        return $injector.invoke([card.cardName, function(service) {
          return new service(card);
        }]);
      } else {
        return new Card(card);
      }
    };

    return CardBuilder;
  });
