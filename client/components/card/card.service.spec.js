'use strict';

describe('Service: Card', function () {

  // load the service's module
  beforeEach(module('opsReviewApp'));

  // instantiate service
  var card;
  beforeEach(inject(function (_Card_) {
    card = _Card_;
  }));

  it('should do something', function () {
    expect(!!card).toBe(true);
  });

});
