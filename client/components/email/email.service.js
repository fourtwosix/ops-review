'use strict';

angular.module('opsReviewApp')
  .factory('Email', function ($http, $q) {

    var Email = {

      emailAllUsers: function (body, subject, PMR, callback) {
        var cb = callback || angular.noop;
        var deferred = $q.defer();

        console.log("\nSENDING POST REQUEST TO /api/emails/blast\n");
        
        $http.post('/api/emails/blast', {
          data: body,
          subject: subject,
          PMR: PMR

        }).success(function (response) {

          console.log("\nSUCCESS RESPONSE FROM POST REQUEST\n");
          
          deferred.resolve(response);
          return cb();
          
        }).error(function(err) {
          
          console.log("\nERROR RESPONSE FROM POST REQUEST: ", err, "\n");

          deferred.reject(err);
          return cb(err);

        });

        return deferred.promise;

      },

      sendAdminNotificationEmail: function (fromUser, callback) {
        var cb = callback || angular.noop;
        var deferred = $q.defer();

        $http.post('api/emails/adminNotificationEmail', {
          from: fromUser
        }).success(function (response) {

          deferred.resolve(response);
          return cb();

        }).error(function (err) {
          
          deferred.reject(err);
          return cb(err);

        });

        return deferred.promise;

      }
    };

    return Email;

  });
