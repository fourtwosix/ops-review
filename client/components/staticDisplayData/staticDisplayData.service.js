'use strict';

angular.module('opsReviewApp')
  .factory('StaticDisplayData', function StaticDisplayData($http, $q) {

    return {
      getDisplayDataForType: function (type, order) {
        var deferred = $q.defer();
        var url = '/api/staticDisplayData/' + type + (order ? '?order=' + order : '');

        $http.get(url).then(function (response) {
          deferred.resolve(response.data);
        }).catch(function (err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },
      addDisplayData: function (type, newVal, newValemail) {
        var deferred = $q.defer();

        $http.post('/api/staticDisplayData', {type: type, display_value: newVal, description: newValemail}).then(function (response) {
          deferred.resolve(response.data);
        }).catch(function (err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },
      updateDisplayDataById: function (id, newVal, newValemail) {
        var deferred = $q.defer();

        $http.put('/api/staticDisplayData/' + id, {display_value: newVal, description:newValemail}).then(function (response) {
          deferred.resolve(response.data);
        }).catch(function (err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },
      deleteDisplayDataById: function (id) {
        var deferred = $q.defer();

        $http.delete('/api/staticDisplayData/' + id).then(function (response) {
          deferred.resolve(response.data);
        }).catch(function (err) {
          deferred.reject(err);
        });

        return deferred.promise;
      }
    };
  });
