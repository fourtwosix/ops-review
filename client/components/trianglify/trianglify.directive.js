'use strict';

angular.module('opsReviewApp')
  .directive('trianglify', function ($parse) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var opts = $parse(attrs.trianglify || {})(scope) || {};
        opts.x_colors = attrs.colors || 'GnBu';

        var init = function (options) {
          options.width = element.width();
          options.height = element.height();

          var pattern = Trianglify(options);
          element.find('#trianglify-canvas').remove();
          element.append($(pattern.canvas()).attr('id', 'trianglify-canvas'));
        };

        init(opts);

        attrs.$observe('colors', function (colors) {
          opts.x_colors = colors || 'GnBu';
          init(opts);
        });
      }
    };
  });
