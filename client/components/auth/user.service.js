'use strict';

angular.module('opsReviewApp')
  .factory('User', function ($resource) {
    return $resource('/api/users/:id/:controller', {
      id: '@id'
    },
    {
      update: {
        method: 'PUT'
      },
      changePassword: {
        method: 'PUT',
        params: {
          controller:'password'
        }
      },
      get: {
        method: 'GET',
        params: {
          id:'me'
        }
      }
	  });
  });
