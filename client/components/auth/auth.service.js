'use strict';

angular.module('opsReviewApp')
  .factory('Auth', function Auth($location, $rootScope, $state, $http, $timeout, $cookieStore, User, $q, $window, Analytics) {
    var currentUser = {};
    if($cookieStore.get('token')) {
      currentUser = User.get();
    }

    var checkRole = function(user, roleName) {
      return _.includes(_.pluck(user.roles, 'name'), roleName);
    };

    var hasProgram = function(user, projectId) {
      return _.includes(_.pluck(user.programs, 'project_id'), projectId);
    };

    var Auth = {

      /**
       * Authenticate user and save token
       *
       * @param  {Object}   user     - login info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      login: function(user, callback) {
        var cb = callback || angular.noop;
        var deferred = $q.defer();

        $http.post('/auth/local', {
          email: user.email,
          password: user.password
        }).
          success(function(data) {
            $cookieStore.put('token', data.token);
            currentUser = User.get(function () {
              deferred.resolve(data);
              return cb();
            });
          }).
          error(function(err) {
            this.logout();
            deferred.reject(err);
            return cb(err);
          }.bind(this));

        return deferred.promise;
      },

      /**
       * Delete access token and user info
       *
       * @param  {Function}
       */
      logout: function() {
        $cookieStore.remove('token');
        currentUser = {};
        $window.location.href = '/auth/logout/';
      },

      /**
       * Create a new user
       *
       * @param  {Object}   user     - user info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      createUser: function(user, callback) {
        var cb = callback || angular.noop;

        return User.save(user,
          function(data) {
            $cookieStore.put('token', data.token);
            currentUser = User.get();
            return cb(user);
          },
          function(err) {
            this.logout();
            return cb(err);
          }.bind(this)).$promise;
      },

      /**
       * Change password
       *
       * @param  {String}   oldPassword
       * @param  {String}   newPassword
       * @param  {Function} callback    - optional
       * @return {Promise}
       */
      changePassword: function(oldPassword, newPassword, callback) {
        var cb = callback || angular.noop;

        return User.changePassword({ id: currentUser.id }, {
          oldPassword: oldPassword,
          newPassword: newPassword
        }, function(user) {
          return cb(user);
        }, function(err) {
          return cb(err);
        }).$promise;
      },

      /**
       * Gets all available info on authenticated user
       *
       * @return {Object} user
       */
      getCurrentUser: function() {
        return currentUser;
      },

      getCurrentDeckGroup: function () {
        var group = $cookieStore.get('selectedGroup');

        if (!group || !_.contains(currentUser.groups, group)) {
          group = _.isEmpty(currentUser.groups) ? null : (currentUser.groups[0] || null);
          $cookieStore.put('selectedGroup', group);
        }

        return group;
      },

      /**
       * Check if a user is logged in
       *
       * @return {Boolean}
       */
      isLoggedIn: function() {
        return currentUser.hasOwnProperty('title');
      },

      /**
       * Waits for currentUser to resolve before checking if user is logged in
       */
      isLoggedInAsync: function(cb) {
        if(currentUser.hasOwnProperty('$promise')) {
          currentUser.$promise.then(function(user) {
            Analytics.set('userId', user.id);
            cb(true);
          }).catch(function() {
            cb(false);
          });
        } else if(currentUser.hasOwnProperty('title')) {
          cb(true);
        } else {
          cb(false);
        }
      },

      saveRedirectUrl: function () {
        if ($location.path().toLowerCase() != '/login') {
          $cookieStore.put('redirectUrl', $location.path());
        } else {
          $cookieStore.put('redirectUrl', '/');
        }
      },

      authenticate: function () {
        var deferred = $q.defer();

        function reject() {
          Auth.saveRedirectUrl();

          $timeout(function () {
            $window.location.href = '/auth/login/';
          });

          deferred.reject(false);
        }

        if (currentUser.hasOwnProperty('$promise')) {
          currentUser.$promise.then(function() {
            if (_.isEmpty(currentUser)) return reject();

            checkRole(currentUser, 'CSRA Validated') ? deferred.resolve(true) : $state.go('validate');
          }).catch(function() {
            reject();
          });
        } else if (_.isEmpty(currentUser)) {
          reject();
        } else if (checkRole(currentUser, 'CSRA Validated')) {
          deferred.resolve(true);
        } else {
          $state.go('validate');
        }

        return deferred.promise;
      },

      /**
       * Check if a user is an admin
       *
       * @return {Boolean}
       */
      isAdmin: function() {
        return checkRole(currentUser, 'Administrator');
      },

      /**
       * Check if a user is a Program Manager
       *
       * @return {Boolean}
       */
      isProgramManager: function() {
        return checkRole(currentUser, 'Program Manager');
      },

      /**
       * Check if a user is an Executive
       *
       * @return {Boolean}
       */
      isExecutive: function() {
        return checkRole(currentUser, 'Executive');
      },

      /**
       * Check if a user is assigned a Program
       *
       * @return {Boolean}
       */
      hasProgram: function(projectId) {
        return hasProgram(currentUser, projectId);
      },

      /**
       * Get auth token
       */
      getToken: function() {
        return $cookieStore.get('token');
      },

      isOktaSession: function () {
        return $cookieStore.get('token') === 'okta';
      },

      forgotPassword: function (email) {
        return $http.post('/auth/local/forgot', {
          email: email
        });
      },

      validateUser: function (email) {
        return $http.post('/auth/local/validate', {
          email: email
        });
      },

      getForgottenUser: function (token) {
        var deferred = $q.defer();

        $http.get('/auth/local/reset/' + token).then(function (response) {
          deferred.resolve(response.data);
        }).catch(function () {
          deferred.resolve(null);
        });

        return deferred.promise;
      },

      getValidatedUser: function (token) {
        var deferred = $q.defer();

        $http.get('/auth/local/validate/' + token).then(function (response) {
          deferred.resolve(response.data);
        }).catch(function () {
          deferred.resolve(null);
        });

        return deferred.promise;
      },

      resetPassword: function (token, user) {
        var deferred = $q.defer();

        $http.post('/auth/local/reset/' + token, user).then(function () {
          deferred.resolve();
        }).catch(function (err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },

      getRoles: function() {
        var deferred = $q.defer();

        $http.get('/auth/local/roles').then(function(response) {
          deferred.resolve(response.data);
        }).catch(function() {
          deferred.resolve(null);
        });

        return deferred.promise;
      },

      getAuths: function() {
        var deferred = $q.defer();

        $http.get('/auth/local/auths').then(function(response) {
          deferred.resolve(response.data);
        }).catch(function() {
          deferred.resolve(null);
        });

        return deferred.promise;
      },

      getPrograms: function() {
        var deferred = $q.defer();

        $http.get('/api/programs').then(function(response) {
          deferred.resolve(response.data);
        }).catch(function() {
          deferred.resolve(null);
        });

        return deferred.promise;
      },

      getContracts: function() {
        var deferred = $q.defer();

        $http.get('/api/contract').then(function(response) {
          deferred.resolve(response.data);
        }).catch(function() {
          deferred.resolve(null);
        });

        return deferred.promise;
      },

      getPmrGroups: function() {
        var deferred = $q.defer();

        $http.get('/api/pmrgroups').then(function(response) {
          deferred.resolve(response.data);
        }).catch(function() {
          deferred.resolve(null);
        });

        return deferred.promise;
      },

      redirectToAttemptedUrl: function() {
        var url = $cookieStore.get('redirectUrl');
        $cookieStore.put('redirectUrl', '/');
        $location.path(url);
      }
    };

    return Auth;
  });
