'use strict';

angular.module('opsReviewApp')
  .factory('utilization', function (Deck, DataManager, FilterService) {
    var utilization = function(deckData) {
      angular.extend(this, new Deck(deckData));

      var layer_2_org_name = _.get(FilterService.getFilterPreset('layer_2_org_name'), 'selected', null);
      var layer_2_manager = _.get(FilterService.getFilterPreset('layer_2_manager'), 'value', null);

      var whereClause = {};

      if (layer_2_org_name) {
        whereClause.layer_2_org_name = layer_2_org_name;
      }

      this.filters = [
        {
          table: 'permast_data_layer_manager_filters',
          columns: [
            DataManager.generateAutocompleteConfig('layer_2_manager', null, 'name', whereClause,
              _.get(FilterService.getFilterPreset('layer_2_manager'), 'selected', null)),
            DataManager.generateAutocompleteConfig('layer_3_manager', null, 'name', whereClause,
              _.get(FilterService.getFilterPreset('layer_3_manager'), 'selected', null)),
            DataManager.generateAutocompleteConfig('layer_4_manager', null, 'name', whereClause,
              _.get(FilterService.getFilterPreset('layer_4_manager'), 'selected', null))
          ]
        }
      ];

      this.summaryBar.filters = {
        managerClause: layer_2_manager ? "layer_2_manager ~ '" + layer_2_manager + "'" : 'true',
        fiscalYearStart: DataManager.getPostgresDate(DataManager.fiscalYearStart),
        fiscalYearEnd: DataManager.getPostgresDate(DataManager.fiscalYearEnd)
      };

      var getFiscalYearYY = function(){
        var fiscalYear = DataManager.fiscalYearStart.getFullYear() + 1;
        return fiscalYear.toString().substr(2, 2);
      };

      var baseFY = "FY" + getFiscalYearYY();
      var compFY = "End of Year FY" + (getFiscalYearYY() - 1);

      this.summaryBar.bars[baseFY] = [
        {
          "name": "billable utilization",
          "column": "base_billable",
          "decimals": 2,
          "suffix": "%"
        },
        {
          "name": "productive utilization",
          "column": "base_productive",
          "decimals": 2,
          "suffix": "%"
        },
        {
          "name": "total utilization",
          "column": "base_total",
          "decimals": 2,
          "suffix": "%"
        }
      ];

      this.summaryBar.bars[compFY] = [
        {
          "name": "billable utilization",
          "column": "comp_billable",
          "decimals": 2,
          "suffix": "%"
        },
        {
          "name": "productive utilization",
          "column": "comp_productive",
          "decimals": 2,
          "suffix": "%"
        },
        {
          "name": "total utilization",
          "column": "comp_total",
          "decimals": 2,
          "suffix": "%"
        }
      ];

      return this;
    };

    utilization.prototype = Deck.prototype;

    return utilization;
  });
