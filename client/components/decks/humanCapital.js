'use strict';

angular.module('opsReviewApp')
  .factory('humanCapital', function (Deck, DataManager, FilterService, SyncCharts, DateUtils) {

    var humanCapital = function(deckData) {
      angular.extend(this, new Deck(deckData));

      var layer_2_org_name = _.get(FilterService.getFilterPreset('layer_2_org_name'), 'selected', null);
      var layer_2_manager = _.get(FilterService.getFilterPreset('layer_2_manager'), 'value', null);

      var whereClause = {};

      if (layer_2_org_name) {
        whereClause.layer_2_org_name = layer_2_org_name;
      }

      this.filters = [
        DataManager.generateAutocompleteConfig(
          '/api/analytics/manager_filters?layer=2',
          'layer2',
          'org_key',
          'Layer 2 Organization',
          'name',
          _.get(FilterService.getFilterPreset('layer_2_manager'), 'selected', null)
        ),

        DataManager.generateAutocompleteConfig(
          '/api/analytics/manager_filters?layer=3',
          'layer3',
          'org_key',
          'Layer 3 Organization',
          'name',
          _.get(FilterService.getFilterPreset('layer_2_manager'), 'selected', null)
        ),

        DataManager.generateAutocompleteConfig(
          '/api/analytics/manager_filters?layer=4',
          'layer4',
          'org_key',
          'Layer 4 Organization',
          'name',
          _.get(FilterService.getFilterPreset('layer_2_manager'), 'selected', null)
        ),

        {
          name: 'remove_casual_employees',
          titleName: 'Employee Group',
          checkboxes: [
            {
              displayName: 'Remove Casual Employees'
            }
          ]
        }
      ];

      this.seriesColorMap = {
        'Head Count': DataManager.colorMap.headcount,
        'Head Count Of Non Casual Employees': DataManager.colorMap.headcount,
        'Hires': DataManager.colorMap.hires,
        'Hires Of Non Casual Employees': DataManager.colorMap.hires,
        'Voluntary Terminations': DataManager.colorMap.vterminations,
        'Voluntary Terminations Of Non Casual Employees': DataManager.colorMap.vterminations,
        'Involuntary Terminations': DataManager.colorMap.iterminations,
        'Involuntary Terminations Of Non Casual Employees': DataManager.colorMap.iterminations
      };

      SyncCharts.createCountdownLatches(this);

      return this;
    };

    humanCapital.prototype = Deck.prototype;

    return humanCapital;
  });
