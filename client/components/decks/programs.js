'use strict';

angular.module('opsReviewApp')
  .factory('programs', function (Deck, DataManager, DateUtils) {
    var programs = function (deckData) {
      angular.extend(this, new Deck(deckData));

      this.maxEndDate = moment().format(DateUtils.defaultDateFormat);

      this.filters = [
        {
          table: 'contract_data_secure',
          columns: [
            DataManager.generateAutocompleteConfig('l3_portfolio', 'L3 Portfolio', 'none')
          ]
        },
        {
          table: 'permast_l3_managers',
          columns: [
            DataManager.generateAutocompleteConfig('layer_3_manager', 'L3 Manager', 'name')
          ]
        },
        {
          table: 'contract_data_secure',
          columns: [
            DataManager.generateAutocompleteConfig('program_manager', null, 'name')
          ]
        },
        {
          table: 'program_metadata_manual_entry',
          columns: [
            DataManager.generateAutocompleteConfig('pra', null, 'name')
          ]
        },
        {
          table: 'programs_secure',
          columns: [
            DataManager.generateAutocompleteConfig('project_id', 'program', 'none')
          ]
        },
        {
          table: 'staffing_level_fte',
          columns: [
            DataManager.generateAutocompleteConfig('staffing_source', null, 'none', {})
          ]
        }
      ];

      this.seriesColorMap = {
        'Baseline': DataManager.colorMap.lightblue,
        'Actuals': DataManager.colorMap.lightgreen,
        'Forecast': DataManager.colorMap.lightorange
      };

      this.dataForms = {
        'Program Info': {
          title: 'Contract Info',
          statusMap: {
            'complete': 'green',
            'in progress': 'yellow',
            'incomplete': 'red'
          },
          tabs: [
            {
              iconClass: 'ops-icon-keyboard-arrow-left',
              onClick: function (form) {
                form.selectedResource = null;
                form.goTo('Program Details');
              },
              displayTab: function (form) {
                return !_.isEmpty(form.selectedResource);
              }
            },
            {
              name: 'Program Overview',
              shortName: 'PO',
              class: 'pi-overview',
              sections: [
                {
                  title: 'Overview',
                  subTitle: 'General Information',
                  fields: [
                    {
                      type: 'html',
                      content: '<program-overview form="form" field="field"></program-overview>' // TODO not finding the form
                    }
                  ]
                }
              ],
              footerButtons: [
                {
                  text: 'Done',
                  color: 'white',
                  onClick: function (form) {
                    form.close();
                  }
                },
                {
                  text: 'Next',
                  onClick: function (form) {
                    form.goTo('Program Details');
                  },
                  displayButton: function (form) {
                    return !_.isEmpty(form.selectedProgram);
                  }
                }
              ],
              displayTab: function (form) {
                return _.isEmpty(form.selectedResource);
              }
            },
            {
              name: 'Program Details',
              shortName: 'PD',
              class: 'pi-overview',
              sections: [
                {
                  fields: [
                    {
                      type: 'html',
                      content: '<program-heatmap field="field" form="form"></program-heatmap>'
                    }

                  ]
                },
                {
                 title: 'General Information',
                 class: 'fields',
                 fields: [
                   {
                     name: 'Are upcoming vacancies in your forecast?',
                     type: 'radio',
                     options: [{text: 'Yes', value: true}, {text: 'No', value: false}]
                   },
                   {
                     name: 'Date of Impact of the Earliest Upcoming Vacancy',
                     placeholder: 'Date of Impact',
                     type: 'text'
                   },
                   {
                     name: 'Current Vacancies',
                     placeholder: 'Number of Current Vacancies',
                     type: 'text'
                   },
                   {
                     name: 'Upcoming Vacancies',
                     placeholder: 'Number of Upcoming Vacancies',
                     type: 'text'
                   },
                   {}
                 ]
                },
                {
                  title: 'Resource Selection',
                  fields: [
                    {
                      type: 'html',
                      content: '<resources-table form="form" field="field"></resources-table>'
                    }
                  ]
                }
              ],
              footerButtons: [
                {
                  text: 'Back',
                  color: 'white',
                  onClick: function (form) {
                    form.goTo('Program Overview');
                  }
                },
                {
                  text: 'Save',
                  color: 'yellow',
                  onClick: function (form) {
                    form.saveCurrentProgram();
                  }
                }
              ],
              displayTab: function (form) {
                return !_.isEmpty(form.selectedProgram) && _.isEmpty(form.selectedResource);
              }
            },
            {
              name: 'Resource Details',
              shortName: 'RD',
              class: 'pi-staffing',
              sections: [
                {
                  fields: [
                    {
                      type: 'html',
                      class: 'full',
                      content: '<resource-heatmap field="field" form="form"></resource-heatmap>'
                    }
                  ]
                }
                ,
                {
                 title: 'Financial Information',
                 class: 'fields',
                 fields: [
                   {
                     name: '% Revenue, FTW',
                     type: 'text'
                   },
                   {
                     name: 'Contract Length',
                     type: 'radio',
                     options: [{text: 'Yes', value: true}, {text: 'No', value: false}]
                   },
                   {
                     name: '% Revenue, ODC',
                     type: 'text'
                   },
                   {
                     name: 'Date of Impact of the Earliest Upcoming Vacancy',
                     type: 'text'
                   },
                   {
                     name: 'Notes',
                     placeholder: 'Notes',
                     type: 'textarea'
                   }
                 ]
                }
              ],
              footerButtons: [
                {
                  text: 'Back',
                  color: 'white',
                  onClick: function (form) {
                    form.selectedResource = null;
                    form.goTo('Program Details');
                  }
                },
                {
                  text: 'save',
                  color: 'yellow',
                  onClick: function (form) {
                    form.saveCurrentResource();
                  }
                }
              ],
              displayTab: function (form) {
                return !_.isEmpty(form.selectedResource);
              }
            }
          ],
          display: function (display) {
            return _.isFunction(display) ? display(this) : (_.isUndefined(display) ? true : display);
          },
          selectTab: function (tab) {
            tab.onClick ? tab.onClick(this) : this.activeTab = tab;
          },
          getTab: function (tabName) {
            return _.find(this.tabs, {name: tabName});
          },
          goTo: function (tabName) {
            this.activeTab = this.getTab(tabName);
          },
          close: function () {
            this.modal.close();
          }
        }
      };

      this.summaryBar.filters = {
        startDate: DateUtils.getFormattedDateFromDateString(this.startDate),
        endDate: DateUtils.getFormattedDateFromDateString(this.endDate),
        projectIdClause: "like '%'" // the column name is omitted here since the project id column can be different in different tables
      };
      this.dataForms['Program Info'].activeTab = this.dataForms['Program Info'].getTab('Program Overview');

      return this;
    };

    programs.prototype = Deck.prototype;

    return programs;
  });
