'use strict';

angular.module('opsReviewApp')
  .factory('laborHourReporting', function (Deck, DataManager, FilterService) {
    var laborHourReporting = function(deckData) {
      angular.extend(this, new Deck(deckData));

      var layer_2_org_name = _.get(FilterService.getFilterPreset('layer_2_org_name'), 'selected', null);
      var layer_2_manager = _.get(FilterService.getFilterPreset('layer_2_manager'), 'value', null);

      var whereClause = {};

      if (layer_2_org_name) {
        whereClause.layer_2_org_name = layer_2_org_name;
      }

      this.filters = [
        {
          table: 'permast_data_layer_manager_filters',
          columns: [
            DataManager.generateAutocompleteConfig('layer_2_manager', null, 'name', whereClause,
              _.get(FilterService.getFilterPreset('layer_2_manager'), 'selected', null)),
            DataManager.generateAutocompleteConfig('layer_3_manager', null, 'name', whereClause,
              _.get(FilterService.getFilterPreset('layer_3_manager'), 'selected', null)),
            DataManager.generateAutocompleteConfig('layer_4_manager', null, 'name', whereClause,
              _.get(FilterService.getFilterPreset('layer_4_manager'), 'selected', null))
          ]
        }
      ];

      this.summaryBar.filters = {
        managerClause: layer_2_manager ? "layer_2_manager ~ '" + layer_2_manager + "'" : 'true',
        fiscalYearStart: DataManager.getPostgresDate(DataManager.fiscalYearStart),
        fiscalYearEnd: DataManager.getPostgresDate(DataManager.fiscalYearEnd)
      };

      this.summaryBar.postQuery = function(deck,data){
        var row = data[0];
        var cfy_last_date = moment(row.cfy_week_ending_date);
        var cfy_first_date = moment(DataManager.fiscalYearStart);

        row.cfy_number_of_weeks = cfy_last_date.diff(cfy_first_date, 'weeks');

        row.cfy_percent_billable_hours = 100.0 *
          row.cfy_billable_hours / row.cfy_total_hours;

        row.cfy_percent_productive_hours = 100.0 *
          row.cfy_productive_hours / row.cfy_total_hours;


        row.cfy_avg_hours_per_week = row.cfy_total_hours / row.cfy_number_of_weeks;

        // do same for the previous fiscal year

        row.pfy_percent_billable_hours = 100.0 *
          row.pfy_billable_hours / row.pfy_total_hours;

        row.pfy_percent_productive_hours = 100.0 *
          row.pfy_productive_hours / row.pfy_total_hours;

        row.pfy_number_of_weeks = 53; // FIXME: magic number provided by brian T.

        row.pfy_avg_hours_per_week = row.pfy_total_hours / row.pfy_number_of_weeks;

       };

      var getFiscalYearYY = function(){
        var fiscalYear = DataManager.fiscalYearStart.getFullYear() + 1;
        return fiscalYear.toString().substr(2, 2);
      };

      var cfy = getFiscalYearYY();
      var pfy = cfy - 1;

      // titles for the bars
      var cfyBarTitle = "FY" + cfy;
      var pfyBarTitle = "End of Year FY" + pfy;

      this.summaryBar.bars[cfyBarTitle] = [
        {
          "name": "average hours/week",
          "column": "cfy_avg_hours_per_week",
          "decimals": 2,
          "suffix": ""
        },
        {
          "name": "billable",
          "column": "cfy_percent_billable_hours",
          "decimals": 2,
          "suffix": "%"
        },
        {
          "name": "productive",
          "column": "cfy_percent_productive_hours",
          "decimals": 2,
          "suffix": "%"
        }
      ];

      this.summaryBar.bars[pfyBarTitle] = [
        {
          "name": "average hours/week",
          "column": "pfy_avg_hours_per_week",
          "decimals": 2,
          "suffix": ""
        },
        {
          "name": "billable",
          "column": "pfy_percent_billable_hours",
          "decimals": 2,
          "suffix": "%"
        },
        {
          "name": "productive",
          "column": "pfy_percent_productive_hours",
          "decimals": 2,
          "suffix": "%"
        }
      ];

      return this;
    };

    laborHourReporting.prototype = Deck.prototype;

    return laborHourReporting;
  });
