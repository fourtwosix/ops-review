'use strict';

angular.module('opsReviewApp')
  .factory('programStatus', function (Deck, DataManager, FilterService, DateUtils) {
    var programStatus = function (deckData) {
      angular.extend(this, new Deck(deckData));

      this.endDate = moment().subtract(1, 'months').format(DateUtils.defaultDateFormat);
      this.maxEndDate = moment().format(DateUtils.defaultDateFormat);

      this.filters = [
        DataManager.generateAutocompleteConfig( //apiUrl, nameColumn, valueColumn, titleName, formatter, selected, dependsOnFilters, dependedByFilters, selections
          null, //apiUrl
          'l1', //nameColumn
          'l1', //valueColumn
          'HQ', //titleName
          'caps', //forMatter
          _.get(FilterService.getFilterPreset('l1'), 'selected', null), //selected
          null,
          ['pra','group_name', 'l2_manager', 'division_name', 'l3_manager', 'program_manager', 'l4_manager', 'project_id'], // these filters will be reset if this filter selection changes
          [{displayName: 'GDIT', l1: 'GDIT'}], // selections
          false
        ),
        DataManager.generateAutocompleteConfig(
          '/api/analytics/program_group_filters?column=group_name', //apiUrl
          'group_name', //nameColumn
          'group_name', //valueColumn
          'Division', //titleName
          'titleKeepSymbols', //forMatter
          _.get(FilterService.getFilterPreset('group_name'), 'selected', null), //selected
          null,
          ['pra','l2_manager', 'division_name', 'l3_manager', 'program_manager', 'l4_manager', 'project_id'], // these filters will be reset if this filter selection changes
          null,
          true
        ),
        DataManager.generateAutocompleteConfig(
          '/api/analytics/program_group_filters?column=l2_manager', //apiUrl
          'l2_manager', //nameColumn
          'l2_manager', //valueColumn
          'Division Manager', //titleName
          'titleKeepSymbols', //forMatter
          _.get(FilterService.getFilterPreset('l2_manager'), 'selected', null), //selected
          null,
          ['pra','group_name', 'division_name', 'l3_manager', 'program_manager', 'l4_manager', 'project_id'], // these filters will be reset if this filter selection changes
          null,
          true
        ),
        DataManager.generateAutocompleteConfig(
          '/api/analytics/program_group_division_filters?column=division_name', //apiUrl
          'division_name', //nameColumn
          'division_name', //valueColumn
          'Sector', //titleName
          'titleKeepSymbols', //forMatter
          _.get(FilterService.getFilterPreset('division_name'), 'selected', null), //selected
          [ // to only display suggestion values based on these filters
            {table: 'pmr_groups', column: 'group_name'},
            {table: 'pmr_groups', column: 'l2_manager'}
          ],
          ['l3_manager'], // these filters will be reset if this filter selection changes
          null,
          true
        ),
        DataManager.generateAutocompleteConfig(
          '/api/analytics/program_group_division_filters?column=l3_manager, division_name', //apiUrl
          'l3_manager', //nameColumn
          'l3_manager', //valueColumn
          'Sector Manager', //titleName
          'titleKeepSymbols', //forMatter
          _.get(FilterService.getFilterPreset('l3_manager'), 'selected', null), //selected
          [ // to only display suggestion values based on these filters
            {table: 'pmr_groups', column: 'group_name'},
            {table: 'pmr_groups', column: 'l2_manager'}
          ],
          ['division_name'], // these filters will be reset if this filter selection changes
          null,
          true
        ),
        DataManager.generateAutocompleteConfig(
          '/api/analytics/program_metadata_filters?column=program_manager, division, program_id',
          'program_manager',
          'program_manager',
          'Program Manager',
          'name',
          _.get(FilterService.getFilterPreset('program_manager'), 'selected', null),
          null,
          ['pra','l2_manager', 'group_name', 'division_name', 'l3_manager', 'l4_manager', 'project_id'], // these filters will be reset if this filter selection changes
          null,
          true
        ),
        DataManager.generateAutocompleteConfig(
          '/api/analytics/program_metadata_filters?column=l4_manager, division', //apiUrl
          'l4_manager', //nameColumn
          'l4_manager', //valueColumn
          'Business Area Manager', //titleName
          'titleKeepSymbols', //forMatter
          _.get(FilterService.getFilterPreset('l4_manager'), 'selected', null), //selected
          null,
          ['pra','program_manager','l2_manager', 'group_name', 'division_name', 'l3_manager', 'project_id'], // these filters will be reset if this filter selection changes
          null,
          true
        ),
      DataManager.generateAutocompleteConfig(
          '/api/analytics/program_metadata_filters?column=pra, division, program_id',
        'pra',
        'pra',
        'PRA',
        'titleKeepSymbols',
        _.get(FilterService.getFilterPreset('pra'), 'selected', null),
        null,
        ['program_manager','l2_manager', 'group_name', 'division_name', 'l3_manager', 'l4_manager', 'project_id'], // these filters will be reset if this filter selection changes
        null,
        true
      ),
        //DataManager.generateAutocompleteConfig(
        //  '/api/analytics/program_metadata_filters?column=pra',
        //  'pra',
        //  'pra',
        //  'PRA',
        //  'name',
        //  _.get(FilterService.getFilterPreset('pra'), 'selected', null)
        //),
        DataManager.generateAutocompleteConfig(
          '/api/analytics/program_filters?column=project_id, pmr_group, division',
          'project_id',
          'project_id',
          'Program',
          'caps',
          _.get(FilterService.getFilterPreset('project_id'), 'selected', null),
          [{table: 'program_metadata_manual_entry', column: 'program_status'}],
          ['pra', 'l2_manager', 'group_name', 'division_name', 'l3_manager', 'program_manager', 'l4_manager'], // these filters will be reset if this filter selection changes
          null,
          true
        ),
        {
          name: 'program_status',
          titleName: 'Program Status',
          checkboxes: [
            {
              displayName: 'Display Closed and Exempt Programs'
            }
          ],
          dependedByFilters: ['project_id'],
          exemptFromDeckStatus: true,
          allowClear: true
        }
      ];

      // If there are more than one filter selected make sure the CSRA filter gets
      // deselected
      if (this.getSelectedFilterValues().length > 1) {
        this.filters[0].selectedFilter = null;
      }

      this.initFilters();

      this.seriesColorMap = {
        'Baseline': DataManager.colorMap.lightblue,
        'Actuals': DataManager.colorMap.lightgreen,
        'Forecast': DataManager.colorMap.lightorange
      };

      return this;
    };

    programStatus.prototype = Deck.prototype;

    return programStatus;
  });
