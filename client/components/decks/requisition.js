'use strict';

angular.module('opsReviewApp')
  .factory('requisition', function (Deck, DataManager, FilterService, DateUtils) {

    var requisition = function(deckData) {
      angular.extend(this, new Deck(deckData));

      var layer_2_org_name = _.get(FilterService.getFilterPreset('layer_2_org_name'), 'selected', null);
      var layer_2_manager = _.get(FilterService.getFilterPreset('layer_2_manager'), 'value', null);

      var whereClause = {};

      if (layer_2_org_name) {
        whereClause.layer_2_org_name = layer_2_org_name;
      }

      this.filters = [
        {
          table: 'permast_data_layer_manager_filters',
          columns: [
            DataManager.generateAutocompleteConfig('layer_2_manager', null, 'name', whereClause,
              _.get(FilterService.getFilterPreset('layer_2_manager'), 'selected', null)),
            DataManager.generateAutocompleteConfig('layer_3_manager', null, 'name', whereClause,
              _.get(FilterService.getFilterPreset('layer_3_manager'), 'selected', null)),
            DataManager.generateAutocompleteConfig('layer_4_manager', null, 'name', whereClause,
              _.get(FilterService.getFilterPreset('layer_4_manager'), 'selected', null))
          ]
        },
        {
          table: 'candidate_data',
          columns: [
            {
              name: 'req_accepted',
              titleName: 'Req Status',
              checkboxes: [
                {
                  displayName: 'Remove Reqs with Accepted Offers'
                }
              ]
            }
          ]
        },
        {
          table: 'candidate_data_recruiting_filters',
          columns: [
            DataManager.generateAutocompleteConfig('client_program_name', 'Client/Program', 'titleKeepSymbols'),
            DataManager.generateAutocompleteConfig('req_hiring_manager_name', 'Hiring Manager', 'name'),
            DataManager.generateAutocompleteConfig('req_recruiter_name', 'Recruiter', 'name')
          ]
        }

      ];

      this.summaryBar.filters = {
        managerClause: layer_2_manager ? "layer_2_manager ~ '" + layer_2_manager + "'" : 'true',
        startDate: DateUtils.getFormattedDateFromDateString(this.startDate),
        endDate: DateUtils.getFormattedDateFromDateString(this.endDate),
        reqAcceptedClause: "TRUE"
      };

      return this;
    };

    requisition.prototype = Deck.prototype;

    return requisition;
  });
