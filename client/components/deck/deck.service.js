'use strict';

angular.module('opsReviewApp')
  .factory('Deck', function ($q, DeckManager, DataManager, Modal, DateUtils, DeckFilterManager) {

    var Deck = function (deckData) {
      return this.setData(deckData);
    };

    Deck.prototype.setData = function (deckData) {
      angular.extend(this, deckData);
      // Set default date range for deck
      this.endDate = this.endDate || this.dateFilterType === "month" ? moment().startOf('month').format(DateUtils.defaultDateFormat) : moment().format(DateUtils.defaultDateFormat);
      this.maxEndDate = this.maxEndDate || null;
      this.startDate = this.startDate || moment(DataManager.fiscalYearStart).format(DateUtils.defaultDateFormat);

      if (DeckFilterManager.getDateFilter()) {
        this.startDate = DeckFilterManager.getDateFilter().start;
        this.endDate = DeckFilterManager.getDateFilter().end;
      }
      else {
        DeckFilterManager.setDateFilter({
          start: this.startDate,
          end: this.endDate,
          maxEndDate: this.maxEndDate
        });
      }

      this.dateFilterType = this.dateFilterType || "day";

      return this;
    };

    Deck.prototype.saveDeck = function () {
      return DeckManager.saveDeck(this);
    };

    Deck.prototype.exportDeck = function () {

    };

    Deck.prototype.getFilterName = function (column) {
      return _.startCase(column);
    };


    Deck.prototype.openDataForm = function (formName) {
      if (_.has(this.dataForms, formName)) {
        var modal = Modal.form();
        modal(this.dataForms[formName]);
      }
    };

    Deck.prototype.getDataFormNames = function () {
      return this.dataForms ? _.keys(this.dataForms) : [];
    };

    Deck.prototype.getSelectedDependingFilters = function (filterGroup) {
      var dependsOn = [];
      var self = this;
      if (filterGroup.dependsOnFilters) {
        _.forEach(filterGroup.dependsOnFilters, function(dependingFilter) {
          var dependingFilterObj = _.find(self.filters, {name: dependingFilter.column});
          var selectedFilter = {};
          if (dependingFilterObj.checkboxes) {
            _.forEach(dependingFilterObj.checkboxes, function (checkbox) {
              selectedFilter = {
                nameColumn: dependingFilterObj.name,
                selectedFilter: checkbox.selected === undefined || checkbox.selected === false ? "open" : "closed"
              }
            });
          }
          else {
            selectedFilter = _.pick(dependingFilterObj, ['nameColumn', 'selectedFilter']);
          }
          if (selectedFilter && !_.isEmpty(selectedFilter.selectedFilter)) {
            if (!dependingFilterObj.checkboxes) {
              selectedFilter.selectedFilter = selectedFilter.selectedFilter[selectedFilter.nameColumn];
            }
            dependsOn.push(selectedFilter);
          }
        });
      }
      return dependsOn;
    };

    Deck.prototype.getFilters = function (apiUrl, filter, query, selections) {
      if (selections) {
        return $q.when(selections);
      } else {
        var dependsOn = this.getSelectedDependingFilters(filter);
        return DeckManager.getFilterAutocomplete(apiUrl, filter, query, dependsOn);
      }
    };

    Deck.prototype.getSelectedFilters = function () {
      var filters =  {};
      _.forEach(this.filters, function (filter) {
        if (filter.checkboxes) {
          // TODO shouldn't be checkboxes should just be a checkbox per filter
          _.forEach(filter.checkboxes, function (checkbox) {
            if (checkbox.selected) {
              filters[filter.name] = checkbox.selected;
            }
          });
        } else if (filter.selectedFilter) {
          filters[filter.name] = filter.valueColumn ? filter.selectedFilter[filter.valueColumn] : filter.selectedFilter;
        }
      });
      return filters;
    };

    Deck.prototype.getSelectedFilterValues = function () {
      return _.map(this.getSelectedFilters(), function (value, key) {
        return _.isBoolean(value) ? _.startCase(key) : _.startCase(value);
      });
    };

    Deck.prototype.getSelectedFiltersJSON = function () {
      var filters = {};

      filters.startDate = this.startDate;
      filters.endDate = this.endDate;
      filters.fiscalStart = DateUtils.getFiscalStartMomentFromDateString(this.endDate).format('L');
      filters.fiscalEnd = DateUtils.getFiscalEndMomentFromDateString(this.endDate).format('L');
      filters.fiscalGDITEnd = DateUtils.getGDITFiscalEndMomentFromDateString(this.endDate).format('L');


      return _.merge(filters, this.selectedFilters);
    };

    Deck.prototype.isNew = function () {
      return !angular.isDefined(this.id);
    };

    Deck.prototype.initFilters = function () {
      this.selectedFilters = this.getSelectedFilters();
    };

    return Deck;
  });
