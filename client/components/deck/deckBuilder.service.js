angular.module('opsReviewApp')
  .factory('DeckBuilder', function ($injector, Deck, CardBuilder) {
    var DeckBuilder = {};

    DeckBuilder.build = function(deck) {
      deck.cards = _.map(deck.cards, function (card) {
        return CardBuilder.build(card);
      });

      if ($injector.has(deck.deckName)) {
        return $injector.invoke([deck.deckName, function(service) {
          return new service(deck);
        }]);
      } else {
        return new Deck(deck);
      }
    };

    return DeckBuilder;
  });
