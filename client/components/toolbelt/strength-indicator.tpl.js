angular.module('toolbelt.strength.tpl', []).run(['$templateCache', function ($templateCache) {
  $templateCache.put(
    'template/toolbelt/strength.html',
    ['<span>',
      '<ul class="strength-indicator" title="{{ result.complexity }}">',
      '<li data-ng-repeat="i in [1,4,5,6,7,8]" class="point" data-ng-class="{ true: result.label }[i <= result.rank]"></li>',
      '</ul>',
      '</span>'
    ].join('\n')
  );
}]);
