'use strict';

angular.module('opsReviewApp')
  .directive('ngPlaceholder', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var placeholder = attrs.ngPlaceholder;
        var placeholderField = attrs.placeholderField || 'placeholder';
        $(element).attr(placeholderField, placeholder);
      }
    }
  });
