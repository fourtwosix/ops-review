'use strict';

angular.module('opsReviewApp')
  .controller('DeckDateCtrl', function ($scope, $uibModalInstance, deck, DataManager, DeckFilterManager, DateUtils) {
    $scope.deck = deck;
    var useMonths = $scope.deck.dateFilterType === 'month';

    $scope.date = {
      start: deck.startDate,
      end: deck.endDate,
      maxEndDate: deck.maxEndDate
    };

    $scope.minStartDate = useMonths ? DateUtils.getMonthStart(DataManager.fiscalYearStart, DateUtils.defaultDateFormat) :
      moment(DataManager.fiscalYearStart).format(DateUtils.defaultDateFormat);


    $scope.$watch('date', function () {

      var startMoment = useMonths ? moment(new Date($scope.date.start)).startOf('month') :
        moment(new Date($scope.date.start));
      var endMoment = useMonths ? moment(new Date($scope.date.end)).endOf('month') :
        moment(new Date($scope.date.end));

      if ($scope.date.start && startMoment.isValid()) {
        $scope.date.start = startMoment.format(DateUtils.defaultDateFormat);
        $scope.minEndDate = startMoment.add(1, useMonths ? 'month' : 'days').format(DateUtils.defaultDateFormat);
      } else {
        $scope.date.start = null;
        $scope.minEndDate = null;
      }

      if ($scope.date.end && endMoment.isValid()) {
        $scope.date.end = endMoment.format(DateUtils.defaultDateFormat);
        $scope.maxStartDate = endMoment.subtract(1, useMonths ? 'month' : 'days').format(DateUtils.defaultDateFormat);
      } else {
        $scope.date.end = null;
        $scope.maxStartDate = null;
      }

      if(useMonths) {
        $scope.date.start = $scope.date.end;
        $scope.minEndDate = null;
        $scope.maxStartDate = null;
        $scope.maxEndDate = $scope.date.maxEndDate;
      }

      $scope.$broadcast('refreshDatepickers');
    }, true);

    $scope.getDayClass = function (date, mode, type) {
      if ($scope.date) {
        var startDate = $scope.date.start ? moment(new Date($scope.date.start)) : null;
        var endDate = $scope.date.end ? moment(new Date($scope.date.end)) : null;
        var selectedDate = moment(new Date(date));
        var onlyDate = !(startDate && endDate);
        var classStr;

        // If there are two dates then check if day falls within range
        if (!onlyDate && selectedDate.isBetween(startDate, endDate, useMonths ? 'month' : 'days')) {
          return 'in-range';
        }

        // If there is one date check if date is the start or end date
        if (type === 'start' && startDate && startDate.isSame(selectedDate, useMonths ? 'month' : 'day')) {
          classStr = 'start-date';

          return onlyDate ? classStr + " only-date" : classStr;
        } else if (type === 'end' && endDate && endDate.isSame(selectedDate, useMonths ? 'month' : 'day')) {
          classStr = 'end-date';

          return onlyDate ? classStr + " only-date" : classStr;
        }
      }
    };

    $scope.applyFilters = function () {
      DeckFilterManager.setDateFilter($scope.date);
      $uibModalInstance.close({filters: {date_range: $scope.date}});
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });

angular.module('ui.bootstrap.datepicker')
  .config(function($provide) {
    $provide.decorator('datepickerDirective', function($delegate) {
      var directive = $delegate[0];
      var link = directive.link;

      directive.compile = function() {
        return function(scope, element, attrs, ctrls) {
          link.apply(this, arguments);

          var datepickerCtrl = ctrls[0];
          var ngModelCtrl = ctrls[1];

          if (ngModelCtrl) {
            datepickerCtrl.refreshView();

            // Listen for 'refreshDatepickers' event...
            scope.$on('refreshDatepickers', function refreshView() {
              datepickerCtrl.refreshView();
            });
          }
        }
      };
      return $delegate;
    });
  });

// Override angular-bootstraps day directive because they incorporated one time binding for the customClass
// and we need it to update everytime we change the range.

angular.module("template/datepicker/day.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("template/datepicker/day.html",
    "<table role=\"grid\" aria-labelledby=\"{{::uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\">\n" +
    "  <thead>\n" +
    "    <tr>\n" +
    "      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-left\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-left\"></i></button></th>\n" +
    "      <th colspan=\"{{::5 + showWeeks}}\"><button id=\"{{::uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" class=\"btn btn-default btn-sm\" ng-click=\"toggleMode()\" ng-disabled=\"datepickerMode === maxMode\" tabindex=\"-1\" style=\"width:100%;\"><strong>{{title}}</strong></button></th>\n" +
    "      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-right\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-right\"></i></button></th>\n" +
    "    </tr>\n" +
    "    <tr>\n" +
    "      <th ng-if=\"showWeeks\" class=\"text-center\"></th>\n" +
    "      <th ng-repeat=\"label in ::labels track by $index\" class=\"text-center\"><small aria-label=\"{{::label.full}}\">{{::label.abbr}}</small></th>\n" +
    "    </tr>\n" +
    "  </thead>\n" +
    "  <tbody>\n" +
    "    <tr ng-repeat=\"row in rows track by $index\">\n" +
    "      <td ng-if=\"showWeeks\" class=\"text-center h6\"><em>{{ weekNumbers[$index] }}</em></td>\n" +
    "      <td ng-repeat=\"dt in row track by dt.date\" class=\"text-center\" role=\"gridcell\" id=\"{{::dt.uid}}\" ng-class=\"dt.customClass\">\n" +
    "        <button type=\"button\" style=\"min-width:100%;\" class=\"btn btn-default btn-sm\" ng-class=\"{'btn-info': dt.selected, active: isActive(dt)}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" tabindex=\"-1\"><span ng-class=\"::{'text-muted': dt.secondary, 'text-info': dt.current}\">{{::dt.label}}</span></button>\n" +
    "      </td>\n" +
    "    </tr>\n" +
    "  </tbody>\n" +
    "</table>\n" +
    "");
}]);

angular.module("template/datepicker/month.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("template/datepicker/month.html",
    "<table role=\"grid\" aria-labelledby=\"{{::uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\">\n" +
    "  <thead>\n" +
    "    <tr>\n" +
    "      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-left\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-left\"></i></button></th>\n" +
    "      <th><button id=\"{{::uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" class=\"btn btn-default btn-sm\" ng-click=\"toggleMode()\" ng-disabled=\"datepickerMode === maxMode\" tabindex=\"-1\" style=\"width:100%;\"><strong>{{title}}</strong></button></th>\n" +
    "      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-right\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-right\"></i></button></th>\n" +
    "    </tr>\n" +
    "  </thead>\n" +
    "  <tbody>\n" +
    "    <tr ng-repeat=\"row in rows track by $index\">\n" +
    "      <td ng-repeat=\"dt in row track by dt.date\" class=\"text-center\" role=\"gridcell\" id=\"{{::dt.uid}}\" ng-class=\"dt.customClass\">\n" +
    "        <button type=\"button\" style=\"min-width:100%;\" class=\"btn btn-default\" ng-class=\"{'btn-info': dt.selected, active: isActive(dt)}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" tabindex=\"-1\"><span ng-class=\"::{'text-info': dt.current}\">{{::dt.label}}</span></button>\n" +
    "      </td>\n" +
    "    </tr>\n" +
    "  </tbody>\n" +
    "</table>\n" +
    "");
}]);
