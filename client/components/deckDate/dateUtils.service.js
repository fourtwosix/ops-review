'use strict';

angular.module('opsReviewApp')
  .factory('DateUtils', function DateUtils() {
    var defaultDateFormat = 'YYYY-MM-DD';

    function getDefinedFormat(dateFormat) {
      return angular.isDefined(dateFormat) ? dateFormat : defaultDateFormat;
    }

    return {
      defaultDateFormat: defaultDateFormat,
      getMomentFromDateString: function(dateString, dateFormat) {
        // new Date('2015-06-01') results in date being off by 1 but new Date('2015/06/01') works fine (go figure)
        // This seems to be a bug in JavaScript, as other libraries are also performing this replacement.
        if(dateFormat) {
          return moment.utc(dateString, dateFormat);
        } else {
          return moment.utc(new Date(dateString.replace(/-/g, '/')));
        }
      },
      getFormattedDateFromDateString: function(dateString, dateFormat) {
        return this.getMomentFromDateString(dateString).format(getDefinedFormat(dateFormat));
      },
      getFormattedDateFromDate: function(date, dateFormat) {
        return moment.utc(date).format(getDefinedFormat(dateFormat));
      },
      getFirstFridayFromDateString: function(dateString, dateFormat) {
        return this.getMomentFromDateString(dateString).startOf('month').endOf('week').subtract(1, 'day').format(getDefinedFormat(dateFormat));
      },
      getMonthsAgoMoment: function(dateString, months, dateFormat) {
        return this.getMomentFromDateString(dateString, dateFormat).subtract(months, 'months').date(1).hours(0).minutes(0).seconds(0).milliseconds(0);
      },
      getMonthsAgoRange: function(data, card, months) {
        var filters = card.$deckster.options.getSelectedFiltersJSON();
        var endDate = this.getMomentFromDateString(filters.endDate);
        var monthsAgoMoment = this.getMonthsAgoMoment(filters.endDate, months);
        return moment.range(monthsAgoMoment, endDate);
      },
      getMonthStart: function (dateString, dateFormat) {
        return moment(dateString).startOf('month').format(getDefinedFormat(dateFormat));
      },
      getFiscalStartMomentFromDateString: function(dateString, dateFormat) {
        var date = this.getMomentFromDateString(dateString, dateFormat);

        return moment("04/01/" + date.fquarter().year, "MM/DD/YYYY");
      },
      getFiscalEndMomentFromDateString: function(dateString, dateFormat) {
        var date = this.getMomentFromDateString(dateString, dateFormat);

        return moment("03/01/" + date.fquarter().nextYear, "MM/DD/YYYY").endOf('month');
      },
      getGDITFiscalEndMomentFromDateString: function(dateString, dateFormat) {
        var date = this.getMomentFromDateString(dateString, dateFormat);

        return moment("12/01/" + date.fquarter().year, "MM/DD/YYYY").endOf('month');
      },

      /**
       * Prepends the months defined in the monthsRange to the data array
       * and sets the values as "N/A"
       * @param data
       * @param monthsRange
       * @param isDisplayColumn
       * @param dateColumn
       * @param primaryColumn
       */
      prependInitializedMonths: function(data, monthsRange, isDisplayColumn, dateColumn, primaryColumn) {
        var missingMonthsRows = [];
        var that = this;

        function createEmptyRow(newRow, existingMonths) {
          monthsRange.by('months', function(month) {
            newRow = existingMonths ? newRow : _.cloneDeep(newRow);
            if(!existingMonths || existingMonths.indexOf(month) === -1) {
              newRow[dateColumn] = month.toDate();

              _.each(newRow, function (value, column) {
                if (isDisplayColumn(column)) {
                  newRow[column] = "N/S";
                }
              });

              missingMonthsRows.push(newRow);
            }
          });
        }

        if(primaryColumn) {
          _.each(_.uniq(data, false, function (row) {
            return row[primaryColumn];
          }), function (uniqueRow) {
            var months = _.map(_.pluck(_.filter(data, function (monthRow) {
              return monthRow[primaryColumn] === uniqueRow[primaryColumn];
            }), dateColumn), function (month) {
              return that.getMomentFromDateString(month);
            });

            createEmptyRow(uniqueRow, months);
          });
        } else {
          createEmptyRow(_.cloneDeep(_.first(data)))
        }

        _.each(missingMonthsRows.reverse(), function(newRow) {
          data.unshift(newRow);
        });
      }
    };
  });
