'use strict';

angular.module('opsReviewApp')
  .controller('DeckSettingsCtrl', function ($scope, $uibModalInstance, deck) {
    $scope.deck = deck;

    $scope.saveDeck = function () {
      $uibModalInstance.close($scope.deck);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
