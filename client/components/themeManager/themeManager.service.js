'use strict';

angular.module('opsReviewApp')
  .factory('ThemeManager', function ($cookies, $rootScope) {
    window.InsightConfig = window.InsightConfig || {};

    var ThemeManager = {
      setTheme: function (theme) {
        window.InsightConfig.theme = theme;
        $rootScope.currentTheme = theme;
      },
      getTheme: function () {
        return 'light';
      },
      init: function () {
        ThemeManager.setTheme(ThemeManager.getTheme());
      }
    };

    return ThemeManager;
  });
