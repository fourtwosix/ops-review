'use strict';

angular.module('opsReviewApp')
  .factory('Contract', function () {
    var Contract = function (programData) {
      return this.setData(programData);
    };

    Contract.prototype.setData = function (programData) {
      return angular.extend(this, programData);
    };

    return Contract;
  });
