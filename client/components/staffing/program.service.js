'use strict';

angular.module('opsReviewApp')
  .factory('Program', function () {
    var Program = function (programData) {
      return this.setData(programData);
    };

    Program.prototype.setData = function (programData) {
      return angular.extend(this, programData);
    };

    Program.prototype.getDisplayId = function () {
      return this.project_id.toUpperCase();
    };

    return Program;
  });
