'use strict';

angular.module('opsReviewApp')
  .factory('ProgramManager', function ($http, $q, Program) {
    var observerCallbacks = [];

    return {
      currentProgram: null,

      registerProgramObserver: function (callback) {
        observerCallbacks.push(callback);
      },

      notifyProgramObservers: function (program) {
        var self = this;

        angular.forEach(observerCallbacks, function (cb) {
          cb && cb(program || self.currentProgram);
        })
      },

      setCurrentProgram: function (program) {
        if (program) {
          this.currentProgram = program;
          this.notifyProgramObservers(program);
        }
      },

      getProgram: function (projectId) {
        var deferred = $q.defer();

        $http.get('/api/programs/' + projectId).then(function (response) {
          deferred.resolve(new Program(response.data));
        }).catch(function () {
          deferred.resolve(null);
        });

        return deferred.promise;
      },

      createProgram: function (programData) {
        var deferred = $q.defer();

        $http.post('/api/programs/', programData).then(function (response) {
          deferred.resolve(new Program(response.data));
        }).catch(function (err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },

      saveProgram: function (program) {
        var deferred = $q.defer();
        $http.put('/api/programs/' + program.id, program).then(function (response) {
          deferred.resolve(response.data);
        }).catch(function (err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },

      getHeatmaps: function (programId, fiscalEnd) {
        var deferred = $q.defer();
        $http.get('/api/programs/' + programId + '/heatmaps', {
          params: {
            fiscalEnd: fiscalEnd
          }
        }).then(function (response) {
          deferred.resolve(response.data);
        }).catch(function (err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },

      getGroupValues: function() {
        var deferred = $q.defer();

        $http.get('/api/programs/groups').then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(err) {
          deferred.reject(err);
        });

        return deferred.promise;
      },

      getDivisionValues: function(groupName) {
        var deferred = $q.defer();

        $http.get('/api/programs/groups/divisions', {
          params: {
            groupName: groupName
          }
        }).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(err) {
          deferred.reject(err);
        });

        return deferred.promise;
      }

    };
  });
