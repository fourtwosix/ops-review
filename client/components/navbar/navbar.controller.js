'use strict';

angular.module('opsReviewApp')
  .controller('NavbarCtrl', function ($rootScope, $cookieStore, $scope, $location, $uibModal, $state, Auth, Deck, ThemeManager, DeckManager) {
    $scope.isCollapsed = true;
    $scope.stateName = $state.current.name;
    $scope.groups = [];

    // Wait for user to resolve
    Auth.isLoggedInAsync(function (isLoggedIn) {
      $scope.isLoggedIn = isLoggedIn;
      $scope.isAdmin = Auth.isAdmin();
      $scope.isOktaSession = Auth.isOktaSession();
      $scope.currentGroup = Auth.getCurrentDeckGroup();
      $scope.currentDeck = DeckManager.selectedDeck;
      if ($scope.currentGroup) {
        $scope.currentGroup = {
          displayName: _.startCase($scope.currentGroup.replace("-reporting", "")).toUpperCase(),
          value: $scope.currentGroup
        };
      }
      $scope.currentUser = Auth.getCurrentUser();
      $scope.groups = _.map($scope.currentUser.groups, function (group) {
        return {
          displayName: _.startCase(group.replace("-reporting", "")).toUpperCase(),
          value: group
        };
      });
    });

    $scope.goBackToMain = function () {
        $state.go('main', {}, {reload: true});
    };

    $scope.selectGroup = function (group) {
      $cookieStore.put('selectedGroup', group);
      $state.go('main', {}, {reload: true});
    };

    $scope.logout = function () {
      Auth.logout();
    };

    $scope.setTheme = function (theme) {
      ThemeManager.setTheme(theme);
      $rootScope.$broadcast('changeTheme', theme);
    };

    $scope.createDeck = function () {
      var modal = $uibModal.open({
        templateUrl: 'components/deckSettings/deckSettingsModal.html',
        controller: 'DeckSettingsCtrl',
        backdrop: 'static',
        resolve: {
          deck: function () {
            return new Deck(); // TODO this should be a Deck proto
          }
        }
      });

      modal.result.then(function(deck) {
        // TODO add new deck to decks
      });
    };

    $scope.showSendFeedbackModal = function() {
      $uibModal.open({
        backdrop : 'static',
        templateUrl: 'app/feedback/feedback.modal.html',
        controller: 'SendFeedbackCtrl',
        windowClass: 'minimal-modal',
        size: 'sm'
      });
    };

    $scope.reloadProgramEntryPage = function() {
      if ($location.path() === '/program') {
        $state.reload();
      }
    };
  });
