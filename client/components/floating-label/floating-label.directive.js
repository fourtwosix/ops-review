'use strict';

angular.module('opsReviewApp')
  .directive('floatingLabel', function ($parse) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var opts = $parse(attrs.floatingLabel || {})(scope) || {};
        element.floatinglabel(opts);
      }
    };
  });
