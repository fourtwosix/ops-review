'use strict';

angular.module('opsReviewApp')
  .factory('FilterService', function ($http, $q, Auth, DateUtils, Utils, DeckFilterManager) {
    var _cardVariables = {};
    var FilterService = {};
    var dateFormat = 'YYYY-MM-DD';


    // Deck filter defaults for each group
    var defaultSelected = {
      "nes-reporting": {
        "layer_2_org_name": {
          selected: "nps enterprise services"
        },
        "layer_2_manager": {
          selected: {
            "displayName": "John Desimone",
            "layer_2_manager": "desimone, john"
          },
          value: "desimone, john",
          readonly: true
        }
      },
      "ppmc-reporting": {
        "layer_2_org_name": {
          selected: "program & project management center"
        },
        "layer_2_manager": {
          selected: {
            "displayName": "Douglas Mcvicar/ Larry Prior",
            "layer_2_manager": "mcvicar, douglas|prior, lawrence"
          },
          value: "mcvicar, douglas|prior, lawrence",
          readonly: true
        }
      },
      "mission-services": {
        "layer_2_org_name": {
          selected: "dod mission engineering services"
        },
        "layer_2_manager": {
          selected: {
            "displayName": "Lee Phillips",
            "layer_2_manager": "phillips, lee"
          },
          value: "phillips, lee",
          readonly: true
        }
      },
      staffing: {},
      "resource-management-reporting": {},
      "recruiting-reporting": {},
      "pmr-reporting": {
        "l1": {
          selected: {
            "displayName": "GDIT",
            "l1": "GDIT"
          },
          value: "GDIT",
          readonly: false
        }
      }
    };

    FilterService.getTemplateVariables = function (cardId) {
      _cardVariables[cardId] = _cardVariables[cardId] || {};
      return _cardVariables[cardId];
    };

    FilterService.getFilterPreset = function (column) {
      var user = Auth.getCurrentUser();
      var group = Auth.getCurrentDeckGroup();
      var nameFormatter = Utils.dataFormatter.name;
      var value = null;
      var cookieFilter = DeckFilterManager.getFilter('insight-deck-filter');

      function createFilter (column, value) {
        var retValue = {
          selected: {
            "displayName": nameFormatter(value)
          },
          value: value
        };

        retValue.selected[column] = value;
        return retValue;
      }

      // If there is already a cookie set override the other values
      if (cookieFilter && angular.isDefined(cookieFilter[column])) {
        value = createFilter(column, cookieFilter[column]);
      } else if (group) {
        value = _.get(defaultSelected, [group, column]);

        // Check if they have orgInfo and the layer_2_org_name matches
        // TODO layer_2_org_name check may need to get changed
        if (user.orgInfo && user.orgInfo[column] &&
          user.orgInfo.layer_2_org_name === defaultSelected[group].layer_2_org_name.selected) {
          value = createFilter(column, user.orgInfo[column]);
        }
      }

      return value;
    };

    FilterService.setSelectedFilter = function (card, filterKeyName) {
      var viewOptions = card.options.getCurrentViewOptions(card.currentSection);

      //if(card.currentSection === "summary" && viewOptions === card.options[card.currentSection + "ViewOptions"].views.daysOpenByProgramColumnRange) {
      var templateVariables = card.options.getTemplateVariables();
      //Sets selected filter
      if (_.isUndefined(viewOptions.controls.filterMenu)) {
        viewOptions.controls.selectedFilter = templateVariables[filterKeyName];
      } else {
        var categories = viewOptions.controls.filterMenu.categories;
        _.forEach(categories, function (category) {
          _.forEach(category.filters, function (filter) {
            if (filter.clauses[filterKeyName] === templateVariables[filterKeyName]) {
              viewOptions.controls.selectedFilter = filter.name;
            }
          });
        });
        //}
      }
    };

    var _query = function (queryParams) {
      var deferred = $q.defer();

      $http.post('/api/data/query/', queryParams).then(function (response) {
        deferred.resolve(response.data);
      }, function (error) {
        deferred.reject(error);
      });
      return deferred.promise;
    };


    var getMatchingFilters = function (deck, column, value, matchColumn, table, filterName) {
      if(!filterName) {
        filterName = matchColumn;
      }
      var query = {json: {expression: "SELECT COUNT(" + matchColumn + ") AS total, " + matchColumn + " FROM " + table + " WHERE " + column + " = " + "$$$" + value + "$$$" + " GROUP BY " + matchColumn + " ORDER BY total DESC" + ";"}};

      var updateFilters = function (match) {
        _.forEach(deck.filters, function (filter) {
          if (filter.name === filterName || 'l1' === filterName) {
            filter.selectedFilter = {};
            filter.selectedFilter[filter.nameColumn] = match;

            if (filter.displayNameFormatter && _.has(Utils.dataFormatter, filter.displayNameFormatter)) {
              filter.selectedFilter.displayName = Utils.dataFormatter[filter.displayNameFormatter](match);
            } else {
              filter.selectedFilter.displayName = filter.displayName;
            }

            deck.selectedFilters = deck.selectedFilters || {};
            deck.selectedFilters[filterName] = match;
          }
        });

        var storeFiltersInCookies = function (selectedFilters) {
          var filterObj = {};

          _.forOwn(selectedFilters, function(value, key) {
            // key is the "name" of the filter, value is the filter value
            var deckFilter = _.find(deck.filters, function(filter) {
              return filter.name === key;
            });
            if (deckFilter) {
              filterObj[deckFilter.name] = value;
            }
          });

          DeckFilterManager.setFilter(filterObj);
        };

        storeFiltersInCookies(deck.selectedFilters);
      };

      _query(query).then(function (data) {
        if (data.length >= 1) {
          updateFilters(data[0][matchColumn]);
        }
      })
    };

    FilterService.setMatchingFilters = function (deck, key, value) {
      if (key === 'layer_3_manager') {
        getMatchingFilters(deck, "layer_3_manager", value, "layer_3_org_name", "permast_l3_managers", "l3_portfolio");
      } else if (key === 'l2_manager') {
        getMatchingFilters(deck, "l2_manager", value, "group_name", "pmr_groups", 'group_name');
      } else if (key === 'group_name') {
        getMatchingFilters(deck, "group_name", value, "l2_manager", "pmr_groups", 'l2_manager');
      } else if (key === 'l3_manager') {
        getMatchingFilters(deck, "l3_manager", value, "division_name", "pmr_groups_divisions", 'division_name');
      } else if (key === 'division_name') {
        getMatchingFilters(deck, "division_name", value, "l3_manager", "pmr_groups_divisions", 'l3_manager');
      } else if (key === 'l3_portfolio') {
        getMatchingFilters(deck, "layer_3_org_name", value, "layer_3_manager", "permast_l3_managers");
      } else if (key === 'l4_manager') {
        getMatchingFilters(deck, "l4_manager", value, "l4_manager", "program_metadata_manual_entry");
      } else if (key === 'pra') {
        getMatchingFilters(deck, "pra", value, "pra", "program_metadata_manual_entry");
      }
    };

    FilterService.createFilteredQuery = function (card, filters, options) {

      options = _.defaults({}, options, {reloadContent: true, deckFiltered: false});

      var layer_2_manager = _.get(FilterService.getFilterPreset('layer_2_manager'), 'value', null);

      // Only use the overrideVariables if you explicitly want that variable to always override the default value.
      var overrideVariables = card.options.getTemplateVariables();
      // set template variables to default values (fiscalYear dates may need to be pulled from DB at some point)
      var defaults = {
        subManagerType: 'layer_3_manager',
        managerType: 'layer_2_manager',
        managerClause: layer_2_manager ? "layer_2_manager ~ '" + layer_2_manager + "'" : 'true',
        startDate: _.get(overrideVariables, 'startDate', DateUtils.getFormattedDateFromDateString(card.$deckster.options.startDate)),
        firstFridayOfStartMonth: _.get(overrideVariables, 'firstFridayOfStartMonth', DateUtils.getFirstFridayFromDateString(card.$deckster.options.startDate, dateFormat)),
        endDate: _.get(overrideVariables, 'endDate', DateUtils.getFormattedDateFromDateString(card.$deckster.options.endDate)),
        fiscalYearStart: DateUtils.getFormattedDateFromDateString("4/4/2015"),
        fiscalYearEnd: DateUtils.getFormattedDateFromDateString("4/6/2016"),
        staffingLevelClause: "type = 'total'",
        activeClause: _.get(overrideVariables, 'activeClause', "employment_status = 'active'"),
        staffingCompositionClause: "los NOTNULL",
        losClause: "true",
        l3ManagerClause: "true",
        divisionClause: "true",
        l2ManagerClause: "true",
        l4ManagerClause: "true",
        groupClause: "true",
        staffingCompositionNameCol: "los",
        binClause: _.get(overrideVariables, 'binClause', 10),
        laborType: _.get(overrideVariables, 'laborType', 'true'),
        rangeClause: _.get(overrideVariables, 'rangeClause', 'true'),
        reqTypeClause: _.get(overrideVariables, 'reqTypeClause', "req_type = 'funded'"),
        programOrResourceColumn: "staffing_source",
        programClause: "true",
        programManagerClause: "true",
        praClause: "true",
        staffingSourceClause: "true",
        candidateStep: _.get(overrideVariables, 'candidateStep', 'true'),
        candidateRollupStatus: _.get(overrideVariables, 'candidateRollupStatus', 'true'),
        candidateReqType: _.get(overrideVariables, 'candidateReqType', 'true'),
        clientProgramClause: "true",
        clientProgramClause_resourceManagementData: "true",
        hiringManagerClause: "true",
        recruiterClause: "true",
        requisitionNumberClause: "true",
        reqAcceptedClause: "TRUE",
        reqAcceptedClause_candidateData: "TRUE",
        reqType: "",
        lhwabmAverage: 0.0,
        cardFilter_program: _.get(overrideVariables, 'cardFilter_program', 'true'),
        cardFilter_role: _.get(overrideVariables, 'cardFilter_role', 'true'),
        cardFilter_pagingClause: _.get(overrideVariables, 'cardFilter_pagingClause', ''),
        cardFilter_pagingRecTotal: _.get(overrideVariables, 'cardFilter_pagingRecTotal', 0),
        kpiClause: "true",
        removeCasualEmployeesClause: "TRUE",
        removeCasualEmployeesClauseHCS: " \"Head Count\", \"Hires\", \"Voluntary Terminations\", \"Involuntary Terminations\" ",
        removeCasualEmployeesClauseHeadCount: " head_count ",
        removeCasualEmployeesClauseHires : "hires",
        removeCasualEmployeesClauseInvolTerms : "involuntary_terminations",
        removeCasualEmployeesClauseVolTerms : "voluntary_terminations",
        termTypeClause: _.get(overrideVariables, 'termTypeClause', "all"),
        requisitionVariableClause: "CASE WHEN csc_title ~ '.*:' THEN trim(split_part(csc_title, ':', 2)) ELSE csc_title END AS role, ",
        requisitionVariableGroupByClause: "role ",
        drillDownReqVariableClause: "trim(split_part(csc_title, ':', 2))",
        cardFilter_variable: _.get(overrideVariables, 'cardFilter_variable', 'csc_title'),
        showAllCSRA: false
      };

      var templateVariables = _.cloneDeep(defaults);

      // change template variables for filters that have been applied
      _.forOwn(filters, function (value, key) {
        if (key.match(/layer_\d_manager/)) {
          templateVariables.subManagerType = key.replace(/\d+/, function (n) {
            return ++n;
          });
          templateVariables.managerType = key;
          templateVariables.managerClause = key + " ~ '" + value + "'";
          if (key === 'layer_3_manager') {
            templateVariables.l3ManagerClause = "permast_l3_managers.layer_3_manager = '" + value + "'";
          }
        } else if (key === 'date_range') {
          templateVariables.startDate = DateUtils.getFormattedDateFromDateString(value.start);
          templateVariables.firstFridayOfStartMonth = DateUtils.getFirstFridayFromDateString(value.start, dateFormat);
          templateVariables.endDate = DateUtils.getFormattedDateFromDateString(value.end);
        } else if (key === 'l3_portfolio') {
          templateVariables.losClause = "program_metadata_manual_entry.l3_portfolio = '" + value + "'";
          templateVariables.staffingCompositionClause = value;
        } else if (key === 'program') {
          templateVariables.programOrResourceColumn = "staffing_source";
        } else if (key === 'program_manager') {
          templateVariables.programManagerClause = "program_metadata_manual_entry.program_manager = '" + value + "'";
        } else if (key === 'l4_manager') {
          templateVariables.l4ManagerClause = "program_metadata_manual_entry.l4_manager = '" + value + "'";
        // } else if (key === 'pra') {
        //   templateVariables.praClause = "program_metadata_manual_entry.pra = '" + value + "'";
        } else if (key === 'project_id') {
          // SMELL: stinks; ought to use new variable name that  is specific to PMR.
          if (card.options.id === "programSummaryKpi" ||
              card.options.id === "programCyberKpi" ||
            card.options.id === "programsByStatus"    ||
            card.options.id === 'programMetadata'     ||
            card.options.id === 'programQuads') {
            templateVariables.programClause = "programs_secure.project_id = '" + value + "'";
          } else {
            templateVariables.programClause = "program = '" + value + "'";
            templateVariables.staffingCompositionClause = value;
          }
        } else if (key === 'staffing_source') {
          templateVariables.programOrResourceColumn = "program";
          templateVariables.staffingSourceClause = "staffing_source = '" + value + "'";
        } else if (key === 'client_program_name') {
          templateVariables.clientProgramClause = "client_program_name = '" + value + "'";
          templateVariables.clientProgramClause_resourceManagementData = "client = '" + value + "'";
        } else if (key === 'req_hiring_manager_name') {
          templateVariables.hiringManagerClause = "req_hiring_manager_name = '" + value + "'";
        } else if (key === 'req_recruiter_name') {
          templateVariables.recruiterClause = "req_recruiter_name = '" + value + "'";
        } else if (key === 'requisition_number') {
          templateVariables.requisitionNumberClause = "requisition_number = '" + value + "'";
        } else if (key === 'req_accepted') {
          if (value) {
            templateVariables.reqAcceptedClause = 'has_accepted = FALSE';
            templateVariables.reqAcceptedClause_candidateData = "application_cws_status != 'onboarding process initiated - waiting on new hire to start'";
          }
        } else if (key === 'remove_casual_employees') {
          if (value) {
            templateVariables.removeCasualEmployeesClause = "employee_group != 'casual'";
            templateVariables.removeCasualEmployeesClauseHCS = " \"Head Count Of Non-Casual Employees\", \"Hires of Non-Casual Employees\", \"Voluntary Terminations of Non-Casual Employees\", \"Involuntary Terminations of Non-Casual Employees\" ";
            templateVariables.removeCasualEmployeesClauseHeadCount = " head_count_non_casual ";
            templateVariables.removeCasualEmployeesClauseHires = "hires_non_casual";
            templateVariables.removeCasualEmployeesClauseInvolTerms = "involuntary_terminations_non_casual";
            templateVariables.removeCasualEmployeesClauseVolTerms = "voluntary_terminations_non_casual";
          }
        } else if (key === 'pra') {
          var tableAlias = '';
          if (card.options.id === "programSummaryKpi" ||   card.options.id === "programCyberKpi") {
            tableAlias = 'program_metadata_manual_entry.';
          }
          templateVariables.praClause = tableAlias + "pra = '" + value + "'";
        } else if (key === 'l1') {
          templateVariables.showAllCSRA = value === 'GDIT';
        } else {
          templateVariables[key] = value;
        }
      });

      //Construct viewOptions array for all views and drilldowns
      var viewOptions = [];

      //Required for drilldown card translation
      var viewTypes = ["summary", "details"];
      _.forEach(viewTypes, function (viewType) {
        var viewOption = card.options[viewType + "ViewOptions"];
        if (card.options[viewType + "ViewType"] === "drilldownView") {
          _.forEach(viewOption.views, function (value, key) {
            viewOptions.push(viewOption.views[key]);
          });
        } else {
          viewOptions.push(viewOption);
        }
      });

      // inject template variables into queryTemplate to set query
      _.forEach(viewOptions, function (viewOption) {
        if (viewOption && viewOption.queryTemplate) {
          viewOption.query = {json: {expression: _.template(viewOption.queryTemplate)(templateVariables)}};
        }
      });

      // Set the templateVariables to the _cardVariables JSON mapping.
      _cardVariables[card.options.id] = templateVariables;

      card.options.afterFiltersApplied && card.options.afterFiltersApplied(card, filters, options.deckFiltered);

      if (options.reloadContent) {
        card.reloadContent();
      }
    };

    return FilterService;
  });
