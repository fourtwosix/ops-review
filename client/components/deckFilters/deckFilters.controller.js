'use strict';

angular.module('opsReviewApp')
  .controller('DeckFiltersCtrl', function ($scope, Auth,  $window, $uibModalInstance, deck, FilterService, DeckFilterManager, DeckManager) {
    $scope.deck = deck;
    $scope.adminRole = false;
    $scope.searches = {};
    $scope.list_projects = null;
    var currentUser  = Auth.getCurrentUser();
    _.forEach(currentUser.roles, function(role){
      if(role.name == "Administrator") $scope.adminRole = true;
    });
    _.forOwn($scope.deck.selectedFilters, function(value, key) {
      // key is the "name" of the filter, value is the filter value
      var deckFilter = _.find($scope.deck.filters, function(filter) {
        return filter.name === key;
      });
      if (deckFilter) {
        deckFilter.selectedFilter = {
          displayName: value
        };
        deckFilter.selectedFilter[deckFilter.nameColumn] = value;
      }
    });

    angular.element($window).on('keydown', (function(event) {
      if (event.which === 13) {
        $scope.applyFilters();
      }
    }));

    $scope.isReadOnly = function (column) {
      return _.get(FilterService.getFilterPreset(column), 'readonly',  false);
    };

    $scope.getPlaceholderText = function (column, filterGroup) {
      return $scope.disableAutocomplete(filterGroup) ? null : "Search for a " + $scope.deck.getFilterName(column.titleName);
    };

    $scope.clearSelectedDependingFilters = function(filterGroup) {
      // remove the depending filters
      _.forEach(filterGroup.dependedByFilters, function(dependedByFilter) {
        var filter = _.find($scope.deck.filters, {nameColumn: dependedByFilter});
        if (filter) {
          filter.selectedFilter = null;
          deleteSelectedDeckFilter(filter.name);
        }
      });
    };

    function deleteSelectedDeckFilter(name) {
      delete $scope.deck.selectedFilters[name];
    }

    $scope.filterSelected = function(item, filter) {
      $scope.deck.selectedFilters = $scope.deck.selectedFilters || {};
      if (item) {
        $scope.deck.selectedFilters[filter.name] = item.displayName; // use same format as in deck.service.js?
      } else {
        deleteSelectedDeckFilter([filter.name]);
      }

      $scope.clearSelectedDependingFilters(filter);

      if (filter.selectedFilter) {
        FilterService.setMatchingFilters($scope.deck, filter.name, filter.selectedFilter[filter.valueColumn]);
      }
      _.forEach($scope.deck.filters, function(deckFilter) {
        if (!_.isEmpty(deckFilter.dependsOnFilters)) {
          $scope.getAutocompleteFilters(deckFilter.autocomplete.apiUrl, deckFilter, '');
        }
      });
    };

    function exceedsMaxNumFilters() {
      if ($scope.deck.maxNumFilters) {
        // Count the number of filters applied
        var total = _.sum(_.map($scope.deck.filters, function (filter) {
          var numSelectedCheckboxes = 0;

          if (filter.checkboxes) {
            numSelectedCheckboxes = _.filter(filter.checkboxes, function (option) {
              return option.selected;
            }).length;
          }

          return (filter.selectedFilter ? 1 : 0) + numSelectedCheckboxes;
        }));

        return total >= $scope.deck.maxNumFilters;
      } else {
        return false;
      }
    }

    function dependingFiltersNotSelected(filterGroup) {
      return filterGroup.dependsOnFilters && _.isEmpty($scope.deck.getSelectedDependingFilters(filterGroup));
    }

    $scope.disableAutocomplete = function (filterGroup) {
      return exceedsMaxNumFilters() ||
        (filterGroup && dependingFiltersNotSelected(filterGroup));
    };

    $scope.getAutocompleteFilters = function (apiUrl, filter, query) {
      if (filter.selections) {
        $scope.searches[filter.name] = filter.selections;
      } else {
        var dependsOn = angular.toJson($scope.deck.getSelectedDependingFilters(filter));
        DeckManager.getFilterAutocomplete(apiUrl, filter, query, dependsOn, filter.selections).then(function (items){
        if(!$scope.adminRole){
          var temp_item = [];
          if(filter.name == "group_name"){
            for(var i = 0; i < currentUser.programs.length; i++){
              _.forEach(items, function(program){
                if(program.group_name == currentUser.programs[i].pmr_group){
                  temp_item.push({group_name: currentUser.programs[i].pmr_group, displayName: currentUser.programs[i].pmr_group});
                }
              });
            }
            if(currentUser.pmr_group !== null && currentUser.pmr_group.length > 0){
              for(var i = 0; i< currentUser.pmr_group.length; i++){
                for(var j = 0; j < items.length; j++){
                  if(currentUser.pmr_group[i] == items[j].group_name){
                      temp_item.push(items[j]);
                  }
                }
              }
            }
            temp_item = _.unique(temp_item, 'group_name');
            $scope.searches[filter.name] = temp_item;
          }
          //#######################################################################

          else if( filter.name == "division_name"){
            var temp_item = [];
            for(var i = 0; i < currentUser.programs.length; i++){
                _.forEach(items, function(program){
                  if(program.division_name == currentUser.programs[i].division){
                    temp_item.push({division_name: currentUser.programs[i].division, displayName: currentUser.programs[i].division});
                  }
              });
            }
            if(currentUser.divisions !== null && currentUser.divisions.length > 0){
              for(var i = 0; i< currentUser.divisions.length; i++){
                for(var j = 0; j < items.length; j++){
                  if(currentUser.divisions[i] == items[j].division_name){
                    temp_item.push(items[j]);
                  }
                }
              }
            }
            temp_item = _.unique(temp_item,'division_name');
            $scope.searches[filter.name] = temp_item;
          }
          //#######################################################################

          else if(filter.name == "l2_manager"){
            var temp_items = [];

            for(var i = 0; i < $scope.searches.group_name.length; i++){
              if($scope.searches.group_name[i].group_name == "Defense"){
                temp_items.push({l2_manager: "Leigh Palmer", displayName: "Leigh Palmer"});
              }
              else if($scope.searches.group_name[i].group_name == "National Security"){
                temp_items.push({l2_manager: "Leigh Palmer", displayName: "Leigh Palmer"});
              }
              else if($scope.searches.group_name[i].group_name == "Federal Civilian"){
                temp_items.push({l2_manager: "Paul Nedzbala", displayName: "Paul Nedzbala"});
              }
              else if($scope.searches.group_name[i].group_name == "Intelligence and Homeland Security"){
                temp_items.push({l2_manager: "Bernie Guerry", displayName: "Bernie Guerry"});
              }
            }
            if(currentUser.pmr_group !== null && currentUser.pmr_group.length >0){
              for(var i = 0; i< currentUser.pmr_group.length; i++){
                if(currentUser.pmr_group[i] == "Defense"){
                  temp_items.push({l2_manager: "Leigh Palmer", displayName: "Leigh Palmer"});
                }
                else if(currentUser.pmr_group[i] == "National Security"){
                  temp_items.push({l2_manager: "Leigh Palmer", displayName: "Leigh Palmer"});
                }
                else if(currentUser.pmr_group[i] == "Federal Civilian"){
                  temp_items.push({l2_manager: "Paul Nedzbala", displayName: "Paul Nedzbala"});
                }
                else if(currentUser.pmr_group[i] == "Intelligence and Homeland Security"){
                  temp_items.push({l2_manager: "Bernie Guerry", displayName: "Bernie Guerry"});
                }
              }
            }
            temp_items = _.unique(temp_items,'l2_manager');
            $scope.searches[filter.name] = temp_items;
          }

          //######################################################################
          else if(filter.name == "l3_manager"){
            var temp_item = [];
            _.forEach($scope.searches.division_name, function(division){
              _.forEach(items, function(l3_manager){
                if(l3_manager.division_name == division.division_name){
                  temp_item.push(l3_manager);
                }
              });
            });
            if(currentUser.divisions !== null && currentUser.divisions.length > 0){
              for(var i = 0; i< currentUser.divisions.length; i++){
                for(var j = 0; j < items.length; j++){
                  if(currentUser.divisions[i] == items[j].division_name){
                      temp_item.push(items[j]);
                  }
                }
              }
            }
            temp_item = _.unique(temp_item,'division_name');
            $scope.searches[filter.name] = temp_item;
          }

          //#######################################################################
          else if(filter.name == "project_id"){
              var temp_item = [];
              var temp_projects = [];
              if(currentUser.divisions !== null && currentUser.divisions.length > 0){
                for(var i = 0; i< currentUser.pmr_group.length; i++){
                  for(var j = 0; j < items.length; j++){
                    if(currentUser.pmr_group[i] == items[j].pmr_group){
                        temp_item.push(items[j]);
                    }
                  }
                }
                for(var i = 0; i< currentUser.divisions.length; i++){
                  for(var j = 0; j < temp_item.length; j++){
                    if(currentUser.divisions[i] == temp_item[j].division){
                        temp_projects.push(temp_item[j]);
                    }
                  }
                }
              }
              _.each(items, function(item){
                for(var i = 0; i < currentUser.programs.length; i++){
                  if(item.project_id == currentUser.programs[i].project_id){
                    temp_projects.push(item);
                  }
                }
                });
              if(query !== ""){
                var queryResult= [];
                _.forEach(temp_projects, function(item){
                  if(item.project_id.toLowerCase().includes(query.toLowerCase())){
                    queryResult.push(item);
                  }
                });
                temp_projects = queryResult;
              }
            temp_projects = _.unique(temp_projects,'project_id');
            $scope.list_projects = temp_projects;
            $scope.searches[filter.name] = temp_projects;
          }
          //#######################################################################
          else if(filter.name == "l4_manager"){
            var l4_manager = [];
            if(currentUser.divisions !== null && currentUser.divisions.length > 0){
              _.each(items,function(item){
                _.each(currentUser.divisions, function(division){
                  if(item.division == division){
                    l4_manager.push(item);
                  }
                });
              });
            }
            _.each(items, function(item){
              for(var i = 0; i < currentUser.programs.length; i++){
                if(item.program_id == currentUser.programs[i].id){
                  l4_manager.push(item);
                }
              }
            });


            if(query !== ""){
              var queryResult= [];
              _.forEach(l4_manager, function(item){
                if(item.l4_manager.includes(query)){
                  queryResult.push(item);
                }
              });
              l4_manager = queryResult;
            }
          l4_manager = _.unique(l4_manager, 'l4_manager');
          $scope.searches[filter.name] = l4_manager;
          }
        //#######################################################################
        else if(filter.name == "program_manager"){
          var program_manager = [];
          if(currentUser.divisions !== null && currentUser.divisions.length > 0){
            _.each(items,function(item){
              _.each(currentUser.divisions, function(division){
                if(item.division == division){
                  program_manager.push(item);
                }
              });
            });
          }
          _.each(items, function(item){
            for(var i = 0; i < currentUser.programs.length; i++){
              if(item.program_id == currentUser.programs[i].id){
                program_manager.push(item);
              }
            }
          });


          if(query !== ""){
            var queryResult= [];
            _.forEach(program_manager, function(item){
              if(item.program_manager.includes(query)){
                queryResult.push(item);
              }
            });
            program_manager = queryResult;
          }
        program_manager = _.unique(program_manager, 'program_manager');
        $scope.searches[filter.name] = program_manager;
        }
        //#######################################################################
        else if(filter.name == "pra"){
          var pra = [];
          if(currentUser.divisions !== null && currentUser.divisions.length > 0){
            _.each(items,function(item){
              _.each(currentUser.divisions, function(division){
                if(item.division == division){
                  pra.push(item);
                }
              });
            });
          }
          _.each(items, function(item){
            for(var i = 0; i < currentUser.programs.length; i++){
              if(item.program_id == currentUser.programs[i].id){
                pra.push(item);
              }
            }
          });


          if(query !== ""){
            var queryResult= [];
            _.forEach(pra, function(item){
              if(item.pra.includes(query)){
                queryResult.push(item);
              }
            });
            pra = queryResult;
          }
        pra = _.unique(pra, 'pra');
        $scope.searches[filter.name] = pra;
        }

          else {
            $scope.searches[filter.name] = items;
          }
        }
        else {
          $scope.searches[filter.name] = items;

        }
        });
      }
    };
    $scope.applyFilters = function () {
      var filters = {};
      _.forEach($scope.deck.filters, function (filter) {
        if (filter.checkboxes) {
          // TODO shouldn't be checkboxes should just be a checkbox per filter
          _.forEach(filter.checkboxes, function (checkbox) {
            if (checkbox.selected) {
              filters[filter.name] = checkbox.selected;
            }
          });
        } else if (filter.selectedFilter) {
          filters[filter.name] = filter.valueColumn ? filter.selectedFilter[filter.valueColumn] : filter.selectedFilter;
        }
      });
      //setting filters in cookies
      DeckFilterManager.setFilter(filters);
      $uibModalInstance.close({filters: filters, deckFilters: $scope.deck.filters});
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
