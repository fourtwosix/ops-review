/**
 * Created by cedam on 1/13/16.
 */

'use strict';

angular.module('opsReviewApp')
  .factory('DeckFilterManager', function ($cookies, $rootScope, DateUtils) {
    var DeckFilterManager = {
      setFilter: function (filter) {
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 180);
        $cookies.putObject('insight-deck-filter', filter, {'expires': expireDate});
        $rootScope.currentFilter = filter;
      },
      getFilter: function () {
        return $cookies.getObject('insight-deck-filter') || null;
      },
      setDateFilter: function (filter) {
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 180);
        $cookies.putObject('insight-deck-date-filter', filter, {'expires': expireDate});
        $rootScope.currentFilter = filter;
      },
      getDateFilter: function () {
        var filter = $cookies.getObject('insight-deck-date-filter') || null;
        var changed = false;
        if(filter) {
          _.each(filter, function(date, i) {
            if(date && date.indexOf('/') !== -1) {
              changed = true;
              filter[i] = moment(date, "MM/DD/YYYY").format(DateUtils.defaultDateFormat);
            }
          });
          if(changed) {
            this.setDateFilter(filter);
          }
        }
        return filter;
      },
      init: function () {
        DeckFilterManager.setFilter(DeckFilterManager.getFilter());
        DeckFilterManager.setDateFilter(DeckFilterManager.getDateFilter());
      }
    };
    return DeckFilterManager;
  });
