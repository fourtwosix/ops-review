'use strict';

angular.module('opsReviewApp')
  .directive('sidebar', function ($timeout, $window, $rootScope, Auth) {
    return {
      templateUrl: 'components/sidebar/sidebar.html',
      transclude: true,
      replace: true,
      restrict: 'E',
      scope: {
        collapsed: "=",
        isDisabled: "=?",
        position: "@"
      },
      link: function (scope) {
        var window = $($window);

        scope.user = Auth.getCurrentUser();

        scope.collapsed = scope.collapsed ? scope.collapsed : false;
        var collapsedFromToggle = false;

        var resizeDeck = function () {
            $rootScope.$broadcast('deckster:resize');
        };

        scope.toggle = function () {
          scope.collapsed = !scope.collapsed;
          collapsedFromToggle = !collapsedFromToggle;
          $timeout(resizeDeck, 250);
        };

        var checkSidebarWidth = function () {
          if (window.width() < 1100 && !scope.collapsed && !collapsedFromToggle) {
            scope.$apply(function() {
              scope.collapsed = true;
              resizeDeck();
            });
          } else if (window.width() >= 1100 && scope.collapsed && !collapsedFromToggle && scope.isDisabled !== true) {
            scope.$apply(function() {
              scope.collapsed = false;
              resizeDeck();
            });
          }
        };

        $timeout(checkSidebarWidth);
        window.on('resize.sidebar', _.debounce(checkSidebarWidth, 100));
      }
    };
  });
