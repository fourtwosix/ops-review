angular.module('opsReviewApp')
  .controller('DeckCommentsCtrl', function ($scope, DeckManager, $rootScope, toastr, Auth, $timeout) {
    var deck = DeckManager.selectedDeck;
    $scope.comments = {};
    $scope.comments.comment = null;
    $scope.comments.thread = [];
    var programId = null;
    var fiscalMonth = null;
    $scope.user = Auth.getCurrentUser();
    $scope.isAdmin = Auth.isAdmin();
    var wasCollapsed = false;
    var wasDisabled;
    $rootScope.rSidebarCollapsed = true;

    $scope.$on('deck-selected', function (oldVal, newVal) {
      init(newVal);
    });

    $scope.$on('deck:filtered', function () {
      init(deck);
    });

    $scope.$on('$destroy', function () {
      $rootScope.rSidebarCollapsed = true;
      $timeout(function () {
        $rootScope.$broadcast('deckster:resize')
      }, 500);
    });

    function loadComments() {
      DeckManager.getDeckComments(deck.id, programId, fiscalMonth).then(function (comments) {
        $scope.comments.thread = comments;
        $rootScope.deckCommentsLength = comments.length;
      });
    };

    $scope.$watch('$root.rSidebarCollapsed', function(newVal, oldVal) {
      if($rootScope.enableComments) {
        wasCollapsed = newVal;
      }
    });

    function init(selectedDeck) {
      $timeout(function () {
        deck = selectedDeck;
        programId = _.get(deck, ['selectedFilters', 'project_id'], null);
        fiscalMonth = _.get(deck, 'endDate', null);

        $rootScope.enableComments = _.get(deck, "enableComments", false) && !_.isNull(programId);

        if (!$rootScope.enableComments) {
          $rootScope.rSidebarCollapsed = true;
        } else {
          if (!_.isUndefined(wasDisabled)) {
            $rootScope.rSidebarCollapsed = wasCollapsed;
          } else {
            $rootScope.rSidebarCollapsed = false;
          }

          loadComments();
        }

        $timeout(function () {
          $rootScope.$broadcast('deckster:resize')
        }, 500);

        wasDisabled = !$rootScope.enableComments;
      });
    };

    $scope.saveComment = function (comment) {
      DeckManager.saveDeckComment(deck.id, programId, fiscalMonth, comment).then(function (comments) {
        $scope.comments.thread = comments;
        $scope.comments.comment = null;
        $rootScope.deckCommentsLength = comments.length;
        toastr.success("Comment successfully saved");
      }).catch(function (err) {
        toastr.error(err);
      });
    };

    $scope.deleteComment = function (comment) {
      DeckManager.deleteDeckComment(comment.id).then(function () {
        _.remove($scope.comments.thread, {id: comment.id});
        $rootScope.deckCommentsLength--;
        toastr.success("Comment successfully deleted");
      }).catch(function (err) {
        toastr.error(err);
      });
    }
  });
