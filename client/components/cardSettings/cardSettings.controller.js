'use strict';

angular.module('opsReviewApp')
  .controller('CardSettingsCtrl', function ($scope, $uibModalInstance, card) {
    $scope.card = card;

    $scope.filters = {cardsFilter: ''};

    $scope.saveCard = function () {
      $uibModalInstance.close($scope.card);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $scope.fields = [
      {name: 'Job Title'},
      {name: 'Candidate ID'},
      {name: 'Application CSW Step'},
      {name: 'Client/Program Name'},
      {name: 'Job Code'},
      {name: 'Work State'},
      {name: 'Word City'}
    ];

    $scope.cards = [
      {
        displayName: 'Bar Chart',
        type: 'barChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-10.png'
      },
      {
        displayName: 'Stacked Bar Chart',
        type: 'stackedBarChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-11.png'
      },
      {
        displayName: 'Grouped Bar Chart',
        type: 'groupedBarChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-6.png'
      },
      {
        displayName: 'Column Chart',
        type: 'columnChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-4.png'
      },
      {
        displayName: 'Stacked Column Chart',
        type: 'stackedColumnChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-7.png'
      },
      {
        displayName: 'Grouped Column Chart',
        type: 'groupedColumnChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-5.png'
      },
      {
        displayName: 'Line And Column Chart',
        type: 'lineColumnChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-14.png'
      },
      {
        displayName: 'Line Chart',
        type: 'lineChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-16.png'
      },
      {
        displayName: 'Spline Chart',
        type: 'splineChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-15.png'
      },
      {
        displayName: 'Area Chart',
        type: 'areaChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-17.png'
      },
      {
        displayName: 'Pie Chart',
        type: 'pieChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-19.png'
      },
      {
        displayName: 'Donut Chart',
        type: 'donutChart',
        thumbSrc: 'assets/images/charts/charts-no-axis-18.png'
      }
    ];

    $scope.tabs = [
      {
        name: 'Card Info',
        onClick: function () {
          $scope.changeTab(this);
        },
        validate: function () {
          return true; // TODO replace stub code
        }
      },
      {
        name: 'Card Data',
        onClick: function () {
          $scope.changeTab(this);
        },
        validate: function () {
          return true; // TODO replace stub code
        }
      },
      {
        name: 'Summary View',
        onClick: function () {
          $scope.changeTab(this);
        },
        validate: function () {
          return true;  // TODO replace stub code
        }
      },
      {
        name: 'Details View',
        onClick: function () {
          $scope.changeTab(this);
        },
        validate: function () {
          return true;  // TODO replace stub code
        }
      }
    ];

    $scope.loadFields = function (query) {
      return _.reject($scope.fields || [], function(field) {
        return !_.contains(field.name.toLowerCase(), query.toLowerCase());
      });
    };

    // Wizard functionality
    $scope.currentTab = $scope.tabs[0];
    $scope.currentIndex = 1;

    $scope.changeTab = function (tab) {
      var index = _.findIndex($scope.tabs, tab);
      if($scope.currentTab.validate()) {
        $scope.currentTab = tab;
        $scope.currentIndex =  index + 1;
      }
    };

    $scope.next = function() {
      var tab = $scope.currentTab;
      var index = _.findIndex($scope.tabs, tab);
      if(tab.validate() && $scope.tabs.length > index + 1) {
        $scope.currentTab = $scope.tabs[index + 1];
        $scope.currentIndex =  index + 2;
      }
    };

    $scope.isLastStep = function () {
      var index = _.findIndex($scope.tabs,  $scope.currentTab);
      return index === $scope.tabs.length - 1;
    };

    $scope.selectType = function(type) {
      $scope.card.type = type; // TODO check with deckster card options
      $scope.next();
    };
  });
