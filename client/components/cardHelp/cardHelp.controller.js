'use strict';

angular.module('opsReviewApp')
  .controller('CardHelpCtrl', function ($scope, $uibModalInstance, $timeout, card) {
    $scope.card = card;

    var cardOptions = card.options;

    var viewOptions = cardOptions.getCurrentViewOptions(card.currentSection);

    var helpContent = [];
    if(!_.isUndefined(cardOptions.help)) {
      helpContent = cardOptions.help;
    } else {
      helpContent.push('components/cards/helps/noHelp.html');
    }

    $timeout(function() {
      $scope.startIndex = 0;
      // If the helpIndex is defined on the view, then it takes precedence
      if(!_.isUndefined(viewOptions.helpIndex)) {
        $scope.startIndex = viewOptions.helpIndex;
      }
      // Otherwise, search the array to see if there is a help defined for this cardName_viewName
      else {
        var section = card.currentSection;
        var view = section; //cardOptions[section + 'ViewOptions'];
        if (cardOptions.getCurrentViewType(section) === "drilldownView") {
          view = cardOptions[section + 'ViewOptions'].activeView;
        }
        var helpName = cardOptions.cardName + "_" + view;

        var helpIndex = _.findIndex(helpContent, function(helpFile) {
          return _.includes(helpFile.toLowerCase(), helpName.toLowerCase());
        });
        if(helpIndex >= 0) {
          $scope.startIndex = helpIndex;
        }
      }
      $scope.helpContent = helpContent;
    });

    $scope.close = function () {
      $uibModalInstance.dismiss('close');
    };
  });
