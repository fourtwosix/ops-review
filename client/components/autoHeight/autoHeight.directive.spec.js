'use strict';

describe('Directive: autoHeight', function () {

  // load the directive's module
  beforeEach(module('opsReviewApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<auto-height></auto-height>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the autoHeight directive');
  }));
});