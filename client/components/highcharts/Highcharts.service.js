'use strict';

angular.module('opsReviewApp')
  .factory('Highcharts', function ($rootScope, ThemeManager) {
    var Highcharts = window.Highcharts;
    var defaults = angular.copy(Highcharts.getOptions());

    //var seriesColors = ["#3182bd", "#6baed6", "#9ecae1", "#c6dbef", "#e6550d", "#fd8d3c", "#fdae6b", "#fdd0a2", "#31a354", "#74c476", "#a1d99b", "#c7e9c0", "#756bb1", "#9e9ac8", "#bcbddc", "#dadaeb", "#843c39", "#ad494a", "#d6616b", "#e7969c", "#7b4173", "#a55194", "#ce6dbd", "#de9ed6", "#637939", "#8ca252", "#b5cf6b", "#cedb9c", "#8c6d31", "#bd9e39", "#e7ba52", "#e7cb94", "#636363", "#969696", "#bdbdbd", "#d9d9d9"];

    var seriesColors = ["#7CB5EC","#8dce39","#F7A35C","#DE546B","#697275","#A158F5","#39536C","#7CB432","#B77944","#9E3C4C","#747E82","#7741B5","#618EB9","#618E27","#DD9253","#C44A5F","#52595B","#904FDB"];


    var labelFormatter = function() {
      if(!isNaN(parseFloat(this.value)) && isFinite(this.value)) {
        return Number(this.value).toLocaleString();
      } else {
        return this.value;
      }
    };

    defaults.exporting.buttons.contextButton.menuItems.push({
      text: 'Deselect/Select All',
      onclick: function () {
        var series = this.series[0];
        if (series.visible) {
          $(this.series).each(function(){
            this.setVisible(false, false);
          });
          this.redraw();
        } else {
          $(this.series).each(function(){
            this.setVisible(true, false);
          });
          this.redraw();
        }
      }
    });

    Highcharts.themes = {
      "light": {
        colors: seriesColors,
        lang: {
          thousandsSep: ','
        },
        chart: {
          style: {
            fontFamily: "'Museo Sans', Arial, sans-serif"
          },
          plotBorderColor: '#606063',
          spacing: [20,30,20,30],
        },
        legend: {
          itemStyle:{
            fontWeight:'500',
            color:'#606060'
          }
        },
        xAxis: {
          gridLineColor: "#D8D8D8",
          lineColor: "#D8D8D8",
          tickColor: "#D8D8D8",
          labels: {
            formatter: labelFormatter,
            style: {
              color: "#606060"
            }
          }
        },
        yAxis: {
          gridLineColor: "#f4f4f4",
          lineColor: "#f4f4f4",
          tickColor: "#D8D8D8",
          labels: {
            formatter: labelFormatter,
            style: {
              color: "#606060"
            }
          }
        },
        plotOptions: {
          line: {
            dataLabels: {
              color: "#606060",
              style: {"textShadow": "rgba(0,0,0,0) 0px 0px 2px, rgba(0,0,0,0) 0px 0px 2px"}
            }
          },
          column: {
             dataLabels: {
              color: '#fff',
              style: {"textShadow": "rgba(0,0,0,0) 0px 0px 2px, rgba(0,0,0,0) 0px 0px 2px"}
             }
          },
        },
        navigation: {
          buttonOptions: {
            symbolStroke: "#7c909d",
          }
        },
      },
      "dark": {
        lang: {
          thousandsSep: ','
        },
        colors: seriesColors,
        chart: {
          backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
            stops: [
              [0, '#2a2a2b'],
              [1, '#3e3e40']
            ]
          },
          style: {
            fontFamily: "'Roboto-Light', sans-serif"
          },
          plotBorderColor: '#606063'
        },
        title: {
          style: {
            color: '#E0E0E3',
            textTransform: 'uppercase',
            fontSize: '20px'
          }
        },
        subtitle: {
          style: {
            color: '#E0E0E3',
            textTransform: 'uppercase'
          }
        },
        xAxis: {
          gridLineColor: '#707073',
          labels: {
            style: {
              color: '#E0E0E3'
            },
            formatter: labelFormatter
          },
          lineColor: '#707073',
          minorGridLineColor: '#505053',
          tickColor: '#707073',
          title: {
            style: {
              color: '#A0A0A3'

            }
          }
        },
        yAxis: {
          gridLineColor: '#707073',
          labels: {
            style: {
              color: '#E0E0E3'
            },
            formatter: labelFormatter
          },
          lineColor: '#707073',
          minorGridLineColor: '#505053',
          tickColor: '#707073',
          tickWidth: 1,
          title: {
            style: {
              color: '#A0A0A3'
            }
          }
        },
        tooltip: {
          backgroundColor: 'rgba(0, 0, 0, 0.85)',
          style: {
            color: '#F0F0F0'
          }
        },
        plotOptions: {
          series: {
            marker: {
              lineColor: '#333'
            },
            dataLabels: {
              style: {"textShadow": "#000 0px 0px 2px, #000 0px 0px 2px" }
            }
          },
          boxplot: {
            fillColor: '#505053'
          },
          candlestick: {
            lineColor: 'white'
          },
          errorbar: {
            color: 'white'
          }
        },
        legend: {
          itemStyle: {
            color: '#E0E0E3',
          },
          itemHoverStyle: {
            color: '#FFF'
          },
          itemHiddenStyle: {
            color: '#606063'
          }
        },
        credits: {
          style: {
            color: '#666'
          }
        },
        labels: {
          style: {
            color: '#707073'
          }
        },

        drilldown: {
          activeAxisLabelStyle: {
            color: '#F0F0F3'
          },
          activeDataLabelStyle: {
            color: '#F0F0F3'
          }
        },

        navigation: {
          buttonOptions: {
            symbolStroke: '#DDDDDD',
            theme: {
              fill: '#505053'
            }
          }
        },

        // scroll charts
        rangeSelector: {
          buttonTheme: {
            fill: '#505053',
            stroke: '#000000',
            style: {
              color: '#CCC'
            },
            states: {
              hover: {
                fill: '#707073',
                stroke: '#000000',
                style: {
                  color: 'white'
                }
              },
              select: {
                fill: '#000003',
                stroke: '#000000',
                style: {
                  color: 'white'
                }
              }
            }
          },
          inputBoxBorderColor: '#505053',
          inputStyle: {
            backgroundColor: '#333',
            color: 'silver'
          },
          labelStyle: {
            color: 'silver'
          }
        },

        navigator: {
          handles: {
            backgroundColor: '#666',
            borderColor: '#AAA'
          },
          outlineColor: '#CCC',
          maskFill: 'rgba(255,255,255,0.1)',
          series: {
            color: '#7798BF',
            lineColor: '#A6C7ED'
          },
          xAxis: {
            gridLineColor: '#505053'
          }
        },

        scrollbar: {
          barBackgroundColor: '#808083',
          barBorderColor: '#808083',
          buttonArrowColor: '#CCC',
          buttonBackgroundColor: '#606063',
          buttonBorderColor: '#606063',
          rifleColor: '#FFF',
          trackBackgroundColor: '#404043',
          trackBorderColor: '#404043'
        },

        // special colors for some of the
        legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
        background2: '#505053',
        dataLabelsColor: '#B0B0B3',
        textColor: '#C0C0C0',
        contrastTextColor: '#F0F0F3',
        maskColor: 'rgba(255,255,255,0.3)'
      }
    };

    // Tooltip delay
    Highcharts.wrap(Highcharts.Tooltip.prototype, 'refresh', function (proceed) {
      var point = arguments[1];
      var chart = this.chart;
      var tooltip = this;
      var refreshArguments = arguments;
      var delayForDisplay = chart.options.tooltip.delayForDisplay ? chart.options.tooltip.delayForDisplay : 500;

      if (chart.tooltipTimer) {
        clearTimeout(chart.tooltipTimer);
      }

      chart.tooltipTimer = window.setTimeout(function () {

        if (point === chart.hoverPoint || $.inArray(chart.hoverPoint, point) > -1) {
          proceed.apply(tooltip, Array.prototype.slice.call(refreshArguments, 1));
        }

      }, delayForDisplay);

    });

    Highcharts.setTheme = function (theme) {
      Highcharts.theme = angular.copy(Highcharts.themes[theme || "light"]);
      Highcharts.setOptions(angular.extend({}, defaults, Highcharts.theme));
    };

    // Apply the theme

    Highcharts.setTheme(ThemeManager.getTheme());

    $rootScope.$on('changeTheme', function () {
      Highcharts.setTheme(ThemeManager.getTheme());
      $rootScope.$broadcast('deckster:redraw');
    });

    return Highcharts;
  });
