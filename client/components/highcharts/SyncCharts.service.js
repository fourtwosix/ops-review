'use strict';

angular.module('opsReviewApp')
  .factory('SyncCharts', function (CountdownLatch, $timeout) {

    var SyncCharts = {};



    SyncCharts.createCountdownLatches = function(deck) {
      // Loops through sync chart groups (defined in deck's json)
      _.forEach(_.keys(deck.synchronizedCharts), function(key) {
        var syncGroup = deck.synchronizedCharts[key];
        // Sets up the countdown latch for each sync group
        syncGroup.countdownLatch = new CountdownLatch(syncGroup.charts.length);
      });
    };



    // When a single card is reloaded, we need to force the countdown latch to execute the await to re-sync the charts.
    // We must first see if the all cards in the deck have options, or else the syncing will fail because not all cards are loaded up yet.
    // onReload will execute on the card's initial load, thus the reason for the undefined options check.
    SyncCharts.onCardReload = function(card) {
      var deckCards = _.values(card.$deckster.$cardHash);
      var allCardsDefined = true;
      _.forEach(deckCards, function(deckCard) {
        if(_.isUndefined(deckCard.options)) {
          allCardsDefined = false;
          return false;
        }
      });
      if(allCardsDefined) {
        $timeout(function() {
          SyncCharts.applyCountdownLatch(card, true);
        }, 100);
      }
    };



    // Loops through sync chart groups (defined in deck's json)
    // and checks to see if the card is part of that group.
    // If so, it applies a countdown to the latch.
    SyncCharts.applyCountdownLatch = function(card, force) {
      var deckOptions = card.$deckster.options;
      _.forEach(_.keys(deckOptions.synchronizedCharts), function(key) {
        var chartSyncGroup = deckOptions.synchronizedCharts[key].charts;
        if(_.includes(chartSyncGroup, card.options.cardName)) {
          if(force) {
            deckOptions.synchronizedCharts[key].countdownLatch.force();
          } else {
            deckOptions.synchronizedCharts[key].countdownLatch.countDown();
          }
        }
      });
    };



    var getChartEl = function(card) {
      var section = card.currentSection;
      var viewType = _.capitalize(card.options.getCurrentViewType(section));
      return $('#' + card[section + viewType + "Id"]);
    };

    var getChart = function(card) {
      var section = card.currentSection;
      var viewType = _.capitalize(card.options.getCurrentViewType(section));
      return card[section + viewType];
    };



    SyncCharts.createCountdownLatchAwaits = function(scope, deckOptions) {
      // Loops through sync chart groups (defined in deck's json).
      _.forEach(_.keys(deckOptions.synchronizedCharts), function(key) {
        // Defines the countdownLatch's await callback for each chart group.
          deckOptions.synchronizedCharts[key].countdownLatch.await(function () {
            // Gets the card objects in the deck
            var deckCards = _.values(scope.deckster.$cardHash);
            // Gets the names of the cards that need to be synced
            var chartSyncGroup = deckOptions.synchronizedCharts[key].charts;
            // Gets the actual card objects that need to be synced
            var cardsToSync = _.map(chartSyncGroup, function (chartName) {
              var cardToSync = _.find(deckCards, function (deckCard) {
                return deckCard.options.cardName === chartName;
              });
              return cardToSync;
            });
            // Loops through each card to be synced
            _.forEach(cardsToSync, function (cardToSync) {
              // Gets the dom element to bind mouse listeners to
              var $chartEl = getChartEl(cardToSync);
              $chartEl.bind('mousemove touchmove', function (e) {
                // Loop through each card and syncs it to every other card (and itself)
                _.forEach(cardsToSync, function (cardToSync) {
                  // Gets the highchart object from the card
                  var chart = getChart(cardToSync);
                  // Prevents the tracker from hiding when focus is lost from the chart
                  chart.pointer.reset = function () {
                    return undefined
                  };
                  e = chart.pointer.normalize(e); // Find coordinates within the chart

                  if (!_.isEmpty(chart.series) && chart.series[0]) {
                    // Get the point for the hovered series
                    var seriesIndex = _.findIndex(chart.series, function (series) {
                      return series.state === "hover" && series.visible;
                    });
                    // If no index was returned, then we are not actively on that chart, so set the index to the first visible one.
                    if(seriesIndex < 0) {
                      seriesIndex = _.findIndex(chart.series, function (series) {
                        return series.visible;
                      });
                    }
                    if(seriesIndex >= 0) {
                      var point = chart.series[seriesIndex].searchPoint(e, true); // Get the hovered point

                      if (point) {
                        point.onMouseOver(); // Show the hover marker
                        chart.tooltip.refresh(point); // Show the tooltip
                        chart.xAxis[0].drawCrosshair(e, point); // Show the crosshair
                      }
                    }
                  }
                });
              });
            });

            // Reset the countdownLatch counter
            this.count = 0;
          });

      });
    };



    SyncCharts.syncChartsZoom = function(card, chartOptions) {

      var syncExtremes = function(cardsToSync) {
        chartOptions.xAxis.events = {};
        chartOptions.xAxis.events.setExtremes = function(e) {
          var thisChart = this.chart;
          _.forEach(cardsToSync, function (cardToSync) {
            var chart = getChart(cardToSync);
            if (chart !== thisChart) {
              if (chart.xAxis[0].setExtremes) { // It is null while updating
                chart.xAxis[0].setExtremes(e.min, e.max);
              }
            }
          });
        }
      };

      var deckOptions = card.$deckster.options;
      // Gets the card objects in the deck
      var deckCards = _.values(card.$deckster.$cardHash);
      // Loops through sync chart groups (defined in deck's json)
      // and checks to see if the card is part of that group.
      _.forEach(_.keys(deckOptions.synchronizedCharts), function(key) {
        // Gets the names of the cards that need to be synced
        var chartSyncGroup = deckOptions.synchronizedCharts[key].charts;
        if(_.includes(chartSyncGroup, card.options.cardName)) {
          // Gets the actual card objects that need to be synced
          var cardsToSync = _.map(chartSyncGroup, function(chartName) {
            var cardToSync = _.find(deckCards, function(deckCard) {
              return deckCard.options.cardName === chartName;
            });
            return cardToSync;
          });
          // Setup zoom extreme syncing
          syncExtremes(cardsToSync);
        }
      });

    };



  return SyncCharts;
});
