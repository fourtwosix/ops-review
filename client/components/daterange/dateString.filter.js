'use strict';

angular.module('opsReviewApp')
  .filter('datestr', function (DateUtils) {
    return function (input, format) {
      return DateUtils.getFormattedDateFromDateString(input, format);
    };
  });
