'use strict';

angular.module('opsReviewApp')
  .directive('daterange', function () {
    return {
      templateUrl: 'components/daterange/daterange.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
        scope.initMode = angular.copy(scope.deck.dateFilterType);
        scope.$watch(attrs.maxStartDate, function (val) {
          scope.maxStartDate = val;
        });

        scope.$watch(attrs.minStartDate, function (val) {
          scope.minStartDate = val;
        });

        scope.$watch(attrs.maxEndDate, function (val) {
          scope.maxEndDate = val;
        });

        scope.$watch(attrs.minEndDate, function (val) {
          scope.minEndDate = val;
        });
      }
    };
  });
