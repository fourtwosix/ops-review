'use strict';

angular.module('opsReviewApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngAnimate',
  'btford.socket-io',
  'ui.router',
  'ui.bootstrap',
  'angularMoment',
  'ngTagsInput',
  'nsPopover',
  'ngMaterial',
  'localytics.directives',
  'angular-bind-html-compile',
  'ngHandsontable',
  'angular.vertilize',
  'ui.select',
  'angular-flexslider',
  'sysen.toolbelt',
  'angular.vertilize',
  'ui.utils.masks',
  'checklist-model',
  'ui.router.grant',
  'toastr',
  'angular-google-analytics',
  'textAngular'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $provide,
    tagsInputConfigProvider, $animateProvider, toastrConfig, AnalyticsProvider) {

    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');
    tagsInputConfigProvider.setTextAutosizeThreshold(20);
    $animateProvider.classNameFilter(/its-animated|toast/);

    angular.extend(toastrConfig, {
      closeButton: true,
      timeOut: 4000,
      extendedTimeOut: 2000,
      toastClass: 'toast',
      messageClass: 'toast-message',
      positionClass: 'toast-top-right',
      progressBar: false,
      titleClass: 'toast-title',
      iconClasses: {
        error: 'toast-error',
        info: 'toast-info',
        success: 'toast-success',
        warning: 'toast-warning'
      }
    });

    $provide.decorator('taOptions', ['$delegate', function(taOptions) {
      // $delegate is the taOptions we are decorating
      // here we override the default toolbars and classes specified in taOptions.
      taOptions.forceTextAngularSanitize = true; // set false to allow the textAngular-sanitize provider to be replaced
      taOptions.keyMappings = []; // allow customizable keyMappings for specialized key boards or languages
      taOptions.toolbar = [
        []
      ];
      taOptions.classes = {
        focussed: 'focussed',
        toolbar: 'btn-toolbar',
        toolbarGroup: 'btn-group',
        toolbarButton: 'btn btn-default',
        toolbarButtonActive: 'active',
        disabled: 'disabled',
        textEditor: 'form-control',
        htmlEditor: 'form-control'
      };
      return taOptions; // whatever you return will be the taOptions
    }]);

    // Google Analytics Config
    AnalyticsProvider
    .setAccount(GlobalConfig.google.trackingId)
    .trackPages(true)
    .setPageEvent('$stateChangeSuccess');
  })

  .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location, $window) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if(response.status === 401) {
          // remove any stale tokens
          $cookieStore.remove('token');

          if ($location.path().toLowerCase() != '/login') {
            $cookieStore.put('redirectUrl', $location.path());
            $window.location.href = '/auth/login/';
          } else {
            $cookieStore.put('redirectUrl', '/');
          }

          return $q.reject(response);
        }
        else {
          return $q.reject(response);
        }
      }
    };
  })

  .run(function ($rootScope, $location, $uibModalStack, Auth, Highcharts, ThemeManager, grant, $q, $window) {
    $rootScope.GlobalConfig = window.GlobalConfig;
    ThemeManager.init();

    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function (event, next) {
      Auth.isLoggedInAsync(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          $window.location.href = '/auth/login/';
        }
      });
    });

    $rootScope.$on('$stateChangeSuccess', function (event, next) {
      $uibModalStack.dismissAll();
    });

    grant.addTest('pmrUser', function () {
      var deferred = $q.defer();

      Auth.isLoggedInAsync(function () {
        var group = Auth.getCurrentDeckGroup();
        var pmrUser = group != null && group.indexOf("pmr-reporting") !== -1;

        pmrUser ? deferred.resolve() : deferred.reject();
      });
      return deferred.promise;
    });

    grant.addTest('admin', function () {
      var deferred = $q.defer();

      Auth.isLoggedInAsync(function () {
        Auth.isAdmin() ? deferred.resolve() : deferred.reject();
      });

      return deferred.promise;
    });
  });
