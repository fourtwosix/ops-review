'use strict';

angular.module('opsReviewApp')
  .controller('GlobalSettingsCtrl', function ($scope, $uibModalInstance, Modal) {
    $scope.globalSettings = angular.copy($scope.settings);
    var authsRemoved = [];

      _.each($scope.selection.users, function (user, index) {
        _.each($scope.globalSettings, function(settings, name) {
          if(index !== 0) {
            _.remove(settings, function(setting) {
              if(name === 'programs') {
                return !_.any(user[name], {project_num: setting.project_num});
              } else {
                return user[name].indexOf(setting) === -1
              }
            })
          } else {
            _.each(user[name], function(setting) {
              settings.push(setting);
            });
          }
        })
      });

    var sharedSettings = angular.copy($scope.globalSettings);

    $scope.addRemovedPrograms = function(auth) {
      var index = _.findIndex(authsRemoved, {auth_name: auth});
      if(index !== -1){
        _.each(authsRemoved[index].programs, function(program) {
          $scope.globalSettings.programs.push(program);
        });
        _.remove(authsRemoved, {auth_name: auth});
      }
    };

    $scope.confirmBatchAuthRemoval = function(auth) {
      var modal = Modal.confirm.general(function(confirmed){
        if(confirmed) {
          authsRemoved.push({auth_name: auth, programs: []});
          var index = authsRemoved.length - 1;
          _.remove($scope.globalSettings.programs, function(program) {
            var ret = program.auth_name === auth;

            if(ret) {
              authsRemoved[index].programs.push(program);
            }

            return ret;
          });
        } else {
          $scope.globalSettings.auths = _.union($scope.globalSettings.auths, [auth]);
        }
      });

      modal('Warning', 'Are you sure you want to remove ' + auth.toUpperCase() + ' access from all selected users? Their ' + auth.toUpperCase() + ' restricted programs will also be removed.');
    };

    $scope.applySettings = function() {
      var removed = angular.copy($scope.settings);

      _.each($scope.globalSettings, function (values, name) {
        removed[name] = _.difference(sharedSettings[name], values);
        _.each($scope.selection.users, function (user) {
          if (name === 'programs') {
            user[name] = _.filter(user[name], function (userValue) {
              return !(_.any(removed[name], {project_num: userValue.project_num}) || _.any(authsRemoved, {auth_name: userValue.auth}));
            });

            _.each(values, function (value) {
              if (!_.find(user[name], {project_num: value.project_num})) {
                user[name].push(value);
              }
            })
          } else {
            user[name] = _.filter(user[name], function (userValue) {
              return removed[name].indexOf(userValue) === -1;
            });

            _.each(values, function (value) {
              if (user[name].indexOf(value) === -1) {
                user[name].push(value);
              }
            })
          }
        });
      });

        $uibModalInstance.close();
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
