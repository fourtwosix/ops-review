'use strict';

angular.module('opsReviewApp')
  .controller('EmailCtrl', function ($scope, $uibModalInstance, Modal, toastr, Email) {

    $scope.recipients = ["All Users", "PMR Users"];
    $scope.email = {to: "All Users"};

    $scope.cancel = function () {
      if($scope.body || $scope.subject) {
      var modal = Modal.confirm.general(function (confirmed) {
        if (confirmed) {
          $uibModalInstance.dismiss('cancel');
        }
      });
      modal('Warning', 'Are you sure you want to cancel sending this email? Your text will be erased.');
      } else {
        $uibModalInstance.dismiss('cancel');
      }
    };

    $scope.send = function() {
        var modal = Modal.confirm.general(function (confirmed) {
          if (confirmed) {
            console.log($scope.body);
            var PMR = ($scope.email.to == ('PMR Users'));
            Email.emailAllUsers($scope.body, $scope.subject, PMR)
              .then(function () {
                $uibModalInstance.close();
                $scope.showSuccessToast("Email Announcement Sent");
              })
              .catch(function (err) {
                toastr.error(err);
              });
          }
        });
        modal('Warning', 'Are you sure you want to send this email announcement?');
    };

    $scope.showSuccessToast = function(message) {
      toastr.success(message);
    }

  });
