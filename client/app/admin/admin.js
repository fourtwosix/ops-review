'use strict';

angular.module('opsReviewApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('admin', {
        url: '/admin',
        templateUrl: 'app/admin/admin.html',
        controller: 'AdminCtrl',
        resolve: {
          admin: function(grant) {
            return grant.only({test: 'admin', state: 'main'});
          }
        }
      });
  });
