'use strict';
angular.module('opsReviewApp')
  .filter('propsFilter', function() {
    return function(items, props) {
      var out = [];

      if (angular.isArray(items)) {
        items.forEach(function(item) {
          var itemMatches = false;

          var keys = Object.keys(props);
          for (var i = 0; i < keys.length; i++) {
            var prop = keys[i];
            var text = props[prop].toLowerCase();
            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
              itemMatches = true;
              break;
            }
          }

          if (itemMatches) {
            out.push(item);
          }
        });
      } else {
        // Let the output be the input untouched
        out = items;
      }

      return out;
    };
  })
  .controller('AdminCtrl', function ($scope, $http, $uibModal, moment, Auth, User, ProgramManager, $q, toastr, Modal, StaticDisplayData, $timeout) {
    //Variables for users
    var currentUser = Auth.getCurrentUser();
    $scope.settings = {roles: [], auths: [], groups: [], pmr_group: [], divisions: [], programs: []};
    $scope.userList = {searchText: "", users: null, saved: false};
    $scope.initialUsers = [];
    $scope.roles = [];
    $scope.auths = [];
    var programs = [];
    $scope.contracts = [];
    $scope.reportGroups = [];
    $scope.selection = {
      users: []
    };
    $scope.validationErrors = [];
    $scope.divisions = [];
    var mainDivisionsArr = [];
    var oneOnOneArr = [];
    $scope.dt = new Date();
    $scope.maxDate = new Date();
    $scope.options = {
      maxMode: 'year', // set this to whatever date you want to set
      minMode: 'month',
      maxDate: $scope.maxDate
    }
    $scope.currentFYStart = new Date();
    $scope.currentFYEnd = new Date();
    //Paper trail variables
    $scope.editorEnabled = false;
    $scope.menuNames =[];
    $scope.searchMenuNames = {};
    $scope.entryNameText = {};
    $scope.entryEmailText = {};

    $scope.firstNameText = {};
    $scope.lastNameText = {};
    var previousUser = "";
    $scope.isDivisionExist = true;
    $scope.programDivision = [];
    $scope.isDivision = true;
    $scope.originalMenuNames = [];
    $scope.groupDivision = {};
    //End Paper trail variables.

    //This code below makes the selection in the menu from selecting from users and paper trail names.
    $scope.sections = [
      { title: "Users", id: "info", checked: true},
      { title: "Papertrail Names", id: "edit", checked: false }
    ];

    $scope.userView = $scope.sections[0].checked;
    $scope.papertrailNameView = $scope.sections[1].checked;

    $scope.status = {
        opened: false
    };
    $scope.openCalendar = function(){
      $scope.status.opened = true;
    }
    $scope.selectedSection = function(position, section) {
      if(section.title === 'Papertrail Names') {
        $scope.sections[1].checked = true;
        $scope.sections[0].checked = false;
        $scope.userView = $scope.sections[0].checked;
        $scope.papertrailNameView = $scope.sections[1].checked;
      }else{
        $scope.sections[0].checked = true;
        $scope.sections[1].checked = false;
        $scope.userView = $scope.sections[0].checked;
        $scope.papertrailNameView = $scope.sections[1].checked;
      }
    };
    // End for selecting menu for users and paper trail names.
    $scope.searchUsers = function (user) {
      var match = false;

      if ($scope.userList.searchText === "") {
        return true;
      }

      var searches = _.words($scope.userList.searchText, /[^,]+/g);

      _.each(searches, function (search) {
        search = _.trim(search).toLowerCase();
        if (user.name.toLowerCase().indexOf(search) != -1 || user.email.toLowerCase().indexOf(search) != -1) {
          match = true;
        }
      });

      return match;
    };
    var loadUsers = function() {
      var users = {};

      $q.all([
        User.query().$promise.then(function (response) {
          users = response;
        }).then(function () {
          $scope.userList.users = [];
          $scope.initialUsers = [];
          _.each(users, function (user) {

            if(user.id === currentUser.id) {
              return;
            }
            user.groups = user.groups || [];
            user.pmr_group = user.pmr_group || [];
            user.divisions = user.divisions || [];
            user.roles = _.pluck(user.roles, 'name');
            user.auths = _.pluck(user.auths, 'name');
            user.programs = _.map(user.programs, function (program) {
              return {project_num: program.project_id, auth_name: program.auth_name, pmr_group: program.pmr_group, division: program.division};
            });
            user.programs.sort(function(a, b){
                if(a.project_num < b.project_num) return -1;
                if(a.project_num > b.project_num) return 1;
                return 0;
            });
            user.isCollapsed = true;
            _.forEach(user.divisions, function(user_division){
              $scope.divisions = _.remove($scope.divisions, function(division){
                return division.division_name != user_division;
              });
            });
            $scope.userList.users.push(user);
            $scope.initialUsers.push(angular.copy(user));
          });
        }),
        StaticDisplayData.getDisplayDataForType("groups").then(function(groups) {
          $scope.reportGroups = _.pluck(groups, 'display_value');
        }),
        //this is to get names for the picklist
        StaticDisplayData.getDisplayDataForType("pmrName").then(function(names) {
          $scope.menuNames = names;
        }),
        Auth.getRoles().then(function (roles) {
          $scope.roles = _.pluck(roles, 'name');
        }),
        Auth.getPmrGroups().then(function(groups) {
          $scope.pmrGroup = (_.pluck(groups, 'group_name'));
        }),
        Auth.getAuths().then(function (auths) {
          $scope.auths = _.pluck(auths, 'name');
        }),
        Auth.getPrograms().then(function (programList) {
          programs = programList;
        }),
        Auth.getContracts().then(function (contracts) {
          $scope.contracts = _.map(contracts, function (contract) {
            contract = _.pick(contract, ['project_num', 'division','auth_name', 'project_name']);
            // if(contract.division === "national security") { //dig access is manual now
            //   contract.auth_name = "dig";
            // }

            return contract;
          });
        })
      ]).then(function() {
        _.each(programs, function(program) {
          if(program.auth_name){
            var contractIndex = _.findIndex($scope.contracts, {project_num: program.project_id});
            if(contractIndex !== -1) {
              $scope.contracts[contractIndex].auth_name = program.auth_name;
            }
          }
        });
        $scope.listContract = _.map($scope.contracts, function (contract) {
          // contract = _.pick(contract, ['project_num']);

          return contract;
        });
        $scope.listProgram = _.map(programs, function (program) {
          program = _.pick(program, ['project_id']);
          program.project_num = program.project_id;
          program = _.omit(program, ['project_id']);
          return program;
        });
        $scope.programContract = _.uniq(_.union($scope.listContract, $scope.listProgram), false, function(data){ return data.project_num });
      });
    };

    loadUsers();

    $scope.selectRole = function (selectedRole) {
      if(selectedRole === 'Executive') {
        $scope.allowToSubmit = false;
      }
    }

    $scope.deselectRole = function (user, deselectedRole) {
      if(deselectedRole === 'Executive') {
        $scope.allowToSubmit = true;
        mainDivisionsArr = [];
        $scope.divisions = [];
        user.pmr_group = [];
        user.divisions = [];
      }
    }
    $scope.dateCheck = function(dt){
      $scope.dt = dt;
      var getYear = $scope.dt.getFullYear();
      $scope.currentFYStart =  moment(getYear + '-04-01').format("YYYY-MM-DD");
      $scope.currentFYEnd = moment(getYear + '-12-01').format("YYYY-MM-DD");
    }

    $scope.statusCollapsed = function(user) {
      _.each($scope.userList.users, function (userObj) {
        userObj.isCollapsed = true;
        if(user.name === userObj.name)     user.isCollapsed = false;
      });
      $scope.divisions = [];
      if(_.includes(user.roles, 'Executive') && user.pmr_group.length > 0) {
        mainDivisionsArr = []
        ProgramManager.getDivisionValues(user.pmr_group).then(function (divObj) {
          _.each(divObj, function(div) {
            mainDivisionsArr.push({group_name:div.group_name, division_name:div.division_name});
            mainDivisionsArr = _.uniq(mainDivisionsArr, 'division_name');
          });
          $scope.divisions = mainDivisionsArr;
          if(user.divisions.length > 0) {
            for(var group in user.divisions){
              var filteredDivisions = mainDivisionsArr.filter(function(division){
                return division !== user.divisions[group];
              });
              $scope.divisions = filteredDivisions;
              _.forEach(user.divisions, function(user_division){
                $scope.divisions = _.remove($scope.divisions, function(division){
                  return division.division_name != user_division;
                });
                if(typeof user_division === 'object' && user_division !== null){
                  $scope.divisions = _.remove($scope.divisions, function(division){
                    return division.division_name != user_division.division_name;
                  });
                }
              });
            }
          }
        });
      }
      if(_.includes(user.roles, 'Executive') && user.divisions.length > 0) {
        oneOnOneArr = user.pmr_group;
      }
      if(user.name === previousUser){
        user.isCollapsed = true;
        previousUser = "";
      }
      else{
        previousUser = user.name;
      }
    }

    //Adds PmrGroup and populates the array for Buisness Groups or Divison Names
    $scope.selectPmrGroup = function(group, user) {
      mainDivisionsArr = [];
      if(group) {
        ProgramManager.getDivisionValues(group).then(function (division) {
          _.each(division, function(divisionName){
            mainDivisionsArr.push({group_name:divisionName.group_name, division_name:divisionName.division_name});
            mainDivisionsArr = _.uniq(mainDivisionsArr, 'division_name');
          });
          var tempDivision = $scope.divisions.concat(mainDivisionsArr);
          $scope.divisions = tempDivision;
        });
      }
    }
    //Removes PmrGroup and removes the Buisness Groups or Divison Names from the array
    $scope.removePmrGroup = function (item, group, user) {
      var filteredDivisions = $scope.divisions.filter(function(division){
        return division.group_name !== group;
      });
      var filterItems = item.filter(function(division){
        return division.group_name !== group;
      });
      user.divisions = filterItems;
      $scope.divisions = filteredDivisions;
      ProgramManager.getDivisionValues(group).then(function (divObj) {
        _.each(divObj, function(item){
          _.remove(user.divisions, function(division){
            return division == item.division_name;
          });
        });
      });
    }

    $scope.removeBusinessUnit = function(division, user){
      ProgramManager.getDivisionValues(user.pmr_group).then(function (divisions) {
        _.each(divisions, function(div){
          if(div.division_name == division){
            $scope.divisions.push(div);
          }
        });
    });
  }
    $scope.selectedProgram = function(user,program) {
      if(program.hasOwnProperty('isTag')){
        user.programs.pop();
        user.programs.push({project_num: program.project_num, pmr_group: program.pmr_group, division: program.division});
        }
      else{
        _.each(programs, function(item_program){
          if(item_program.project_id == program.project_num){
            user.programs.pop();
            user.programs.push({project_num: item_program.project_id, pmr_group: item_program.pmr_group, division: item_program.division});
          }
        });
      }
      _.each(programs, function(item_program){
        for(var i = 0; i < user.programs.length; i++){
          if(item_program.project_id == user.programs[i].project_num){
            user.programs[i] = {project_num: item_program.project_id, pmr_group: item_program.pmr_group, division: item_program.division};
          }
        }
      });
    }
    $scope.selectedBusinessUnit = function(user) {
      if(_.includes(user.roles, 'Executive') && _.includes(user.roles, 'Program Manager')){
        $scope.programDivision = [];
        _.each(user.programs.filter(function(division){ return division.division !== null}), function(program){
          $scope.isDivision = false;
          _.each(user.divisions.filter(function(division){ return division.division_name !== null }), function(userDivision){
            if(program.division == userDivision) $scope.isDivision = true;
          });
          if($scope.isDivision == false){
            $scope.programDivision.push(program);
          }
        });

      $scope.programDivision = _.uniq($scope.programDivision);
        if($scope.programDivision.length == 0){
          $scope.isDivision = true;
        }
      }
    }
    $scope.cancelChanges = function() {
      _.each($scope.userList.users, function(user, index) {
        var isCollapsed = user.isCollapsed;
        angular.copy(_.findWhere($scope.initialUsers, {id: user.id}), $scope.userList.users[index]);
        user.isCollapsed = isCollapsed;
      });
    };

    $scope.saveChanges = function () {
      var userUpdates = [];
      var programCreates = [];
      var problemUsers = [];
      $scope.validationErrors = [];
      _.each($scope.userList.users, function (user) {
        var initUser = _.findWhere($scope.initialUsers, {id: user.id});
        var invalidPrograms = [];
        var createPrograms = [];
        var userDivisions = [];
        $scope.isDivisionExist = false;
        var isOneOnOne = false;

        _.each(user.divisions, function(division){
          if(typeof division === 'object' && division !== null){
            userDivisions.push(division.division_name);
          }
          else{
            userDivisions.push(division);
          }
        });
        user.divisions = userDivisions;
        if (!angular.equals(user, initUser)) {
          _.each(programs, function(item_program){
            for(var i = 0; i < user.programs.length; i++){
              if(user.programs[i].hasOwnProperty('isTag'))return
              if(item_program.project_id == user.programs[i].project_num){
                user.programs[i] = {project_num: item_program.project_id, pmr_group: item_program.pmr_group, division: item_program.division};
              }
            }
          });

          var oneOnOne = [];
          if(_.includes(user.roles, 'Program Manager') && user.programs.length === 0) {
            showErrorToasts("A Program Manager role is add, enter at least one program or remove user role");
            return;
          }else if(!_.includes(user.roles, 'Program Manager') && user.programs.length > 0){
            showErrorToasts("Enter the Program Manager role or remove the Program(s)");
            return;
          }

          if(_.includes(user.roles, 'Executive') && _.includes(user.roles, 'Program Manager')){
            _.each(user.programs.filter(function(division){ return division.division !== null && division.division !== undefined}), function(program){
              $scope.isDivisionExist = false;
              _.each(user.divisions.filter(function(division){ return division.division_name !== null}), function(userDivision){
                if(program.division == userDivision){
                  $scope.isDivisionExist = true;
                }
              });
              if($scope.isDivisionExist == false){
                $scope.groupDivision = program;
                return false;
              }
            });
            if(user.programs.filter(function(division){ return division.division !== null && division.division !== undefined }).length == 0){
                $scope.isDivisionExist = true;
            }
            if($scope.isDivisionExist == false){
              showErrorToasts("Program Group/BU: Please fill out the Group/Business Unit");
              showInfoToasts("Group: " +$scope.groupDivision.pmr_group);
              showInfoToasts("Business Unit: "+$scope.groupDivision.division);

              console.log("Not Allowed: program division needs to filled out");
              return;
            }
          }
          if(_.includes(user.roles, 'Executive')){
            if(user.divisions.length == 0 || user.pmr_group.length == 0){
              showErrorToasts("Please fill out all items in red before saving.");
              return;
            }
            ProgramManager.getDivisionValues(user.pmr_group).then(function (divisions) {
              _.each(divisions, function(div){
                _.each(user.divisions.filter(function(division){ return division.division_name !== null }), function(userDivision){
                  if(div.division_name == userDivision){
                    oneOnOne.push(div);
                  }
                });
              });
              _.each(user.pmr_group, function(group){
                _.each(oneOnOne, function(division){
                  if(division.group_name == group){
                     isOneOnOne = true;
                  }
                });
                if(isOneOnOne === false){
                  return false;
                };
              });
              if(isOneOnOne === false){
                showErrorToasts("Please fill out the Group/Division in the form");
                console.log("Not Allowed: program division needs to be matched.");
                return;
              }
            });
          }
          _.each(user.programs, function(program) {
            var found = _.findWhere(programs, {project_id: program.project_num});

            if(program.auth_name && user.auths.indexOf(program.auth_name) === -1) {
              invalidPrograms.push(program);
            } else if(!found) {
              createPrograms.push(program);
            }
          });

          if(invalidPrograms.length === 0) {
            createPrograms = _.map(createPrograms, function(program) {
              return ProgramManager.createProgram({project_id: program.project_num, name: program.project_name, auth_name: program.auth_name}).then(function(savedProgram) {
                programs.push(savedProgram);
              })
            });

            programCreates.push(Promise.all(createPrograms).then(function() {
              userUpdates.push(User.update({id: user.id}, user).$promise.then(function() {
                $timeout(function() {
                  showSaveToasts(user.name + " Saved!");
                })

              }).catch(function() {
                showErrorToasts(user.name + " failed to save!  Please contact the Administrator.");
              }));
            }));

          } else {
            var searchText = $scope.validationErrors.length === 0 ? "" : $scope.userList.searchText + ", ";
            _.each(invalidPrograms, function(program) {
              $scope.validationErrors.push("CANNOT ASSIGN DIG PROGRAM " + program.project_num + " TO " + user.name);
              searchText += user.email + ", ";
              problemUsers.push(user);
            });

            $scope.userList.searchText = _.trimRight(searchText, ", ");
            user.isCollapsed = false;
          }
        }
      });

      Promise.all(programCreates).then(function() {
        Promise.all(userUpdates).then(function () {
          if($scope.validationErrors.length === 0) {
            $scope.initialUsers = angular.copy($scope.userList.users); //todo don't overwrite users who have validation errors
          } else {
            _.each(problemUsers, function(user, index) {
              problemUsers[index] = _.find($scope.initialUsers, {id: user.id});
            });

            $scope.initialUsers = angular.copy($scope.userList.users);

            _.each(problemUsers, function(user) {
              var loc = _.findIndex($scope.initialUsers, {id: user.id});
              $scope.initialUsers[loc] = user;
            });
          }

          $scope.$apply();
        });
      });
    };

    var showSaveToasts = function(message) {
      toastr.success(message);
    };

    var showErrorToasts = function(message) {
      toastr.error(message);
    };

    var showInfoToasts = function(message) {
      toastr.info(message);
    };

    $scope.confirmAuthRemoval = function(auth, user) {
      var modal = Modal.confirm.general(function(confirmed){
        if(confirmed) {
          $scope.removeRestrictedPrograms(auth, user);
        } else {
          user.auths = _.union(user.auths, [auth]);
        }
      });

      modal('Warning', 'Are you sure you want to remove ' + auth.toUpperCase() + ' access from this user? Their ' + auth.toUpperCase() + ' restricted programs will also be removed.');
    };

    $scope.removeRestrictedPrograms = function (auth, user) {
      user.programs = _.reject(user.programs, {auth_name: auth});
    };


    $scope.delete = function (user) {

      var modal = Modal.confirm.general(function(confirmed) {
        if(confirmed) {
          User.remove({id: user.id}).$promise.then(function() {
            _.pullAt($scope.userList.users, _.findIndex($scope.userList.users, {id: user.id}));
            _.pullAt($scope.initialUsers, _.findIndex($scope.initialUsers, {id: user.id}));
            showSaveToasts("User " + user.name + " successfully deleted");
          });
        }
      });

      modal('Warning', 'Are you sure you want to delete user ' + user.name + "?");
    };

    $scope.globalSettingsModal = function () {

      var modal = $uibModal.open({
        templateUrl: 'app/admin/globalSettingsModal.html',
        controller: 'GlobalSettingsCtrl',
        scope: $scope,
        backdrop: true,
        windowClass: 'minimal-modal admin-batch-modal'
      });
    };

    $scope.emailModal = function () {

      var modal = $uibModal.open({
        templateUrl: 'app/admin/emailModal.html',
        controller: 'EmailCtrl',
        scope: $scope,
        backdrop: false,
        windowClass: 'minimal-modal admin-email-modal'
      });
    };

    $scope.addNewProgram = function(newTag) {
      return  {
        project_num: newTag.toLowerCase()
      };
    };

    //Functions below are for paper trail names
    $scope.cancelEdit = function() {
      $scope.editorEnabled = false;
      $scope.menuNames = $scope.originalMenuNames;
      $scope.searchMenuNames.name = '';
    };

    $scope.editEntryName = function(entry) {
      $scope.editorEnabled = true;
      $scope.searchMenuNames.name = entry.display_value;
      $scope.entryNameText.text = entry.display_value;
      $scope.entryEmailText.text = entry.description;
      $scope.originalMenuNames = angular.copy($scope.menuNames);
    };

    $scope.addEntryName = function (entry, newVal, newValemail) {
      if($scope.editorEnabled === true) {
        //update
        StaticDisplayData.updateDisplayDataById(entry.id, newVal, newValemail).then(function () {
          showSaveToasts(newVal + " successfully updated");
          loadUsers();
        }).catch(function () {
          showErrorToasts(newVal + " failed to update!  Please contact the Administrator.");
        });
        $scope.editorEnabled = false;
        $scope.searchMenuNames.name = '';
        $scope.entryNameText.text = '';
        $scope.entryEmailText.text = '';
      }else {
        //check if new name exists
        var entryExists = !!_.findWhere($scope.menuNames, {'display_value': entry.display_value || newVal});

        if(!entryExists) {
          //save
          StaticDisplayData.addDisplayData(entry, newVal, newValemail).then(function () {
            showSaveToasts(newVal + " successfully got saved");
            $scope.entryNameText.text = '';
            $scope.firstNameText.text = '';
            $scope.lastNameText.text = '';
            $scope.entryEmailText.text = '';

            loadUsers();
          }).catch(function () {
            showErrorToasts(newVal + " failed to save!  Please contact the Administrator.");
          })
        }else{
          showErrorToasts(newVal + " already exists, try another name");
        }
      }
    };

    $scope.removeEntryName = function (entry) {
      var modal = Modal.confirm.general(function(confirmed) {
        if(confirmed) {
          StaticDisplayData.deleteDisplayDataById(entry.id).then(function () {
            showSaveToasts("Entry " + entry.display_value + " successfully deleted");
            loadUsers();
            $scope.searchMenuNames.name = '';
          }).catch(function () {
            showErrorToasts(entry.display_value + " failed to delete!  Please contact the Administrator.");
          })
        }
      });

      modal('Warning', 'Are you sure you want to delete ' + entry.display_value + "?");
    };
  });
