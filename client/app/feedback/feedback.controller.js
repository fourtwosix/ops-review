'use strict';

angular.module('opsReviewApp')
  .controller('SendFeedbackCtrl', function ($scope, $uibModalInstance, $http, $state, Auth, toastr) {
    var currentUser = Auth.getCurrentUser();
    $scope.subjects = ["Program Reporting", "General"];
    $scope.feedback = {subject: "Program Reporting"};
    
    if (_.includes($state.current.templateUrl, 'admin')) {
      $scope.feedback.subject = "General";
    }

    $scope.getFieldStyle = function(fieldName) {
      return $scope.feedbackForm[fieldName].$valid ?
        {border: '1px solid green'} :
        {border: '1px solid red'}
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };

    $scope.sendFeedbackEmail = function() {
      $uibModalInstance.close();

      $http.post('/api/feedback', {
        fromUser: currentUser,
        feedbackSubject: $scope.feedback.subject,
        feedbackText: $scope.feedback.text
      }).then(function() {
        toastr.success("Feedback email sent.");
      }).catch(function(err) {
        toastr.error("Feedback email failed to send. Please contact the Administrator (" + err.data + ")");
      });
    }
  });

