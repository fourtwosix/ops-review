'use strict';

angular.module('opsReviewApp')
.controller('MainCtrl', function ($scope, $rootScope, $timeout, $uibModal, Auth, DataManager, DeckManager, DeckBuilder) {
  Auth.isLoggedInAsync(function () {
    var currentGroup = Auth.getCurrentDeckGroup();
    var currentUser = Auth.getCurrentUser();
    var receivesEmails = currentUser.receives_emails;
    $scope.currentGroup = currentGroup == null ? null : _.startCase(currentGroup.replace("-reporting", ""));
    var checkIE = function() {
      if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true ) || navigator.userAgent.indexOf("Edge") > -1) //IF IE > 10
        {
          $uibModal.open({
            templateUrl: 'app/main/adminIENotice.modal.html',
            backdrop: false,
            controller: 'AdminIENoticeCtrl',
            windowClass: 'minimal-modal',
            size: 'sm'
          });
        }
      }
      checkIE();
    if (currentGroup === null && receivesEmails === null) {
      $uibModal.open({
        backdrop : false,
        templateUrl: 'app/main/adminApprovalNotice.modal.html',
        controller: 'AdminApprovalNoticeCtrl',
        windowClass: 'minimal-modal',
        size: 'sm'
      });
    } else if(currentGroup === null && receivesEmails == true) {
      $uibModal.open({
        templateUrl: 'app/main/adminApprovalSuccess.modal.html',
        controller: 'AdminApprovalSuccessCtrl',
        backdrop: false,
        windowClass: 'minimal-modal',
        size: 'sm'
      });
    }

    DeckManager.getDefaultDecks(currentGroup).then(function(defaultDecks) {
      $scope.decks = _.map(defaultDecks, function (deck) {
        return DeckBuilder.build(deck);
      });

      $scope.scrollToCard = function (card, event) {
        event.stopPropagation();
        event.preventDefault();
        $scope.$broadcast('deckster-card:scrollto-' + card.id);
      };

      $scope.toggleCard = function (card, event) {
        event.stopPropagation();
        event.preventDefault();
        $scope.$broadcast('deckster-card:toggle-' + card.id);
        card.hidden = !card.hidden;
      };

      $scope.initialized = false;
      $scope.selectedDeck = null;

      $scope.selectDeck = function (deck) {
        if ($scope.selectedDeck && $scope.selectedDeck.id === deck.id) return;
        $scope.selectedDeck = deck;
        $scope.initialized = false;
        $timeout(function () {
          DeckManager.setCurrentDeck(deck);
          $scope.selectedDeck.sectionOpen = true;
          $scope.initialized = true;
        });
      };
      // By default lets open the first deck if there is one
      $timeout(function () {
        if ($scope.decks.length > 0) {
          $scope.selectDeck($scope.decks[0]);
        }
      });
    });
  });
});
