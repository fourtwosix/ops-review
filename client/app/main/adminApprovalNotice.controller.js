'use strict';

angular.module('opsReviewApp')
  .controller('AdminApprovalNoticeCtrl', function ($scope, $uibModal, $uibModalInstance, Auth, toastr, User, $window, Email, $timeout) {
    var currentUser = Auth.getCurrentUser();
    $scope.disableRegistration = false;

    $scope.sendAdminEmail = function() {
      $uibModalInstance.close();

      Email.sendAdminNotificationEmail(currentUser).then(function() {

        $timeout(function () {
          $uibModal.open({
            templateUrl: 'app/main/adminApprovalSuccess.modal.html',
            controller: 'AdminApprovalSuccessCtrl',
            backdrop: false,
            windowClass: 'minimal-modal',
            size: 'sm'
          });
        });


      }).catch(function(err) {
        toastr.error("Admin notification email failed to send. Please contact the Administrator (" + err + ")");
      });
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss();
        if(Auth.isOktaSession()) {
          window.location.href = 'https://gdit.okta.com';
        } else {
          $window.location.href = '/login';
        }
    };
  });
