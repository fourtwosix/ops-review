'use strict';

angular.module('opsReviewApp')
.controller('AdminApprovalSuccessCtrl', function ($scope, $uibModal, $uibModalInstance, Auth, $window) {

  $scope.close = function() {
    $uibModalInstance.dismiss();
    if(Auth.isOktaSession()) {
      window.location.href = 'https://gdit.okta.com';
    } else {
      $window.location.href = '/login';
    }
  };
});
