'use strict';

angular.module('opsReviewApp')
.controller('AdminIENoticeCtrl', function ($scope, $uibModal, $uibModalInstance, Auth, $window) {

  $scope.close = function() {
    $uibModalInstance.dismiss();
  };
});
