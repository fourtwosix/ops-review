'use strict';

angular.module('opsReviewApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        authenticate: true,
        resolve: {
          authenticate: ['Auth', function (Auth) {return Auth.authenticate();}],
          ColumnMetadata: function(DataManager) {
            return DataManager.promise;
          }
        }
      });
  });
