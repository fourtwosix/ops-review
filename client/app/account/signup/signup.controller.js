'use strict';

angular.module('opsReviewApp')
  .controller('SignupCtrl', function ($scope, $state, Auth, $location) {
    $scope.user = {};
    $scope.errors = {};
    $scope.absUrl = $location.absUrl();

    $scope.resend = function () {
      $state.go('validate');
    };

    $scope.register = function(form) {
      $scope.submitted = true;

      if(form.$valid) {
        $scope.errors = {};

        if ($scope.user.email != $scope.confirmEmail) {
          $scope.errors.email = 'Emails do not match.'
        }

        if ($scope.user.password != $scope.confirmPassword) {
          $scope.errors.password = 'Passwords do not match.'
        }

        if (!_.isEmpty($scope.errors)) return;

        var name = $scope.user.firstName + ' ' + $scope.user.lastName;
        Auth.createUser({
          name: name.trim(),
          email: $scope.user.email,
          password: $scope.user.password
        })
        .then( function() {
          // Account created, redirect to home
          Auth.validateUser($scope.user.email).then(function () {
            $scope.success = true;
          });
        })
        .catch( function(err) {
          err = err.data;

          if (err) {
            $scope.errors = {};

            // Update validity of form fields that match the mongoose errors
            angular.forEach(err.errors, function(error) {
              form[error.path].$setValidity('mongoose', false);
              $scope.errors[error.path] = error.message[0].toUpperCase() + error.message.substring(1);
            });
          }
        });
      }
    };

  });
