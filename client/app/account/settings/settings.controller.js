'use strict';

angular.module('opsReviewApp')
  .controller('SettingsCtrl', function ($scope, User, Auth, $location) {
    $scope.errors = {};
    $scope.absUrl = $location.absUrl();

    $scope.changePassword = function(form) {
      $scope.submitted = true;
      $scope.errors = {};
      if(form.$valid) {
        if ($scope.user.confirmPassword === $scope.user.newPassword) {
          Auth.changePassword( $scope.user.oldPassword, $scope.user.newPassword )
            .then( function() {
              $scope.message = 'Password successfully changed.';
            })
            .catch( function() {
              form.password.$setValidity('mongoose', false);
              $scope.message = '';
            });
        } else {
          form.confirmPassword.$setValidity('mongoose', false);
        }
      }
		};
  });
