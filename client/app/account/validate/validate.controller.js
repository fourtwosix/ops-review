'use strict';

angular.module('opsReviewApp')
.controller('ValidateCtrl', function ($scope, User, Auth, $location, $stateParams, validatedUser, $state) {
  $scope.validated = !_.isNull(validatedUser);
  $scope.tokenExist = !_.isNull($stateParams.token);
  $scope.errors = {};
  $scope.absUrl = $location.absUrl();

  // If there is a token and no user show a message to send new email
  if (!$scope.validated  && $scope.tokenExist) {
    $scope.errors.validation = 'Invalid token. Please try sending a new validation email.'
  }

  $scope.login = function () {
    $state.go('login');
  };

  $scope.validateUser = function(form) {
    $scope.submitted = true;
    $scope.errors = {};
    if(form.$valid) {
      if ($scope.email) {
        Auth.validateUser( $scope.email )
        .then( function() {
          $scope.message = 'An email has been sent to ' + $scope.email + ' with further instructions.';
        })
        .catch( function() {
          form.email.$setValidity('mongoose', false);
          $scope.message = '';
        });
      } else {
        form.confirmPassword.$setValidity('mongoose', false);
      }
    }
  };
});
