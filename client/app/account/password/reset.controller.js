'use strict';

angular.module('opsReviewApp')
.controller('ResetCtrl', function ($scope, Auth, $location, user, $stateParams) {
  $scope.errors = {};
  $scope.absUrl = $location.absUrl();
  $scope.disabled = false;
  $scope.user = user;

  if (!$scope.user) {
    $scope.errors.user = 'No user found matching this token.';
    $scope.disabled = true;
  }

  $scope.changePassword = function(form) {
    $scope.submitted = true;
    $scope.errors = {};
    if(form.$valid) {
      if ($scope.user.confirmPassword === $scope.user.newPassword) {
        Auth.resetPassword($stateParams.token, $scope.user)
        .then( function() {
          $scope.message = 'Password successfully changed.';
        })
        .catch( function() {
          form.newPassword.$setValidity('mongoose', false);
          $scope.message = '';
        });
      } else {
        form.confirmPassword.$setValidity('mongoose', false);
      }
    }
  };
});
