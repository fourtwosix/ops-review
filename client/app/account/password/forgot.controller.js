'use strict';

angular.module('opsReviewApp')
  .controller('ForgotCtrl', function ($scope, User, Auth, $location) {
    $scope.errors = {};
    $scope.absUrl = $location.absUrl();

    $scope.changePassword = function(form) {
      $scope.submitted = true;
      $scope.errors = {};
      if(form.$valid) {
        if ($scope.email) {
          Auth.forgotPassword( $scope.email )
            .then( function() {
              $scope.message = 'An email has been sent to ' + $scope.email + ' with further instructions.';
            })
            .catch( function(err) {
              form.email.$setValidity('mongoose', false);
              $scope.errorMsg = 'The password reset email failed to send.  Please contact the Administrator.';
            });
        } else {
          form.confirmPassword.$setValidity('mongoose', false);
        }
      }
		};
  });
