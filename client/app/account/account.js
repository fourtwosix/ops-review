'use strict';

angular.module('opsReviewApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/account/login/login.html',
        controller: 'LoginCtrl',
        onEnter: function($state) {
          if(window.GlobalConfig.environment === 'production') {
            $state.go('main');
          }
        }
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/account/signup/signup.html',
        controller: 'SignupCtrl',
        onEnter: function($state) {
          if(window.GlobalConfig.environment === 'production') {
            $state.go('main');
          }
        }
      })
      .state('settings', {
        url: '/settings',
        templateUrl: 'app/account/settings/settings.html',
        controller: 'SettingsCtrl',
        resolve: {authenticate: ['Auth', function (Auth) {return Auth.authenticate();}]}
      })
      .state('forgot', {
        url: '/password/forgot',
        templateUrl: 'app/account/password/forgot.html',
        controller: 'ForgotCtrl',
        onEnter: function($state) {
          if(window.GlobalConfig.environment === 'production') {
            $state.go('main');
          }
        }
      })
      .state('reset', {
        url: '/password/reset/:token',
        templateUrl: 'app/account/password/reset.html',
        controller: 'ResetCtrl',
        resolve: {
          user: ['Auth', '$stateParams', function (Auth, $stateParams) {
            return Auth.getForgottenUser($stateParams.token);
          }]
        },
        onEnter: function($state) {
          if(window.GlobalConfig.environment === 'production') {
            $state.go('main');
          }
        }
      })
      .state('validate', {
        url: '/validate/:token?',
        params:  {
          token: {
            value: null,
            squash: true
          }
        },
        templateUrl: 'app/account/validate/validate.html',
        controller: 'ValidateCtrl',
        resolve: {
          validatedUser: ['$stateParams', '$state', 'Auth', function ($stateParams, $state, Auth) {
            return Auth.getValidatedUser($stateParams.token);
          }]
        },
        onEnter: function($state) {
          if(window.GlobalConfig.environment === 'production') {
            $state.go('main');
          }
        }
      });
  });
