'use strict';

angular.module('opsReviewApp')
  .controller('LoginCtrl', function ($scope, Auth, $location) {
    $scope.user = {};
    $scope.errors = {};
    $scope.absUrl = $location.absUrl();

    $scope.login = function(form) {
      $scope.submitted = true;

      if(form.$valid) {
        Auth.login({
          email: $scope.user.email,
          password: $scope.user.password
        })
        .then( function() {
          // Logged in, redirect to home
          Auth.redirectToAttemptedUrl();
        })
        .catch( function(err) {
          if (err) {
            $scope.errors.other = err.message;
          }
        });
      }
    };

  });
